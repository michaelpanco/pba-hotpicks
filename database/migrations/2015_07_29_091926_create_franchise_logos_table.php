<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchiseLogosTable extends Migration
{
    public function up()
    {
        Schema::create('franchise_logos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename', 32);
            $table->char('type', 4);
            $table->char('status', 5);
        });
    }

    public function down()
    {
        Schema::drop('franchise_logos');
    }
}
