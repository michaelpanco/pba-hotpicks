<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformancesTable extends Migration
{
    public function up()
    {
        Schema::create('performances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id')->unsigned();
            $table->foreign('game_id')->references('id')->on('games');
            $table->integer('player_id')->unsigned();
            $table->foreign('player_id')->references('id')->on('players');
            $table->integer('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->integer('opponent')->unsigned();
            $table->foreign('opponent')->references('id')->on('teams');
            $table->tinyInteger('points');
            $table->tinyInteger('assists');
            $table->tinyInteger('rebounds');
            $table->tinyInteger('steals');
            $table->tinyInteger('blocks');
            $table->tinyInteger('turnovers');
            $table->boolean('win_game');
            $table->tinyInteger('sequence');
            $table->boolean('ready')->default(0);
            $table->smallInteger('fantasy_points');
        });
    }

    public function down()
    {
        Schema::drop('performances');
    }
}
