<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChristmasGifts extends Migration
{
    public function up()
    {
        Schema::create('christmas_gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('managers');
            $table->smallInteger('received');
            $table->date('date');
        });
    }

    public function down()
    {
        Schema::drop('christmas_gifts');
    }
}
