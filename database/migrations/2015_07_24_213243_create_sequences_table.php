<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSequencesTable extends Migration
{
    public function up()
    {
        Schema::create('sequences', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('season');
            $table->string('conference', 32);
            $table->char('status', 4);
        });
    }

    public function down()
    {
        Schema::drop('sequences');
    }
}
