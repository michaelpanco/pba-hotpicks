<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaguesTable extends Migration
{
    public function up()
    {
        Schema::create('leagues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->integer('host')->unsigned();
            $table->foreign('host')->references('id')->on('managers');
            $table->string('desc', 255);
            $table->integer('channel_id')->unsigned();
            $table->foreign('channel_id')->references('id')->on('channels');
            $table->string('password', 16) -> nullable();
            $table->integer('sequence')->unsigned();
            $table->foreign('sequence')->references('id')->on('sequences');
            $table->boolean('ready');
            $table->date('created_at');
        });
    }

    public function down()
    {
        Schema::drop('leagues');
    }
}
