<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgesTable extends Migration {

	public function up()
	{
		Schema::create('badges', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 32);
			$table->string('src', 128);
		});
	}

	public function down()
	{
		Schema::drop('badges');
	}

}
