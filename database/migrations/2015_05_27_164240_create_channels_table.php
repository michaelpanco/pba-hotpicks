<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration {

	public function up()
	{
		Schema::create('channels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 32);
			$table->char('type', 4);
			$table->char('status', 4);
		});
	}


	public function down()
	{
		Schema::drop('channels');
	}

}
