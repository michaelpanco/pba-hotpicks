<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsPointsTable extends Migration {

	public function up()
	{
		Schema::create('posts_points', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('manager_id')->unsigned();
			$table->foreign('manager_id')->references('id')->on('managers');
			$table->integer('post_id')->unsigned();
			$table->foreign('post_id')->references('id')->on('posts');
		});
	}
	
	public function down()
	{
		Schema::drop('posts_points');
	}

}
