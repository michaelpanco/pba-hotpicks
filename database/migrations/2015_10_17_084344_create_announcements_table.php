<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->tinyInteger('order');
        });
    }

    public function down()
    {
        Schema::drop('announcements');
    }
}
