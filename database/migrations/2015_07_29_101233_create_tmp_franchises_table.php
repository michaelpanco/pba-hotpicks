<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpFranchisesTable extends Migration
{
    public function up()
    {
        Schema::create('tmp_franchises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag', 32);
            $table->string('name', 32);
            $table->integer('logo_id')->unsigned();
            $table->foreign('logo_id')->references('id')->on('franchise_logos');
        });
    }

    public function down()
    {
        Schema::drop('tmp_franchises');
    }
}
