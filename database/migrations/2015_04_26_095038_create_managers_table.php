<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagersTable extends Migration {

	public function up()
	{
		Schema::create('managers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug', 64)->unique();
			$table->string('firstname', 32);
			$table->string('lastname', 32);
			$table->char('gender', 1);
			$table->date('birthday');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('avatar');
			$table->char('status', 4)->default('ACTV');
			$table->timestamp('last_online')->nullable();
			$table->string('privilege', 16)->nullable();
			$table->boolean('league_leader')->default(0);
			$table->date('date_registered');
			$table->rememberToken();
			$table->integer('credits')->default(0);
			$table->index('password');
			$table->date('restrain_until')->nullable();
			$table->integer('recruiter')->nullable();
			$table->string('ip_address')->nullable();
			$table->text('user_agent')->nullable();
			$table->string('reg_type', 6);
			$table->boolean('claimed_referal_prize')->default(0);
			$table->string('fav_team', 32)->nullable();
			$table->string('fav_2nd_team', 32)->nullable();
			$table->string('fav_player', 64)->nullable();
			$table->string('fav_match_up', 64)->nullable();
		});
	}

	public function down()
	{
		Schema::drop('managers');
	}

}
