<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLimitToExpressBet extends Migration
{
    public function up()
    {
        Schema::table('express_bet', function ($table) {
            $table->smallInteger('limit')->default(999);
        });
    }

    public function down()
    {
        Schema::table('express_bet', function ($table) {
            $table->dropColumn('limit');
        });
        
    }
}
