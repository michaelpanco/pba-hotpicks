<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchisesPlayersTable extends Migration
{
    public function up()
    {
        Schema::create('franchises_players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('franchise_id')->unsigned();
            $table->foreign('franchise_id')->references('id')->on('franchises');
            $table->integer('player_id')->unsigned();
            $table->foreign('player_id')->references('id')->on('players');
            $table->integer('position');
            $table->char('status', 4)->default('ACTV');
            $table->integer('fantasy_points')->default(0);
            $table->integer('last_game_seed')->default(0);
            $table->date('date_hired');
        });
    }

    public function down()
    {
        Schema::drop('franchises_players');
    }
}
