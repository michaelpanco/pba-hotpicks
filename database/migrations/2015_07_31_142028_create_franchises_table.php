<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFranchisesTable extends Migration
{
    public function up()
    {
        Schema::create('franchises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('managers');
            $table->integer('logo_id')->unsigned();
            $table->foreign('logo_id')->references('id')->on('franchise_logos');
            $table->integer('league_id')->nullable();
            $table->date('created_at');
        });
    }

    public function down()
    {
        Schema::drop('franchises');
    }
}
