<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('managers');
            $table->char('type', 6);
            $table->string('details');
            $table->boolean('seen')->default(0);
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::drop('notifications');
    }
}
