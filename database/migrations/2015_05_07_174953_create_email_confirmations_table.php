<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailConfirmationsTable extends Migration {

	public function up()
	{
		Schema::create('email_confirmations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('manager_id')->unsigned();
			$table->foreign('manager_id')->references('id')->on('managers')->unique();
			$table->string('confirmation_code')->unique()->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('email_confirmations');
	}

}
			