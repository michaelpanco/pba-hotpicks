<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaguesFranchisesTable extends Migration
{
    public function up()
    {
        Schema::create('leagues_franchises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('league_id')->unsigned();
            $table->foreign('league_id')->references('id')->on('leagues');
            $table->integer('franchise_id')->unsigned();
            $table->foreign('franchise_id')->references('id')->on('franchises');
        });
    }

    public function down()
    {
        Schema::drop('leagues_franchises');
    }
}
