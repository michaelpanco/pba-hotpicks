<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagersBadgesTable extends Migration {

	public function up()
	{
		Schema::create('managers_badges', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('manager_id')->unsigned();
			$table->foreign('manager_id')->references('id')->on('managers');
			$table->integer('badge_id')->unsigned();
			$table->foreign('badge_id')->references('id')->on('badges');
			$table->char('status', 4)->default('DSBL');
			$table->index('status');
		});
	}

	public function down()
	{
		Schema::drop('managers_badges');
	}

}
