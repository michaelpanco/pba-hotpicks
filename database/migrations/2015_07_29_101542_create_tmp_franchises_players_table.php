<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpFranchisesPlayersTable extends Migration
{
    public function up()
    {
        Schema::create('tmp_franchises_players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tmp_franchise_id')->unsigned();
            $table->foreign('tmp_franchise_id')->references('id')->on('tmp_franchises');
            $table->integer('player_id')->unsigned();
            $table->foreign('player_id')->references('id')->on('players');
        });
    }

    public function down()
    {
        Schema::drop('tmp_franchises_players');
    }
}
