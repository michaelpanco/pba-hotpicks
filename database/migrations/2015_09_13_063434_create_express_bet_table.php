<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpressBetTable extends Migration
{
    public function up()
    {
        Schema::create('express_bet', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 255);
            $table->string('option', 255);
            $table->tinyInteger('won')->nullable();
            $table->dateTime('gamedate');
            $table->dateTime('expiry');
        });
    }

    public function down()
    {
        Schema::drop('express_bet');
    }
}
