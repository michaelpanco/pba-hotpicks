<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_x_id')->unsigned();
            $table->foreign('team_x_id')->references('id')->on('teams');
            $table->integer('team_y_id')->unsigned();
            $table->foreign('team_y_id')->references('id')->on('teams');
            $table->tinyInteger('winner')->nullable();
            $table->char('score', 7)->nullable();
            $table->string('venue', 64);
            $table->tinyInteger('season');
            $table->string('conference', 32);
            $table->tinyInteger('game_no');
            $table->tinyInteger('cluster_no');
            $table->string('game_type', 16);
            $table->tinyInteger('sequence');
            $table->dateTime('datetime');
        });
    }

    public function down()
    {
        Schema::drop('games');
    }
}
