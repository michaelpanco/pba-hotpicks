<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 32);
            $table->string('lastname', 32);
            $table->string('slug')->unique();
            $table->integer('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->smallInteger('position');
            $table->tinyInteger('jersey');
            $table->date('birthday')->nullable();
            $table->string('school', 64);
            $table->char('height', 5);
            $table->string('avatar', 64);
            $table->char('status', 4)->default('ACTV');
            $table->integer('sign_count')->default(0);
            $table->mediumInteger('salary');
        });
    }

    public function down()
    {
        Schema::drop('players');
    }
}
