<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->char('type', 4);
            $table->smallInteger('product_id');
            $table->string('img', 128);
            $table->mediumInteger('price');
        });
    }

    public function down()
    {
        Schema::drop('products');
    }
}
