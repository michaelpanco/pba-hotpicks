<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChannelIdForeignToPostsTable extends Migration {

	public function up()
	{
		Schema::table('posts', function(Blueprint $table)
		{
			$table->foreign('channel_id')->references('id')->on('channels');
		});
	}


	public function down()
	{
		Schema::table('posts', function(Blueprint $table)
		{
			$table->dropForeign('posts_channel_id_foreign');
		});
	}

}
