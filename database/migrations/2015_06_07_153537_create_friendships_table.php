<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendshipsTable extends Migration {

	public function up()
	{
		Schema::create('friendships', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('manager_id')->unsigned();
			$table->foreign('manager_id')->references('id')->on('managers');
			$table->integer('friend_id')->unsigned();
			$table->foreign('friend_id')->references('id')->on('managers');
			$table->char('status', 4)->default('PNDG');
		});
	}

	public function down()
	{
		Schema::drop('friendships');
	}

}
