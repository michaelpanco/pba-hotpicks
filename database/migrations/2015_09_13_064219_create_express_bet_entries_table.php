<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpressBetEntriesTable extends Migration
{
    public function up()
    {
        Schema::create('express_bet_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('express_bet_id')->unsigned();
            $table->foreign('express_bet_id')->references('id')->on('express_bet');
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('managers');
            $table->tinyInteger('bet');
            $table->smallInteger('amount');
            $table->boolean('claimed')->default(0);
            $table->dateTime('datetime');
        });
    }

    public function down()
    {
        Schema::drop('express_bet_entries');
    }
}
