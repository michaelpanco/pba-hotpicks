<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('manager_id')->unsigned();
			$table->foreign('manager_id')->references('id')->on('managers');
			$table->integer('channel_id')->unsigned();
			$table->text('content');
			$table->integer('parent_post')->unsigned();
			$table->foreign('parent_post')->references('id')->on('posts');
			$table->char('status', 4)->default('ACTV');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('posts');
	}

}