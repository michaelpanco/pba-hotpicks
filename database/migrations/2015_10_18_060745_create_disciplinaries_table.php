<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplinariesTable extends Migration
{
    public function up()
    {
        Schema::create('disciplinaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('officer_id')->unsigned();
            $table->foreign('officer_id')->references('id')->on('managers');
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('managers');
            $table->text('description');
            $table->char('status', 4);
            $table->dateTime('created_at');
        });
    }

    public function down()
    {
        Schema::drop('disciplinaries');
    }
}
