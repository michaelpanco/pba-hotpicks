<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->string('slug', 32);
            $table->string('manager', 32);
            $table->string('coach', 32);
            $table->string('assistant_coaches', 255);
            $table->smallInteger('joined');
            $table->tinyInteger('titles');
            $table->decimal('win', 3,1)->default(0);
            $table->tinyInteger('loss')->default(0);
            $table->string('logo', 64);
        });
    }

    public function down()
    {
        Schema::drop('teams');
    }
}
