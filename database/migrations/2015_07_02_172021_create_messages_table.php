<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->char('conversation_id', 10);
            $table->text('message');
            $table->integer('sender')->unsigned();
            $table->foreign('sender')->references('id')->on('managers');
            $table->integer('recipient')->unsigned();
            $table->foreign('recipient')->references('id')->on('managers');
            $table->boolean('read')->default(0);
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::drop('messages');
    }
}