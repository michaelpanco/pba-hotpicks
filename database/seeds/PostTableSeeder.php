<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class PostTableSeeder extends Seeder {

	public function run()
	{
        foreach($this -> posts() as $post){
            Post::create($post);
        }
        
    }

    public function posts()
    {
        return [
            [
                'manager_id' => 1,
                'channel_id' => 1,
                'content' => 'Welcome to PinoyTenPicks',
                'parent_post' => 1,
                'status' => 'HIDE',
            ],
            [
                'manager_id' => 5,
                'channel_id' => 1,
                'content' => "Hello, I'm new here, d best ang site na toh. astig.",
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 2,
                'channel_id' => 1,
                'content' => "Kaya nga eh, ang cool. invite lang tau ng iba pang members para dumami ung mga player",
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 1,
                'channel_id' => 1,
                'content' => 'Oy kamusta kayo dyan, sino may league dyan pasali ako.hahaha',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 3,
                'channel_id' => 1,
                'content' => "Try ko lang mag post.ahaha.",
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 2,
                'channel_id' => 1,
                'content' => "edi wow.hehe. invite ko mga friends ko",
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 7,
                'channel_id' => 1,
                'content' => 'pasali po mga league ate/kuya newbie here',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 6,
                'channel_id' => 1,
                'content' => 'gawa ba ako league?, sali kau sa leagu ko gagawa ako.',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 7,
                'channel_id' => 1,
                'content' => 'Oy pm mo ko alvin naka register na ako.',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 4,
                'channel_id' => 1,
                'content' => 'Hello po. :)',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 2,
                'channel_id' => 1,
                'content' => 'kaway-kaway sa mga online na dyan. ',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 4,
                'channel_id' => 1,
                'content' => 'pano po ba gamitin ang express bet? at pano po ba mag ear ng credits? Salamat',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 15,
                'channel_id' => 1,
                'content' => 'Idol Earl Scothie Thompson',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 17,
                'channel_id' => 1,
                'content' => 'Good Day mga kabaranggay',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 19,
                'channel_id' => 1,
                'content' => 'Official fantasy site po ba ito ng PBA?',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 2,
                'channel_id' => 1,
                'content' => 'wahahahaaha... san ba pwede sumali league. ung pede maging close.',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 22,
                'channel_id' => 1,
                'content' => 'Ito po ba ung dating fantasy site ng PBA?',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 3,
                'channel_id' => 1,
                'content' => 'Aldub you all..hehe.. wala masabi.haha',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 4,
                'channel_id' => 1,
                'content' => 'Hello senyo mga bradir,. kamustasa?',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 13,
                'channel_id' => 1,
                'content' => 'parang ang astig ng mga badges. kaso mamahal.ahaha',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 23,
                'channel_id' => 1,
                'content' => 'penge po ng credits.hehe',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 22,
                'channel_id' => 1,
                'content' => 'gagawa ako ng league wait lang mga madalang people',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 8,
                'channel_id' => 1,
                'content' => 'pede din ba maghanap ng jowa dito.hahahaha.joke. :)',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 4,
                'channel_id' => 1,
                'content' => 'Ligo lang muna. wait lang sa mga ka message ko.hehe',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 1,
                'channel_id' => 1,
                'content' => 'haha. mali na pindot amf.',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 17,
                'channel_id' => 1,
                'content' => 'mga repapeeeps pa join sa league. may available ba dyan? gusto ko madami.hhe',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 19,
                'channel_id' => 1,
                'content' => 'Pag tinatamad sa gawaing bahay, Better call Drake!hahahahaha',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 18,
                'channel_id' => 1,
                'content' => 'orayt :D',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],
            [
                'manager_id' => 12,
                'channel_id' => 1,
                'content' => 'Thanks God panalo ang team ko kanina.hehe. :)',
                'parent_post' => 1,
                'status' => 'ACTV',
            ],

            
        ];
    }

}