<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Channel;

class ChannelTableSeeder extends Seeder {

	public function run()
	{
		$channels = array(
			array('name' => 'Public', 'type' => 'PUBL', 'status' => 'ACTV'),
			array('name' => 'Alaska', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Barako Bull', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Brgy. Ginebra', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Blackwater', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Globalport', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Mahindra', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Meralco', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'NLEX', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Purefoods', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Rain or Shine', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'San Miguel', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'Talk N Text', 'type' => 'TEAM', 'status' => 'ACTV'),
			array('name' => 'PBA Series Cup', 'type' => 'LGUE', 'status' => 'ACTV'),
			array('name' => 'PinoyExchange', 'type' => 'LGUE', 'status' => 'ACTV'),
			array('name' => 'Best Team Tournament', 'type' => 'LGUE', 'status' => 'ACTV'),

		);

		foreach($channels as $channel){
        	Channel::create($channel);
    	}
	}

}