<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Team;

class TeamTableSeeder extends Seeder {

	public function run()
	{
        foreach($this -> teams() as $team){
            Team::create($team);
        }
        
    }

    public function teams()
    {
        return [
            [
                'name' => 'Alaska Aces',
                'slug' => 'alaskaaces',
                'manager' => 'Richard Bachman',
                'coach' => 'Alex Compton',
                'assistant_coaches' => 'Louie Alas,Topex Robinson,Monch Gavieres,Franco Atienza',
                'joined' => 1986,
                'titles' => 14,
                'win' => 4.1,
                'loss' => 1,
                'logo' => '/assets/img/teams/alaska.png'
            ],
            [
                'name' => 'Barako Bull Energy',
                'slug' => 'barakobullenergycola',
                'manager' => 'Manny Alvarez',
                'coach' => 'Koy Banal',
                'assistant_coaches' => 'Art Dela Cruz,Jigs Mendoza,Paolo Rivero',
                'joined' => 2002,
                'titles' => 0,
                'win' => 3.4,
                'loss' => 2,
                'logo' => '/assets/img/teams/barako.png'
            ],
            [
                'name' => 'Brgy. Ginebra San Miguel',
                'slug' => 'barangayginebrasanmiguel',
                'manager' => 'Alfrancis Chua',
                'coach' => 'Earl Timothy Cone',
                'assistant_coaches' => 'Richard del Rosario,Olcen Racela,Juno Sauler,Allan Caidic',
                'joined' => 1979,
                'titles' => 8,
                'win' => 2.2,
                'loss' => 3,
                'logo' => '/assets/img/teams/ginebra.png'
            ],
            [
                'name' => 'Blackwater Elite',
                'slug' => 'blackwaterelite',
                'manager' => 'Johnson Martines',
                'coach' => 'Leo Isaac',
                'assistant_coaches' => 'Rodil Sablan,Patrick Aquino,Junjie Ablan,Aris Dimaunahan',
                'joined' => 2014,
                'titles' => 0,
                'win' => 1.3,
                'loss' => 3,
                'logo' => '/assets/img/teams/blackwater.png'
            ],
            [
                'name' => 'Globalport Batang Pier',
                'slug' => 'globalportbatangpier',
                'manager' => 'Bonnie Tan',
                'coach' => 'Pido Jarencio',
                'assistant_coaches' => 'Eric Gonzales,Senen Duenas,John Cardel,Oliver Bunye',
                'joined' => 2012,
                'titles' => 0,
                'win' => 3.3,
                'loss' => 2, 
                'logo' => '/assets/img/teams/globalport.png'
            ],
            [
                'name' => 'Mahindra Enforcer',
                'slug' => 'mahindraenforcer',
                'manager' => 'Enrico Pineda',
                'coach' => 'Manny Pacquiao',
                'assistant_coaches' => 'Chito Victolero,Rob Wainwright,Chris Gavina,Marlon Martin,Romulo Orillosa,Alex Angeles',
                'joined' => 2014,
                'titles' => 0,
                'win' => 1.2,
                'loss' => 4,
                'logo' => '/assets/img/teams/mahindra.png'
            ],
            [
                'name' => 'Meralco Bolts',
                'slug' => 'meralcobolts',
                'manager' => 'Paolo Trillo',
                'coach' => 'Norman Black',
                'assistant_coaches' => 'Ronnie Magsanoc,Luigi Trillo,Patrick Roy Fran,Xavier Nunag,Mike Jarin,Gene Afable',
                'joined' => 2010,
                'titles' => 0,
                'win' => 1.1,
                'loss' => 5,
                'logo' => '/assets/img/teams/meralco.png'
            ],
            [
                'name' => 'NLEX Road Warriors',
                'slug' => 'nlexroadwarriors',
                'manager' => 'Ronald Dulatre',
                'coach' => 'Boyet Fernandez',
                'assistant_coaches' => 'Adonis Tierra,Jojo Lastimosa,Alex Arespacochaga,Raymund Celis,Claiford Arao,Gino Manuel,Jay Serrano',
                'joined' => 2014,
                'titles' => 0,
                'win' => 3.3,
                'loss' => 2,
                'logo' => '/assets/img/teams/nlex.png'
            ],
            [
                'name' => 'Purefoods Star Hotshots',
                'slug' => 'purefoodsstarhotshots',
                'manager' => 'Alvin Patrimonio',
                'coach' => 'Jason Webb',
                'assistant_coaches' => 'Johnny Abarrientos,Mon Jose,Tony Boy Espinosa',
                'joined' => 1988,
                'titles' => 13,
                'win' => 2.1,
                'loss' => 4,
                'logo' => '/assets/img/teams/purefoods.png'
            ],
            [
                'name' => 'Rain or Shine Elasto Painters',
                'slug' => 'rainorshineelastopainters',
                'manager' => 'Mamerto Mondragon',
                'coach' => 'Yeng Guiao',
                'assistant_coaches' => 'Caloy Garcia,Mike Buendia,Ricky Umayam',
                'joined' => 2006,
                'titles' => 1,
                'win' => 3.5,
                'loss' => 1,
                'logo' => '/assets/img/teams/rainorshine.png'
            ],
            [
                'name' => 'San Miguel Beermen',
                'slug' => 'sanmiguelbeermen',
                'manager' => 'Robert Non',
                'coach' => 'Leo Austria',
                'assistant_coaches' => 'Peter Martin,Dayong Mendoza,Boysie Zamar,Biboy Ravanes,Jorge Gallent,Ato Agustin',
                'joined' => 1975,
                'titles' => 21,
                'win' => 4.2,
                'loss' => 1,
                'logo' => '/assets/img/teams/sanmiguel.png'
            ],
            [
                'name' => 'Talk N Text Tropang Texters',
                'slug' => 'talkntexttropangtexters',
                'manager' => 'Virgil Villavicencio',
                'coach' => 'Jong Uichico',
                'assistant_coaches' => 'Nash Racela,Joshua Reyes,Bong Ravena',
                'joined' => 1990,
                'titles' => 8,
                'win' => 3.1,
                'loss' => 2,
                'logo' => '/assets/img/teams/talkntext.png'
            ],
        ];
    }

}