<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\FranchiseLogo;

class FranchiseLogoSeeder extends Seeder {

	public function run()
	{
		$logos = array(
			array('filename' => 'star_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'star_orange.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'dragon_blue.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'dragon_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'fire_blue.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'fire_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'flake_blue.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'flake_green.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'blade_black.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'blade_blue.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'paw_green.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'paw_orange.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'lion_orange.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'lion_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'phoenix_orange.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'phoenix_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'skull_black.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'skull_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'bull_green.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'bull_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'thunder_blue.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'thunder_green.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'king_red.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'king_green.png', 'type' => 'FREE', 'status' => 'ENBL'),
			array('filename' => 'prem_eagle_green.png', 'type' => 'PREM', 'status' => 'ENBL'),
			array('filename' => 'prem_fire_red.png', 'type' => 'PREM', 'status' => 'ENBL'),
			array('filename' => 'prem_king_gold.png', 'type' => 'PREM', 'status' => 'ENBL'),
			array('filename' => 'prem_skull_black.png', 'type' => 'PREM', 'status' => 'ENBL'),
			array('filename' => 'prem_star_blue.png', 'type' => 'PREM', 'status' => 'ENBL'),
			array('filename' => 'superprem_fire.png', 'type' => 'PREM', 'status' => 'ENBL'),
		);

		foreach($logos as $logo){
        	FranchiseLogo::create($logo);
    	}
	}

}