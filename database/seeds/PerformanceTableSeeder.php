<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Performance;
use App\Repositories\PlayerRepository;

class PerformanceTableSeeder extends Seeder {

    protected $player;

    public function __construct(PlayerRepository $player)
    {
        $this -> player = $player;
    }

	public function run()
	{
        foreach($this -> games() as $game){
            foreach($game['performance'] as $performance){
                Performance::create([
                    "game_id" => $game['game_id'],
                    "player_id" => $performance['player_id'],
                    "team_id" => $performance['team_id'],
                    "opponent" => $performance['opponent'],
                    "points" => $performance['points'],
                    "assists" => $performance['assists'],
                    "rebounds" => $performance['rebounds'],
                    "steals" => $performance['steals'],
                    "blocks" => $performance['blocks'],
                    "turnovers" => $performance['turnovers'],
                    "win_game" => $performance['win_game'],
                    "sequence" => 1,
                    "ready" => 1,
                    "fantasy_points" => $this -> computeFantasyPoints($performance),
                ]);
            }
        }
    }

    public function games()
    {
		$performance_json = File::get(storage_path() . "/jsondata/performances.json");
		return json_decode($performance_json, true);
    }

    private function computeFantasyPoints($performance)
    {
        $fantasy_points = (($performance['points'] * 2) + ($performance['rebounds'] * 2) + ($performance['assists'] * 3) + ($performance['steals'] * 3) + ($performance['blocks'] * 3) + ($performance['win_game'] * 10) + (0 - $performance['turnovers'])) * 2;
        return $fantasy_points;
    }
}