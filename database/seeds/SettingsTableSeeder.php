<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setting;

class SettingsTableSeeder extends Seeder {

	public function run()
	{
        foreach($this -> games() as $game){
            Setting::create($game);
        }
    }

    public function games()
    {
        return [
            [
                'field' => 'NOTIFIER_POLLING_INTERVAL',
                'value' => '6000'
            ],
            [
                'field' => 'FLAG_ONLINE_POLLING_INTERVAL',
                'value' => '6000'
            ],
            [
                'field' => 'CONVERSATION_POLLING_INTERVAL',
                'value' => '5000'
            ],
            [
                'field' => 'SEQUENCE',
                'value' => '1'
            ],
            [
                'field' => 'INITIAL_SALARY_CAP',
                'value' => '300000'
            ],
            [
                'field' => 'MINIMUM_LEAGUE_PARTICIPANTS',
                'value' => '10'
            ],
            [
                'field' => 'MAXIMUM_LEAGUE_PARTICIPANTS',
                'value' => '30'
            ],
            [
                'field' => 'SEASON',
                'value' => '41'
            ],
            [
                'field' => 'ALLOW_TRADE',
                'value' => 'YES'
            ],
            [
                'field' => 'DISTRIBUTE_REWARD',
                'value' => 'NO'
            ],
            [
                'field' => 'BEGIN_LEAGUE',
                'value' => 'YES'
            ]
        ];
    }

}