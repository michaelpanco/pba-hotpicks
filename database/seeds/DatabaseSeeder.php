<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class DatabaseSeeder extends Seeder {

	public function run()
	{
		Redis::flushall();
		Model::unguard();
		$this -> call('BadgeTableSeeder');
		$this -> call('ManagerTableSeeder');
		$this -> call('ChannelTableSeeder');
		$this -> call('PostTableSeeder');
		$this -> call('MessageTableSeeder');
		$this -> call('TeamTableSeeder');
		$this -> call('PlayerTableSeeder');
		$this -> call('GameTableSeeder');
		$this -> call('PerformanceTableSeeder');
		$this -> call('SettingsTableSeeder');
		$this -> call('FranchiseLogoSeeder');
		$this -> call('SequenceTableSeeder');
		$this -> call('FranchiseTableSeeder');
		$this -> call('LeaguesTableSeeder');
		$this -> call('ExpressBetTableSeeder');
		$this -> call('ExpressBetEntriesTableSeeder');
		$this -> call('ProductTableSeeder');
		$this -> call('AnnouncementTableSeeder');
		//$this -> call('NotificationTableSeeder');
	}

}
