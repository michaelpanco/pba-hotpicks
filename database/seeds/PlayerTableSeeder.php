<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Player;

class PlayerTableSeeder extends Seeder {

	public function run()
	{
        $salaries = [12000, 15000, 20000, 22000, 25000, 27000, 30000, 33000, 35000, 40000, 45000];

        foreach($this -> players() as $player){
            Player::create([
            		"firstname" => $player['firstname'],
            		"lastname" => $player['lastname'],
                    "slug" => str_replace(' ', '', strtolower($player['firstname'].$player['lastname'])),
            		"team_id" => $player['team_id'],
            		"position" => $player['position'],
            		"jersey" => $player['jersey'],
            		"birthday" => $player['birthday'],
            		"school" => $player['school'],
            		"height" => str_replace("-", "", $player['height']),
            		"avatar" => '/assets/img/players/avatars/' . $player['avatar'],
                    "sign_count" => (isset($player['sign_count'])) ? $player['sign_count'] : 0,
                    "status" => (isset($player['status'])) ? $player['status'] : 'ACTV',
            		"salary" => $player['salary'],

            	]);
        }
        
    }

    public function players()
    {
		$players_json = File::get(storage_path() . "/jsondata/players.json");
		return json_decode($players_json, true);
    }

}