<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SequenceTableSeeder extends Seeder {

	public function run()
	{
		$sequences = array(
			array(
				'season' => 41,
				'conference' => 'Governors Cup',
				'status' => 'ONGO'
			)
		);

		foreach($sequences as $sequence){
        	DB::table('sequences')->insert($sequence);
    	}
	}

}