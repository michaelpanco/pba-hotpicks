<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manager;

class ManagerTableSeeder extends Seeder {

    protected $manager;

    public function __construct(Manager $manager){
        $this -> manager = $manager;
    }

	public function run()
	{
        $this -> createManagerAccounts();
    }

    private function createManagerAccounts()
    {
        $managers = array(
            array(
                'slug' => 'markjosephperona',
                'firstname' => 'Mark Joseph',
                'lastname' => 'Perona',
                'gender' => 'm',
                'birthday' => '1988-06-26',
                'email' => 'ptpadmin1@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/markjosephperona.png',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'privilege' => 'administrator',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [1,3,5,7,9],
            ),
            array(
                'slug' => 'joycemorales',
                'firstname' => 'Joyce',
                'lastname' => 'Morales',
                'gender' => 'f',
                'birthday' => '1991-12-11',
                'email' => 'ptptester2@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/joycemorales.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [2,3,8],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'geraldmarquez',
                'firstname' => 'Gerald',
                'lastname' => 'Marquez',
                'gender' => 'm',
                'birthday' => '1982-08-22',
                'email' => 'ptptester3@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/geraldmarquez.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => 4,
            ),
            array(
                'slug' => 'jonathanarguiza',
                'firstname' => 'Jonathan',
                'lastname' => 'Arguiza',
                'gender' => 'm',
                'birthday' => '1986-02-20',
                'email' => 'ptptester4@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jonathanarguiza.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [1,6],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'jennamaesanchez',
                'firstname' => 'Jenna mae',
                'lastname' => 'Sanchez',
                'gender' => 'f',
                'birthday' => '1992-01-17',
                'email' => 'ptptester5@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jennamaesanchez.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [9,10,12],
            ),
            array(
                'slug' => 'ramsissandoval',
                'firstname' => 'Ramsis',
                'lastname' => 'Sandoval',
                'gender' => 'm',
                'birthday' => '1993-07-03',
                'email' => 'ptptester6@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/ramsissandoval.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [3,5],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'robinsonalcala',
                'firstname' => 'Robinson',
                'lastname' => 'Alacala',
                'gender' => 'm',
                'birthday' => '1985-11-11',
                'email' => 'ptptester7@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/robinsonalcala.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [3,7],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'danielgonzales',
                'firstname' => 'Daniel',
                'lastname' => 'Gonzales',
                'gender' => 'm',
                'birthday' => '1984-07-13',
                'email' => 'ptptester8@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/danielgonzales.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [4,5],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'shielaveloso',
                'firstname' => 'Shiela',
                'lastname' => 'Veloso',
                'gender' => 'f',
                'birthday' => '1995-03-28',
                'email' => 'ptptester9@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/shielaveloso.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [1,2],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'froilandelesa',
                'firstname' => 'Froilan',
                'lastname' => 'Delesa',
                'gender' => 'm',
                'birthday' => '1991-02-04',
                'email' => 'ptptester10@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/froilandelesa.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => 2,
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'johnmichaelardales',
                'firstname' => 'John Michael',
                'lastname' => 'Ardales',
                'gender' => 'm',
                'birthday' => '1993-10-09',
                'email' => 'ptptester11@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/johnmichaelardales.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => 3,
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'jonalynsantos',
                'firstname' => 'Jonalyn',
                'lastname' => 'Santos',
                'gender' => 'f',
                'birthday' => '1994-12-04',
                'email' => 'ptptester12@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jonalinsantos.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [8,9],
                'friend_requests' => 1,
            ),
            array(
                'slug' => 'nidorayu',
                'firstname' => 'Nidora',
                'lastname' => 'Yu',
                'gender' => 'f',
                'birthday' => '1976-07-18',
                'email' => 'ptptester13@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/nidorayu.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [2,3],
                'friend_requests' => [1,3,5],
            ),
            array(
                'slug' => 'jhayrorsales',
                'firstname' => 'JhayR',
                'lastname' => 'Orsales',
                'gender' => 'm',
                'birthday' => '1994-03-27',
                'email' => 'ptptester15@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jhayrorsales.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [3],
                'friend_requests' => [1,2],
            ),
            array(
                'slug' => 'christianalemento',
                'firstname' => 'Chritian',
                'lastname' => 'Alemento',
                'gender' => 'm',
                'birthday' => '1993-02-17',
                'email' => 'ptptester16@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/christianalemento.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                'friend_requests' => [1,3],
            ),
            array(
                'slug' => 'johnpauldeguzman',
                'firstname' => 'John Paul',
                'lastname' => 'De Guzman',
                'gender' => 'm',
                'birthday' => '1992-11-18',
                'email' => 'ptptester17@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/johnpauldeguzman.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                'friend_requests' => [1,6],
            ),
            array(
                'slug' => 'jessecalapan',
                'firstname' => 'Jesse',
                'lastname' => 'Calapan',
                'gender' => 'f',
                'birthday' => '1991-05-26',
                'email' => 'ptptester18@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jessecalapan.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                'friend_requests' => [1,6],
            ),
            array(
                'slug' => 'andruschong',
                'firstname' => 'Andrus',
                'lastname' => 'Chong',
                'gender' => 'm',
                'birthday' => '1996-09-04',
                'email' => 'ptptester19@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/andruschong.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [3,4],
                'friend_requests' => [1,6],
            ),
            array(
                'slug' => 'xyrellego',
                'firstname' => 'Xyrelle',
                'lastname' => 'Go',
                'gender' => 'f',
                'birthday' => '1997-04-03',
                'email' => 'ptptester20@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/xyrellego.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [5,7,8],
                'friend_requests' => [1,6],
            ),
            array(
                'slug' => 'nickverano',
                'firstname' => 'Nick',
                'lastname' => 'Verano',
                'gender' => 'm',
                'birthday' => '1993-06-13',
                'email' => 'ptptester21@pbahotpicks.com',
                'avatar' => '/assets/img/avatars/012015/nickverano.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => 7,
                'friend_requests' => [1,2,3],
            ),
            array(
                'slug' => 'jocelperdigon',
                'firstname' => 'Jocel',
                'lastname' => 'Perdigon',
                'gender' => 'f',
                'birthday' => '1992-03-19',
                'email' => 'ptptester22@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jocelperdigon.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => 3,
                'friend_requests' => [1,2,3],
            ),
            array(
                'slug' => 'jamescabatin',
                'firstname' => 'James',
                'lastname' => 'Cabatin',
                'gender' => 'm',
                'birthday' => '1983-12-03',
                'email' => 'ptptester23@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/jamescabatin.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [7,9],
                'friend_requests' => [1,2,3],
            ),
            array(
                'slug' => 'paulryanneri',
                'firstname' => 'Paul Ryan',
                'lastname' => 'Neri',
                'gender' => 'm',
                'birthday' => '1989-01-30',
                'email' => 'ptptester24@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/paulryanneri.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [10],
                'friend_requests' => [1,2,3,4],
            ),
            array(
                'slug' => 'alexmiranda',
                'firstname' => 'Alex',
                'lastname' => 'Miranda',
                'gender' => 'm',
                'birthday' => '1993-07-27',
                'email' => 'ptptester25@pinoytenpicks.com',
                'avatar' => '/assets/img/avatars/012015/alexmiranda.jpg',
                'password' => '$2y$10$GckrcMsd8fr8c3dU5ljA9e7.JAgwSqMFwrx6A0s.1xUFt1fCxiFLC',
                'last_online' => date('Y-m-d h:i:s'),
                //'badges' => [3,5],
                'friend_requests' => [1,2],
            ),
        );

        foreach($managers as $mdata){
            $manager = new $this -> manager;
            $manager -> slug = $mdata['slug'];
            $manager -> firstname = $mdata['firstname'];
            $manager -> lastname = $mdata['lastname'];
            $manager -> gender = $mdata['gender'];
            $manager -> birthday = $mdata['birthday'];
            $manager -> email = $mdata['email'];
            $manager -> avatar = $mdata['avatar'];

            $manager -> ip_address = '127.0.0.1';
            $manager -> user_agent = 'PTP 306 BOT';
            $manager -> reg_type = 'DIRECT';

            $manager -> password = $mdata['password'];
            $manager -> last_online = $mdata['last_online'];
            if(isset($mdata['privilege'])){
                $manager -> privilege = $mdata['privilege'];
            }
            $manager -> date_registered = date('Y-m-d H:i:s');

            $credits_arr = [100,250,55,167,0,0,0,34,213,340,540,322,432,122,0,0,321,675,1120];
            $credits_ran_arr = array_rand($credits_arr, 2);

            $manager -> credits = $credits_arr[$credits_ran_arr[0]];
            $manager -> save();
            if(isset($mdata['badges'])){
            $manager -> badges() -> attach($mdata['badges'], array('status' => 'ENBL'));
            }
            if(isset($mdata['friend_requests'])){
                $manager -> friends() -> attach($mdata['friend_requests']);
            }
        }
    }
}