<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class ProductTableSeeder extends Seeder {

	public function run()
	{
        foreach($this -> items() as $item){
            Product::create($item);
        }
        
    }

    public function items()
    {
        return [

        	// BEGIN BADGE SEED ITEMS

            [
                'name' => 'Alaska Aces',
                'type' => 'BDGE',
                'product_id' => 1,
                'img' => '/assets/img/badges/alaska.png',
                'price' => 3000
            ],
            [
                'name' => 'Barako Bull Energy',
                'type' => 'BDGE',
                'product_id' => 2,
                'img' => '/assets/img/badges/barako.png',
                'price' => 3000
            ],
            [
                'name' => 'Brgy. Ginebra San Miguel',
                'type' => 'BDGE',
                'product_id' => 3,
                'img' => '/assets/img/badges/ginebra.png',
                'price' => 3000
            ],
            [
                'name' => 'Blackwater Elite',
                'type' => 'BDGE',
                'product_id' => 4,
                'img' => '/assets/img/badges/blackwater.png',
                'price' => 3000
            ],
            [
                'name' => 'Globalport Batang Pier',
                'type' => 'BDGE',
                'product_id' => 5,
                'img' => '/assets/img/badges/globalport.png',
                'price' => 3000
            ],
            [
                'name' => 'KIA Carnival',
                'type' => 'BDGE',
                'product_id' => 6,
                'img' => '/assets/img/badges/kia.png',
                'price' => 3000
            ],
            [
                'name' => 'Meralco Bolts',
                'type' => 'BDGE',
                'product_id' => 7,
                'img' => '/assets/img/badges/meralco.png',
                'price' => 3000
            ],
            [
                'name' => 'NLEX Road Warriors',
                'type' => 'BDGE',
                'product_id' => 8,
                'img' => '/assets/img/badges/nlex.png',
                'price' => 3000
            ],
            [
                'name' => 'Purefoods Star Hotshots',
                'type' => 'BDGE',
                'product_id' => 9,
                'img' => '/assets/img/badges/purefoods.png',
                'price' => 3000
            ],
            [
                'name' => 'Rain or Shine Elasto Painters',
                'type' => 'BDGE',
                'product_id' => 10,
                'img' => '/assets/img/badges/rainorshine.png',
                'price' => 3000
            ],
            [
                'name' => 'San Miguel Beermen',
                'type' => 'BDGE',
                'product_id' => 11,
                'img' => '/assets/img/badges/sanmiguel.png',
                'price' => 3000
            ],
            [
                'name' => 'Talk N Text Tropang Texters',
                'type' => 'BDGE',
                'product_id' => 12,
                'img' => '/assets/img/badges/talkntext.png',
                'price' => 3000
            ],

            // BEGIN FRANCHISE ICON SEED ITEMS

            [
                'name' => 'Red Star',
                'type' => 'FICO',
                'product_id' => 1,
                'img' => '/assets/img/franchise_logos/32/star_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Orange Star',
                'type' => 'FICO',
                'product_id' => 2,
                'img' => '/assets/img/franchise_logos/32/star_orange.png',
                'price' => 1500
            ],
            [
                'name' => 'Blue Dragon',
                'type' => 'FICO',
                'product_id' => 3,
                'img' => '/assets/img/franchise_logos/32/dragon_blue.png',
                'price' => 1500
            ],
            [
                'name' => 'Red Dragon',
                'type' => 'FICO',
                'product_id' => 4,
                'img' => '/assets/img/franchise_logos/32/dragon_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Blue Fire',
                'type' => 'FICO',
                'product_id' => 5,
                'img' => '/assets/img/franchise_logos/32/fire_blue.png',
                'price' => 1500
            ],
            [
                'name' => 'Red Fire',
                'type' => 'FICO',
                'product_id' => 6,
                'img' => '/assets/img/franchise_logos/32/fire_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Blue Flake',
                'type' => 'FICO',
                'product_id' => 7,
                'img' => '/assets/img/franchise_logos/32/flake_blue.png',
                'price' => 1500
            ],
            [
                'name' => 'Green Flake',
                'type' => 'FICO',
                'product_id' => 8,
                'img' => '/assets/img/franchise_logos/32/flake_green.png',
                'price' => 1500
            ],
            [
                'name' => 'Black Blade',
                'type' => 'FICO',
                'product_id' => 9,
                'img' => '/assets/img/franchise_logos/32/blade_black.png',
                'price' => 1500
            ],
            [
                'name' => 'Blue Blade',
                'type' => 'FICO',
                'product_id' => 10,
                'img' => '/assets/img/franchise_logos/32/blade_blue.png',
                'price' => 1500
            ],
            [
                'name' => 'Green Paw',
                'type' => 'FICO',
                'product_id' => 11,
                'img' => '/assets/img/franchise_logos/32/paw_green.png',
                'price' => 1500
            ],
            [
                'name' => 'Orange Paw',
                'type' => 'FICO',
                'product_id' => 12,
                'img' => '/assets/img/franchise_logos/32/paw_orange.png',
                'price' => 1500
            ],
            [
                'name' => 'Orange Lion',
                'type' => 'FICO',
                'product_id' => 13,
                'img' => '/assets/img/franchise_logos/32/lion_orange.png',
                'price' => 1500
            ],
            [
                'name' => 'Red Lion',
                'type' => 'FICO',
                'product_id' => 14,
                'img' => '/assets/img/franchise_logos/32/lion_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Orange Phoenix',
                'type' => 'FICO',
                'product_id' => 15,
                'img' => '/assets/img/franchise_logos/32/phoenix_orange.png',
                'price' => 1500
            ],
            [
                'name' => 'Red Phoenix',
                'type' => 'FICO',
                'product_id' => 16,
                'img' => '/assets/img/franchise_logos/32/phoenix_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Black Skull',
                'type' => 'FICO',
                'product_id' => 17,
                'img' => '/assets/img/franchise_logos/32/skull_black.png',
                'price' => 1500
            ],
            [
                'name' => 'Red Skull',
                'type' => 'FICO',
                'product_id' => 18,
                'img' => '/assets/img/franchise_logos/32/skull_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Green Bull',
                'type' => 'FICO',
                'product_id' => 19,
                'img' => '/assets/img/franchise_logos/32/bull_green.png',
                'price' => 1500
            ],
            [
                'name' => 'Red Bull',
                'type' => 'FICO',
                'product_id' => 20,
                'img' => '/assets/img/franchise_logos/32/bull_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Blue Thunder',
                'type' => 'FICO',
                'product_id' => 21,
                'img' => '/assets/img/franchise_logos/32/thunder_blue.png',
                'price' => 1500
            ],
            [
                'name' => 'Green Thunder',
                'type' => 'FICO',
                'product_id' => 22,
                'img' => '/assets/img/franchise_logos/32/thunder_green.png',
                'price' => 1500
            ],
            [
                'name' => 'Red King',
                'type' => 'FICO',
                'product_id' => 23,
                'img' => '/assets/img/franchise_logos/32/king_red.png',
                'price' => 1500
            ],
            [
                'name' => 'Green King',
                'type' => 'FICO',
                'product_id' => 24,
                'img' => '/assets/img/franchise_logos/32/king_green.png',
                'price' => 1500
            ],

            // Premium Franchise icon start here!!!


            [
                'name' => 'Premium Eagle Green',
                'type' => 'FICO',
                'product_id' => 25,
                'img' => '/assets/img/franchise_logos/32/prem_eagle_green.png',
                'price' => 5000
            ],

            [
                'name' => 'Premium Fire Red',
                'type' => 'FICO',
                'product_id' => 26,
                'img' => '/assets/img/franchise_logos/32/prem_fire_red.png',
                'price' => 5000
            ],

            [
                'name' => 'Premium King Gold',
                'type' => 'FICO',
                'product_id' => 27,
                'img' => '/assets/img/franchise_logos/32/prem_king_gold.png',
                'price' => 5000
            ],

            [
                'name' => 'Premium Skull Black',
                'type' => 'FICO',
                'product_id' => 28,
                'img' => '/assets/img/franchise_logos/32/prem_skull_black.png',
                'price' => 5000
            ],

            [
                'name' => 'Premium Star Blue',
                'type' => 'FICO',
                'product_id' => 29,
                'img' => '/assets/img/franchise_logos/32/prem_star_blue.png',
                'price' => 5000
            ],

            [
                'name' => 'Super Premium Fire',
                'type' => 'FICO',
                'product_id' => 30,
                'img' => '/assets/img/franchise_logos/32/superprem_fire.png',
                'price' => 10000
            ],
        ];
    }

}