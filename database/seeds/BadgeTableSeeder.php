<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Badge;

class BadgeTableSeeder extends Seeder {

	public function run()
	{
		$badges = array(
			array('name' => 'Alaska Aces', 'src' => '/assets/img/badges/alaska.png'),
			array('name' => 'Barako Bull Energy', 'src' => '/assets/img/badges/barako.png'),
			array('name' => 'Brgy. Ginebra San Miguel', 'src' => '/assets/img/badges/ginebra.png'),
			array('name' => 'Blackwater Elite', 'src' => '/assets/img/badges/blackwater.png'),
			array('name' => 'Globalport Batang Pier', 'src' => '/assets/img/badges/globalport.png'),
			array('name' => 'KIA Carnival', 'src' => '/assets/img/badges/kia.png'),
			array('name' => 'Meralco Bolts', 'src' => '/assets/img/badges/meralco.png'),
			array('name' => 'NLEX Road Warriors', 'src' => '/assets/img/badges/nlex.png'),
			array('name' => 'Purefoods Star Hotshots', 'src' => '/assets/img/badges/purefoods.png'),
			array('name' => 'Rain or Shine Elasto Painters', 'src' => '/assets/img/badges/rainorshine.png'),
			array('name' => 'San Miguel Beermen', 'src' => '/assets/img/badges/sanmiguel.png'),
			array('name' => 'Talk N Text Tropang Texters', 'src' => '/assets/img/badges/talkntext.png'),
			array('name' => 'Champion', 'src' => '/assets/img/badges/champion.png'),
		);

		foreach($badges as $badge){
        	Badge::create($badge);
    	}
	}

}