<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Message;

class MessageTableSeeder extends Seeder {

	public function run()
	{
		$messages = array(
			array(
				'conversation_id' => 'kD3j0lTn2x',
				'message' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.',
				'sender' => 1,
				'recipient' => 2,
				'read' => 1,
				'created_at' => date('2015-06-28 09:25:00'),
			),
			array(
				'conversation_id' => 'kD3j0lTn2x',
				'message' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco nisi ut',
				'sender' => 2,
				'recipient' => 1,
				'read' => 1,
				'created_at' => date('2015-06-28 09:27:00'),
			),
			array(
				'conversation_id' => 'kD3j0lTn2x',
				'message' => 'Temporibus autem quibusdam et aut ',
				'sender' => 1,
				'recipient' => 2,
				'read' => 1,
				'created_at' => date('2015-06-28 09:28:00'),
			),
			array(
				'conversation_id' => 'ol3kFh2D0q',
				'message' => 'Duis aute irure dolor in reprehenderit in voluptate velit?',
				'sender' => 1,
				'recipient' => 3,
				'read' => 1,
				'created_at' => date('2015-06-29 01:43:00'),
			),
			array(
				'conversation_id' => 'ol3kFh2D0q',
				'message' => 'Excepteur sint occaecat cupidatat non proiden. :)',
				'sender' => 3,
				'recipient' => 1,
				'read' => 1,
				'created_at' => date('2015-06-29 03:43:40'),
			),
			array(
				'conversation_id' => '7uFlW2pzX2',
				'message' => 'Equis nostrud exercitation ullamco nisi ut...haha',
				'sender' => 1,
				'recipient' => 4,
				'read' => 1,
				'created_at' => date('2015-06-29 12:09:30'),
			),
			array(
				'conversation_id' => 'Lb9o4mQXp2',
				'message' => 'Ok. adipiscing occaecat.',
				'sender' => 5,
				'recipient' => 1,
				'read' => 0,
				'created_at' => date('2015-06-30 12:09:30'),
			),
			array(
				'conversation_id' => 'Lb9o4mQXp2',
				'message' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
				'sender' => 1,
				'recipient' => 5,
				'read' => 0,
				'created_at' => date('2015-07-01 07:05:00'),
			),
			array(
				'conversation_id' => 'Lb9o4mQXp2',
				'message' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt',
				'sender' => 5,
				'recipient' => 1,
				'read' => 0,
				'created_at' => date('2015-07-01 07:12:13'),
			),
			array(
				'conversation_id' => 'Lb9o4mQXp2',
				'message' => 'Neque porro quisquam est, qui dolorem?',
				'sender' => 5,
				'recipient' => 1,
				'read' => 0,
				'created_at' => date('2015-07-01 07:14:28'),
			),
			array(
				'conversation_id' => 'Lb9o4mQXp2',
				'message' => 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas ',
				'sender' => 1,
				'recipient' => 5,
				'read' => 0,
				'created_at' => date('2015-07-01 07:23:33'),
			),
			array(
				'conversation_id' => 'Lb9o4mQXp2',
				'message' => 'hahaha. ok :)',
				'sender' => 5,
				'recipient' => 1,
				'read' => 0,
				'created_at' => date('2015-07-02 12:02:15'),
			),
			array(
				'conversation_id' => 'gt2d85Mnf8',
				'message' => 'Hey, Lunch? :)',
				'sender' => 2,
				'recipient' => 3,
				'read' => 1,
				'created_at' => date('2015-06-30 15:32:43'),
			),
			array(
				'conversation_id' => 'l3Do6Vxq4P',
				'message' => 'Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure',
				'sender' => 6,
				'recipient' => 1,
				'read' => 1,
				'created_at' => date('2015-07-01 01:12:00'),
			),
			array(
				'conversation_id' => 'l3Do6Vxq4P',
				'message' => 'To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it?',
				'sender' => 1,
				'recipient' => 6,
				'read' => 1,
				'created_at' => date('2015-07-01 03:25:10'),
			),
			array(
				'conversation_id' => 'dlp3Dp9Sm0',
				'message' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum',
				'sender' => 1,
				'recipient' => 7,
				'read' => 1,
				'created_at' => date('2015-07-02 11:25:00'),
			),
			array(
				'conversation_id' => 'dlp3Dp9Sm0',
				'message' => 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint',
				'sender' => 7,
				'recipient' => 1,
				'read' => 1,
				'created_at' => date('2015-07-02 11:34:00'),
			),
		);

		foreach($messages as $message){
        	Message::create($message);
    	}
	}

}