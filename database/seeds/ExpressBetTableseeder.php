<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\ExpressBet;

class ExpressBetTableSeeder extends Seeder {

	public function run()
	{
        foreach($this -> items() as $item){
            ExpressBet::create($item);
        }
        
    }

    public function items()
    {
        return [
            [
                'description' => 'Who do you think will win between Rain or Shine Elasto Painters and Purefoods Star Hotshots?',
                'option' => serialize([1=>'Rain or Shine Elasto Painters', 2=>'Purefoods Star Hotshots']),
                'won' => 1,
                'gamedate' => date('2015-09-04 18:00:00'),
                'expiry' => date('2015-09-06 15:00:00')
            ],
            [
                'description' => 'Who do you think will win between Talk N Text Tropang Texters and San Miguel Beermen?',
                'option' => serialize([1=>'Talk N Text Tropang Texters', 2=>'San Miguel Beermen']),
                'won' => 2,
                'gamedate' => date('2015-09-04 18:00:00'),
                'expiry' => date('2015-09-06 15:00:00')
            ],
            [
                'description' => 'Who do you think will win between Alaska Aces and Ginebra San Miguel?',
                'option' => serialize([1=>'Alaska Aces', 2=>'Ginebra San Miguel']),
                'won' => 1,
                'gamedate' => date('2015-09-04 18:00:00'),
                'expiry' => date('2015-09-16 15:00:00')
            ],
        ];
    }

}