<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Announcement;

class AnnouncementTableSeeder extends Seeder {

	public function run()
	{
		$announcements = array(
			array('order' => 1, 'text' => "Welcome to PinoyTenPicks, don't forget to share this website to your friends. Enjoy and have a great day Pickers. :)"),
			array('order' => 5, 'text' => "Dec 25, 2015 - Alaska vs. San Miguel, KIA vs. Barako Bull games are now available, You can update your league now.")
		);

		foreach($announcements as $announcement){
        	Announcement::create($announcement);
    	}
	}

}