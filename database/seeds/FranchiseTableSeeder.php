<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Franchise;

class FranchiseTableSeeder extends Seeder {

    protected $franchise;

    public function __construct(Franchise $franchise){
        $this -> franchise = $franchise;
    }

	public function run()
	{
        $this -> createFranchises();
    }

    private function createFranchises()
    {
        $franchises = array(
            array(
                'name' => 'Black Pirates',
                'manager_id' => 1,
                'logo_id' => 17,
                'created_at' => '2015-08-12',
                'players' => [6,1,21,4,12,19,8,11,26,48],
            ),
            array(
                'name' => 'Flame Blade',
                'manager_id' => 2,
                'logo_id' => 6,
                'created_at' => '2015-08-12',
                'players' => [132,6,15,4,12,17,32,28,38,48],
            ),
            array(
                'name' => 'Barako Kings',
                'manager_id' => 3,
                'logo_id' => 23,
                'created_at' => '2015-08-12',
                'players' => [159,157,165,168,129,141,126,142,150,153],
            ),
            array(
                'name' => 'Nature Slayers',
                'manager_id' => 4,
                'logo_id' => 11,
                'created_at' => '2015-08-12',
                'players' => [16,15,24,22,17,14,32,33,38,48],
            ),
            array(
                'name' => 'Galaxy',
                'manager_id' => 5,
                'logo_id' => 2,
                'created_at' => '2015-08-12',
                'players' => [75,80,90,79,55,41,81,97,158,169],
            ),
            array(
                'name' => 'Malabon Sonics',
                'manager_id' => 6,
                'logo_id' => 3,
                'created_at' => '2015-08-12',
                'players' => [164,6,170,168,161,172,126,145,128,155],
            ),
            array(
                'name' => 'Onepiece',
                'manager_id' => 7,
                'logo_id' => 7,
                'created_at' => '2015-08-12',
                'players' => [50,47,49,45,86,140,146,148,120,131],
            ),
            array(
                'name' => 'Doflamingo Pirates',
                'manager_id' => 8,
                'logo_id' => 18,
                'created_at' => '2015-08-12',
                'players' => [116,43,127,111,129,139,145,142,123,128],
            ),
            array(
                'name' => 'Kupunan ni Taguro',
                'manager_id' => 9,
                'logo_id' => 19,
                'created_at' => '2015-08-12',
                'players' => [99,34,30,4,70,3,28,5,123,126],
            ),
            array(
                'name' => 'Knight Blader',
                'manager_id' => 10,
                'logo_id' => 9,
                'created_at' => '2015-08-12',
                'players' => [79,80,83,82,52,41,74,76,18,54],
            ),

            array(
                'name' => 'COC Clashers',
                'manager_id' => 11,
                'logo_id' => 20,
                'created_at' => '2015-08-12',
                'players' => [6,1,21,4,12,19,8,11,26,48],
            ),
            array(
                'name' => 'Pabebe Team',
                'manager_id' => 12,
                'logo_id' => 4,
                'created_at' => '2015-08-12',
                'players' => [132,6,15,4,12,17,32,28,38,48],
            ),
            array(
                'name' => 'Trail Blazers',
                'manager_id' => 13,
                'logo_id' => 22,
                'created_at' => '2015-08-12',
                'players' => [159,157,165,168,129,141,126,142,150,153],
            ),
            array(
                'name' => 'Brgy. Purefoods',
                'manager_id' => 14,
                'logo_id' => 15,
                'created_at' => '2015-08-12',
                'players' => [16,15,24,22,17,14,32,33,38,48],
            ),
            array(
                'name' => 'Programmers Heaven',
                'manager_id' => 15,
                'logo_id' => 11,
                'created_at' => '2015-08-12',
                'players' => [75,80,90,79,55,41,81,97,158,169],
            ),
            array(
                'name' => 'Aldub Nation',
                'manager_id' => 16,
                'logo_id' => 7,
                'created_at' => '2015-08-12',
                'players' => [164,6,170,168,161,172,126,145,128,155],
            ),
            array(
                'name' => 'Smart Gilas 4',
                'manager_id' => 17,
                'logo_id' => 3,
                'created_at' => '2015-08-12',
                'players' => [50,47,49,45,86,140,146,148,120,131],
            ),
            array(
                'name' => 'Hurricane Shadow',
                'manager_id' => 18,
                'logo_id' => 16,
                'created_at' => '2015-08-12',
                'players' => [116,43,127,111,129,139,145,142,123,128],
            ),
            array(
                'name' => 'Source Tree',
                'manager_id' => 19,
                'logo_id' => 5,
                'created_at' => '2015-08-12',
                'players' => [99,34,30,4,70,3,28,5,123,126],
            ),
            array(
                'name' => 'Fliptop Boys',
                'manager_id' => 20,
                'logo_id' => 3,
                'created_at' => '2015-08-12',
                'players' => [79,80,83,82,52,41,74,76,18,54],
            ),
            array(
                'name' => 'Crimson Fritz',
                'manager_id' => 21,
                'logo_id' => 23,
                'created_at' => '2015-08-12',
                'players' => [6,1,164,168,12,19,8,142,26,48],
            ),
            array(
                'name' => 'James Team',
                'manager_id' => 22,
                'logo_id' => 12,
                'created_at' => '2015-08-12',
                'players' => [50,47,49,168,161,172,8,142,128,155],
            ),
            array(
                'name' => 'PRN Warriors',
                'manager_id' => 23,
                'logo_id' => 19,
                'created_at' => '2015-08-12',
                'players' => [99,34,49,45,86,140,146,5,123,126],
            ),
            
            array(
                'name' => 'Red Bull',
                'manager_id' => 24,
                'logo_id' => 20,
                'created_at' => '2015-08-12',
                'players' => [6,21,1,19,17,14,7,10,26,38],
            ),
            
        );

        foreach($franchises as $fdata){
            $franchise = new $this -> franchise;
            $franchise -> name = $fdata['name'];
            $franchise -> manager_id = $fdata['manager_id'];
            $franchise -> logo_id = $fdata['logo_id'];
            $franchise -> created_at = $fdata['created_at'];
            $franchise -> save();

            $positions = [1,1,2,2,3,3,4,4,5,5];
            $i = 0;

            foreach($fdata['players'] as $playerid){
                $fp_list = ['2015-08-12','2015-03-01','2015-08-01','2015-09-01','2015-08-25','2015-08-29','2015-08-24','2015-07-23','2015-07-27','2015-07-04'];
                $fp_points = [10,23,26,22,45,23,11,16,23,27,36,38,48,34,];
                $fp_rand_arr = array_rand($fp_list, 2);
                $fp_points_arr = array_rand($fp_points, 2);
                $franchise -> players() -> attach($playerid, array('fantasy_points' => $fp_points[$fp_points_arr[0]], 'position' => $positions[$i], 'date_hired' => $fp_list[$fp_rand_arr[0]]));

                $i++;

                // $fp_list = [13,18,21,25,29,31,33,37,41,42,46,48,50,52,56,58,59,62,65];
                // $fp_rand_arr = array_rand($fp_list, 2);
                /*$fp_list[$fp_rand_arr[0]]*/
                /*
                if(!in_array($fdata['manager_id'], [21,22,23,24]))
                {
                    $franchise -> players() -> attach($playerid, array('fantasy_points' => 0, 'date_hired' => '2015-08-12'));
                }else{
                    $franchise -> players() -> attach($playerid, array('fantasy_points' => 0, 'date_hired' => '2015-08-12'));
                }
                */
                
            }
        }
    }
}