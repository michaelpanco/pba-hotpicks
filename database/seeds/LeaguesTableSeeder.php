<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\League;
use App\Models\Franchise;

class LeaguesTableSeeder extends Seeder {

    protected $league;
    protected $franchise;

    public function __construct(League $league, Franchise $franchise){
        $this -> league = $league;
        $this -> franchise = $franchise;
    }

	public function run()
	{
        $this -> createLeagues();
    }

    private function createLeagues()
    {
        $leagues = array(
            array(
                'name' => 'PBA Series Cup',
                'host' => 1,
                'desc' => 'Feel free to join, Please be online and active, and be friendly to our fellow member. Lets enjoy here. :)',
                'channel_id' => 14,
                'password' => '',
                'sequence' => 1,
                'ready' => 1,
                'created_at' => '2015-08-06',
                'participants' => [1,2,3,4,5,6,7,8,9,10]
            ),
            array(
                'name' => 'Kings Unite',
                'host' => 14,
                'desc' => 'Welcome Kings Unite Members, tara na at maglaro at mag enjoy. Goodluck',
                'channel_id' => 15,
                'password' => '123456',
                'sequence' => 1,
                'ready' => 1,
                'created_at' => '2015-08-06',
                'participants' => [11,12,13,14,15,16,17,18,19,20]
            ),
            array(
                'name' => 'Best Team Tornament',
                'host' => 22,
                'desc' => 'Pasok mga pre. all members should be active, lets fill this up to 30 particpants.',
                'channel_id' => 16,
                'password' => '',
                'sequence' => 1,
                'ready' => 1,
                'created_at' => '2015-08-06',
                'participants' => [21,22,23,24]
            ),
        );

        foreach($leagues as $ldata){
            $league = new $this -> league;
            $league -> name = $ldata['name'];
            $league -> host = $ldata['host'];
            $league -> desc = $ldata['desc'];
            $league -> channel_id = $ldata['channel_id'];
            $league -> password = $ldata['password'];
            $league -> sequence = $ldata['sequence'];
            $league -> ready = $ldata['ready'];
            $league -> created_at = '2015-11-05';
            $league -> save();
            $this -> setManagerLeague($ldata['participants'], $league -> id);
            $league -> participants() -> attach($ldata['participants']);
        }
    }

    private function setManagerLeague($manager_ids, $league_id){
    	$this -> franchise -> whereIn('id', $manager_ids) -> update(['league_id' => $league_id]);
    }
}