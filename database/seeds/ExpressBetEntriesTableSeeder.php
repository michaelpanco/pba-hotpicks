<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\ExpressBetEntries;

class ExpressBetEntriesTableSeeder extends Seeder {

	public function run()
	{
        foreach($this -> bets() as $bet){
            ExpressBetEntries::create($bet);
        }
        
    }

    public function bets()
    {
        return [
            [
                'express_bet_id' => 1,
                'manager_id' => 1,
                'bet' => 1,
                'amount' => 750,
                'claimed' => 1,
                'datetime' => date('2015-09-03 09:43:19')
            ],
            [
                'express_bet_id' => 1,
                'manager_id' => 2,
                'bet' => 2,
                'amount' => 500,
                'claimed' => 0,
                'datetime' => date('2015-09-03 09:43:19')
            ],
            [
                'express_bet_id' => 1,
                'manager_id' => 3,
                'bet' => 1,
                'amount' => 1200,
                'claimed' => 1,
                'datetime' => date('2015-09-03 09:43:19')
            ],
            [
                'express_bet_id' => 1,
                'manager_id' => 4,
                'bet' => 1,
                'amount' => 970,
                'claimed' => 1,
                'datetime' => date('2015-09-03 09:43:19')
            ],
            [
                'express_bet_id' => 1,
                'manager_id' => 5,
                'bet' => 2,
                'amount' => 341,
                'claimed' => 0,
                'datetime' => date('2015-09-03 09:43:19')
            ],
            [
                'express_bet_id' => 2,
                'manager_id' => 1,
                'bet' => 2,
                'amount' => 830,
                'claimed' => 0,
                'datetime' => date('2015-09-05 14:12:32')
            ],
            [
                'express_bet_id' => 2,
                'manager_id' => 2,
                'bet' => 2,
                'amount' => 655,
                'claimed' => 1,
                'datetime' => date('2015-09-05 14:12:32')
            ],
            [
                'express_bet_id' => 2,
                'manager_id' => 3,
                'bet' => 1,
                'amount' => 800,
                'claimed' => 0,
                'datetime' => date('2015-09-05 14:12:32')
            ],
            [
                'express_bet_id' => 3,
                'manager_id' => 3,
                'bet' => 1,
                'amount' => 1350,
                'claimed' => 0,
                'datetime' => date('2015-09-13 12:18:32')
            ],
            [
                'express_bet_id' => 3,
                'manager_id' => 5,
                'bet' => 2,
                'amount' => 670,
                'claimed' => 0,
                'datetime' => date('2015-09-15 21:42:16')
            ],
        ];
    }

}