<?php

use App\Models\Manager;

class GuestTest extends TestCase {

	public function testIndexPage()
	{
    	$response = $this->call('GET', '/');
    	$this->assertEquals(200, $response->status());
	}

	public function testRegisterPage()
	{
    	$response = $this->call('GET', '/register');
    	$this->assertEquals(200, $response->status());
	}

	public function testMemberProfilePage()
	{
    	$response = $this->call('GET', '/manager/markjosephperona');
    	$this->assertEquals(200, $response->status());
	}
}