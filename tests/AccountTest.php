<?php

use App\Models\Manager;

class AccountTest extends TestCase {

	public function setUp()
	{
		parent::setUp(); 
		Session::start();
		Artisan::call('migrate:refresh');
		Artisan::call('db:seed');
		Mail::pretend(true);
	}

	public function testManagerRegistration()
	{
		$params = array(
			'firstname' => 'Enel',
			'lastname' => 'Newgate',
			'gender' => 'male',
			'email' => 'michael@omytxt.com',
			'birth_day' => 11,
			'birth_month' => 12,
			'birth_year' => 1991,
			'password' => 'nje83hekd9',
			'password_confirmation' => 'nje83hekd9',
			'_token' => Session::token()
		);
		$this -> post('/api/manager/register', $params)
             ->seeJson([
                'status' => 'success',
             ]);

        $this -> confirmEmail();
	}

	public function confirmEmail()
	{
		$result = Manager::where('email', 'michael@omytxt.com') -> get() -> first();
		$confirmation = $result -> confirmation -> confirmation_code;
		$this -> visit('/confirm/manager/' . $result -> id . '/confirmation/' . $confirmation)->see('success');

		$params = array(
			'email' => 'michael@omytxt.com',
			'password' => 'nje83hekd9',
			'_token' => Session::token()
		);

		$this -> post('/api/manager/login', $params)
             -> seeJson([
                'status' => 'success',
             ]);

        $this -> loginRegisteredManager();
	}

	public function loginRegisteredManager()
	{
		$params = array(
			'email' => 'michael@omytxt.com',
			'password' => 'nje83hekd9',
			'_token' => Session::token()
		);
		$this -> post('/api/manager/login', $params)
             -> seeJson([
                'status' => 'success',
             ]);

        $this -> visitDashboardPage();
	}

	public function visitDashboardPage()
	{
    	$response = $this -> call('GET', '/dashboard');
    	$this -> assertEquals(200, $response -> status());

		$this -> selectChannels();
	}

	public function selectChannels()
	{
		$channel_ids = [1,2,3,4,5,6,7,8,9,10,11,12,13];

		foreach($channel_ids as $channel_id)
		{
			$params = array(
				"channel_id" => $channel_id,
				"_token" => Session::token()
			);

			$response = $this -> call('POST', '/api/manager/getChannel', $params);
	    	$this -> assertEquals(200, $response->status());
		}

		$this -> getMorePosts();
	}

	public function getMorePosts()
	{
		$params = array(
			"channel_id" => 1,
			"pointer" => 3,
			"_token" => Session::token()
		);

		$response = $this -> call('POST', '/api/manager/getMorePosts', $params);
    	$this -> assertEquals(200, $response->status());

		$this -> postStatus();
	}

	public function postStatus()
	{
		$params = array(
			"channel_id" => 1,
			"content" => "Hello",
			"_token" => Session::token()
		);

		$response = $this -> call('POST', '/api/manager/createPost', $params);
    	$this -> assertEquals(200, $response -> status());

    	$this -> upVoteStatus();
	}

	public function upVoteStatus()
	{
		$params = array(
			'post_id' => 2,
			'_token' => Session::token()
		);
		$this -> post('/api/manager/upVotePost', $params)
             -> seeJson([
                'status' => 'success',
             ]);

        $this -> replyStatus();
	}

	public function replyStatus()
	{
		$params = array(
			"channel_id" => 1,
			"content" => "Hi",
			"parent_post" => 2,
			"_token" => Session::token()
		);

		$response = $this -> call('POST', '/api/manager/replyPost', $params);
    	$this -> assertEquals(200, $response -> status());

    	$this -> upVoteReplyStatus();
	}

	public function upVoteReplyStatus()
	{
		$upvote_params = array(
			'post_id' => 3,
			'_token' => Session::token()
		);
		$this -> post('/api/manager/upVotePost', $upvote_params)
             -> seeJson([
                'status' => 'success',
             ]);

        $this -> checkNotices();
	}

	public function checkNotices()
	{
		// Notifications
    	$notifications_response = $this -> call('POST', '/api/manager/fetchNotifications', ['_token' => Session::token()]);
    	$this -> assertEquals(200, $notifications_response -> status());

		// Message
    	$latest_messages_response = $this -> call('POST', '/api/manager/fetchLatestMessages', ['_token' => Session::token()]);
    	$this -> assertEquals(200, $latest_messages_response -> status());

		// Requests
    	$requests_response = $this -> call('POST', '/api/manager/fetchRequests', ['_token' => Session::token()]);
    	$this -> assertEquals(200, $requests_response -> status());

		$this -> visitManagerProfilePage();
	}

	public function visitManagerProfilePage()
	{
    	$response = $this -> call('GET', '/manager/markjosephperona');
    	$this -> assertEquals(200, $response -> status());

    	$this -> addManagerAsFriend();
	}

	public function addManagerAsFriend()
	{
		$params = array(
			'manager_id' => 1,
			'_token' => Session::token()
		);
		$this -> post('/api/manager/addFriend', $params)
             ->seeJson([
                'status' => 'success',
             ]);

        $this -> MessagePage();
	}

	public function MessagePage()
	{
    	$response = $this -> call('GET', '/messages/markjosephperona');
    	$this -> assertEquals(200, $response -> status());

    	$this -> sendMesage();
	}

	public function sendMesage()
	{
		$params = array(
			'conversation_id' => 'd3Gm9Ris2e',
			'message' => 'Hello',
			'recipient' => 1,
			'_token' => Session::token()
		);
		$this -> post('/api/manager/sendMessage', $params)
             ->seeJson([
                'status' => 'success',
             ]);

        $this -> friendsPage();
	}

	public function friendsPage()
	{
    	$response = $this -> call('GET', '/friends');
    	$this -> assertEquals(200, $response -> status());

    	$this -> accountLogout();
	}

	public function accountLogout()
	{
    	$response = $this -> call('GET', '/logout');
    	$this -> assertEquals(302, $response -> status());

    	$this -> newAccountLogin();
	}

	/******* Account Response *******/

	public function newAccountLogin()
	{
		$params = array(
			'email' => 'phtester1@pbahotpicks.com',
			'password' => 'c4e1ynkwohl',
			'_token' => Session::token()
		);
		$this -> post('/api/manager/login', $params)
             -> seeJson([
                'status' => 'success',
             ]);

        $this -> checkRecievedNotices();
	}

	public function checkRecievedNotices()
	{
		$params = array(
			'_token' => Session::token()
		);
		$this -> post('/api/manager/notifier', $params)
             -> seeJson([
                'notifications' => 0,
                'newMessages' => 2,
                'requests' => 9,
                'friends' => 0,
             ]);

        $this -> approveFriendRequest();
	}

	public function approveFriendRequest()
	{
		$params = array(
			'manager_id' => 12,
			'_token' => Session::token()
		);
		$this -> post('/api/manager/approveRequest', $params)
             -> seeJson([
                'status' => 'success'
             ]);

        $this -> declineFriendRequest();
	}

	public function declineFriendRequest()
	{
		$params = array(
			'manager_id' => 2,
			'_token' => Session::token()
		);
		$this -> post('/api/manager/declineRequest', $params)
             -> seeJson([
                'status' => 'success'
             ]);
	}

}