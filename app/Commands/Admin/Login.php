<?php namespace App\Commands\Admin;

use Auth;
use Event;
use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Events\ManagerSuccessfullyLogin;

class Login extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle()
	{
		if(Auth::attempt(['email' => $this -> details['email'], 'password' => $this -> details['password'], 'privilege' => 'administrator'], $this -> details['remember']))
		{
			return ['success' => true, 'message' => '0'];
		}else{
			return ['success' => false, 'message' => 'Email and/or password is incorrect'];
		}
	}

}