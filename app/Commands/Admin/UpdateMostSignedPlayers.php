<?php namespace App\Commands\Admin;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\PlayerRepository;
use Helper;
use Redis;

class UpdateMostSignedPlayers extends Command implements SelfHandling {

	public function handle(PlayerRepository $player)
	{
		$most_signed_players = $player -> mostSigned();
		Redis::del('laravel:most_signed_players');
		foreach ($most_signed_players as $player) {

			$details['avatar'] = $player -> avatar;
			$details['slug'] = $player -> slug;
			$details['name'] = $player -> fullname;
			$details['total_sign'] = $player -> sign_count;
			$details['team'] = $player -> team -> name;
			$details['team_slug'] = $player -> team -> slug;
			$details['position'] = Helper::getLabelPosition($player -> position);

			Redis::zadd('laravel:most_signed_players', $player -> sign_count, json_encode($details, JSON_PRETTY_PRINT));
		}
	}

}