<?php namespace App\Commands\Admin;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\FranchiseRepository;
use Redis;

class TopManagers extends Command implements SelfHandling {

	public function handle(FranchiseRepository $franchise)
	{
		$top_franchise = $franchise -> top();

		Redis::del('laravel:top_managers');
		foreach ($top_franchise as $franchise_info) {

			$franchise_details = $franchise -> details($franchise_info -> franchise_id);

			$details['id'] = $franchise_details -> manager -> id;
			$details['avatar'] = $franchise_details -> manager -> avatar;
			$details['slug'] = $franchise_details -> manager -> slug;
			$details['name'] = $franchise_details -> manager -> fullname;
			$details['total_fantasy_points'] = $franchise_info -> total_fantasy_points;
			$details['franchise'] = $franchise_details -> name;
			$details['franchise_id'] = $franchise_details -> id;
			$details['league_id'] = $franchise_details -> league -> id;
			$details['league_name'] = $franchise_details -> league -> name;

			Redis::zadd('laravel:top_managers', $details['total_fantasy_points'], json_encode($details, JSON_PRETTY_PRINT));
		}
		
	}

}