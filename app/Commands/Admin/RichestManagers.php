<?php namespace App\Commands\Admin;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use Redis;

class RichestManagers extends Command implements SelfHandling {

	public function handle(ManagerRepository $manager)
	{
		$richest_managers = $manager -> richest();

		Redis::del('laravel:richest_managers');
		foreach ($richest_managers as $manager) {

			$details['id'] = $manager -> id;
			$details['avatar'] = $manager -> avatar;
			$details['slug'] = $manager -> slug;
			$details['name'] = $manager -> fullname;
			$details['credits'] = $manager -> credits;
			$details['franchise'] = $manager -> franchise -> name;
			$details['franchise_id'] = $manager -> franchise -> id;
			$details['league_id'] = $manager -> franchise-> league -> id;
			$details['league_name'] = $manager -> franchise -> league -> name;

			Redis::zadd('laravel:richest_managers', $details['credits'], json_encode($details, JSON_PRETTY_PRINT));
		}
		
	}

}