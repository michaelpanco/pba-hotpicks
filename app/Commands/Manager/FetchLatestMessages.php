<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\MessageRepository;

class FetchLatestMessages extends Command implements SelfHandling {

	public function handle(MessageRepository $message)
	{
		$requests = $message -> lists('notification');
		return $requests;
	}
	
}