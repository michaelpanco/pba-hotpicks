<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use Auth;
use Event;
use App\Events\TriggerNotification;

class ClaimReferalPrize extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{
		$details['manager_id'] = Auth::user() -> id;

		$claim_referal = $manager -> claimReferalPrize($details);

		if($claim_referal)
		{
			$credit['manager_id'] = $details['manager_id'];
			$credit['amount'] = 5000;

			$manager -> creditMoney($credit);

			$this -> notifyRequestAcceptance($details);

			return true;
		}
		
		return false;
	}

	public function notifyRequestAcceptance($details)
	{
		$notification_params['manager_id'] = $details['manager_id'];
		$notification_params['type'] = "REFCLM";
		$notification_params['details'] = "Congratulations, You earned 5,000 credits for the referal program. Thank you for helping us to grow this community. Cheers!";
		Event::fire(new TriggerNotification($notification_params));
	}
}