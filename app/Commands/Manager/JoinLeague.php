<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;
use App\Events\FranchiseSuccessfullyJoin;
use Event;

class JoinLeague extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$join_league = $league -> join($this -> details);

		if($join_league){
			Event::fire(new FranchiseSuccessfullyJoin($this -> details));
		}

		return $join_league;
	}
	
}