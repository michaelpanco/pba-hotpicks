<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\PostRepository;
use Event;
use Auth;
use App\Events\TriggerDisciplinary;

class DeletePost extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(PostRepository $post)
	{
		$delete_post = $post -> delete($this -> details);

		$post_details = $post -> details($this -> details['post_id']);

		$details['manager_id'] = $post_details -> manager_id;
		$details['description'] = $post_details -> content;

		$this -> disciplinaryLogs($details);

		return $delete_post;
	}

	private function disciplinaryLogs($details)
	{
		$action_logs['officer_id'] = Auth::user()->id;
		$action_logs['manager_id'] = $details['manager_id'];
		$action_logs['status'] = "DELP";
		$action_logs['description'] = $details['description'];
		$action_logs['created_at'] = date('Y-m-d H:i:s');
		Event::fire(new TriggerDisciplinary($action_logs));
	}
}