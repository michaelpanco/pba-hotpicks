<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ExpressBetRepository;

class MakeExpressBet extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ExpressBetRepository $expressBet)
	{
		$express_bet = $expressBet -> makeBet($this -> details);
		return $express_bet;
	}
	
}