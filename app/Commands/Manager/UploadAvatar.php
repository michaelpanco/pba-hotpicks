<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;

class UploadAvatar extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{
		$upload_avatar = $manager -> uploadAvatar($this -> details);
		return $upload_avatar;
	}
	
}