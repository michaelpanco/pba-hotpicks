<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ProductRepository;
use Auth;

class PurchaseProduct extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ProductRepository $product)
	{
		$product_details = $product -> details($this -> details -> input('item_id'));

		$details['manager_id'] = Auth::user() -> id;
		$details['product_type'] = $product_details -> type;
		$details['product_id'] = $product_details -> product_id;
		$details['product_price'] = $product_details -> price;

		$purchase = $product -> purchase($details);
		return $purchase;
	}
	
}