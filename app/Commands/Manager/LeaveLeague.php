<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;
use App\Events\FranchiseSuccessfullyLeave;
use Event;

class LeaveLeague extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$leave_league = $league -> leave($this -> details);

		if($leave_league){
			Event::fire(new FranchiseSuccessfullyLeave($this -> details));
		}

		return $leave_league;
	}
	
}