<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\EmailConfirmationRepository;

class ConfirmRegistration extends Command implements SelfHandling {

	protected $request;

	public function __construct($request)
	{
		$this -> request = $request;
	}

	public function handle(EmailConfirmationRepository $emailConfirmation)
	{

		if($emailConfirmation -> exist($this -> request))
		{
			$email_confirm = $emailConfirmation -> confirm($this -> request['manager_id']);

		}else{
			
			throw new \Exception('<span class="glyphicon glyphicon-remove" aria-hidden="true" style="margin-right:5px;"></span> Sorry! You verification request might already expired or not exist.');
		}

		return $email_confirm;
	}

}