<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ExpressBetRepository;
use Auth;
use Event;
use App\Events\TriggerNotification;

class ClaimExpressBetPrize extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ExpressBetRepository $expressBet)
	{
		$details['manager_id'] = Auth::user() -> id;
		$details['express_bet_id'] = $this -> details -> input('express_bet_id');

		$claim_express_bet_prize = $expressBet -> claimPrize($details);
		$details['amount'] = $claim_express_bet_prize[1];
		
		$this -> notifyRequestAcceptance($details);

		return $claim_express_bet_prize[0];
	}

	public function notifyRequestAcceptance($details)
	{
		$notification_params['manager_id'] = $details['manager_id'];
		$notification_params['type'] = "EXBCLM";
		$notification_params['details'] = "Congratulations, You earned " . number_format($details['amount']) . "  from Express bet";
		Event::fire(new TriggerNotification($notification_params));
	}
}