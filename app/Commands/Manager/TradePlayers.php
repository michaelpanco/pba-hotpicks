<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\FranchiseRepository;

class TradePlayers extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(FranchiseRepository $franchise)
	{
		$trade_player = $franchise -> trade($this -> details);
		return $trade_player;
	}
	
}