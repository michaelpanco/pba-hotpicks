<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use App\Events\RequestPasswordReset;
use Event;

class ForgotPassword extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{
		$forgot_password = $manager -> forgotPassword($this -> details);
		
		if($forgot_password[0])
		{
			$details = $forgot_password[1];
			Event::fire(new RequestPasswordReset($details));
		}

		return $forgot_password[0];
	}
	
}