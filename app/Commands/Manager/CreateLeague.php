<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;

class CreateLeague extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$create_league = $league -> create($this -> details);
		return $create_league;
	}
	
}