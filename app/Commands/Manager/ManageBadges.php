<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;

class ManageBadges extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{
		$manage_badges = $manager -> manageBadges($this -> details);
	}
	
}