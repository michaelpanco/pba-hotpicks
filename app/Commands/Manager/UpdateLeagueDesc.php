<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;

class UpdateLeagueDesc extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$update_desc = $league -> updateDesc($this -> details);
		return $update_desc;
	}
	
}