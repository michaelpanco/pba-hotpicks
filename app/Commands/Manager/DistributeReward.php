<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Event;
use App\Events\DistributeRewardPrizes;

class DistributeReward extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle()
	{
		Event::fire(new DistributeRewardPrizes($this -> details));
	}

}