<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\MessageRepository;

class SendMessage extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(MessageRepository $message)
	{
		$send_message = $message -> send($this -> details);
		$reply = $send_message -> save();
		return $send_message;
	}
	
}