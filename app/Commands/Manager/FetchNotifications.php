<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use App\Events\NotificationSeen;
use Event;

class FetchNotifications extends Command implements SelfHandling {

	public function handle(ManagerRepository $manager)
	{
		$notifications = $manager -> fetchNotifications();

		Event::fire(new NotificationSeen());

		return $notifications;
	}
	
}