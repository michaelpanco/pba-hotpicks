<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\FranchiseRepository;

class CreateTeam extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(FranchiseRepository $franchise)
	{
		$franchise = $franchise -> build($this -> details);

		if($franchise['result'])
		{
			return ['success' => true];

		}else{
			return ['success' => false, 'message' => 'Error creating team, Please try again.'];
		}
	}
	
}