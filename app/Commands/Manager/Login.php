<?php namespace App\Commands\Manager;

use Auth;
use Event;
use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Events\ManagerSuccessfullyLogin;

class Login extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle()
	{
		if(Auth::attempt(['email' => $this -> details['email'], 'password' => $this -> details['password']], $this -> details['remember']))
		{
			if(Auth::user() -> status == 'ACTV')
			{
				Event::fire(new ManagerSuccessfullyLogin(Auth::user()));
				return ['success' => true, 'message' => '0'];
			}elseif(Auth::user() -> status == 'DSBL'){
				Auth::logout();
				return ['success' => false, 'message' => 'Sorry, Your account has been disabled by our administrator'];
			}else{
				Auth::logout();
				return ['success' => false, 'message' => 'Your account has not been activated yet. Please check your email to activate your account.'];
			}

		}else{
			return ['success' => false, 'message' => 'Email and/or password is incorrect'];
		}
	}

}