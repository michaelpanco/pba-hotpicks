<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\PostRepository;

class FetchReplies extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(PostRepository $post)
	{
		$parent_id =  $this -> details -> input('parent_id');
		$replies = $post -> fetchReplies($parent_id);

		return $replies;
	}
	
}