<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\PostRepository;
use Auth;

class UpVotePost extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(PostRepository $post)
	{
		$params['post_id'] =  $this -> details['post_id'];
		$params['manager_id'] =  Auth::user() -> id;
		$replies = $post -> upVote($params);
		$replies -> save();
	}
	
}