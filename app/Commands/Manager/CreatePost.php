<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\PostRepository;

class CreatePost extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(PostRepository $post)
	{
		$create_post = $post -> create($this -> details);
		$create = $create_post -> save();

		return $create_post;
	}
	
}