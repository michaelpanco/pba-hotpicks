<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use Event;
use Auth;
use App\Events\TriggerDisciplinary;
use App\Events\TriggerNotification;

class RestrainManager extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{
		$restrain_manager = $manager -> restrain($this -> details);

		$details['manager_id'] = $this -> details -> input('restrain_manager_id');
		$details['description'] = $this -> details -> input('restrain_reason') . ' - restrain for ' . $this -> details -> input('no_of_days') . ' day(s)';
		$details['no_of_days'] = $this -> details -> input('no_of_days');
		$this -> disciplinaryLogs($details);

		return $restrain_manager;
	}

	private function disciplinaryLogs($details)
	{
		$action_logs['officer_id'] = Auth::user()->id;
		$action_logs['manager_id'] = $details['manager_id'];
		$action_logs['status'] = "REST";
		$action_logs['description'] = $details['description'];
		$action_logs['created_at'] = date('Y-m-d H:i:s');
		Event::fire(new TriggerDisciplinary($action_logs));

		$resdetails['manager_id'] = $details['manager_id'];
		$resdetails['until'] = date('F j, Y', strtotime("+" . $details['no_of_days'] . " days"));
		$this -> notifyRestrainAccount($resdetails);
	}

	private function notifyRestrainAccount($details)
	{
		$notification_params['manager_id'] = $details['manager_id'];
		$notification_params['type'] = "ACTRES";
		$notification_params['details'] = "Your account has been restrained because you failed to respect our Terms of Use, You can't be able to create a post until " . $details['until'];
		Event::fire(new TriggerNotification($notification_params));
	}
}