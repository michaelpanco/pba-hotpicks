<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;
use App\Events\FranchiseSuccessfullyLeave;
use Event;
use App\Events\TriggerNotification;

class KickTeam extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$leave_league = $league -> kick($this -> details);

		if($leave_league){
			$this -> notifyManager($this -> details);
		}

		return $leave_league;
	}

	public function notifyManager($details)
	{
		$notification['manager_id'] = $details['manager_id'];
		$notification['type'] = "LGKICK";
		$notification['details'] = 'You have been kicked off in ' . $details['league_name'] . ' league';
		Event::fire(new TriggerNotification($notification));
	}

}