<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;
use Event;
use App\Events\MassTriggerNotification;

class DismissLeague extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$this -> notifyManagers($this -> details);
		$dismiss_league = $league -> dismiss($this -> details);
		return $dismiss_league;
	}

	public function notifyManagers($details)
	{
		$notification['managers'] = $details['participants'];
		$notification['type'] = "LGSTRT";
		$notification['details'] = $details['league_name'] . ' has dismissed the league';
		Event::fire(new MassTriggerNotification($notification));
	}
}