<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;
use App\Repositories\LeagueRepository;
use App\Events\UpdateLeague as UpdateLeagueState;
use App\Events\NotifyLeagueUpdate;
use Event;

class UpdateLeague extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league, Request $request)
	{
		$updates = $league -> update($this -> details);

		foreach($updates as $key => $value){
			if($value > 0){
				$details['franchise_id'] = $key;
				$details['fantasy_points'] = $value;
				Event::fire(new NotifyLeagueUpdate($details));
			}
			
		}
		$request -> session() -> put('iUpdateLeague', time());
		Event::fire(new UpdateLeagueState($this -> details));
	}
	
}