<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use Event;
use Auth;
use App\Events\TriggerNotification;

class ApproveRequest extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{	
		$account = $manager -> instance();
		$add_friend = $manager -> approveRequest($this -> details);
		$manager -> approveRequestHandShake($this -> details);
		
		$this -> notifyRequestAcceptance($this -> details['manager_id']);

		return $add_friend;
	}

	public function notifyRequestAcceptance($manager_id)
	{
		$notification_params['manager_id'] = $manager_id;
		$notification_params['type'] = "ACPREQ";
		$notification_params['details'] = Auth::user() -> fullname . ' has accepted your friend request.';
		Event::fire(new TriggerNotification($notification_params));
	}


}