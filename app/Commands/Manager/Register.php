<?php namespace App\Commands\Manager;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;
use App\Repositories\ManagerRepository;
use Event;
use App\Events\ManagerSuccessfullyRegistered;
use Validator;
use Auth;

class Register extends Command implements SelfHandling {

	protected $details;
	protected $team;

	public function __construct($details, $team)
	{
		$this -> details = $details;
		$this -> team = $team;
	}

	public function handle(Request $request, ManagerRepository $manager)
	{

		$provided_recruiter = $request->session()->get('referal', null);

		if(isset($provided_recruiter))
		{
			$manager_recruiter = $manager -> idExists($provided_recruiter);

			if($manager_recruiter){
				$recruiter = $provided_recruiter;
			}else{
				$recruiter = null;
			}

		}else{
			$recruiter = null;
		}

		$create_manager = $manager -> register($this -> details, $recruiter);
		$create = $create_manager -> save();

		if($create)
		{
			Auth::loginUsingId($create_manager -> id);
		}

		
		Event::fire(new ManagerSuccessfullyRegistered($create_manager, $this -> team));

		return $create;
	}

}