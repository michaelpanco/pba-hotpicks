<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\LeagueRepository;
use App\Events\LeagueStarted;
use Event;
use App\Events\MassTriggerNotification;

class StartLeague extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(LeagueRepository $league)
	{
		$start_league = $league -> start($this -> details);

		if($start_league){
			$this -> notifyManagers($this -> details);
			Event::fire(new LeagueStarted($this -> details));
		}

		return $start_league;
	}

	public function notifyManagers($details)
	{
		$notification['managers'] = $details['participants'];
		$notification['type'] = "LGSTRT";
		$notification['details'] = $details['league_name'] . ' league has started';
		Event::fire(new MassTriggerNotification($notification));
	}
}