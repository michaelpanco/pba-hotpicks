<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;
use Auth;
use Event;
use App\Events\TriggerNotification;

class OpenGift extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager)
	{
		$this -> details['manager_id'] = Auth::user()->id;
		$claim_gift = $manager -> openGift($this -> details);

		if($claim_gift)
		{
			$credit['manager_id'] = $this -> details['manager_id'];
			$credit['amount'] = $this -> details['gift'];

			$manager -> creditMoney($credit);

			$this -> notifyOpeningGift($credit);

			return ['success' => true, 'received' => $credit['amount']];
		}
		
		return ['success' => false];
	}

	public function notifyOpeningGift($details)
	{
		$notification_params['manager_id'] = $details['manager_id'];
		$notification_params['type'] = "GIFCLM";
		$notification_params['details'] = "Congratulations, You've got " . number_format($details['amount']) . " credits for today's gift. You can get another gift by logging in everyday. Merry Christmas and Happy New Year";
		Event::fire(new TriggerNotification($notification_params));
	}
}