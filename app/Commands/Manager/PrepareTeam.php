<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\FranchiseRepository;

class PrepareTeam extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(FranchiseRepository $franchise)
	{
		$franchise = $franchise -> prepare($this -> details);

		if($franchise['result'])
		{
			return ['success' => true, 'tag' => $franchise['tag']];

		}else{
			return ['success' => false, 'message' => 'Error creating team, Please try again.'];
		}
	}
	
}