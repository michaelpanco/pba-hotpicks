<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\PostRepository;

class ReplyPost extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(PostRepository $post)
	{
		$reply_post = $post -> reply($this -> details);
		$reply = $reply_post -> save();

		return $reply_post;
	}
	
}