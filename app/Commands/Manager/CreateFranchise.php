<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\FranchiseRepository;

class CreateFranchise extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(FranchiseRepository $franchise)
	{
		$create_franchise = $franchise -> create($this -> details);
		return $create_franchise;
	}
	
}