<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;

use App\Repositories\TeamRepository;
use App\Repositories\PlayerRepository;

class ChangeFavorites extends Command implements SelfHandling {

	protected $details;

	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function handle(ManagerRepository $manager, TeamRepository $team, PlayerRepository $player)
	{
		$request = $this -> details;

		$teams = $team -> lists();
		
		$favorite['team'] = "empty";
		$favorite['player'] = "empty";
		$favorite['team2'] = "empty";
		$favorite['match1'] = "empty";
		$favorite['match2'] = "empty";

		if($request -> has('team')){
			$favorite['team'] = ($request -> input('team') != 999) ? $teams -> where('id', (int)$request -> input('team')) -> first() -> name : "";
		}

		if($request -> has('player')){
			$favorite['player'] = ($request -> input('player') != 999) ? $player -> detailsByID((int)$request -> input('player')) -> fullname : "";
		}

		if($request -> has('team2')){
			$favorite['team2'] = ($request -> input('team2') != 999) ? $teams -> where('id', (int)$request -> input('team2')) -> first() -> name : "";
		}

		if($request -> has('match1')){
			$favorite['match1'] = ($request -> input('match1') != 999) ? $teams -> where('id', (int)$request -> input('match1')) -> first() -> name : "";
		}

		if($request -> has('match2')){
			$favorite['match2'] = ($request -> input('match2') != 999) ? $teams -> where('id', (int)$request -> input('match2')) -> first() -> name : "";
		}

		$change_favorites = $manager -> changeFavorites($favorite);
		return $change_favorites;
	}
}