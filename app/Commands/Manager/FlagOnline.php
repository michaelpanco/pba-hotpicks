<?php namespace App\Commands\Manager;

use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Repositories\ManagerRepository;

class FlagOnline extends Command implements SelfHandling {

	public function handle(ManagerRepository $manager)
	{
		$manager -> FlagOnline();
	}
	
}