<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model{

	protected $table = 'games';
	public $timestamps = false;

	public function team_x(){
		return $this->hasOne('App\Models\Team', 'id', 'team_x_id');
	}

	public function team_y(){
		return $this->hasOne('App\Models\Team', 'id', 'team_y_id');
	}

	public function team_winner(){
		return $this->hasOne('App\Models\Team', 'id', 'winner');
	}

    public function performances()
    {
        return $this -> hasMany('App\Models\Performances');
    }
}