<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpressBetEntries extends Model{

	protected $table = 'express_bet_entries';
	public $timestamps = false;

	public function details()
	{
		return $this -> belongsTo('App\Models\ExpressBet', 'express_bet_id');
	}

	public function manager()
	{
		return $this -> belongsTo('App\Models\Manager', 'manager_id');
	}
}