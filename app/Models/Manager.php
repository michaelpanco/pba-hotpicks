<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Server;

class Manager extends Model implements AuthenticatableContract{

	use Authenticatable;
    use SyncableGraphNodeTrait;

	protected $table = 'managers';
	public $timestamps = false;

	protected $fillable = [
		'firstname', 
		'lastname', 
		'gender',
		'birthday',
		'email',
        'avatar', 
		'password'
	];

	protected $hidden = ['password', 'remember_token'];
    protected static $graph_node_date_time_to_string_format = 'Y-m-d';

    protected static $graph_node_field_aliases = [
        'id' => 'slug',
        'first_name' => 'firstname',
        'last_name' => 'lastname',
        'gender' => 'gender',
        'email' => 'email',
        'birthday' => 'birthday',
        'avatar' => 'avatar',
        'ip_address' => 'ip_address',
        'user_agent' => 'user_agent',
        'reg_type' => 'reg_type',
        'credits' => 'credits',
    ];


	public function getFullnameAttribute()
	{
		return $this -> firstname . ' ' . $this -> lastname;
	}

    public function franchise(){
        return $this->hasOne('App\Models\Franchise');
    }

	public function confirmation(){
		return $this->hasOne('App\Models\EmailConfirmation');
	}

    public function posts()
    {
        return $this -> hasMany('App\Models\Post');
    }

    public function badges($status='ALL')
    {
    	if($status == 'ENBL'){
    		return $this -> belongsToMany('App\Models\Badge', 'managers_badges', 'manager_id', 'badge_id') -> wherePivot('status', '=', 'ENBL');
    	}
        return $this -> belongsToMany('App\Models\Badge', 'managers_badges', 'manager_id', 'badge_id') -> withPivot('status');
    }

    public function requests()
    {
    	return $this -> belongsToMany('App\Models\Manager', 'friendships', 'friend_id', 'manager_id') -> wherePivot('status', '=', 'PNDG');
    }

    public function newMessages()
    {
        return $this -> hasMany('App\Models\Message', 'recipient', 'id') -> where('read', 0) -> groupBy('conversation_id');
    }

    public function friends()
    {
    	return $this -> belongsToMany('App\Models\Manager', 'friendships', 'manager_id', 'friend_id');
    }

    public function approveFriends()
    {
    	return $this -> belongsToMany('App\Models\Manager', 'friendships', 'friend_id', 'manager_id') -> wherePivot('status', '=', 'ACPT') -> orderBy('last_online', 'desc');
    }

    public function onlineFriends()
    {
        $date = new \DateTime;
        $date->modify('-2 minutes');
        $less_datetime = $date->format('Y-m-d H:i:s');
        return $this -> belongsToMany('App\Models\Manager', 'friendships', 'friend_id', 'manager_id')-> wherePivot('status', '=', 'ACPT') -> where('last_online', '>=', $less_datetime);
    }

    public function notifications($type="all")
    {	
    	switch ($type) {

    		case 'new':
    			return $this -> hasMany('App\Models\Notification') -> where('seen', '=', 0);
    		break;

    		case 'seen':
    			return $this -> hasMany('App\Models\Notification') -> where('seen', '=', 1);
    		break;

    		default:
    			return $this -> hasMany('App\Models\Notification');
    		break;
    	}
    }

    public function referals()
    {   
        return $this -> hasMany('App\Models\Manager', 'recruiter') -> where('status', '=', 'ACTV');
    }

	public static function boot()
	{
	    parent::boot();

	    static::creating(function($model){
	    	//$model -> avatar = '/assets/img/default_avatar.jpg';
	        $model -> date_registered = date('Y-m-d H:i:s');
	    });
	}
    
}
