<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Post extends Model{

	protected $table = 'posts';
	public $timestamps = true;

	protected $fillable = [
		'channel_id', 
		'content',
		'parent_post',
		'status',
		'points'
	];

    public function manager()
    {
        return $this -> belongsTo('App\Models\Manager');
    }

    public function replies()
    {
        return $this -> hasMany('App\Models\Post', 'parent_post', 'id') -> where('status', 'ACTV');
    }

    public function parent()
    {
        return $this -> belongsTo('App\Models\Post', 'id', 'parent_post');
    }

    public function points()
    {
        return $this -> hasMany('App\Models\PostPoint', 'post_id', 'id');
    }
    /*
	public function getPostpointsAttribute()
	{
		if($this -> points == 1){
			return $this -> points;
		}else if($this -> points > 1){
			return $this -> points;
		}else{
			return 0;
		}
	}
	*/
}
