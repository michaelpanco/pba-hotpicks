<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplinary extends Model{

	protected $table = 'disciplinaries';
	public $timestamps = false;

	public function officer()
	{
		return $this -> belongsTo('App\Models\Manager');
	}

	public function manager()
	{
		return $this -> belongsTo('App\Models\Manager');
	}
}