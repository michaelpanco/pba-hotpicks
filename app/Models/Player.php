<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model{

	protected $table = 'players';
	public $timestamps = false;

	public function getFullnameAttribute()
	{
		return $this -> firstname . ' ' . $this -> lastname;
	}

	public function team(){
		return $this -> hasOne('App\Models\Team', 'id', 'team_id');
	}

    public function performance()
    {
        return $this -> hasMany('App\Models\Performance');
    }

}