<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Franchise extends Model{

	protected $table = 'franchises';
	public $timestamps = false;

    public function players()
    {
    	return $this -> belongsToMany('App\Models\Player', 'franchises_players', 'franchise_id', 'player_id')-> withPivot('position');;
    }

	public function icon()
	{
		return $this -> hasOne('App\Models\FranchiseLogo', 'id', 'logo_id');
	}

	public function manager()
	{
		return $this->belongsTo('App\Models\Manager');
	}

	public function league()
	{
		return $this->belongsTo('App\Models\League');
	}

	public function statistic()
	{
		return $this -> hasMany('App\Models\FranchisePlayer', 'franchise_id', 'id');
	}

}