<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model{

	protected $table = 'notifications';
	public $timestamps = false;

	public static function boot()
	{
	    parent::boot();

	    static::creating(function($model){
	        $model -> created_at = date('Y-m-d H:i:s');
	    });
	}
}