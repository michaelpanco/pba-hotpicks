<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model{

	protected $table = 'badges';
	public $timestamps = false;

	protected $fillable = [
		'id', 
		'name',
		'src'
	];
	
    public function manager()
    {
        return $this -> belongsTo('App\Models\Manager');
    }
}