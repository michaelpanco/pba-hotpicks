<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchisePlayer extends Model{

	protected $table = 'franchises_players';
	public $timestamps = false;

}