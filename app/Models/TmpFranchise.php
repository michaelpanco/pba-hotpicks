<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmpFranchise extends Model{

	protected $table = 'tmp_franchises';
	public $timestamps = false;

    public function players()
    {
    	return $this -> belongsToMany('App\Models\Player', 'tmp_franchises_players', 'tmp_franchise_id', 'player_id');
    }

	public function icon()
	{
		return $this -> hasOne('App\Models\FranchiseLogo', 'id', 'logo_id');
	}
}