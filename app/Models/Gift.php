<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model{

	protected $table = 'christmas_gifts';
	public $timestamps = false;

}