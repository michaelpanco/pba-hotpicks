<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model{

	protected $table = 'performances';
	public $timestamps = false;

	public function player()
	{
		return $this->hasOne('App\Models\Player', 'id', 'player_id');
	}

	public function adversary()
	{
		return $this->hasOne('App\Models\Team', 'id', 'opponent');
	}

}