<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpressBet extends Model{

	protected $table = 'express_bet';
	public $timestamps = false;

    public function entries()
    {
        return $this -> hasMany('App\Models\ExpressBetEntries');
    }

    public function bets()
    {
        return $this -> hasMany('App\Models\ExpressBetEntries');
    }
}