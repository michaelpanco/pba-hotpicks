<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagerBadge extends Model{

	protected $table = 'managers_badges';
	public $timestamps = false;
}