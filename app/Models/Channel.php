<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model{

	protected $table = 'channels';
	public $timestamps = false;

	protected $fillable = [
		'id', 
		'name',
		'type',
		'status'
	];

    public function posts()
    {
    	return $this -> hasMany('App\Models\Post', 'channel_id', 'id');
    }
}