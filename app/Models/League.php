<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class League extends Model{

	protected $table = 'leagues';
	public $timestamps = false;

    public function participants()
    {
    	return $this -> belongsToMany('App\Models\Franchise', 'leagues_franchises', 'league_id', 'franchise_id');
    }

	public function creator()
	{
		return $this -> belongsTo('App\Models\Manager', 'host');
	}

    public function channel()
    {
        return $this->hasOne('App\Models\Channel', 'id', 'channel_id');
    }


}