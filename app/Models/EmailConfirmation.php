<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Server;

class EmailConfirmation extends Model {

	protected $table = 'email_confirmations';
	public $timestamps = true;

	protected $fillable = [
		'manager_id', 
		'confirmation_code'
	];
}
