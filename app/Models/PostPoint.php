<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostPoint extends Model{

	protected $table = 'posts_points';
	public $timestamps = false;

    public function post()
    {
        return $this -> belongsTo('App\Models\Post', 'post_id', 'id');
    }
}
