<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchiseLogo extends Model{

	protected $table = 'franchise_logos';
	public $timestamps = false;
}