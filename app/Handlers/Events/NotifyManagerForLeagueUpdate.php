<?php namespace App\Handlers\Events;

use App\Events\NotifyLeagueUpdate;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\ManagerRepository;
use App\Repositories\FranchiseRepository;

class NotifyManagerForLeagueUpdate implements ShouldBeQueued{

	protected $manager;
	protected $franchise;

	public function __construct(ManagerRepository $manager, FranchiseRepository $franchise)
	{
		$this -> manager = $manager;
		$this -> franchise = $franchise;
	}

	public function handle(NotifyLeagueUpdate $leagueUpdate)
	{
		$details = $leagueUpdate -> getDetails();

		$notice['manager_id'] = $this -> franchise -> owner($details['franchise_id']);
		$notice['type'] = 'LEAGUP';
		$notice['details'] = 'You earned ' . number_format($details['fantasy_points']) . ' credits from the league update';

		$credit['manager_id'] = $notice['manager_id'];
		$credit['amount'] = $details['fantasy_points'];

		$this -> manager -> creditMoney($credit);
		
		$this -> manager -> notify($notice);
	}
}
