<?php namespace App\Handlers\Events;

use App\Events\MassTriggerNotification;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\ManagerRepository;

class MassNotification {

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function handle(MassTriggerNotification $massTriggerNotification)
	{
		$details = $massTriggerNotification -> getDetails();
		$this -> manager -> broadcast($details);
	}
}
