<?php namespace App\Handlers\Events;

use App\Events\ManagerSuccessfullyLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Session;

class InitializeManagerSession {

	protected $email_confirmation;

	public function __construct()
	{

	}

	public function handle(ManagerSuccessfullyLogin $authenticated_manager)
	{
		/* 
		$enabled_badges = $this -> enabledBadges($authenticated_manager -> getManagerDetails() -> badges);
		Session::put('enabled_badges', $enabled_badges);
		*/
	}

	private function enabledBadges($manager_badges){

		$enabled_badges = array();

		foreach ($manager_badges as $badge)
		{
			if($badge -> status == 'ENBL')
			{
				$enabled_badges = array_add($enabled_badges, $badge -> info -> name, $badge -> info -> src);
			}
		}

		return $enabled_badges;
	}
}
