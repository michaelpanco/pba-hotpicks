<?php namespace App\Handlers\Events;

use App\Events\RequestPasswordReset;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\EmailConfirmationRepository;
use Mail;

class PasswordResetConfirmation implements ShouldBeQueued{

	protected $email_confirmation;

	public function __construct(EmailConfirmationRepository $email_confirmation_repository)
	{
		$this -> email_confirmation = $email_confirmation_repository;
	}

	public function handle(RequestPasswordReset $request_password_reset)
	{
		$details = $request_password_reset -> getDetails();

		$mail_data['email'] = $details['email'];
		$mail_data['token'] = $details['token'];

		Mail::send('emails.password_reset', $mail_data, function($message) use($mail_data){
			$message -> from('contact@pinoytenpicks.com', 'PinoyTenPicks');
		    $message -> to($mail_data['email'], 'asdsa') -> subject('Password Reset');
		});
	}
}
