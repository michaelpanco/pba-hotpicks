<?php namespace App\Handlers\Events;

use App\Events\FranchiseSuccessfullyJoin;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Commands\Manager\CreatePost as CreatePostCommand;
use App\Repositories\LeagueRepository;

use Bus;

class PostLeagueChannelNotice {

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function handle(FranchiseSuccessfullyJoin $franchise)
	{
		$details = $franchise -> getFranchiseDetails();

		$league = $this -> league -> details($details['league_id']);

		$post['channel_id'] = $league -> channel_id;
		$post['content'] = config('app.content_secret') . '-join';

	    Bus::dispatch(
	        New CreatePostCommand($post)
	    );
	}
}
