<?php namespace App\Handlers\Events;

use App\Events\UpdateLeague;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\LeagueRepository;
use App\Repositories\ManagerRepository;

class IdentifyLeagueTopTeam implements ShouldBeQueued{

	protected $league;

	public function __construct(LeagueRepository $league, ManagerRepository $manager)
	{
		$this -> league = $league;
		$this -> manager = $manager;
	}

	public function handle(UpdateLeague $updateLeague)
	{
		$participants = array();
		$league_id = $updateLeague -> leagueID();

		$league_details = $this -> league -> details($league_id);

		$franchises = $league_details -> participants;

		foreach ($franchises as $franchise) {
			array_push($participants, $franchise -> manager_id);
		}

		$this -> manager -> resetLeagueLeader($participants);

		$i = 1;

		foreach($league_details -> participants as $participant)
		{
			$league_participants[$i]['manager_id'] = $participant -> manager -> id;
			$league_participants[$i]['franchise_fantasy_points'] = $participant -> statistic -> sum('fantasy_points');
			$i++;
		}
		
		$franchise_rank = collect($league_participants) -> sortByDesc('franchise_fantasy_points');

		$top_manager = $franchise_rank -> first()['manager_id'];

		if($franchise_rank -> first()['franchise_fantasy_points'] > 0)
		{
			$this -> manager -> setLeagueLeader($top_manager);
		}
	}
}
