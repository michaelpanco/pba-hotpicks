<?php namespace App\Handlers\Events;

use App\Events\TriggerNotification;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\ManagerRepository;

class NotifyManager {

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function handle(TriggerNotification $triggerNotification)
	{
		$details = $triggerNotification -> getDetails();
		$this -> manager -> notify($details);
	}
}
