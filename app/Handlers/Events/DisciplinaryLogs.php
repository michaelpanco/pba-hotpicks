<?php namespace App\Handlers\Events;

use App\Events\TriggerDisciplinary;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\DisciplinaryRepository;

class DisciplinaryLogs {

	protected $disciplinary;

	public function __construct(DisciplinaryRepository $disciplinary)
	{
		$this -> disciplinary = $disciplinary;
	}

	public function handle(TriggerDisciplinary $triggerDisciplinary)
	{
		$details = $triggerDisciplinary -> getDetails();
		$this -> disciplinary -> create($details);
	}
}
