<?php namespace App\Handlers\Events;

use App\Events\ManagerSuccessfullyRegistered;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\EmailConfirmationRepository;
use Mail;

class EmailRegistrationConfirmation implements ShouldBeQueued{

	protected $email_confirmation;

	public function __construct(EmailConfirmationRepository $email_confirmation_repository)
	{
		$this -> email_confirmation = $email_confirmation_repository;
	}

	public function handle(ManagerSuccessfullyRegistered $registered_manager)
	{
		$manager = $registered_manager -> getManagerDetails();
		$team = $registered_manager -> getManagerTeam();
		
		$email_confirmation_details['manager_id'] = $manager -> id;
		$email_confirmation_details['confirmation_code'] = str_random(32);

		$this -> email_confirmation -> create($email_confirmation_details);

		$mail_data['fullname'] = $manager -> firstname . ' ' . $manager -> lastname;
		$mail_data['manager_id'] = $manager -> id;
		$mail_data['confirmation_code'] = $email_confirmation_details['confirmation_code'];


		$mail_data['team'] = ($team != null) ? '?team=' . $team : '';

		Mail::send('emails.registration', $mail_data, function($message) use($manager){
			$message -> from('contact@pinoytenpicks.com', 'PinoyTenpicks');
		    $message -> to($manager -> email, $manager -> firstname . ' ' . $manager -> lastname)->subject('Registration Confirmation');
		});
	}
}
