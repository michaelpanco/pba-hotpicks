<?php namespace App\Handlers\Events;

use App\Events\DistributeRewardPrizes;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\ManagerRepository;

class NotifyLeagueWinners implements ShouldBeQueued{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function handle(DistributeRewardPrizes $distributeRewardPrizes)
	{
		/*
			Collection {#641
			  #items: array:3 [
			    0 => array:2 [
			      "manager_id" => 5
			      "fantasy_points" => 675
			    ]
			    1 => array:2 [
			      "manager_id" => 10
			      "fantasy_points" => 557
			    ]
			    2 => array:2 [
			      "manager_id" => 8
			      "fantasy_points" => 468
			    ]
			  ]
			}
		*/

		$winners = $distributeRewardPrizes -> getDetails();

		foreach ($winners as $key => $value) {

			switch ($key) {

				case 0:
					$manager_id = $value['manager_id'];
					$message = "Congratulations, Your franchise won the championship in your league, you will recieve Championship badge and a credit worth 25,000. Thank you and have a great day.";
					$prize_credit = 25000;
					$this -> manager -> giveChampionshipBadge($manager_id);
				break;

				case 1:
					$manager_id = $value['manager_id'];
					$message = "Congratulations, Your franchise rank 2nd place in your league, you will recieve a credit worth 15,000. Thank you and have a great day.";
					$prize_credit = 15000;
				break;

				case 2:
					$manager_id = $value['manager_id'];
					$message = "Congratulations, Your franchise rank 3rd place in your league, you will recieve a credit worth 10,000. Thank you and have a great day.";
					$prize_credit = 10000;
				break;
			}

			$notice['manager_id'] = $manager_id;
			$notice['type'] = 'PRIZES';
			$notice['details'] = $message;

			$credit['manager_id'] = $manager_id;
			$credit['amount'] = $prize_credit;

			$this -> manager -> creditMoney($credit);
			
			$this -> manager -> notify($notice);

		}


	}
}
