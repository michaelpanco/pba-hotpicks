<?php namespace App\Handlers\Events;

use App\Events\NotificationSeen;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Repositories\ManagerRepository;

class SetNotificationSeen {

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function handle(NotificationSeen $notificationSeen)
	{
		$manager_id = $notificationSeen -> member();
		$this -> manager -> setNotificationSeen($manager_id);
	}
}
