<?php namespace App\Handlers\Events;

use App\Events\LeagueStarted;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\Commands\Manager\CreatePost as CreatePostCommand;
use App\Repositories\LeagueRepository;

use Bus;

class LeagueStartedChannelNotice {

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function handle(LeagueStarted $league)
	{
		$details = $league -> getLeagueDetails();

		$league = $this -> league -> details($details['league_id']);

		$post['channel_id'] = $league -> channel_id;
		$post['content'] = config('app.content_secret') . '-start';
		
	    Bus::dispatch(
	        New CreatePostCommand($post)
	    );
	}
}
