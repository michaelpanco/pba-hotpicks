<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'App\Events\ManagerSuccessfullyRegistered' => [
			'App\Handlers\Events\EmailRegistrationConfirmation',
		],
		'App\Events\ManagerSuccessfullyLogin' => [
			'App\Handlers\Events\InitializeManagerSession',
		],
		'App\Events\TriggerNotification' => [
			'App\Handlers\Events\NotifyManager',
		],
		'App\Events\MassTriggerNotification' => [
			'App\Handlers\Events\MassNotification',
		],
		'App\Events\NotificationSeen' => [
			'App\Handlers\Events\SetNotificationSeen',
		],
		'App\Events\FranchiseSuccessfullyJoin' => [
			'App\Handlers\Events\PostLeagueChannelNotice',
		],
		'App\Events\FranchiseSuccessfullyLeave' => [
			'App\Handlers\Events\PostLeaveLeagueChannelNotice',
		],
		'App\Events\LeagueStarted' => [
			'App\Handlers\Events\LeagueStartedChannelNotice',
		],
		'App\Events\UpdateLeague' => [
			'App\Handlers\Events\IdentifyLeagueTopTeam',
		],
		'App\Events\NotifyLeagueUpdate' => [
			'App\Handlers\Events\NotifyManagerForLeagueUpdate',
		],
		'App\Events\RequestPasswordReset' => [
			'App\Handlers\Events\PasswordResetConfirmation',
		],
		'App\Events\TriggerDisciplinary' => [
			'App\Handlers\Events\DisciplinaryLogs',
		],
		'App\Events\DistributeRewardPrizes' => [
			'App\Handlers\Events\NotifyLeagueWinners',
		],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
