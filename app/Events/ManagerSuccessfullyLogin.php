<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class ManagerSuccessfullyLogin extends Event {

	use SerializesModels;

	protected $manager_details;
	
	public function __construct($manager_details)
	{
		$this -> manager_details = $manager_details;
	}

	public function getManagerDetails(){
		return $this -> manager_details;
	}

}
