<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class ManagerSuccessfullyRegistered extends Event {

	use SerializesModels;

	protected $manager_details;
	protected $manager_team;
	
	public function __construct($manager_details, $manager_team)
	{
		$this -> manager_details = $manager_details;
		$this -> manager_team = $manager_team;
	}

	public function getManagerDetails(){
		return $this -> manager_details;
	}

	public function getManagerTeam(){
		return $this -> manager_team;
	}
}