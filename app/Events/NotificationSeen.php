<?php namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Auth;

class NotificationSeen extends Event {

	use SerializesModels;

	protected $member;
	
	public function __construct()
	{
		$this -> member = Auth::user() -> id;
	}

	public function member(){
		return $this -> member;
	}

}
