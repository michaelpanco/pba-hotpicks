<?php namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class UpdateLeague extends Event {

	use SerializesModels;

	protected $details;
	
	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function leagueID(){
		return $this -> details['league_id'];
	}

}
