<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class FranchiseSuccessfullyJoin extends Event {

	use SerializesModels;

	protected $details;
	
	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function getFranchiseDetails(){
		return $this -> details;
	}

}
