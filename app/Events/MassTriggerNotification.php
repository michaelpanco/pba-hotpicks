<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class MassTriggerNotification extends Event {

	use SerializesModels;

	protected $details;
	
	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function getDetails(){
		return $this -> details;
	}

}
