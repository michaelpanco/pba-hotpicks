<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class AdminSuccessfullyLogin extends Event {

	use SerializesModels;

	protected $admin_details;
	
	public function __construct($admin_details)
	{
		$this -> admin_details = $admin_details;
	}

	public function getManagerDetails(){
		return $this -> admin_details;
	}

}
