<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class LeagueStarted extends Event {

	use SerializesModels;

	protected $details;
	
	public function __construct($details)
	{
		$this -> details = $details;
	}

	public function getLeagueDetails(){
		return $this -> details;
	}

}
