<?php namespace App\Repositories;

use App\Models\Performance;
use DB;
use Setting;

class PerformanceRepository extends BaseRepository{

	protected $model;

	public function __construct(Performance $performance)
	{
		$this -> model = $performance;
		$this -> sequence = Setting::get('SEQUENCE');
	}

	public function lists($game_id)
	{
		$result = $this -> model -> where('game_id', $game_id) -> where('sequence', $this -> sequence) -> with('player') -> get();
		return $result;
	}

	public function create($details)
	{
		$performance = new Performance;
		$performance -> game_id = $details['game_id'];
		$performance -> player_id = $details['player_id'];
		$performance -> team_id = $details['team_id'];
		$performance -> opponent = $details['opponent'];
		$performance -> points = $details['points'];
		$performance -> assists = $details['assists'];
		$performance -> rebounds = $details['rebounds'];
		$performance -> steals = $details['steals'];
		$performance -> blocks = $details['blocks'];
		$performance -> turnovers = $details['turnovers'];
		$performance -> win_game = $details['win_game'];
		$performance -> sequence = $this -> sequence;
		$performance -> fantasy_points = $details['fantasy_points'];

		$performance_save = $performance -> save();
		return $performance_save;
	}

	public function publish($game_id)
	{
		DB::table('performances') -> where('game_id', $game_id) -> update(['ready' => 1]);
        return true;
	}

	public function result($game_id)
	{
		$result = $this -> model -> where('game_id', $game_id) -> with('player') -> get();
		return $result;
	}

	public function featured($game_ids)
	{
		$result = $this -> model -> whereIn('game_id', $game_ids) -> where('sequence', $this -> sequence) -> with('player', 'adversary') -> get();
		return $result;
	}

	public function playerPerformance($player_id)
	{
		$performances = $this -> model -> where('player_id', $player_id) -> where('sequence', $this -> sequence) -> with('adversary') -> get();
		return $performances;
	}

	public function playerPercentage($player_id)
	{
		$percentage = $this -> model -> where('sequence', $this -> sequence) -> where('player_id', $player_id) -> groupBy('player_id') -> select('player_id', DB::raw('AVG(points) as avg_points'), DB::raw('AVG(assists) as avg_assists'), DB::raw('AVG(rebounds) as avg_rebounds'), DB::raw('AVG(steals) as avg_steals'), DB::raw('AVG(blocks) as avg_blocks'), DB::raw('SUM(fantasy_points) as total_fantasy_points')) -> first();
		return $percentage;
	}

	public function teamPerformance($players)
	{
		$player_ids = [];

		$i=0;
		foreach($players as $player){
			$player_ids[$i] = $player -> id;
			$i++;
		}

		$performances = $this -> model -> where('sequence', $this -> sequence) -> whereIn('player_id', $player_ids) -> groupBy('player_id') -> select('player_id', DB::raw('AVG(points) as avg_points'), DB::raw('AVG(assists) as avg_assists'), DB::raw('AVG(rebounds) as avg_rebounds'), DB::raw('AVG(steals) as avg_steals'), DB::raw('AVG(blocks) as avg_blocks')) -> with('player') -> get();
		return $performances;
	}

}