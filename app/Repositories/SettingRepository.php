<?php namespace App\Repositories;

use App\Models\Setting;

class SettingRepository extends BaseRepository{

	protected $model;

	public function __construct(Setting $setting)
	{
		$this -> model = $setting;
	}

	public function setup()
	{
		$settings = $this -> model -> get();

		foreach($settings as $setting)
		{
			$setup[$setting -> field] = $setting -> value;
		}

		return $setup;
	}

	public function update($fields)
	{
		foreach($fields as $field => $value)
		{
			$update = $this -> model -> where('field', $field) -> first();
			$update -> value = $value;
			$update_details = $update -> save();
		}

		return true;
	}

}