<?php namespace App\Repositories;

use App\Models\Channel;

class ChannelRepository extends BaseRepository{

	protected $model;

	public function __construct(Channel $channel)
	{
		$this -> model = $channel;
	}

	public function create($name, $type)
	{
		$league = new Channel;
		$league -> name = $name;
		$league -> type = $type;
		$league -> status = 'ACTV';
		$save = $league -> save();

		if($save){
			return $league -> id;
		}

		return false;
	}

	public function delete($id)
	{
		$channel = $this -> model -> where('id', $id) -> first();
		$channel -> delete();
	}

	public function teams()
	{
		$team_channels = $this -> model -> where('type', '=', 'TEAM') -> where('status', '=', 'ACTV') -> get();
		return $team_channels;
	}

}