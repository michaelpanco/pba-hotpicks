<?php namespace App\Repositories;

use App\Models\League;
use App\Models\Franchise;
use App\Repositories\ChannelRepository;
use App\Repositories\FranchiseRepository;
use App\Repositories\ManagerRepository;
use App\Commands\Manager\DistributeReward;
use Setting;
use Bus;

class LeagueRepository extends BaseRepository{

	protected $model;
	protected $franchise;
	protected $franchise_repository;
	protected $channel;
	protected $manager;

	public function __construct(League $league, Franchise $franchise, FranchiseRepository $franchise_repository, ChannelRepository $channel, ManagerRepository $manager)
	{
		$this -> model = $league;
		$this -> franchise = $franchise;
		$this -> franchise_repository = $franchise_repository;
		$this -> channel = $channel;
		$this -> manager = $manager;
	}

	public function create($details)
	{
		$channel_id = $this -> channel -> create($details['name'], 'LGUE');

        $league = new League;
        $league -> name = $details['name'];
        $league -> host = $details['host'];
        $league -> desc = $details['description'];
        $league -> channel_id = $channel_id;
        $league -> password = $details['password'];
        $league -> sequence = Setting::get('SEQUENCE');
        $league -> ready = 0;
        $league -> created_at = date('Y-m-d');
        $league_save = $league -> save();

        if($league_save){
	        $league -> participants() -> attach($details['host_franchise']);
	        $this -> franchise_repository -> updateLeague($details['host_franchise'], $league -> id);
    	}

        return array('result' => $league_save, 'id' => $league -> id);
	}

	public function lists($request, $limit=24)
	{
		$leagues = $this -> model;

		if ($request -> has('search')) {
		    $leagues = $leagues -> where('name', 'like', '%' . $request -> input('search') . '%');
		}

		$leagues = $leagues -> with('participants','creator') -> orderBy('id', 'desc') -> paginate($limit);
		
		if ($request -> has('search')) {
		    $leagues -> appends(['search' => $request -> input('search')]);
		}

		$leagues -> setPath('leagues');
		return $leagues;
	}

	public function details($league_id)
	{
		$league = $this -> model -> where('id', $league_id) -> with('participants','creator', 'channel') -> first();
		return $league;
	}

	public function isOpen($league_id)
	{
		$league = $this -> model -> where('id', $league_id) -> first();
		
		if($league -> ready){
			return false;
		}
		return true;
	}

	public function join($manager)
	{
		$manager_id = $manager['id'];
		$franchise_id = $manager['franchise_id'];
		$league_id = $manager['league_id'];

		$league = $this -> model -> where('id', $league_id) -> first();

		$count_before = $league -> participants() -> count();
		$league -> participants() -> attach($franchise_id);
		$count_after = $league -> participants() -> count();

		$this -> manager -> demoteLeageLeader($manager_id);

		if($count_after > $count_before)
		{
			$update_franchise_league = $this -> franchise_repository -> updateLeague($franchise_id, $league_id);

			if($update_franchise_league){
				return true;
			}
		}
		return false;
	}

	public function leave($manager)
	{
		$manager_id = $manager['id'];
		$franchise_id = $manager['franchise_id'];
		$league_id = $manager['league_id'];

		$league = $this -> model -> where('id', $league_id) -> first();

		$count_before = $league -> participants() -> count();
		$league -> participants() -> detach($franchise_id);
		$count_after = $league -> participants() -> count();

		$this -> manager -> demoteLeageLeader($manager_id);

		if($count_after < $count_before)
		{
			$update_franchise_league = $this -> franchise_repository -> updateLeague($franchise_id, null);

			if($update_franchise_league){
				return true;
			}
		}
		return false;
	}

	public function kick($details)
	{
		$franchise_id = $details['franchise_id'];
		$league_id = $details['league_id'];

		$league = $this -> model -> where('id', $league_id) -> first();

		$count_before = $league -> participants() -> count();
		$league -> participants() -> detach($franchise_id);
		$count_after = $league -> participants() -> count();

		if($count_after < $count_before)
		{
			$update_franchise_league = $this -> franchise_repository -> updateLeague($franchise_id, null);

			if($update_franchise_league){
				return true;
			}
		}
		return false;
	}

	public function start($details)
	{
		$league_id = $details['league_id'];

		$league = $this -> model -> where('id', $league_id) -> first();

		$participants_collection = $league -> participants;
		$participants = $participants_collection -> lists('id') -> toArray();

		$league -> ready = 1;
		$league -> sequence = Setting::get('SEQUENCE');

		$this -> franchise_repository -> reboot_franchises($participants);

		return $league -> save();
	}

	public function dismiss($manager)
	{
		$manager_id = $manager['id'];
		$franchise_id = $manager['franchise_id'];
		$league_id = $manager['league_id'];

		$league = $this -> model -> where('id', $league_id) -> first();

		$league_channel_id = $league -> channel_id;

		$league -> participants() -> detach();

		if($league -> participants() -> count() == 0)
		{
			$league -> delete();
			$this -> channel -> delete($league_channel_id);
			$this -> franchise -> where('league_id', $league_id) -> update(['league_id' => null]);
			return true;
		}
		return false;
	}

	public function update($manager)
	{
		$manager_id = $manager['id'];
		$league_id = $manager['league_id'];

		// get the params [manager_id, league_id]

		$league = $this -> model -> where('id', $league_id) -> first();

		// get league details

		$league_franchises = $this -> franchise -> select('id') -> where('league_id', $league_id) -> get();

		// create empty franchises array for league

		$franchises = array();

		// get all franchise of the league and put it in empty array

		foreach($league_franchises as $franchise){
			$franchises[] = $franchise -> id;
		}

		// feed all league franchises

		$feed = $this -> franchise_repository -> feed($franchises);
		
		if(Setting::get('DISTRIBUTE_REWARD') == 'YES')
		{
			$league_participants = $league -> participants;
			$i = 1;

			foreach($league_participants as $participant)
			{
				$participants[$i]['manager_id'] = $participant -> manager -> id;
				$participants[$i]['fantasy_points'] = $participant -> statistic -> sum('fantasy_points');
				$i++;
			}
			$winners = collect($participants) -> sortByDesc('fantasy_points') -> take(3);

		    Bus::dispatch(
		        New DistributeReward($winners)
		    );

		    $league -> ready = false;
		    $league -> save();

		}

		return $feed;
	}

	public function updateDesc($details)
	{
		$league_id = $details['league_id'];
		$desc = $details['league_desc'];

		$league = $this -> model -> where('id', $league_id) -> first();
		$league -> desc = $desc;
		return $league -> save();
	}

	public function updatePassword($details)
	{
		$league_id = $details['league_id'];
		$password = $details['league_password'];

		$league = $this -> model -> where('id', $league_id) -> first();
		$league -> password = $password;
		return $league -> save();
	}

	public function updateDetails($details)
	{
		$update = $this -> model -> where('id', (int)$details['id']) -> first();
		$update -> name = $details['name'];
		$update -> desc = $details['desc'];
		$update -> host = $details['host'];
		$update -> sequence = $details['sequence'];
		$update -> ready = $details['status'];
		$update -> created_at = $details['date_created'];
		$update_details = $update -> save();
		return $update_details;
	}

	public function host($league_id)
	{
		$league = $this -> model -> where('id', $league_id) -> first();
		return $league -> host;
	}

	public function newLeagues()
	{
		$leagues = $this -> model -> where('created_at', '>=', new \DateTime('today')) -> with('participants','creator') -> get();
		return $leagues;
	}
	
	public function total()
	{
		$members = $this -> model -> get();
		return $members -> count();
	}

}