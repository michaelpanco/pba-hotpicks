<?php namespace App\Repositories;

use App\Models\Announcement;

class AnnouncementRepository extends BaseRepository{

	protected $model;

	public function __construct(Announcement $announcement)
	{
		$this -> model = $announcement;
	}

	public function lists()
	{
		return $this -> model -> orderBy('order', 'asc') -> get();
	}

	public function create($details)
	{
		$announcement = new Announcement;
		$announcement -> text = $details['text'];
		$announcement -> order = $details['order'];

		$announcement_save = $announcement -> save();
		return array('status' => $announcement_save);
	}

	public function details($announcement_id)
	{
		$details = $this -> model -> where('id', $announcement_id) -> first();
		return $details;
	}

	public function update($details)
	{
		$update = $this -> model -> where('id', (int)$details['id']) -> first();
		$update -> text = $details['text'];
		$update -> order = $details['order'];
		$update_details = $update -> save();
		return $update_details;
	}

	public function delete($details)
	{
		$announcement = $this -> model -> where('id', (int)$details['id']) -> first();
		$delete_announcement = $announcement -> delete();
		return $delete_announcement;
	}

}