<?php namespace App\Repositories;

use App\Models\EmailConfirmation;
use App\Models\Manager;

class EmailConfirmationRepository extends BaseRepository{

	protected $email_confirmation_model;
	protected $manager_model;

	public function __construct(EmailConfirmation $email, Manager $manager)
	{
		$this -> email_confirmation_model = $email;
		$this -> manager_model = $manager;
	}

	public function create($params)
	{

		foreach($params as $key => $value)
		{
		    $this -> email_confirmation_model -> $key = $value;
		}

		return $this -> email_confirmation_model -> save();
	}

	public function exist($request)
	{
		return $this -> email_confirmation_model -> where('manager_id', '=', $request['manager_id']) -> where('confirmation_code', '=', $request['confirmation_code']) -> exists();
	}

	public function confirm($manager_id)
	{

		$email = $this -> email_confirmation_model -> where('manager_id', '=', $manager_id);
		$email -> delete();

		$member = $this -> manager_model -> find($manager_id);
		$member -> status = 'ACTV';
		$confirm = $member -> save();
		return $confirm;
	}

}