<?php namespace App\Repositories;

use App\Models\Disciplinary;

class DisciplinaryRepository extends BaseRepository{

	protected $model;

	public function __construct(Disciplinary $disciplinary)
	{
		$this -> model = $disciplinary;
	}

	public function lists($request)
	{
		$disciplinaries = $this -> model;
		return $disciplinaries -> orderBy('id', 'desc') -> paginate(10);;
	}

	public function create($details)
	{
		$disciplinary = new Disciplinary;
		$disciplinary -> officer_id = $details['officer_id'];
		$disciplinary -> manager_id = $details['manager_id'];
		$disciplinary -> status = $details['status'];
		$disciplinary -> description = $details['description'];
		$disciplinary -> created_at = $details['created_at'];
		$disciplinary_save = $disciplinary -> save();
		return array('status' => $disciplinary_save);
	}
}