<?php namespace App\Repositories;

use App\Models\Product;
use App\Repositories\ManagerRepository;
use App\Repositories\FranchiseRepository;
use Auth;

class ProductRepository extends BaseRepository{

	protected $model;
	protected $manager;
	protected $franchise;

	public function __construct(Product $product, ManagerRepository $manager, FranchiseRepository $franchise)
	{
		$this -> model = $product;
		$this -> manager = $manager;
		$this -> franchise = $franchise;
	}

	public function lists()
	{
		$lists = $this -> model -> get();
		return $lists;
	}

	public function details($item_id)
	{
		$details = $this -> model -> where('id', $item_id) -> first();
		return $details;
	}

	public function create($details)
	{
		$product = new Product;
		$product -> name = $details['name'];
		$product -> type = $details['type'];
		$product -> product_id = $details['product_id'];
		$product -> img = $details['image'];
		$product -> price = $details['price'];

		$product_save = $product -> save();
		return array('status' => $product_save);
	}

	public function update($details)
	{
		$update = $this -> model -> where('id', (int)$details['id']) -> first();
		$update -> name = $details['name'];
		$update -> img = $details['image'];
		$update -> type = $details['type'];
		$update -> product_id = $details['product_id'];
		$update -> price = $details['price'];
		$update_details = $update -> save();
		return $update_details;
	}

	public function purchase($details){

		switch ($details['product_type']) {

			case 'BDGE':
				Auth::user() -> badges() -> attach($details['product_id'], ['status' => 'DSBL']);
			break;

			case 'FICO':
				$updateLogo['franchise_id'] = Auth::user() -> franchise -> id;
				$updateLogo['logo_id'] = $details['product_id'];
				$this -> franchise -> updateLogo($updateLogo);
			break;
		}

		$debit['manager_id'] = $details['manager_id'];
		$debit['amount'] = $details['product_price'];
		$this -> manager -> debitMoney($debit);

		return true;

	}
}