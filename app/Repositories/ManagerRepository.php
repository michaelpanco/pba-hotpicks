<?php namespace App\Repositories;

use App\Models\Manager;
use App\Models\Notification;
use App\Models\ForgotPassword;
use App\Models\Gift;

use App\Repositories\MessageRepository;
use Auth;
use Hash;
use Image;
use DB;


class ManagerRepository extends BaseRepository{

	protected $model;

	public function __construct(Manager $manager)
	{
		$this -> model = $manager;
	}

	public function lists($request)
	{
		$managers = $this -> model;

		if ($request -> has('name'))
		{
			$managers = $managers -> where(DB::raw("CONCAT( firstname,  ' ', lastname )"), 'like', '%' . $request -> input('name') . '%');
		}

		return $managers -> orderBy('id', 'desc') -> paginate(10);;
	}

	public function instance()
	{
		$requests = $this -> model -> find(Auth::user() -> id);
		return $requests;
	}

	public function idExists($id)
	{
		$exists = $this -> model -> where('id', $id) -> exists();
		return $exists;
	}

	public function details($manager_id)
	{
		$details = $this -> model -> where('id', $manager_id) -> with('franchise') -> first();
		return $details;
	}

	public function checkEmailExistence($email)
	{
		$exists = $this -> model -> where('email', $email) -> exists();
		return $exists;
	}

	public function ForgotPasswordTimeValid($email)
	{
		$forgot_password = new ForgotPassword;
		$forgot = $forgot_password -> where('email', $email) -> first();

		if($forgot != null && strtotime($forgot -> created_at) + 600 > strtotime(date('Y-m-d H:i:s')))
		{
			return false;
		}

		return true;
	}

	public function forgotPassword($request)
	{
		$forgot_password = new ForgotPassword;

		$forgot_delete = clone $forgot_password;
		$forgot = clone $forgot_password;

		if($forgot_password -> where('email', $request -> input('email')) -> exists()){
			$forgot_delete -> where('email', $request -> input('email')) -> delete();
		}

		$generated_token = str_random(64);

		$forgot -> email = $request -> input('email');
		$forgot -> token = $generated_token;
		$forgot -> created_at = date('Y-m-d H:i:s');
		$forgot_save = $forgot -> save();

		$misc['email'] = $request -> input('email');
		$misc['token'] = $generated_token;

		return [$forgot_save, $misc];
	}

	public function passwordResetRequestExist($request)
	{
		$reset_password = new ForgotPassword;

		$exist = $reset_password -> where('email', $request['email']) -> where(DB::raw('BINARY `token`'), $request['token']) -> exists();

		if($exist){
			return true;
		}

		return false;
	}

	public function passwordResetRequestDetails($request)
	{
		$reset_password = new ForgotPassword;

		$details = $reset_password -> where('email', $request['email']) -> where('token', $request['token']) -> first();

		return $details;
	}

	public function resetPassword($details)
	{
		$manager = $this -> model -> where('email', $details['email']) -> first();
		$manager -> password = Hash::make($details['password']);
		$change_password = $manager -> save();

		if($change_password)
		{
			$forgot_password = new ForgotPassword;
			$forgot_password -> where('email', $details['email']) -> delete();
		}
		return $change_password;
	}

	public function register($params, $recruiter)
	{
		$params['status'] = 'ACTV';
		$params['slug'] = $this -> generateSlug($params);
		$params['avatar'] = '/assets/img/default_avatar.jpg';
		$params['ip_address'] = $this -> _get_client_ip();
		$params['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$params['reg_type'] = 'DIRECT';
		$params['recruiter'] = $recruiter;
		$params['credits'] = 250;
		$params['date_registered'] = date('Y-m-d H:i:s');
		
		foreach($params as $key => $value)
		{
		    $this -> model -> $key = $value;
		}

		return $this -> model;
	}


	private function _get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}	

	public function changePassword($request)
	{
		$manager = $this -> model -> where('id', Auth::user() -> id) -> first();
		$manager -> password = Hash::make($request -> input('new_password'));
		return $manager -> save();
	}

	public function claimReferalPrize($details)
	{
		$manager = $this -> model -> where('id', $details['manager_id']) -> first();
		$manager ->  claimed_referal_prize = 1;
		$manager_save = $manager -> save();
		return $manager_save;
	}

	public function changeFavorites($favorite)
	{
		$manager = $this -> model -> where('id', Auth::user() -> id) -> first();

		if($favorite['team'] != "empty")
		{
			$manager -> fav_team = $favorite['team'];
		}

		if($favorite['player'] != "empty")
		{
			$manager -> fav_player = $favorite['player'];
		}

		if($favorite['team2'] != "empty")
		{
			$manager -> fav_2nd_team = $favorite['team2'];
		}

		if($favorite['match1'] != "empty" && $favorite['match2'] != "empty")
		{
			$manager -> fav_match_up = $favorite['match1'] . '|' . $favorite['match2'];
		}
		
		return $manager -> save();
	}

	private function generateSlug($params)
	{
		$slug_final = false;
		$slug_method = 1;
		$slug = $this -> constructSlug($params, $slug_method);

		while (!$slug_final)
		{
			if($this -> slugExist($slug))
			{
				$slug_method++;
				$slug = $this -> constructSlug($params, $slug_method);
			}else{
				$slug_final = true;
			}
		}
		return $slug;
	}

	private function constructSlug($params, $method){

		$firstname = $params['firstname'];
		$lastname = $params['lastname'];
		$birth = explode('-', $params['birthday']);
		$birth_month = (string) $birth[1];
		$birth_day = (string) $birth[2];

		switch ($method)
		{
			case 1:
				$slug = $firstname . $lastname;
			break;

			case 2:
				$slug = $firstname . $lastname . $birth_day;
			break;

			case 3:
				$slug = $lastname . $firstname ;
			break;

			case 3:
				$slug = $lastname . $firstname . $birth_day;;
			break;

			case 4:
				$slug = $firstname . $lastname . $birth_month . $birth_day;;
			break;

			case 5:
				$slug = $lastname . $firstname . $birth_month . $birth_day;;
			break;

			default:
				$slug = $firstname . $lastname . '-' . substr(md5(microtime()),rand(0,26),4);;
			break;
		}

		return $this -> slugToStandard($slug);
	}

	public function players($manager_id)
	{
		$franchise = $this -> model -> where('id', $manager_id) -> with('players', 'manager', 'league', 'statistic') -> first();
		return $franchise;
	}

	private function slugToStandard($slug)
	{
		$string = str_replace(' ', '', $slug);
		$string = strtolower($slug);
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}

	private function slugExist($slug)
	{
		$slug_exist = $this -> model -> where('slug', '=', $slug) -> exists();
		return $slug_exist;
	}

	public function getManager($field, $value)
	{
		$manager = $this -> model -> where($field, '=', $value) -> first();
		return $manager;
	}

	public function setActiveStatus($manager_id)
	{
		$manager = $this -> model -> find($manager_id);
		$manager -> status = 'ACTV';
		$manager -> save();
	}

	public function addFriend($params)
	{
		return $this -> model -> find(Auth::user() -> id) -> friends() -> attach($params['manager_id']);
	}

	public function friends()
	{
		return $this -> model -> find(Auth::user() -> id) -> friends();
	}

	public function managerRelationship($params)
	{
		if(!Auth::check())
		{
			return "guest";
		}

		if($params['account_id'] == $params['manager_id'])
		{
			return "self";
		}

		$friend = $this -> model -> find($params['account_id']) -> friends() -> where('friend_id', $params['manager_id']) -> withPivot('status');

		if($friend -> exists()){

			$friend_details = $friend -> first();

			if($friend_details -> pivot -> status == "ACPT"){
				return "friend";
			}

			return "pending";
		}

		$request = $this -> model -> find($params['manager_id']) -> friends() -> where('friend_id', $params['account_id']) -> withPivot('status');

		if($request -> exists()){
			return "waiting";
		}

		return 'stranger';
	}

	public function fetchNotifications()
	{
		$notifications = $this -> model -> find(Auth::user() -> id) -> notifications() -> orderBy('created_at', 'desc') -> take(20) -> get();
		return $notifications;
	}

	public function fetchRequests()
	{
		$requests = $this -> model -> find(Auth::user() -> id) -> requests() -> get();
		return $requests;
	}

	public function hasRequest($manager_id)
	{
		$requests = $this -> model -> find(Auth::user() -> id) -> requests() -> where('manager_id', $manager_id);
		return $requests -> exists();
	}

	public function approveRequest($params)
	{
		$this -> model -> find(Auth::user() -> id) -> requests() -> updateExistingPivot($params['manager_id'], ['status' => 'ACPT']);
	}

	public function approveRequestHandShake($params)
	{
		$this -> model -> find(Auth::user() -> id) -> friends() -> attach($params['manager_id'], ['status' => 'ACPT']);
	}

	public function declineRequest($params)
	{
		return $this -> model -> find(Auth::user() -> id) -> requests() -> detach($params['manager_id']);
	}

	public function notify($params)
	{
	    $notification = new Notification;
	    $notification -> manager_id = $params['manager_id'];
	    $notification -> type = $params['type'];
	    $notification -> details = $params['details'];
	    $notification -> save();
	}

	public function broadcast($details)
	{
		foreach($details['managers'] as $manager){
			$notification = new Notification;
		    $notification -> manager_id = $manager;
		    $notification -> type = $details['type'];
		    $notification -> details = $details['details'];
		    $notification -> save();
		}
		return true;
	}

	public function setNotificationSeen($manager_id)
	{
		$notification = new Notification;
		$notification -> where('manager_id', $manager_id) -> where('seen', 0) -> update(['seen' => 1]);
	}

	public function FlagOnline()
	{
		$manager = $this -> model -> find(Auth::user() -> id);
		$manager -> last_online = date('Y-m-d H:i:s');
		$manager -> save();
	}

	public function getConversation($member_id)
	{
		$message_repository = new MessageRepository;
		$conversations = $message_repository -> conversations($member_id);
		return $conversations;
	}

	public function creditMoney($details)
	{
		$manager_id = $details['manager_id'];
		$amount = $details['amount'];

		$manager = $this -> model -> where('id', $manager_id) -> first();
		$manager -> credits = $manager -> credits + $amount;
		$manager -> save();
	}

	public function debitMoney($details)
	{
		$manager_id = $details['manager_id'];
		$amount = $details['amount'];

		$manager = $this -> model -> where('id', $manager_id) -> first();
		$manager -> credits = $manager -> credits - $amount;
		$manager -> save();
	}

	public function uploadAvatar($file)
	{
		list($width, $height) = getimagesize($file);
		$crop_size = ($width > $height) ? $height : $width;
		$manager = Auth::user();

        $filename  = $manager -> slug . '.' . $file -> getClientOriginalExtension();
        $manager_avatar_dir = 'assets/img/avatars/' . date('dY', strtotime($manager -> date_registered));
        $complete_avatar_url = $manager_avatar_dir . $filename;

        $path = public_path($complete_avatar_url);
		Image::make($file -> getRealPath()) -> crop($crop_size, $crop_size) -> resize(110, 110) -> save($path);

		if($complete_avatar_url != $manager -> avatar){
			$manager_upload = $this -> model -> where('id', $manager -> id) -> first();
			$manager_upload -> avatar = $complete_avatar_url;
			$manager_upload -> save();
		}

		return $complete_avatar_url ;
	}

	public function manageBadges($request)
	{
		$badges = $request -> input('badges');

		$badges_array = [];

		foreach($badges as $key => $value){
			$badge_id = (int) substr($key, 5);
			$badge_status = ($value == 'true') ? 'ENBL' : 'DSBL';
			$badges_array[$badge_id ] = ['status' => $badge_status];
		}

		$manager = $this -> model -> where('id', Auth::user() -> id) -> first();
		$manager -> badges() -> sync($badges_array, false);
	}

	public function resetLeagueLeader($managers)
	{
		$this -> model -> whereIn('id', $managers) -> update(['league_leader' => 0]);
	}

	public function setLeagueLeader($manager_id)
	{
		$this -> model -> where('id', $manager_id) -> update(['league_leader' => 1]);
	}

	public function update($details)
	{
		$update = $this -> model -> where('id', (int)$details['id']) -> first();
		$update -> firstname = $details['firstname'];
		$update -> lastname = $details['lastname'];
		$update -> slug = $details['slug'];
		$update -> gender = $details['gender'];
		$update -> status = $details['status'];
		$update -> credits = $details['credits'];
		$update -> restrain_until = $details['restrain_until'];
		$update -> birthday = $details['birthday'];
		$update -> privilege = $details['privilege'];
		$update -> fav_team = $details['favteam'];
		$update -> fav_2nd_team = $details['fav2ndteam'];
		$update -> fav_player = $details['favplayer'];
		$update -> fav_match_up = $details['favmatchup'];
		$update_details = $update -> save();
		return $update_details;
	}

	public function restrain($details)
	{
		$manager_id = $details -> input('restrain_manager_id');
		$no_of_days = $details -> input('no_of_days');
		$restrain_reason = $details -> input('restrain_reason');

		$restrain_date = date('Y-m-d', strtotime("+" . $no_of_days . " days"));

		$update = $this -> model -> where('id', (int)$manager_id) -> first();
		$update -> restrain_until = $restrain_date;
		$restrain_manager = $update -> save();
		return $restrain_manager;
	}

	public function giveChampionshipBadge($manager_id)
	{
		$user = $this -> model -> where('id', $manager_id) -> first();
		$user -> badges() -> attach(13, ['status' => 'DSBL']);
	}

	public function newManagers()
	{
		$members = $this -> model -> where('date_registered', '=', new \DateTime('today')) -> get();
		return $members;
	}

	public function activeManagers()
	{
		$members = $this -> model -> where('last_online', '>=', new \DateTime('today')) -> get();
		return $members;
	}

	public function total()
	{
		$members = $this -> model -> get();
		return $members -> count();
	}

	public function getRegisterCountFromDate($date)
	{
		$members = $this -> model -> where('date_registered', '=', $date) -> get();
		return $members -> count();
	}

	public function demoteLeageLeader($manager_id)
	{
		$member = $this -> model -> where('id', $manager_id) -> first();
		$member -> league_leader = 0;
		$member -> save();
	}

	public function richest()
	{
		$managers = $this -> model -> where('last_online', '>', date('Y-m-d 00:00:00', strtotime("-3 days")))
						-> orderBy('credits', 'desc') 
						-> take(50) 
						-> get();
		return $managers;
	}

	public function canOpenGift()
	{
		$gift = new Gift;

		$already_claimed = $gift -> where('manager_id', Auth::user()->id) -> where('date', date('Y-m-d')) -> exists();

		if($already_claimed){
			return false;
		}

		if(date('Y-m-d') > '2016-01-01'){
			return false;
		}

		return true;
	}

	public function openGift($details)
	{
	    $gift = new Gift;
	    $gift -> manager_id = $details['manager_id'];
	    $gift -> received = $details['gift'];
	    $gift -> date = date('Y-m-d');
	    $open_gift = $gift -> save();
	    return $open_gift;
	}

}