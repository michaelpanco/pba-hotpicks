<?php namespace App\Repositories;

use App\Models\Message;
use Auth;
use DB;

class MessageRepository extends BaseRepository{

	protected $model;

	public function __construct(Message $message)
	{
		$this -> model = $message;
	}

	public function lists($render)
	{
		$num_item = ($render == 'page') ? 4 : null;
 		$lists = $this -> model ->  where('messages.id', function ($sub) {
		   $sub->selectRaw('max(id)')
		       ->from('messages as m')
		       ->where('m.conversation_id', DB::raw('messages.conversation_id'));
		})-> where(function ($query){
			$query -> where('sender', Auth::user() -> id)
				-> orWhere('recipient', Auth::user() -> id);
		})
		->join('managers as recipient', 'messages.recipient', '=', 'recipient.id')
		->join('managers as sender', 'messages.sender', '=', 'sender.id')	
		->select('messages.*', 'recipient.firstname as recipient_firstname', 'recipient.lastname as recipient_lastname', 'recipient.avatar as recipient_avatar', 'recipient.slug as recipient_slug', 'sender.firstname as sender_firstname', 'sender.lastname as sender_lastname', 'sender.avatar as sender_avatar', 'sender.slug as sender_slug')
		->orderBy('messages.created_at', 'desc')->paginate($num_item);

		return $lists;
	}

	public function conversations($manager_id)
	{
		$conversations = $this -> model -> where(function($query) use($manager_id){
			$query -> where('sender', $manager_id)
				   -> where('recipient', Auth::user() -> id);
		}) ->orWhere(function($query) use($manager_id){
			$query -> where('sender', Auth::user() -> id)
				   -> where('recipient', $manager_id);
		})
		->join('managers as recipient', 'messages.recipient', '=', 'recipient.id')
		->join('managers as sender', 'messages.sender', '=', 'sender.id')	
		->select('messages.*', 'recipient.avatar as recipient_avatar', 'recipient.slug as recipient_slug', 'sender.avatar as sender_avatar', 'sender.slug as sender_slug')
		->orderBy('messages.id', 'desc');
		return $conversations -> take(20) -> get() -> reverse();
	}

	public function send($message)
	{
		$send_message['conversation_id'] = $message['conversation_id'];
		$send_message['message'] = $message['message'];
		$send_message['sender'] = Auth::user()->id;
		$send_message['recipient'] = $message['recipient'];
		$send_message['created_at'] = date('Y-m-d H:i:s');

		foreach($send_message as $key => $value)
		{
		    $this -> model -> $key = $value;
		}

		return $this -> model;
	}

	public function stream($manager_id, $pointer)
	{
		$conversations = $this -> model -> where(function($query) use($manager_id, $pointer){
			$query -> where('sender', $manager_id)
			->where('messages.id', '<', $pointer)
				   -> where('recipient', Auth::user() -> id);
		}) ->orWhere(function($query) use($manager_id, $pointer){
			$query -> where('sender', Auth::user() -> id)
			->where('messages.id', '<', $pointer)
				   -> where('recipient', $manager_id);
		})
		
		->join('managers as recipient', 'messages.recipient', '=', 'recipient.id')
		->join('managers as sender', 'messages.sender', '=', 'sender.id')	
		->select('messages.*', 'recipient.avatar as recipient_avatar', 'recipient.slug as recipient_slug', 'sender.avatar as sender_avatar', 'sender.slug as sender_slug')
		->orderBy('messages.id', 'desc');
		return $conversations -> take(20) -> get() -> reverse();
	}

	public function markMessageRead($manager_id)
	{
		$this -> model -> where('sender', $manager_id) -> where('recipient', Auth::user() -> id) -> update(['read' => 1]);
	}

	public function total()
	{
		$members = $this -> model -> get();
		return $members -> count();
	}
	
}