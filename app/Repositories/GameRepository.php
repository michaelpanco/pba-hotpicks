<?php namespace App\Repositories;

use App\Models\Game;
use Setting;

class GameRepository extends BaseRepository{

	protected $model;
	protected $sequence;

	public function __construct(Game $game)
	{
		$this -> model = $game;
		$this -> sequence = Setting::get('SEQUENCE');
	}

	public function lists($team_id, $outcome)
	{
		if($team_id != 'all')
		{
			$games = $this -> model -> where(function ($query) use($team_id){
				$query -> where('team_x_id', $team_id)
					-> orWhere('team_y_id', $team_id);
			});

			if($outcome != 'all')
			{
				$games = ($outcome == 'win') ? $games -> where('winner', $team_id) : $games -> where('winner', '!=', $team_id);
			}

			$games = $games -> where('sequence', $this -> sequence)
			-> with('team_x', 'team_y') -> get();

		}else{
			$games = $this -> model -> with('team_x', 'team_y') -> where('sequence', $this -> sequence) -> get();
		}
		
		$i = 0;

		$cluster_container = "";

		if($games -> count() <= 0)
		{
			return false;
		}

		foreach($games as $game)
		{
			if(isset($tmpc) &&  $tmpc != $game -> cluster_no){
				$i++;
			}

			$game_list[$i]['conference'] = $game -> conference;
			$game_list[$i]['date'] = date('F j, Y - l', strtotime($game -> datetime));
			$game_list[$i]['venue'] = $game -> venue;
			$game_list[$i]['game_type'] = $game -> game_type;

			$score = explode('-', $game -> score);

			$game_list[$i]['games'][$game -> game_no]['game_id'] = $game -> id;
			$game_list[$i]['games'][$game -> game_no]['team_x_img'] = $game -> team_x -> logo;
			$game_list[$i]['games'][$game -> game_no]['team_x_slug'] = $game -> team_x -> slug;
			$game_list[$i]['games'][$game -> game_no]['team_x_score'] = (isset($score[0])) ? $score[0] : null;
			$game_list[$i]['games'][$game -> game_no]['team_y_img'] = $game -> team_y -> logo;
			$game_list[$i]['games'][$game -> game_no]['team_y_slug'] = $game -> team_y -> slug;
			$game_list[$i]['games'][$game -> game_no]['team_y_score'] = (isset($score[1])) ? $score[1] : null;
			$game_list[$i]['games'][$game -> game_no]['time'] = date('g:i A', strtotime($game -> datetime));
			$game_list[$i]['games'][$game -> game_no]['score'] = $game -> score;
			$game_list[$i]['games'][$game -> game_no]['result_team_x'] = ($game -> winner != null && $game -> team_x_id != $game -> winner) ? 'loss' : 'na';
			$game_list[$i]['games'][$game -> game_no]['result_team_y'] = ($game -> winner != null && $game -> team_y_id != $game -> winner) ? 'loss' : 'na';

			$tmpc  = $game -> cluster_no;
		}

		return $game_list;
	}

	public function create($details)
	{
		$game = new Game;
		$game -> team_x_id = $details['team_x_id'];
		$game -> team_y_id = $details['team_y_id'];
		$game -> venue = $details['venue'];
		$game -> season = $details['season'];
		$game -> conference = $details['conference'];
		$game -> game_no = $details['game_no'];
		$game -> cluster_no = $details['cluster_no'];
		$game -> game_type = $details['game_type'];
		$game -> sequence = $details['sequence'];
		$game -> datetime = $details['datetime'];
		$game_save = $game -> save();
		return $game_save;
	}

	public function update($details)
	{
		$game = $this -> model -> where('id', (int)$details['id']) -> first();
		$game -> team_x_id = $details['team_x_id'];
		$game -> team_y_id = $details['team_y_id'];
		$game -> winner = $details['winner'];
		$game -> score = $details['score'];
		$game -> venue = $details['venue'];
		$game -> season = $details['season'];
		$game -> conference = $details['conference'];
		$game -> game_no = $details['game_no'];
		$game -> cluster_no = $details['cluster_no'];
		$game -> game_type = $details['game_type'];
		$game -> sequence = $details['sequence'];
		$game -> datetime = $details['datetime'];
		$update_game = $game -> save();
		return $update_game;
	}

	public function schedules($request)
	{
		$this_model = $this -> model;
		if ($request -> has('team'))
		{	
			$team_id = $request -> input('team');
			$this_model = $this_model  -> where(function ($query) use($team_id){
				$query -> where('team_x_id', $team_id)
					-> orWhere('team_y_id', $team_id);
			});
		}

		$schedules = $this_model -> with('team_x', 'team_y') -> orderBy('id', 'asc') -> paginate(10);
		return $schedules;
	}

	public function matchups($team_id)
	{
		// $games = $this -> model -> where('team_x_id', $team_id) -> orWhere('team_y_id', $team_id) -> with('team_x', 'team_y') -> get();

		$games = $this -> model -> where(function ($query) use($team_id){
			$query -> where('team_x_id', $team_id)
				-> orWhere('team_y_id', $team_id);
		})
		-> where('sequence', $this -> sequence)
		-> with('team_x', 'team_y') -> get();


		$i = 0;
		foreach($games as $game)
		{
			$matchups[$i]['game_id'] = $game -> id;
			$matchups[$i]['opponent'] = ($game -> team_x_id != $team_id) ? $game -> team_x_id : $game -> team_y_id;
			$matchups[$i]['opponent_team'] = ($game -> team_x_id != $team_id) ? $game -> team_x -> name : $game -> team_y -> name;
			$matchups[$i]['opponent_img'] = ($game -> team_x_id != $team_id) ? $game -> team_x -> logo : $game -> team_y -> logo;
			$matchups[$i]['opponent_slug'] = ($game -> team_x_id != $team_id) ? $game -> team_x -> slug : $game -> team_y -> slug;
			$matchups[$i]['venue'] = $game -> venue;
			$matchups[$i]['datetime'] = $game -> datetime;
			$matchups[$i]['game_type'] = $game -> game_type;
			$matchups[$i]['score'] = $game -> score;
			$matchups[$i]['final'] = ($game -> score != null) ? true : false;
			$matchups[$i]['outcome'] = ($game -> winner == $team_id) ? 'win' : 'loss';
			$i++;
		}

		return $matchups;
	}

	public function featured()
	{
		$latest_cluster = $this -> latest_cluster();
		$games = $this -> model -> where('cluster_no', '<=', $latest_cluster) 
			-> where('cluster_no', '>=', $latest_cluster - 3)
			-> where('sequence', $this -> sequence);

		$featured = $games -> with('team_x', 'team_y') -> get();

		$i = 0;

		$cluster_container = "";

		if($featured -> count() <= 0)
		{
			return false;
		}

		foreach($featured as $game)
		{
			if(isset($tmpc) &&  $tmpc != $game -> cluster_no){
				$i++;
			}

			$featured_games[$i]['conference'] = $game -> conference;
			$featured_games[$i]['date'] = date('F j, Y - l', strtotime($game -> datetime));
			$featured_games[$i]['game_type'] = $game -> game_type;
			$featured_games[$i]['game_start'][] = date('Y/m/d H:i', strtotime($game -> datetime));


			$featured_games[$i]['games'][$game -> game_no]['game_id'] = $game -> id;
			$featured_games[$i]['games'][$game -> game_no]['team_x_img'] = $game -> team_x -> logo;
			$featured_games[$i]['games'][$game -> game_no]['team_x_slug'] = $game -> team_x -> slug;
			$featured_games[$i]['games'][$game -> game_no]['team_y_img'] = $game -> team_y -> logo;
			$featured_games[$i]['games'][$game -> game_no]['team_y_slug'] = $game -> team_y -> slug;
			$featured_games[$i]['games'][$game -> game_no]['time'] = date('g:i A', strtotime($game -> datetime));
			$featured_games[$i]['games'][$game -> game_no]['score'] = $game -> score;
			$featured_games[$i]['games'][$game -> game_no]['result_team_x'] = ($game -> winner != null && $game -> team_x_id != $game -> winner) ? 'loss' : 'na';
			$featured_games[$i]['games'][$game -> game_no]['result_team_y'] = ($game -> winner != null && $game -> team_y_id != $game -> winner) ? 'loss' : 'na';

			$tmpc  = $game -> cluster_no;
		}

		return $featured_games;
	}

	public function details($game_id)
	{
		$details = $this -> model -> where('id', $game_id) -> where('sequence', $this -> sequence) -> with('team_x', 'team_y') -> first();
		return $details;
	}


	public function broadDetails($game_id)
	{
		$details = $this -> model -> where('id', $game_id) -> with('team_x', 'team_y', 'team_winner') -> first();
		return $details;
	}

	private function latest_cluster()
	{
		$games = $this -> model -> where('datetime', '<', date('Y-m-d H:i:s')) -> where('sequence', $this -> sequence) -> orderBy('id', 'desc') -> first();
		
		if($games === null)
		{
			return false;
		}

		if($games -> game_type == 'Championship')
		{
			return $games -> cluster_no;
		}

		return $games -> cluster_no + 1;
	}

}