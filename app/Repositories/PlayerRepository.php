<?php namespace App\Repositories;

use App\Models\Player;
use Illuminate\Http\Request;
use DB;

class PlayerRepository extends BaseRepository{

	protected $model;
	protected $request;

	public function __construct(Player $player, Request $request)
	{
		$this -> model = $player;
		$this -> request = $request;
	}

	public function details($slug)
	{
		$details = $this -> model -> where('slug', $slug) -> with('team', 'performance') -> first();
		return $details;
	}

	public function create($details)
	{
		$player = new Player;
		$player -> firstname = $details['firstname'];
		$player -> lastname = $details['lastname'];
		$player -> slug = $details['slug'];
		$player -> team_id = $details['team_id'];
		$player -> position = $details['position'];
		$player -> jersey = $details['jersey'];
		$player -> birthday = $details['birthday'];
		$player -> height = $details['height'];
		$player -> school = $details['school'];
		$player -> avatar = $details['avatar'];
		$player -> salary = $details['salary'];
		$player_save = $player -> save();
		return array('status' => $player_save, 'player_id' => $player -> id);
	}

	public function detailsByID($player_id)
	{
		$details = $this -> model -> where('id', $player_id) -> with('team', 'performance') -> first();
		return $details;
	}

	public function salary($player_id)
	{
		$details = $this -> model -> where('id', $player_id) -> first();
		return $details -> salary;
	}

	public function lists($positions='all')
	{
		$players = $this -> model -> where('status', 'ACTV');

		if ($this -> request -> has('team') && $this -> request -> input('team') != 'false')
		{
			$players = $players -> where('team_id', $this -> request -> input('team'));
		}

		if($this -> request -> has('position') || $positions != 'all')
		{	
			if($this -> request -> input('position') != 'false'){
				if($this -> request -> has('position'))
				{
					$positions = $this -> request -> input('position');
				}

				$players = $players -> whereIn('position', $this -> positionCriteria($positions));
			}
		}

		if ($this -> request -> has('name'))
		{
			$players = $players -> where(DB::raw("CONCAT( firstname,  ' ', lastname )"), 'like', '%' . $this -> request -> input('name') . '%');
		}

		if ($this -> request -> has('min'))
		{
			$players = $players -> where('salary', '>=', (int) $this -> request -> input('min'));
		}

		if ($this -> request -> has('max'))
		{
			$players = $players -> where('salary', '<=', (int) $this -> request -> input('min'));
		}


		return $players -> with('team') -> get();
	}

	public function playersDetails($team_ids){
		$players = $this -> model -> whereIn('id', $team_ids) -> get();
		return $players;
	}

	public function getPlayerTeamID($player_id)
	{
		$team_id = $this -> model -> where('id', $player_id) -> first() -> team_id;
		return $team_id;
	}

	private function positionCriteria($position)
	{
		switch ($position) {

			case 1:
				return [1,12];
			break;

			case 2:
				return [12,2,23];
			break;

			case 3:
				return [23,3,34];
			break;

			case 4:
				return [4,45];
			break;

			case 5:
				return [45,5];
			break;
		}
	}

	public function mostSigned()
	{
		$top_five_signed_players = $this -> model -> orderBy('sign_count', 'desc') -> take(10) -> get();
		return $top_five_signed_players;
	}

	public function update($details)
	{
		$update = $this -> model -> where('id', (int)$details['id']) -> first();
		$update -> firstname = $details['firstname'];
		$update -> lastname = $details['lastname'];
		$update -> slug = $details['slug'];
		$update -> position = $details['position'];
		$update -> jersey = $details['jersey'];
		$update -> team_id = $details['team_id'];
		$update -> birthday = $details['birthday'];
		$update -> school = $details['school'];
		$update -> height = $details['height'];
		$update -> avatar = $details['avatar'];
		$update -> status = $details['status'];
		$update -> salary = $details['salary'];
		$update_details = $update -> save();
		return $update_details;
	}
}