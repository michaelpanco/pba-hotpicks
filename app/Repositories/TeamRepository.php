<?php namespace App\Repositories;

use App\Models\Team;

class TeamRepository extends BaseRepository{

	protected $model;

	public function __construct(Team $team)
	{
		$this -> model = $team;
	}

	public function lists()
	{
		return $this -> model -> get();
	}

	public function details($slug)
	{
		$details = $this -> model -> where('slug', $slug) -> with('players') -> first();
		return $details;
	}

	public function getIDBySlug($slug){
		$team_id = $this -> model -> where('slug', $slug) -> first();
		return $team_id;
	}

	public function detailsByID($team_id)
	{
		$details = $this -> model -> where('id', $team_id) -> with('players') -> first();
		return $details;
	}

	public function update($details)
	{
		$update = $this -> model -> where('id', (int)$details['id']) -> first();
		$update -> name = $details['name'];
		$update -> slug = $details['slug'];
		$update -> manager = $details['manager'];
		$update -> coach = $details['coach'];
		$update -> assistant_coaches = $details['assistant_coaches'];
		$update -> joined = $details['joined'];
		$update -> titles = $details['titles'];
		$update -> win = $details['win'];
		$update -> loss = $details['loss'];
		$update -> logo = $details['logo'];
		$update_details = $update -> save();
		return $update_details;
	}

}