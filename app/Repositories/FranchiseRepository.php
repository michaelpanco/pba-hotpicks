<?php namespace App\Repositories;

use App\Models\TmpFranchise;
use App\Models\Franchise;
use App\Models\FranchisePlayer;
use App\Models\FranchiseLogo;
use App\Models\Performance;
use App\Models\Player;
use App\Repositories\ManagerRepository;

use Auth;
use DB;

class FranchiseRepository extends BaseRepository{

	protected $tmp_franchise;
	protected $franchise;
	protected $franchise_player;
	protected $franchise_logo;
	protected $performance;
	protected $player;
	protected $manager;

	public function __construct(TmpFranchise $tmp_franchise, FranchiseLogo $franchise_logo, Franchise $franchise, FranchisePlayer $franchise_player, Performance $performance, Player $player, ManagerRepository $manager)
	{
		$this -> tmp_franchise = $tmp_franchise;
		$this -> franchise_logo = $franchise_logo;
		$this -> franchise_player = $franchise_player;
		$this -> performance = $performance;
		$this -> franchise = $franchise;
		$this -> player = $player;
		$this -> manager = $manager;
	}

	public function details($franchise_id)
	{
		$franchise = $this -> franchise -> where('id', $franchise_id) -> with('players', 'manager', 'league', 'statistic') -> first();
		return $franchise;
	}

	public function owner($franchise_id)
	{
		$franchise = $this -> franchise -> where('id', $franchise_id) -> first();
		return $franchise -> manager_id;
	}

	public function players($franchise_id)
	{
		$franchise_players = $this -> franchise_player -> where('franchise_id', $franchise_id) -> get();
		return $franchise_players;
	}

	public function create($details)
	{
		$this -> franchise -> name = $details['name'];
		$this -> franchise -> manager_id = $details['manager_id'];
		$this -> franchise -> logo_id = $details['logo_id'];
		$this -> franchise -> created_at = date('Y-m-d');
		$result = $this -> franchise -> save();

        $positions = [1,1,2,2,3,3,4,4,5,5];
        $i = 0;

		foreach($details['picks'] as $pick)
		{
			DB::update('update players set sign_count = sign_count + 1 where id = ?', [$pick]);
			$this -> franchise -> players() -> attach($pick, ['position' => $positions[$i], 'date_hired' => date('Y-m-d')]);
			$i++;
		}
		
		return $result;
	}

	public function feed($franchises_ids)
	{
		// $franchises_ids variable is just a collection of franchises of the league
		$franchises_points = array(); // set empty array

		// Get all the players of all franchises of the league
		$franchise_players = $this -> franchise_player -> whereIn('franchise_id', $franchises_ids) -> get();
		
		// initialize franchises to zero
		foreach($franchises_ids as $franchise){
			$franchises_points[$franchise] = 0;
		}

		foreach($franchise_players as $player)
		{
			$update_franchise = $this -> update_franchise($player -> player_id, $player -> last_game_seed);

			if($update_franchise[0] > 0){
				DB::update('update franchises_players set fantasy_points = fantasy_points + ' . $update_franchise[0] . ', last_game_seed='. $update_franchise[1] .' where id = ?', [$player -> id]);
				$franchises_points[$player -> franchise_id] = $franchises_points[$player -> franchise_id] + $update_franchise[0];
			}
		}

		return $franchises_points;
	}

	public function update_franchise($player_id, $last_game_seed)
	{
		$performances = $this -> performance -> select('id', DB::raw('SUM(fantasy_points) as total_fantasy_points'), DB::raw('MAX(game_id) as last_game_id')) 
							-> groupBy('player_id') -> where('player_id', $player_id) 
								-> where('game_id', '>', $last_game_seed)
									-> where('ready', 1) -> first();
		
		if($performances)
		{
			return array($performances -> total_fantasy_points, $performances -> last_game_id);
		}

		return array(0,0);
	}

	public function playerPosition($details)
	{
		$player = $this -> franchise_player -> where('franchise_id' , $details['franchise_id']) -> where('player_id', $details['player_id']) -> first();
		return ($player === null) ? false : $player -> position;
	}

	public function updateLogo($details)
	{
		$franchise = $this -> franchise -> where('id', $details['franchise_id']) -> first();
		$franchise -> logo_id = $details['logo_id'];
		$franchise -> save();
	}

	public function manager($manager_id)
	{
		$franchise = $this -> franchise -> where('manager_id', $manager_id) -> with('players', 'icon') -> first();
		return $franchise;
	}

	public function tmpDetailsByID($tag)
	{
		$franchise = $this -> tmp_franchise -> where('id', $tag) -> with('players') -> first();
		return $franchise;
	}

	public function tmpDetailsByTag($tag)
	{
		$franchise = $this -> tmp_franchise -> where('tag', $tag) -> first();
		return $franchise;
	}

	public function build($request)
	{
		$franchise_details['name'] = $request['name'];
		$franchise_details['manager_id'] = Auth::user() -> id;
		$franchise_details['logo_id'] = $request['logo'];
		$franchise_details['picks'] = $request['picks'];

		$result = $this -> create($franchise_details);

		return ['result' => $result];
	}

	public function prepare($request)
	{
		$franchise_details['name'] = $request['name'];
		$franchise_details['picks'] = $request['picks'];
		$franchise_details['logo'] = $request['logo'];

		$save_tmp_franchise = $this -> createTmpFranchise($franchise_details);

		return $save_tmp_franchise;
	}

	public function logos($type='FREE', $status='ENBL'){
		$logos = $this -> franchise_logo -> where('type', $type) -> where('status', 'ENBL') -> get();
		return $logos;
	}

	public function logoFree($logo_id){
		$logo = $this -> franchise_logo -> where('id', $logo_id) -> first();
		if($logo -> type == "FREE"){
			return true;
		}
		return false;
	}

	public function createTmpFranchise($details)
	{
		$this -> tmp_franchise -> tag = str_random(32);
		$this -> tmp_franchise -> name = $details['name'];
		$this -> tmp_franchise -> logo_id = $details['logo'];
		$result = $this -> tmp_franchise -> save();
		$this -> tmp_franchise -> players() -> attach($details['picks']);
		return ['result' => $result, 'tag' => $this -> tmp_franchise -> tag];
	}

	public function updateLeague($franchise_id, $league_id)
	{
		$franchise = $this -> franchise -> find($franchise_id);

		$franchise -> league_id = $league_id;

		return $franchise -> save();
	}

	public function trade($player)
	{
		$player_get_id = (int) $player['get'];
		$player_give_id = (int) $player['give'];

		$players = $this -> player -> whereIn('id', [$player_give_id, $player_get_id]) -> get();

		$player_give_salary = $players -> where('id', $player_give_id) -> first() -> salary;
		$player_get_salary = $players -> where('id', $player_get_id) -> first() -> salary;

		$franchise = $this -> franchise_player -> where('franchise_id', Auth::user() -> franchise -> id) -> where('player_id', $player_give_id) -> first();
		$franchise -> player_id = $player['get'];
		$franchise -> date_hired = date('Y-m-d');

		$franchise_save = $franchise -> save();

		if($player_get_salary > $player_give_salary && $franchise_save){
			$salary_difference = $player_get_salary - $player_give_salary;

			$debit['manager_id'] = Auth::user() -> id;
			$debit['amount'] = $salary_difference;
			$this -> manager -> debitMoney($debit);
		}

		return $franchise_save;
	}

	public function reboot_franchises($franchises){
		$last_game_query = DB::select('SELECT id, game_id from performances ORDER BY id DESC LIMIT 1');
		$last_game = $last_game_query[0] -> game_id;
		DB::table('franchises_players') -> whereIn('franchise_id', $franchises) -> update(['last_game_seed' => $last_game]);
	}

	public function top()
	{
		$managers = $this -> franchise_player -> selectRaw('franchise_id, sum(fantasy_points) as total_fantasy_points') 
						-> join('franchises', 'franchises_players.franchise_id', '=', 'franchises.id')
						-> join('managers', 'franchises.manager_id', '=', 'managers.id')
						-> where('managers.last_online', '>', date('Y-m-d 00:00:00', strtotime("-3 days")))
						-> groupBy('franchise_id') -> orderBy('total_fantasy_points', 'desc') 
						-> take(50) 
						-> get();
		return $managers;
	}
}