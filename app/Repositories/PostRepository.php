<?php namespace App\Repositories;

use App\Models\Post;
use App\Models\PostPoint;
use Auth;

class PostRepository extends BaseRepository{

	protected $model;

	public function __construct(Post $post)
	{
		$this -> model = $post;
	}

	public function details($post_id)
	{
		$details = $this -> model -> where('id', $post_id) -> first();
		return $details;
	}

	public function create($post)
	{
		$post['parent_post'] = 1;
		$post['manager_id'] = Auth::user()->id;

		foreach($post as $key => $value)
		{
		    $this -> model -> $key = $value;
		}

		return $this -> model;
	}

	public function replyCount($post_id)
	{
		$post = $this -> model -> where('id', $post_id) -> first();
		return $post -> replies -> count();
	}

	public function reply($post)
	{
		$post['parent_post'] = (int)$post['parent_post'];
		$post['manager_id'] = Auth::user()->id;
		
		foreach($post as $key => $value)
		{
		    $this -> model -> $key = $value;
		}

		return $this -> model;
	}

	public function fetchReplies($parent_id)
	{
		$replies = $this -> model -> whereParentPost($parent_id) -> whereStatus('ACTV') -> take(10) -> orderBy('created_at', 'asc') -> get();
		return $replies;
	}

	public function colle(){
		$replies = $this -> model -> whereParentPost(1) -> whereStatus('ACTV') -> paginate(2);
		return $replies;
	}

	public function canUpVote($params){
		$postpoint_model = new PostPoint;
		$postpoint = $postpoint_model -> whereManagerId($params['manager_id']) -> wherePostId($params['post_id']) -> count();
		if($postpoint > 0){
			return false;
		}
		return true;
	}

	public function upVote($params)
	{
		$give_point = new PostPoint;
		$give_point -> manager_id = $params['manager_id'];
		$give_point -> post_id = $params['post_id'];
		return $give_point;
	}

	public function delete($params)
	{
		$post = $this -> model -> where('id', $params['post_id']) -> first();
		$post -> status = 'HIDE';
		$delete_post = $post -> save();
		return $delete_post;
	}

	public function stream($channel, $pointer=false)
	{
		if($pointer === false)
		{
			$users = $this -> model -> whereChannelId($channel) -> whereParentPost(1) -> whereStatus('ACTV') -> with('manager') -> take(10) -> orderBy('id','desc');
		}else{
			$users = $this -> model -> whereChannelId($channel) -> whereParentPost(1) -> whereStatus('ACTV') -> where('id', '<', $pointer) -> with('manager') -> take(10) -> orderBy('id','desc');
		}
		
		return $users;
	}

	public function newPosts()
	{
		$posts = $this -> model -> where('created_at', '>=', new \DateTime('today')) -> with('manager') -> orderBy('id','desc') -> get() ;
		return $posts;
	}

	public function total()
	{
		$posts = $this -> model -> get();
		return $posts -> count();
	}

}