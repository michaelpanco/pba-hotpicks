<?php namespace App\Repositories;

use App\Models\ExpressBet;
use App\Models\ExpressBetEntries;
use App\Repositories\ManagerRepository;
use Auth;

class ExpressBetRepository extends BaseRepository{

	protected $express_bet;
	protected $express_bet_entries;
	protected $manager;

	public function __construct(ExpressBet $express_bet, ExpressBetEntries $express_bet_entries, ManagerRepository $manager)
	{
		$this -> express_bet = $express_bet;
		$this -> express_bet_entries = $express_bet_entries;
		$this -> manager = $manager;
	}

	public function lists($request)
	{
		$bets = $this -> express_bet;
		return $bets -> orderBy('id', 'desc') -> paginate(10);;
	}

	public function details($express_bet_id)
	{
		$details = $this -> express_bet -> where('id', $express_bet_id) -> first();
		return $details;
	}

	public function create($details)
	{
		$express_bet = new ExpressBet;
		$express_bet -> description = $details['description'];
		$express_bet -> option = $details['option'];
		$express_bet -> won = $details['won'];
		$express_bet -> limit = $details['limit'];
		$express_bet -> gamedate = $details['gamedate'];
		$express_bet -> expiry = $details['expiry'];

		$express_bet_save = $express_bet -> save();
		return array('status' => $express_bet_save, 'express_bet_id' => $express_bet -> id);
	}

	public function update($details)
	{
		$update = $this -> express_bet -> where('id', (int)$details['id']) -> first();
		$update -> description = $details['description'];
		$update -> option = $details['option'];
		$update -> won = $details['won'];
		$update -> limit = $details['limit'];
		$update -> gamedate = $details['gamedate'];
		$update -> expiry = $details['expiry'];
		$update_details = $update -> save();
		return $update_details;
	}

	public function entryDetails($express_bet_id, $manager_id)
	{
		$details = $this -> express_bet_entries -> where('express_bet_id', $express_bet_id) -> where('manager_id', $manager_id) -> with('details') -> first();
		return $details;
	}

	public function latest()
	{
		$latest = $this -> express_bet -> where('won', 0) -> get();
		return $latest;
	}

	public function myBets($manager_id)
	{
		$my_bets = $this -> express_bet_entries -> where('manager_id', $manager_id);
		return $my_bets;
	}

	public function exist($express_bet_id, $manager_id)
	{
		$express_bet = $this -> express_bet_entries -> where('express_bet_id', $express_bet_id) -> where('manager_id', $manager_id);
		
		if($express_bet -> exists()){
			return true;
		}

		return false;
	}

	public function makeBet($request)
	{
		$this -> express_bet_entries -> express_bet_id = $request -> input('express_bet_id');
		$this -> express_bet_entries -> manager_id = Auth::user() -> id;
		$this -> express_bet_entries -> bet = $request -> input('bet');
		$this -> express_bet_entries -> amount = $request -> input('amount');
		$this -> express_bet_entries -> datetime = date('Y-m-d H:i:s');

		$add_express_bet = $this -> express_bet_entries -> save();

		if($add_express_bet)
		{
			$debit['manager_id'] = Auth::user() -> id;
			$debit['amount'] = $request -> input('amount');
			$this -> manager -> debitMoney($debit);

			return true;
		}

		return false;
	}

	public function claimPrize($details)
	{
		$express_bet_entry = $this -> express_bet_entries -> where('manager_id', $details['manager_id']) -> where('express_bet_id', $details['express_bet_id']) -> first();
		$amount_prize = $express_bet_entry -> amount;

		$express_bet_entry -> claimed = 1;

		$tag_claimed = $express_bet_entry -> save();

		if($tag_claimed)
		{
			$credit['manager_id'] = Auth::user() -> id;
			$credit['amount'] = $amount_prize*2;
			$this -> manager -> creditMoney($credit);

			return [true, $credit['amount']];
		}

		return [false, 0];

	}
}