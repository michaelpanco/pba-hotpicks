<?php namespace App\Pagination;

use Landish\Pagination\Pagination as BasePagination;

class FriendsPagination extends BasePagination {

    protected $paginationWrapper = '<ul class="pagination">%s %s %s</ul>';

    protected $availablePageWrapper = '<li><a ng-click="paginate(\'friendsContent\', \'%s\', true)" href="">%s</a></li>';
}