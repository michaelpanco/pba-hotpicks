<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;
use Auth;
use Input;
use Validator;

class UploadAvatar{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($avatar)
	{
		$file = array('avatar' => $avatar);

		$validation = Validator::make($file, $this -> rules());
		return $validation;
	}

	public function rules()
	{
		return [
	    	'avatar' => 'required|image|between:1, 2000|mimes:jpg,jpeg,bmp,png'
		];
	}

}