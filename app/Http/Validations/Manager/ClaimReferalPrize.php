<?php namespace App\Http\Validations\Manager;

use Auth;

class ClaimReferalPrize{

	public function validate($request)
	{
		try {

			$auth_user = Auth::user();
			$referals = $auth_user -> referals() -> get();

			if($auth_user  -> claimed_referal_prize)
			{
				throw new \Exception("Referal prize was already claimed");
			}

			if($referals -> count() < 10)
			{
				throw new \Exception("Sorry, You need atleast 10 referals to claim your prize");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}