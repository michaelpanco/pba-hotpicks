<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Auth;

class UpdateLeaguePassword{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function validate($request)
	{
		$params['league_id'] = $request -> input('league_id');
		$params['league_password'] = $request -> input('league_password');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			$auth_user = Auth::user();

			if($auth_user -> id != $this -> league -> host($params['league_id']))
			{
				throw new \Exception("Unable to update league password");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}