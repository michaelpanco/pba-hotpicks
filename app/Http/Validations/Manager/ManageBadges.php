<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;
use Auth;

class ManageBadges{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['badges'] = $request -> input('badges');
		$enabled_badges = 0;

		foreach($params['badges'] as $key => $value)
		{
			if($value=='true'){
				$enabled_badges++;
			}
		}

		try {

			if($enabled_badges > 3){
				throw new \Exception("Only 3 badges can be activate");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}