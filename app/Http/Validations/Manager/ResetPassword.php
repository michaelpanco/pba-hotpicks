<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;

class ResetPassword{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['newpassword'] = $request -> input('newpassword');
		$params['email'] = $request -> input('email');
		$params['token'] = $request -> input('token');

		try {

			if($params['newpassword'] == '' || $params['newpassword'] == null)
			{
				throw new \Exception("Please enter your new password");
			}

			if($params['email'] == '' || $params['email'] == null)
			{
				throw new \Exception("Invalid request");
			}

			if($params['token'] == '' || $params['token'] == null)
			{
				throw new \Exception("Invalid request");
			}

			if(strlen($params['newpassword']) < 6)
			{
				throw new \Exception("Password must be at least 6 characters long");
			}

			$request_details = $this -> manager -> passwordResetRequestDetails($params);

			if($request_details === null)
			{
				throw new \Exception("Invalid request");
			}

			if(strtotime($request_details -> created_at) + 86400 < strtotime(date('Y-m-d H:i:s')))
			{
				throw new \Exception("Reset password request is already expired. Please request another one");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;
	}
}