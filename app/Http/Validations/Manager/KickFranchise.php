<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Auth;

class KickFranchise{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}


	public function validate($request)
	{
		$params['franchise_id'] = $request -> input('franchise_id');
		$params['league_id'] = $request -> input('league_id');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			$auth_user = Auth::user();
			$league_details = $this -> league -> details($params['league_id']);

			if($league_details -> ready)
			{
				throw new \Exception("League already started");
			}

			if($auth_user -> id != $league_details -> host)
			{
				throw new \Exception("Only league host can kick a team");
			}

			if($params['franchise_id'] == $auth_user -> franchise -> id)
			{
				throw new \Exception("You cannot kick your own team");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;
	}
}