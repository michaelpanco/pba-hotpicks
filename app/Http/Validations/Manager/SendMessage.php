<?php namespace App\Http\Validations\Manager;

use Validator;

class SendMessage{

	public function validate($request)
	{
		$override_messages = [
			'recipient.required' => 'Invalid message recipient',
			'conversation_id.required' => 'Invalid message',
			'message.required' => 'Sending failed, your message is empty',
		];
		$validation = Validator::make($this -> whitefields($request), $this -> rules(), $override_messages);
		return $validation;
	}

	public function whitefields($request)
	{
		$valid = [
			'recipient',
			'conversation_id',
			'message',
		];

		return $request -> only($valid);
	}

	public function finalfields($request)
	{
		$data = $this -> whitefields($request);
    	return $data;
	}

	public function rules()
	{
		return [
			'recipient' => 'required',
	    	'conversation_id' => 'required',
	    	'message' => 'required',
		];
	}

}