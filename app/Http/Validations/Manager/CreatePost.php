<?php namespace App\Http\Validations\Manager;

use Auth;

class CreatePost{

	public function validate($request)
	{
		$params['channel_id'] = $request -> input('channel_id');
		$params['content'] = $request -> input('content');

		try {

			if($params['channel_id'] == '' || $params['channel_id'] == null){
				throw new \Exception('Invalid post entry. Please try again');
			}

			if($params['content'] == '' || $params['content'] == null){
				throw new \Exception('Please type any message at status box');
			}

			$restrain_date = Auth::user() -> restrain_until;

			if($restrain_date != '')
			{
				if(date('Y-m-d') < $restrain_date)
				{
					throw new \Exception('<div class="text-center cred" style="padding: 15px 0;" >You are not allowed to create a post until ' . date('F j, Y', strtotime($restrain_date)) . '</div>');
				}
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}