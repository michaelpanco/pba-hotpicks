<?php namespace App\Http\Validations\Manager;

use App\Repositories\PostRepository;
use Auth;

class UpVotePost{

	protected $post;

	public function __construct(PostRepository $post)
	{
		$this -> post = $post;
	}

	public function validate($request)
	{
		$params['manager_id'] = Auth::user() -> id;
		$params['post_id'] = $request -> input('post_id');

		if($this -> post -> canUpVote($params)){
			$response = array('fails' => false, 'message' => '0');
		}else{
			$response = array('fails' => true, 'message' => 'You already upvote this post');
		}

		return $response;
	}
}