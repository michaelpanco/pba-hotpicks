<?php namespace App\Http\Validations\Manager;

use Auth;

class RestrainManager{

	public function validate($request)
	{
		$params['no_of_days'] = $request -> input('no_of_days');
		$params['restrain_reason'] = $request -> input('restrain_reason');

		try {

			if($params['no_of_days'] == '' || $params['no_of_days'] == null){
				throw new \Exception('Please choose the no. of days to restrain');
			}
			if($params['restrain_reason'] == '' || $params['restrain_reason'] == null){
				throw new \Exception('Please enter the reason of restraining the manager');
			}

			if(!in_array($params['no_of_days'], [1,3,7,30])){
				throw new \Exception('Invalid no. of restrain days');
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}