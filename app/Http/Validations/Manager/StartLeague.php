<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Setting;
use Auth;

class StartLeague{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function validate($request)
	{
		$params['league_id'] = $request -> input('league_id');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			$auth_user = Auth::user();
			$league_details = $this -> league -> details($params['league_id']);

			if($league_details -> ready)
			{
				throw new \Exception("League already started");
			}

			if($auth_user -> id != $league_details -> host)
			{
				throw new \Exception("Only league host can start league");
			}

			$minimum_league_participants = Setting::get('MINIMUM_LEAGUE_PARTICIPANTS');
			$league_participants = $league_details -> participants -> count();

			if($league_participants < $minimum_league_participants){
				throw new \Exception("League particpants must be minimum of " . $minimum_league_participants);
			}

			$maximum_league_participants = Setting::get('MAXIMUM_LEAGUE_PARTICIPANTS');
			
			if($league_participants > $maximum_league_participants){
				throw new \Exception("League particpants must be maximum of " . $maximum_league_participants);
			}

			if(Setting::get('BEGIN_LEAGUE') == 'NO')
			{
				throw new \Exception("Oh wait, Beginning league isn't yet available for now. We will notify you if you can start your league. Thanks");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}