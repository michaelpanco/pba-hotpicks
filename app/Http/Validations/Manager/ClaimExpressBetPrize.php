<?php namespace App\Http\Validations\Manager;

use App\Repositories\ExpressBetRepository;
use Auth;

class ClaimExpressBetPrize{

	protected $express_bet;

	public function __construct(ExpressBetRepository $express_bet)
	{
		$this -> express_bet = $express_bet;
	}

	public function validate($request)
	{
		$params['express_bet_id'] = $request -> input('express_bet_id');

		try {

			$auth_user = Auth::user();
			$entry_bet_details = $this -> express_bet -> entryDetails($params['express_bet_id'], Auth::user() -> id);

			if($entry_bet_details === null){
				throw new \Exception('Invalid request');
			}

			if($entry_bet_details -> claimed == 1)
			{
				throw new \Exception("Amount prize was already claimed");
			}

			if($entry_bet_details -> bet != $entry_bet_details -> details -> won)
			{
				throw new \Exception("Invalid requests");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}