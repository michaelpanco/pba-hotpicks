<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Setting;
use Auth;

class JoinLeague{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function validate($request)
	{
		$params['league_id'] = $request -> input('league_id');
		$params['league_password'] = $request -> input('league_password');

		try {

			$auth_user = Auth::user();

			if($auth_user -> franchise == null)
			{
				throw new \Exception("You don't have a team to join this league");
			}

			if($auth_user -> franchise -> league)
			{
				throw new \Exception("You already have a league");
			}

			$league_details = $this -> league -> details($params['league_id']);
			$league_participants = $league_details -> participants;

			if($league_details -> ready)
			{
				throw new \Exception("League already started");
			}

			$league_password = $league_details -> password;

			if($league_password)
			{
				if($params['league_password'] == ""){
					throw new \Exception("Enter league password");
				}
				if($league_password != $params['league_password']){
					throw new \Exception("Wrong league password");
				}
				
			}		

			$league_participants = $league_details -> participants -> count();
			$maximum_league_participants = Setting::get('MAXIMUM_LEAGUE_PARTICIPANTS');
			
			if($league_participants >= $maximum_league_participants){
				throw new \Exception("League is full, Only " . $maximum_league_participants . " teams are allowed to participate");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}