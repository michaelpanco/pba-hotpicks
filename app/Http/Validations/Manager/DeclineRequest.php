<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;

class DeclineRequest{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['manager_id'] = $request -> input('manager_id');
		$hasRequest = $this -> manager -> hasRequest($params['manager_id']);

		if($hasRequest)
		{
			$response['fails'] = false;
			$response['message'] = "0";
			
		}else{
			$response['fails'] = true;
			$response['message'] = "Invalid request approval";
		}

		return $response;

	}
}