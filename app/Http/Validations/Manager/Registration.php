<?php namespace App\Http\Validations\Manager;

use Validator;
use Hash;

class Registration{

	public function validate($request)
	{
		$recaptcha_security = [
			'recaptcha.required' => 'Please confirm reCAPTCHA anti-spam security.',
			'recaptcha.captcha' => 'Invalid reCAPTCHA verification. Please refresh the page and try again.',
		];
		$registration_rules = $this -> rules();

		if (app() -> environment() == 'testing') {
			$recaptcha_security = [];
			$registration_rules = array_except($registration_rules, ['recaptcha']);
		}

		$validation = Validator::make($this -> whitefields($request), $registration_rules, $recaptcha_security);
		return $validation;
	}

	public function whitefields($request)
	{
		$valid = [
			'firstname', 
			'lastname', 
			'password', 
			'password_confirmation', 
			'gender', 
			'email', 
			'birth_month', 
			'birth_day', 
			'birth_year',
			'recaptcha',
		];

		return $request -> only($valid);
	}

	public function finalfields($request)
	{
		$data = $this -> whitefields($request);

		$data['password'] = Hash::make($request['password']);
		$data['gender'] = substr($request['gender'], 0, 1);
		$data['birthday'] = $request['birth_year'] . '-' . $request['birth_month'] . '-' . $request['birth_day'];

    	unset($data['password_confirmation']);
    	unset($data['birth_month']);
    	unset($data['birth_day']);
    	unset($data['birth_year']);
    	unset($data['recaptcha']);

    	return $data;
	}

	public function rules()
	{
		return [
	    	'firstname' => 'required|alpha|min:2',
	    	'lastname' => 'required|alpha|min:2',
	    	'password' => 'required|min:6|confirmed',
	    	'password_confirmation' => 'required|min:6',
	    	'gender' => 'required|in:male,female',
            'email' => 'required|email|unique:managers,email',
            'birth_month' => 'required',
            'birth_day' => 'required',
            'birth_year' => 'required',
            'recaptcha' => 'required|captcha'
		];
	}

}