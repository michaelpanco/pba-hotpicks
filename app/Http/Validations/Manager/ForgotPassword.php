<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;

class ForgotPassword{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['email'] = $request -> input('email');

		try {

			if($params['email'] == '' || $params['email'] == null)
			{
				throw new \Exception("Please enter your email address");
			}

			if(filter_var($params['email'], FILTER_VALIDATE_EMAIL) === false) {
				throw new \Exception("Invalid email address");
			}

			if(!$this -> manager -> checkEmailExistence($params['email']))
			{
				throw new \Exception("Invalid request");
			}

			if(!$this -> manager -> ForgotPasswordTimeValid($params['email']))
			{
				throw new \Exception("You can only request forgot password every 10 minutes");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}