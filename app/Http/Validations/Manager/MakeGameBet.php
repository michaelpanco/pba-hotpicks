<?php namespace App\Http\Validations\Manager;

use App\Repositories\ExpressBetRepository;
use Auth;

class MakeGameBet{

	protected $express_bet;

	public function __construct(ExpressBetRepository $express_bet)
	{
		$this -> express_bet = $express_bet;
	}

	public function validate($request)
	{
		$params['express_bet_id'] = $request -> input('express_bet_id');
		$params['bet'] = $request -> input('bet');
		$params['amount'] = $request -> input('amount');

		try {

			$auth_user = Auth::user();
			$express_bet_details = $this -> express_bet -> details($params['express_bet_id']);

			if($params['bet'] == '' || $params['bet'] == null){
				throw new \Exception('Please select your bet for this item');
			}

			if($params['amount'] < 100)
			{
				throw new \Exception("Invalid bet amount, minimum bet amount is 100");
			}

			if($params['amount'] > $express_bet_details -> limit)
			{
				throw new \Exception("Invalid bet amount, maximum bet amount is " . $express_bet_details -> limit);
			}

			if($express_bet_details === null)
			{
				throw new \Exception("Express bet not exist");
			}

			if(strtotime($express_bet_details -> expiry) < strtotime(date('Y-m-d H:i:s')))
			{
				throw new \Exception("Express bet for this item already expired");
			}

			if($auth_user -> credits < $params['amount'])
			{
				throw new \Exception("Your credit is not enough to make this bet");
			}

			if($this -> express_bet -> exist($params['express_bet_id'], $auth_user -> id))
			{
				throw new \Exception("You already made a bet for this item");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}