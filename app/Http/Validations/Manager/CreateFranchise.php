<?php namespace App\Http\Validations\Manager;

use App\Repositories\FranchiseRepository;
use App\Repositories\PlayerRepository;
use Setting;
use Auth;

class CreateFranchise{

	protected $franchise;

	public function __construct(FranchiseRepository $franchise, PlayerRepository $player)
	{
		$this -> franchise = $franchise;
		$this -> player = $player;

	}

	public function validate($request)
	{
		$params['name'] = $request -> input('name');
		$params['logo'] = $request -> input('logo');
		$params['picks'] = $request -> input('picks');

		try {

			if(Auth::check() && Auth::user() -> franchise){
				throw new \Exception("You already have a team, you can't create another one");
			}

			if($params['name'] == '' || $params['name'] == null){
				throw new \Exception('Please enter the name of your team');
			}
			if(strlen($params['name']) < 5){
				throw new \Exception('Team name must be at least 5 characters long');
			}
 			if(strlen($params['name']) > 26){
				throw new \Exception('Team name must not exceed 26 characters');
			}
			if(!preg_match('/^[a-z0-9 ]+$/i', $params['name'])){
				throw new \Exception('Team name must not contain special characters');
			}
			if($params['logo'] == '' || $params['logo'] == null){
				throw new \Exception('Please choose your team logo');
			}
			if(!is_numeric($params['logo'])){
				throw new \Exception('Invalid team logo');
			}
			if(!$this -> franchise -> logoFree($params['logo'])){
				throw new \Exception('Team logo is not available');
			}
			if(count($params['picks']) != 10){
				throw new \Exception('Please complete your 10 player lineup team');
			}

			$pick_players = $this -> player -> playersDetails($params['picks']);

			$position_must = [
					0 => [1,12],
					1 => [1,12],
					2 => [12,2,23],
					3 => [12,2,23],
					4 => [23,3,34],
					5 => [23,3,34],
					6 => [34,4,45],
					7 => [34,4,45],
					8 => [45,5],
					9 => [45,5]
				];

			$i = 0;

			foreach($params['picks'] as $pick){
				$player = $pick_players -> where('id', (int)$pick) -> first();
				if(!in_array((int)$player -> position, $position_must[$i])){
					throw new \Exception("Invalid line up position");
					break;
				}
				$i++;
			}

			if($pick_players -> sum('salary') > Setting::get('INITIAL_SALARY_CAP')){
				throw new \Exception("You've reached the salary limit. Please adjust your picks");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}