<?php namespace App\Http\Validations\Manager;

use App\Repositories\ProductRepository;
use Auth;

class PurchaseProduct{

	protected $product;

	public function __construct(ProductRepository $product)
	{
		$this -> product = $product;
	}

	public function validate($request)
	{
		$params['item_id'] = $request -> input('item_id');

		try {

			$auth_user = Auth::user();
			$product_details = $this -> product -> details($params['item_id']);

			if($product_details === null){
				throw new \Exception('Invalid request');
			}

			if($auth_user -> credits < $product_details -> price){
				throw new \Exception('Sorry, You dont have enough credits to purchase this item');
			}

			if($product_details -> type == 'BDGE')
			{
				$my_badges = $auth_user -> badges -> where('id', (int)$params['item_id']);
				if($my_badges -> count() > 0){
					throw new \Exception('Badge already existed in your account');
				}
			}

			if($product_details -> type == 'FICO')
			{
				$logo_id = $auth_user -> franchise -> logo_id;

				if($logo_id == $product_details -> product_id){
					throw new \Exception('You currently have this franchise icon');
				}
			}

			$response['fails'] = false;
			$response['message'] = 'Congratulations, Product was purchased successfully';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}