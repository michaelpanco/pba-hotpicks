<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Auth;

class CreateLeague{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function validate($request)
	{
		$params['name'] = $request -> input('name');
		$params['description'] = $request -> input('description');
		$params['password'] = $request -> input('password');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			if($params['name'] == '' || $params['name'] == null){
				throw new \Exception('Please enter your league name');
			}

			if(!preg_match('/^[a-zA-Z0-9 ]+$/i', $params['name'])){
				throw new \Exception('League name must not contain special characters');
			}

			if(mb_strlen($params['description']) < 6)
			{
				throw new \Exception("League description is too short");
			}

			if(mb_strlen($params['description']) > 255)
			{
				throw new \Exception("League description exceeds, only maximum of 255 characters allowed");
			}

			if(mb_strlen($params['password']) > 16)
			{
				throw new \Exception("League password exceeds, only maximum of 16 characters allowed");
			}

			$auth_user = Auth::user();

			if($auth_user -> franchise == null)
			{
				throw new \Exception("You don't have a team to create a league");
			}

			if($auth_user -> franchise -> league != null)
			{
				throw new \Exception("You are currently affiliated in a league.");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}