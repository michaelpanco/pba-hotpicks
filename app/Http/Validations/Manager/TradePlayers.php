<?php namespace App\Http\Validations\Manager;

use App\Repositories\PlayerRepository;
use App\Repositories\FranchiseRepository;
use Auth;
use Setting;

class TradePlayers{

	protected $player;
	protected $franchise;

	public function __construct(PlayerRepository $player, FranchiseRepository $franchise)
	{
		$this -> player = $player;
		$this -> franchise = $franchise;
	}

	public function validate($request)
	{
		$params['give'] = $request -> input('give');
		$params['get'] = $request -> input('get');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}
			
			if($params['give'] == '' || $params['give'] == null || $params['get'] == '' || $params['get'] == null)
			{
				throw new \Exception('Incomplete request data');
			}

			$auth_user = Auth::user();
			$manager_credits = $auth_user -> credits;
			$franchise_id = $auth_user -> franchise -> id;

			$franchise_players = $this -> franchise -> players($franchise_id);

			$date_hired = $franchise_players -> where('player_id', (int) $params['give']) -> first()-> date_hired;

			$date_hire_obj = new \DateTime($date_hired);
			$date_now_obj = new \DateTime(date('Y-m-d'));
			$interval = $date_hire_obj -> diff($date_now_obj);
			
			if($interval -> days < 10)
			{
				throw new \Exception('You are only allowed to trade player after 10 days from the date when the player was hired.');
			}

			if($franchise_players -> where('player_id', (int) $params['give']) -> count() < 1)
			{
				throw new \Exception('Player not exist in franchise');
			}

			if($franchise_players -> where('player_id', (int) $params['get']) -> count() > 0)
			{
				throw new \Exception('Selected player already part of your team');
			}

			$give_player_position = $franchise_players -> where('player_id', (int) $params['give']) -> first() -> position;

			$get_player_details = $this -> player -> detailsByID((int) $params['get']);

			$get_player_position = $get_player_details -> position;

			if($this -> positionMatch($give_player_position, $get_player_position) == false)
			{
				throw new \Exception('Players position does not match');
			}
			
			$give_player_salary = $this -> player -> salary($params['give']);
			$get_player_salary = $get_player_details -> salary;

			if($give_player_salary < $get_player_salary)
			{
				$difference = $get_player_salary - $give_player_salary;

				if($difference > $manager_credits){
					throw new \Exception("You don't have enough credits to perform this trade.");
				}
			}

			if(Setting::get('ALLOW_TRADE') == 'NO')
			{
				throw new \Exception("Sorry, Trading players only allowed during elimination round.");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}

	private function positionMatch($give_position, $get_position)
	{
		switch($give_position){

			case 1:
			return (in_array($get_position, [1,12]) ? true : false);
			break;

 			case 2:
			return (in_array($get_position, [12,2,23]) ? true : false);
			break;

			case 3:
			return (in_array($get_position, [23,3,34]) ? true : false);
			break;

			case 4:
			return (in_array($get_position, [34,4,45]) ? true : false);
			break;

			case 5:
			return (in_array($get_position, [45,5]) ? true : false);
			break;
		}

		return false;
	}


}