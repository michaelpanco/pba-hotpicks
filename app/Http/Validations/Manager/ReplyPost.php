<?php namespace App\Http\Validations\Manager;

use App\Repositories\PostRepository;
use Auth;

class ReplyPost{

	protected $post;

	public function __construct(PostRepository $post)
	{
		$this -> post = $post;
	}

	public function validate($request)
	{
		$params['channel_id'] = $request -> input('channel_id');
		$params['content'] = $request -> input('content');
		$params['parent_post'] = $request -> input('parent_post');

		try {

			if($params['channel_id'] == '' || $params['channel_id'] == null){
				throw new \Exception('Invalid post entry. Please try again');
			}

			if($params['content'] == '' || $params['content'] == null){
				throw new \Exception('Please type any message at status box');
			}

			if($params['parent_post'] == '' || $params['parent_post'] == null){
				throw new \Exception('Invalid post entry. Please try again');
			}

			$restrain_date = Auth::user() -> restrain_until;

			if($restrain_date != '')
			{
				if(date('Y-m-d') < $restrain_date)
				{
					throw new \Exception('<div class="text-center cred" style="padding: 15px 0;" >You are not allowed to create a post until ' . date('F j, Y', strtotime($restrain_date)) . '</div>');
				}
			}

			$parent_reply_count = $this -> post -> replyCount((int)$params['parent_post']);

			if($parent_reply_count > 29)
			{
				throw new \Exception('<div class="text-center cred" style="padding: 15px 0;" >Each post can only have 30 replies</div>');
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}