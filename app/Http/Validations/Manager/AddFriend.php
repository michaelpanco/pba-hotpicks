<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;
use Auth;

class AddFriend{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['account_id'] = Auth::user() -> id;
		$params['manager_id'] = $request -> input('manager_id');

		if($params['account_id'] == $params['manager_id'])
		{
			$response['fails'] = true;
			$response['message'] = "Invalid friend request, you cannot add yourself";

			return $response;
		}

		switch ($this -> manager -> managerRelationship($params)) {

			case 'pending':

				$response['fails'] = true;
				$response['message'] = "Friend request still waiting for response";

				return $response;

				break;

			case 'friend':

				$response['fails'] = true;
				$response['message'] = "Invalid friend request, you're already friends with this person";

				return $response;

				break;

			default:

				$response['fails'] = false;
				$response['message'] = "0";

				return $response;

				break;
		}


	}
}