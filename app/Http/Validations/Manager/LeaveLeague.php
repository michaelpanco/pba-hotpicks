<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Auth;

class LeaveLeague{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function validate($request)
	{
		$params['league_id'] = $request -> input('league_id');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			$auth_user = Auth::user();

			if($auth_user -> franchise -> league -> id != $params['league_id'])
			{
				throw new \Exception("You're not a member of this league");
			}

			if(!$this -> league -> isOpen($params['league_id']))
			{
				throw new \Exception("You can't leave league while still on-going.");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}