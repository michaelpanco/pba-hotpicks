<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;
use Auth;
use Hash;

class ChangePassword{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['current_password'] = $request -> input('current_password');
		$params['new_password'] = $request -> input('new_password');
		$params['confirm_password'] = $request -> input('confirm_password');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			if($params['current_password'] == '' || $params['current_password'] == null){
				throw new \Exception('Please enter your current password');
			}

			if($params['new_password'] == '' || $params['new_password'] == null){
				throw new \Exception('Please enter your new password');
			}

			if(mb_strlen($params['new_password']) < 6)
			{
				throw new \Exception("New password must be at least 6 characters long");
			}

			if (!Hash::check($params['current_password'], Auth::user()->password))
			{ 
			    throw new \Exception("Your current password is incorrect");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}