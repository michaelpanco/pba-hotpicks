<?php namespace App\Http\Validations\Manager;

use App\Repositories\ManagerRepository;
use Auth;

class ChangeFavorites{

	protected $manager;

	public function __construct(ManagerRepository $manager)
	{
		$this -> manager = $manager;
	}

	public function validate($request)
	{
		$params['team'] = $request -> input('team');
		$params['player'] = $request -> input('player');
		$params['team2'] = $request -> input('team2');
		$params['matchup1'] = $request -> input('match1');
		$params['matchup2'] = $request -> input('match2');

		try {

			if($params['team'] != "" && !is_numeric($params['team'])){
				throw new \Exception('Invalid favorite team request');
			}

			if($params['player'] != "" && !is_numeric($params['player'])){
				throw new \Exception('Invalid favorite player request');
			}

			if(!$params['team2'] != "" && is_numeric($params['team2'])){
				throw new \Exception('Invalid 2nd favorite team request');
			}

			if($params['matchup1'] == "" && $params['matchup1'] != ""){
				throw new \Exception('Invalid matchup');
			}

			if($params['matchup2'] == "" && $params['matchup2'] != ""){
				throw new \Exception('Invalid matchup');
			}

			if($params['matchup1'] != "" || $params['matchup2'] != ""){
				if($params['matchup1'] != '999'){
					if($params['matchup1'] == $params['matchup2']){
						throw new \Exception('Teams in matchup must be different');
					}
				}
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}