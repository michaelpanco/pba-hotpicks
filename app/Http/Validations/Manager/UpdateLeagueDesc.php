<?php namespace App\Http\Validations\Manager;

use App\Repositories\LeagueRepository;
use Auth;

class UpdateLeagueDesc{

	protected $league;

	public function __construct(LeagueRepository $league)
	{
		$this -> league = $league;
	}

	public function validate($request)
	{
		$params['league_id'] = $request -> input('league_id');
		$params['league_desc'] = $request -> input('league_desc');

		try {

			if(!Auth::check())
			{
				throw new \Exception("Sorry, You are not allowed to perform this action");
			}

			if(mb_strlen($params['league_desc']) < 6)
			{
				throw new \Exception("League description is too short");
			}

			$auth_user = Auth::user();

			if($auth_user -> id != $this -> league -> host($params['league_id']))
			{
				throw new \Exception("Unable to update league description");
			}

			$response['fails'] = false;
			$response['message'] = '0';

		} catch (\Exception $e) {
			$response['fails'] = true;
			$response['message'] = $e -> getMessage();
		}

		return $response;

	}
}