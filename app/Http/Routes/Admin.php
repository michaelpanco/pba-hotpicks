<?php

$admin_slug = env('ADMIN_SLUG');

Route::get($admin_slug . '/ptp-admin', ['as' => 'admin_index', 'uses' => 'AdminController@index']);


Route::group(['middleware' => 'administer'], function() use($admin_slug)
{
	Route::get($admin_slug . '/dashboard', ['as' => 'admin_dashboard', 'uses' => 'AdminController@dashboard']);
	Route::get($admin_slug . '/teams', ['as' => 'admin_teams', 'uses' => 'AdminController@pbaTeams']);
	Route::get($admin_slug . '/teams/{team_id}', ['as' => 'admin_team_profile', 'uses' => 'AdminController@teamProfile']);
	Route::get($admin_slug . '/players', ['as' => 'admin_players', 'uses' => 'AdminController@pbaPlayers']);
	Route::get($admin_slug . '/players/{player_id}', ['as' => 'admin_player_profile', 'uses' => 'AdminController@playerProfile']);
	Route::get($admin_slug . '/games', ['as' => 'admin_games', 'uses' => 'AdminController@pbaGames']);
	Route::get($admin_slug . '/games/{game_id}', ['as' => 'admin_game_details', 'uses' => 'AdminController@pbaGameDetails']);
	Route::get($admin_slug . '/games/result/{game_id}', ['as' => 'admin_game_result', 'uses' => 'AdminController@pbaGameResult']);
	Route::get($admin_slug . '/managers', ['as' => 'admin_managers', 'uses' => 'AdminController@managers']);
	Route::get($admin_slug . '/managers/{manager_id}', ['as' => 'admin_manager_details', 'uses' => 'AdminController@managerDetails']);
	Route::get($admin_slug . '/leagues', ['as' => 'admin_leagues', 'uses' => 'AdminController@leagues']);
	Route::get($admin_slug . '/leagues/{league_id}', ['as' => 'admin_league_details', 'uses' => 'AdminController@leagueDetails']);

	Route::get($admin_slug . '/expressbets', ['as' => 'admin_expressbets', 'uses' => 'AdminController@expressBets']);
	Route::get($admin_slug . '/expressbets/{expressbet_id}', ['as' => 'admin_expressbet_details', 'uses' => 'AdminController@expressBetDetails']);
	Route::get($admin_slug . '/products', ['as' => 'admin_products', 'uses' => 'AdminController@products']);
	Route::get($admin_slug . '/products/{product_id}', ['as' => 'admin_product_details', 'uses' => 'AdminController@productDetails']);
	Route::get($admin_slug . '/announcements', ['as' => 'admin_announcements', 'uses' => 'AdminController@announcements']);
	Route::get($admin_slug . '/announcements/{announcement_id}', ['as' => 'admin_announcement_details', 'uses' => 'AdminController@announcementDetails']);
	Route::get($admin_slug . '/disciplinaries', ['as' => 'admin_disciplinaries', 'uses' => 'AdminController@disciplinaries']);
	Route::get($admin_slug . '/settings', ['as' => 'admin_settings', 'uses' => 'AdminController@settings']);

	Route::get($admin_slug . '/dashboard/new-managers', ['as' => 'admin_new_managers_lists', 'uses' => 'AdminController@newManagers']);
	Route::get($admin_slug . '/dashboard/active-managers', ['as' => 'admin_active_managers_lists', 'uses' => 'AdminController@activeManagers']);
	Route::get($admin_slug . '/dashboard/new-posts', ['as' => 'admin_new_posts_lists', 'uses' => 'AdminController@newPosts']);
	Route::get($admin_slug . '/dashboard/new-leagues', ['as' => 'admin_new_leagues_lists', 'uses' => 'AdminController@newLeagues']);

	Route::get($admin_slug . '/proxy_login/{manager_id}', ['as' => 'proxy_login', 'uses' => 'AdminController@proxyLogin']);

	Route::get($admin_slug . '/logout', ['as' => 'admin_logout', 'uses' => 'AdminController@logout']);
});