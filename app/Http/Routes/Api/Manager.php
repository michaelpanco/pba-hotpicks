<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('register', 'Api\Manager\ManagerController@register');
Route::post('login', 'Api\Manager\ManagerController@login');
Route::post('tmpCreateTeam', 'Api\Manager\ManagerController@tmpCreateTeam');
Route::post('createTeam', 'Api\Manager\ManagerController@createTeam');
Route::post('forgotPassword', 'Api\Manager\ManagerController@forgotPassword');
Route::post('resetPassword', 'Api\Manager\ManagerController@resetPassword');

/* PRIVATE ROUTES */

Route::group(['middleware' => 'auth'], function()
{
	// POST

	Route::post('getChannel', 'Api\Manager\PostController@getChannel');
	Route::post('getMorePosts', 'Api\Manager\PostController@getMorePosts');
	Route::post('createPost', 'Api\Manager\PostController@createPost');
	Route::post('replyPost', 'Api\Manager\PostController@replyPost');
	Route::post('fetchReplies', 'Api\Manager\PostController@fetchReplies');
	Route::post('upVotePost', 'Api\Manager\PostController@upVote');

	// MANAGER ACCOUNT

	Route::post('addFriend', 'Api\Manager\ManagerController@addFriend');
	Route::post('approveRequest', 'Api\Manager\ManagerController@approveRequest');
	Route::post('declineRequest', 'Api\Manager\ManagerController@declineRequest');
	Route::get('fetchFriends', 'Api\Manager\ManagerController@fetchFriends');
	Route::get('fetchMessages', 'Api\Manager\ManagerController@fetchMessages');
	Route::get('fetchConversations/{manager_slug}', 'Api\Manager\ManagerController@fetchConversations');
	Route::post('getMoreConversations', 'Api\Manager\ManagerController@getMoreConversations');
	Route::post('markMessageRead', 'Api\Manager\ManagerController@markMessageRead');
	Route::post('sendMessage', 'Api\Manager\ManagerController@sendMessage');
	Route::post('changePassword', 'Api\Manager\ManagerController@changePassword');
	Route::post('changeFavorites', 'Api\Manager\ManagerController@changeFavorites');
	Route::post('uploadAvatar', 'Api\Manager\ManagerController@uploadAvatar');
	Route::post('manageBadges', 'Api\Manager\ManagerController@manageBadges');
	Route::post('makeGameBet', 'Api\Manager\ManagerController@makeGameBet');
	Route::post('claimExpressBetPrize', 'Api\Manager\ManagerController@claimExpressBetPrize');
	Route::post('purchaseProduct', 'Api\Manager\ManagerController@purchaseProduct');

	Route::post('claimReferalPrize', 'Api\Manager\ManagerController@claimReferalPrize');
	Route::post('openGift', 'Api\Manager\ManagerController@openGift');

	// NOTIFICATIONS

	Route::post('notifier', 'Api\Manager\NotificationController@notifier');
	Route::post('fetchNotifications', 'Api\Manager\NotificationController@fetchNotifications');
	Route::post('fetchLatestMessages', 'Api\Manager\NotificationController@fetchLatestMessages');
	Route::post('fetchRequests', 'Api\Manager\NotificationController@fetchRequests');

	// FRANCHISE

	Route::post('tradePlayers', 'Api\Manager\ManagerController@tradePlayers');

	// LEAGUE

	Route::post('joinLeague', 'Api\Manager\LeagueController@join');
	Route::post('leaveLeague', 'Api\Manager\LeagueController@leave');
	Route::post('dismissLeague', 'Api\Manager\LeagueController@dismiss');
	Route::post('updateLeague', 'Api\Manager\LeagueController@update');
	Route::post('updateLeagueDesc', 'Api\Manager\LeagueController@updateLeagueDesc');
	Route::post('startLeague', 'Api\Manager\LeagueController@start');
	Route::post('kickTeam', 'Api\Manager\LeagueController@kick');
	Route::post('updateLeaguePassword', 'Api\Manager\LeagueController@updateLeaguePassword');
	Route::post('createLeague', 'Api\Manager\LeagueController@createLeague');
	
	// STATE

	Route::post('flagOnline', 'Api\Manager\ManagerController@flagOnline');

});

Route::group(['middleware' => 'peacekeeper'], function()
{
	Route::post('deletePost', 'Api\Manager\PostController@deletePost');
	Route::post('restrainManager', 'Api\Manager\ManagerController@restrainManager');
});