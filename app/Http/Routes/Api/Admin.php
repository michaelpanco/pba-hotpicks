<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('login', 'Api\Admin\AdminController@login');
Route::get('updateMostSignedPlayers', 'Api\Admin\AdminController@updateMostSignedPlayers');
Route::get('topManagers', 'Api\Admin\AdminController@topManagers');

Route::group(['middleware' => 'administer'], function()
{
	Route::post('updateTeamProfile', 'Api\Admin\AdminController@updateTeamProfile');
	Route::post('updatePlayerProfile', 'Api\Admin\AdminController@updatePlayerProfile');
	Route::post('createGame', 'Api\Admin\AdminController@createGame');
	Route::post('createPlayer', 'Api\Admin\AdminController@createPlayer');
	Route::post('updateGameDetails', 'Api\Admin\AdminController@updateGameDetails');
	Route::post('createGamePerformance', 'Api\Admin\AdminController@createGamePerformance');
	Route::post('publishGameResult', 'Api\Admin\AdminController@publishGameResult');
	Route::post('updateManagerProfile', 'Api\Admin\AdminController@updateManagerProfile');
	Route::post('updateLeagueProfile', 'Api\Admin\AdminController@updateLeagueProfile');
	Route::post('updateExpressBet', 'Api\Admin\AdminController@updateExpressBet');
	Route::post('createExpressBet', 'Api\Admin\AdminController@createExpressBet');
	Route::post('createProduct', 'Api\Admin\AdminController@createProduct');
	Route::post('updateProduct', 'Api\Admin\AdminController@updateProduct');
	Route::post('createAnnouncement', 'Api\Admin\AdminController@createAnnouncement');
	Route::post('updateAnnouncement', 'Api\Admin\AdminController@updateAnnouncement');
	Route::post('deleteAnnouncement', 'Api\Admin\AdminController@deleteAnnouncement');
	Route::post('updateSettings', 'Api\Admin\AdminController@updateSettings');
	Route::post('updateStatistics', 'Api\Admin\AdminController@updateStatistics');
});
