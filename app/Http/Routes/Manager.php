<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* PUBLIC ROUTES */

Route::get('/', ['as' => 'home', 'uses' => 'MainController@index']);
Route::get('register', ['as' => 'register', 'uses' => 'MainController@register']);
Route::get('how-to-play', 'MainController@howToPlay');
Route::get('facebook/callback', 'MainController@facebookCallback');

/* PUBLIC SERVICE ACTION */

Route::get('confirm/manager/{manager_id}/confirmation/{confirmation_code}', ['as' => 'confirm_email', 'uses' => 'MainController@verifyEmail']);
Route::get('create-your-team', ['as' => 'create_franchise', 'uses' => 'MainController@createFranchise']);
Route::get('leagues', ['as' => 'leagues', 'uses' => 'MainController@leagues']);
Route::get('leagues/{league_id}', ['as' => 'league_profile', 'uses' => 'MainController@leagueProfile']);
Route::get('manager/{manager_slug}', ['as' => 'profile', 'uses' => 'AccountController@managerProfile']);
Route::get('franchises/{franchise_id}', ['as' => 'franchise_profile', 'uses' => 'MainController@franchiseProfile']);
Route::get('teams', ['as' => 'teams', 'uses' => 'MainController@teams']);
Route::get('teams/{team_slug?}', ['as' => 'team_profile', 'uses' => 'MainController@teamProfile']);
Route::get('players', ['as' => 'players', 'uses' => 'MainController@players']);
Route::get('players/{player_slug}', ['as' => 'player_profile', 'uses' => 'MainController@playerProfile']);
Route::get('games/{team_slug?}/{outcome?}', ['as' => 'games', 'uses' => 'MainController@games']);
Route::get('game/result/{game_id}', ['as' => 'game_result', 'uses' => 'MainController@gameResult']);
Route::get('rules', ['as' => 'how_to_play', 'uses' => 'MainController@rules']);
Route::get('password-reset', ['as' => 'password_reset', 'uses' => 'MainController@passwordReset']);
Route::get('privacy', ['as' => 'privacy', 'uses' => 'MainController@privacy']);
Route::get('terms-and-condition', ['as' => 'terms_and_condition', 'uses' => 'MainController@termsAndCondition']);
Route::get('disclaimer', ['as' => 'disclaimer', 'uses' => 'MainController@disclaimer']);
Route::get('contact', ['as' => 'contact', 'uses' => 'MainController@contact']);

Route::get('top-50-active-managers', ['as' => 'top_50_managers', 'uses' => 'MainController@top_50']);
Route::get('richest-50-active-managers', ['as' => 'richest_50_managers', 'uses' => 'MainController@richest_50']);

/* PRIVATE ROUTES */

Route::group(['middleware' => 'auth'], function()
{
	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'AccountController@index']);
	Route::get('messages', ['as' => 'messages', 'uses' => 'AccountController@messages']);
	Route::get('messages/{manager_slug}', ['as' => 'conversation', 'uses' => 'AccountController@conversation']);
	Route::get('friends', ['as' => 'friends', 'uses' => 'AccountController@friends']);
	Route::get('referals', ['as' => 'referals', 'uses' => 'AccountController@referals']);
	Route::get('express-bet', ['as' => 'expressbet', 'uses' => 'AccountController@expressBet']);
	Route::get('shop', ['as' => 'shop', 'uses' => 'AccountController@shop']);
	Route::get('trade/{player_id}', ['as' => 'trade', 'uses' => 'AccountController@trade']);
	Route::get('logout', ['as' => 'logout', 'uses' => 'AccountController@logout']);
	Route::get('settings', ['as' => 'settings', 'uses' => 'AccountController@settings']);
	
});