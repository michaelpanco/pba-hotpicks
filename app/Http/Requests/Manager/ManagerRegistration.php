<?php namespace App\Http\Requests\Manager;

use App\Http\Requests\Request;
use Validator;
use Hash;

class ManagerRegistration extends Request {


	public function authorize()
	{
		return true;
	}

	public function process($request)
	{
		$validation = Validator::make($this -> whitefields($request), $this -> rules());
		return $validation;
	}

	public function whitefields($request)
	{
		$valid = [
			'firstname', 
			'lastname', 
			'password', 
			'password_confirmation', 
			'gender', 
			'email', 
			'birth_month', 
			'birth_day', 
			'birth_year',
			'recaptcha',
		];

		return $request -> only($valid);
	}

	public function finalfields($request)
	{
		$data = $this -> whitefields($request);

		$data['password'] = Hash::make($request['password']);
		$data['gender'] = substr($request['gender'], 0, 1);
		$data['birthday'] = $request['birth_year'] . '-' . $request['birth_month'] . '-' . $request['birth_day'];

    	unset($data['password_confirmation']);
    	unset($data['birth_month']);
    	unset($data['birth_day']);
    	unset($data['birth_year']);
    	unset($data['recaptcha']);

    	return $data;
	}

	public function rules()
	{
		return [
	    	'firstname' => 'required|min:2',
	    	'lastname' => 'required|min:2',
	    	'password' => 'required|min:6|confirmed',
	    	'password_confirmation' => 'required|min:6',
	    	'gender' => 'required|in:male,female',
            'email' => 'required|email|unique:managers,email',
            'birth_month' => 'required',
            'birth_day' => 'required',
            'birth_year' => 'required',
            'recaptcha' => 'required',
		];
	}

}
