<?php namespace App\Http\Controllers;

use App\Repositories\TeamRepository;
use App\Repositories\PostRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\GameRepository;
use App\Repositories\MessageRepository;
use App\Repositories\ManagerRepository;
use App\Repositories\PerformanceRepository;
use App\Repositories\LeagueRepository;
use App\Repositories\ExpressBetRepository;
use App\Repositories\ProductRepository;
use App\Repositories\AnnouncementRepository;
use App\Repositories\DisciplinaryRepository;
use App\Repositories\SettingRepository;
use App\Repositories\FranchiseRepository;
use Illuminate\Http\Request;
use Auth;
use Redirect;

class AdminController extends Controller {

	public function __construct()
	{
		
	}

	public function index()
	{
		if(Auth::user() && Auth::user()->privilege == 'administrator'){
			return Redirect::to('/' . env('ADMIN_SLUG') . '/dashboard');
		}
		
		return view('admin.login');
	}

	public function dashboard(ManagerRepository $manager, PostRepository $post, LeagueRepository $league, MessageRepository $message)
	{
		$data['new_managers'] = $manager -> newManagers() -> count();
		$data['active_managers'] = $manager -> activeManagers() -> count();
		$data['new_posts'] = $post -> newPosts() -> count();
		$data['new_leagues'] = $league -> newLeagues() -> count();

		$data['total_accounts'] = $manager -> total();
		$data['total_posts'] = $post -> total();
		$data['total_messages'] = $message -> total();
		$data['total_leagues'] = $league -> total();

		$chart_data = [];
		$dates = [];

		for($i = 0; $i < 30; $i++){
			$dates[] = date("Y-m-d", strtotime('-'. $i .' days'));
		}

		foreach($dates as $date)
		{
			$chart_data[$date] = $manager -> getRegisterCountFromDate($date);
		}

		$data['chart_data'] = $chart_data;

		return view('admin.auth.dashboard', $data);
	}

	public function pbaTeams(TeamRepository $team)
	{
		$data['teams'] = $team -> lists();
		return view('admin.auth.pba_teams', $data);
	}

	public function pbaPlayers(PlayerRepository $player, TeamRepository $team)
	{
		$data['players'] = $player -> lists();
		$data['teams'] = $team -> lists();
		return view('admin.auth.pba_players', $data);
	}

	public function teamProfile($team_id, TeamRepository $team)
	{
		$data['team'] = $team -> detailsByID($team_id);
		return view('admin.auth.pba_team_profile', $data);
	}

	public function playerProfile($player_id, PlayerRepository $player, TeamRepository $team)
	{
		$data['player'] = $player -> detailsByID($player_id);
		$data['teams'] = $team -> lists();
		return view('admin.auth.pba_player_profile', $data);
	}

	public function pbaGames(GameRepository $games, TeamRepository $team, Request $request)
	{
		$data['games'] = $games -> schedules($request);
		$data['teams'] = $team -> lists();
		return view('admin.auth.pba_games', $data);
	}

	public function pbaGameDetails($game_id, GameRepository $game, TeamRepository $team)
	{
		$data['details'] = $game -> broadDetails($game_id);
		$data['teams'] = $team -> lists();
		return view('admin.auth.pba_game_details', $data);
	}

	public function pbaGameResult($game_id, GameRepository $game, PerformanceRepository $performance, TeamRepository $team)
	{
		$data['details'] = $game -> broadDetails($game_id);
		$data['results'] = $performance -> result($game_id);

		$data['team_x_players'] = $team -> detailsByID($data['details'] -> team_x_id) -> players -> sortBy('lastname');
		$data['team_y_players'] = $team -> detailsByID($data['details'] -> team_y_id) -> players -> sortBy('lastname');

		$data['team_x_outcome'] =  ($data['details'] -> team_x_id == $data['details'] -> winner) ? 1 : 0;
		$data['team_y_outcome'] =  ($data['details'] -> team_y_id == $data['details'] -> winner) ? 1 : 0;

		return view('admin.auth.pba_game_result', $data);
	}

	public function managers(ManagerRepository $manager, Request $request)
	{
		$data['managers'] = $manager -> lists($request);
		return view('admin.auth.managers', $data);
	}

	public function managerDetails($manager_id, ManagerRepository $manager, FranchiseRepository $franchise_repository)
	{
		
		$data['manager'] = $manager -> details($manager_id);
		$data['team'] = $franchise_repository -> manager($data['manager'] -> id);
		return view('admin.auth.manager_details', $data);
	}

	public function leagues(LeagueRepository $league, Request $request)
	{
		$data['leagues'] = $league -> lists($request, 10);
		return view('admin.auth.leagues', $data);
	}

	public function leagueDetails($league_id, LeagueRepository $league)
	{
		$data['league'] = $league -> details($league_id);
		return view('admin.auth.league_details', $data);
	}

	public function expressBets(ExpressBetRepository $expressBet, Request $request)
	{
		$data['expressbets'] = $expressBet -> lists($request);
		return view('admin.auth.expressbets', $data);
	}

	public function expressBetDetails($expressbet_id, ExpressBetRepository $expressBet)
	{
		$data['expressbet'] = $expressBet -> details($expressbet_id);
		return view('admin.auth.expressbet_details', $data);
	}

	public function products(ProductRepository $product)
	{
		$data['products'] = $product -> lists();
		return view('admin.auth.products', $data);
	}

	public function productDetails($product_id, ProductRepository $product)
	{
		$data['product'] = $product -> details($product_id);
		return view('admin.auth.product_details', $data);
	}

	public function announcements(AnnouncementRepository $announcement)
	{
		$data['announcements'] = $announcement -> lists();
		return view('admin.auth.announcements', $data);
	}

	public function announcementDetails($announcement_id, AnnouncementRepository $announcement)
	{
		$data['announcement'] = $announcement -> details($announcement_id);
		return view('admin.auth.announcement_details', $data);
	}

	public function disciplinaries(DisciplinaryRepository $disciplinary, Request $request)
	{
		$data['disciplinaries'] = $disciplinary -> lists($request);
		return view('admin.auth.disciplinaries', $data);
	}

	public function settings(SettingRepository $setting)
	{
		$data['settings'] = $setting -> setup();
		return view('admin.auth.settings', $data);
	}

	public function newManagers(ManagerRepository $manager, Request $request)
	{
		$data['managers'] = $manager -> newManagers();
		return view('admin.auth.new_managers', $data);
	}

	public function activeManagers(ManagerRepository $manager, Request $request)
	{
		$data['managers'] = $manager -> activeManagers();
		return view('admin.auth.active_managers', $data);
	}

	public function newPosts(PostRepository $post, Request $request)
	{
		$data['posts'] = $post -> newPosts();
		return view('admin.auth.new_posts', $data);
	}

	public function newLeagues(LeagueRepository $league, Request $request)
	{
		$data['leagues'] = $league -> newLeagues();
		return view('admin.auth.new_leagues', $data);
	}

	public function proxyLogin($manager_id, ManagerRepository $manager)
	{
		Auth::loginUsingId($manager_id);
		return redirect('dashboard');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('/' . env('ADMIN_SLUG') . '/ptp-admin');
	}

}
