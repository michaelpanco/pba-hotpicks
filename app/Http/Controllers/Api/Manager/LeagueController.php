<?php namespace App\Http\Controllers\Api\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Commands\Manager\CreateLeague;
use App\Commands\Manager\JoinLeague;
use App\Commands\Manager\LeaveLeague;
use App\Commands\Manager\StartLeague;
use App\Commands\Manager\DismissLeague;
use App\Commands\Manager\KickTeam;
use App\Commands\Manager\UpdateLeague;
use App\Commands\Manager\UpdateLeagueDesc;
use App\Commands\Manager\UpdateLeaguePassword;

use App\Http\Validations\Manager\CreateLeague as CreateLeagueValidation;
use App\Http\Validations\Manager\JoinLeague as JoinLeagueValidation;
use App\Http\Validations\Manager\LeaveLeague as LeaveLeagueValidation;
use App\Http\Validations\Manager\DismissLeague as DismissLeagueValidation;
use App\Http\Validations\Manager\UpdateLeague as UpdateLeagueValidation;
use App\Http\Validations\Manager\UpdateLeagueDesc as UpdateLeagueDescValidation;
use App\Http\Validations\Manager\UpdateLeaguePassword as UpdateLeaguePasswordValidation;
use App\Http\Validations\Manager\StartLeague as StartLeagueValidation;
use App\Http\Validations\Manager\KickFranchise as KickFranchiseValidation;

use App\Repositories\LeagueRepository;
use App\Repositories\FranchiseRepository;

use Auth;

class LeagueController extends Controller {

	protected $franchise;
	protected $league;

	public function __construct(LeagueRepository $league, FranchiseRepository $franchise){
		$this -> franchise = $franchise;
		$this -> league = $league;
	}

	public function createLeague(CreateLeagueValidation $createLeague,Request $request)
	{
		$response = array();
	    $validation = $createLeague -> validate($request);

		try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$league['name'] = $request -> input('name');
			$league['host'] = Auth::user() -> id;
			$league['description'] = $request -> input('description');
			$league['password'] = $request -> input('password');
			$league['host_franchise'] = Auth::user() -> franchise -> id;
			$create_league = $this -> dispatch(New CreateLeague($league));

			if(!$create_league['result'])
			{
				throw new \Exception($create_league['message']);
			}

			$response['status'] = 'success';
			$response['message'] = $create_league['id'];
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function join(JoinLeagueValidation $joinLeague, Request $request)
	{
		$response = array();
	    $validation = $joinLeague -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$manager['id'] = Auth::user() -> id;
			$manager['franchise_id'] = Auth::user() -> franchise -> id;
			$manager['league_id'] = $request -> input('league_id');

			$join_league = $this -> dispatch(New JoinLeague($manager));

			if(!$join_league){
				throw new \Exception('Failed to join league, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'You successfuly join the league';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function leave(LeaveLeagueValidation $leaveLeague, Request $request)
	{
		$response = array();
	    $validation = $leaveLeague -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$manager['id'] = Auth::user() -> id;
			$manager['franchise_id'] = Auth::user() -> franchise -> id;
			$manager['league_id'] = $request -> input('league_id');

			$leave_league = $this -> dispatch(New LeaveLeague($manager));

			if(!$leave_league){
				throw new \Exception('Failed to leave league, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'You successfuly leave the league';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function start(StartLeagueValidation $startLeague, Request $request)
	{
		$response = array();
	    $validation = $startLeague -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['league_id'] = $request -> input('league_id');

			$league = $this -> league -> details($details['league_id']);
			$details['league_name'] = $league -> name;

			$participants_collection = $league -> participants;
			$details['participants'] = $participants_collection -> lists('id') -> toArray();

			$start_league = $this -> dispatch(New StartLeague($details));

			if(!$start_league){
				throw new \Exception('Failed to start league, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'You successfuly dismiss the league';
	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function kick(KickFranchiseValidation $kickFranchise, Request $request)
	{
		$response = array();

	    $validation = $kickFranchise -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['id'] = Auth::user() -> id;
			$details['franchise_id'] = $request -> input('franchise_id');
			$details['league_id'] = $request -> input('league_id');

			$franchise_details = $this -> franchise -> details($details['franchise_id']);
			$details['league_name'] = $franchise_details -> league -> name;
			$details['manager_id'] = $franchise_details -> manager -> id;

			$kick_team = $this -> dispatch(New KickTeam($details));

			if(!$kick_team){
				throw new \Exception('Failed to start league, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'You successfuly kick the team';
	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function dismiss(DismissLeagueValidation $dismissLeague, Request $request)
	{
		$response = array();
	    $validation = $dismissLeague -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['id'] = Auth::user() -> id;
			$details['franchise_id'] = Auth::user() -> franchise -> id;
			$details['league_id'] = $request -> input('league_id');

			$league = $this -> league -> details($details['league_id']);
			$details['league_name'] = $league -> name;
			$participants_collection = $league -> participants;
			$details['participants'] = $participants_collection -> lists('id') -> toArray();

			$dismiss_league = $this -> dispatch(New DismissLeague($details));

			if(!$dismiss_league){
				throw new \Exception('Failed to dismiss league, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'You successfuly dismiss the league';
	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function update(UpdateLeagueValidation $updateLeague, Request $request)
	{
		$response = array();
	    $validation = $updateLeague -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			if((int)session('iUpdateLeague') + 300 < time() || Auth::user()->privilege == 'administrator')
			{
				$manager['id'] = Auth::user() -> id;
				$manager['league_id'] = $request -> input('league_id');
				$this -> dispatch(New UpdateLeague($manager));
			}
			
			$response['status'] = 'success';
			$response['message'] = 'League successfully updated';
	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateLeagueDesc(UpdateLeagueDescValidation $updateLeagueDesc, Request $request)
	{
		$response = array();
	    $validation = $updateLeagueDesc -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['league_id'] = $request -> input('league_id');
			$details['league_desc'] = $request -> input('league_desc');

			$update_league_desc = $this -> dispatch(New UpdateLeagueDesc($details));

			if(!$update_league_desc){
				throw new \Exception('Unable to update league description, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'League description successfully updated';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateLeaguePassword(UpdateLeaguePasswordValidation $updateLeaguePassword, Request $request)
	{
		$response = array();
	    $validation = $updateLeaguePassword -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['league_id'] = $request -> input('league_id');
			$details['league_password'] = $request -> input('league_password');

			$update_league_desc = $this -> dispatch(New UpdateLeaguePassword($details));

			if(!$update_league_desc){
				throw new \Exception('Unable to update league password, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'League password successfully updated';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

}