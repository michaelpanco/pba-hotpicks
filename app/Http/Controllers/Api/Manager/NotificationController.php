<?php namespace App\Http\Controllers\Api\Manager;

use App\Http\Controllers\Controller;
use App\Commands\Manager\FetchNotifications as FetchNotificationsCommand;
use App\Commands\Manager\FetchLatestMessages as FetchLatestMessagesCommand;
use App\Commands\Manager\FetchRequests as FetchRequestsCommand;
use App\Repositories\ManagerRepository;
use Auth;

class NotificationController extends Controller {

	public function __construct()
	{

	}

	public function notifier(ManagerRepository $manager)
	{
		$account = $manager -> instance();
		$response['notifications'] = $account -> notifications('new') -> get() -> count();
		$response['newMessages'] = $account -> newMessages() -> get() -> count();
		$response['requests'] = $account -> requests() -> get() -> count();
		$response['friends'] = $account -> approveFriends() -> get() -> count();
		$response['onlineFriends'] = $account -> onlineFriends() -> get() -> count();

		return response() -> json($response);
	}

	public function fetchNotifications()
	{
		$data['notifications'] = $this -> dispatch(New FetchNotificationsCommand());
		return response() -> view('web.response.notifications', $data);
	}

	public function fetchLatestMessages()
	{
		$data['messages'] = $this -> dispatch(New FetchLatestMessagesCommand());
		$data['render'] = 'notification';
		return response() -> view('web.response.messages', $data);
	}

	public function fetchRequests()
	{
		$data['requests'] = $this -> dispatch(New FetchRequestsCommand());
		return response() -> view('web.response.requests', $data);
	}
}