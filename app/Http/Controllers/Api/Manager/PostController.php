<?php namespace App\Http\Controllers\Api\Manager;

use App\Http\Controllers\Controller;
use App\Commands\Manager\CreatePost as CreatePostCommand;
use App\Commands\Manager\ReplyPost as ReplyPostCommand;
use App\Commands\Manager\FetchReplies as FetchRepliesCommand;
use App\Commands\Manager\UpVotePost as UpVotePostCommand;
use App\Commands\Manager\DeletePost as DeletePostCommand;
use Illuminate\Http\Request;
use App\Http\Validations\Manager\CreatePost as CreatePostValidation;
use App\Http\Validations\Manager\ReplyPost as ReplyPostValidation;
use App\Http\Validations\Manager\UpVotePost as UpVotePostValidation;
use App\Repositories\PostRepository;
use Auth;

class PostController extends Controller {

	public function getChannel(Request $request, PostRepository $post)
	{
		$data['posts'] = $post -> stream($request -> input('channel'));
		return response() -> view('web.response.posts', $data);
	}

	public function getMorePosts(Request $request, PostRepository $post)
	{
		$data['posts'] = $post -> stream($request -> input('channel'), $request -> input('pointer'));
		return response() -> view('web.response.posts', $data);
	}

	public function createPost(CreatePostValidation $createPost, Request $request)
	{
		$response = array();
	    $validation = $createPost -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$params['channel_id'] = $request -> input('channel_id');
			$params['content'] = $request -> input('content');

			$data['post'] = $this -> dispatch(New CreatePostCommand($params));

			return response()->view('web.response.singlepost', $data);

	    }
	    catch (\Exception $e) 
	    {
			return $e -> getMessage();
	    }
	}

	public function replyPost(ReplyPostValidation $replyPost, Request $request)
	{
		$response = array();
	    $validation = $replyPost -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$params['channel_id'] = $request -> input('channel_id');
			$params['content'] = $request -> input('content');
			$params['parent_post'] = $request -> input('parent_post');

			$data['reply'] = $this -> dispatch(New ReplyPostCommand($params));

			return response() -> view('web.response.submitreply', $data);

	    }catch (\Exception $e){

			return $e -> getMessage();
	    }
	}

	public function replyPofsdst2(ReplyPostValidation $replyPost, Request $request)
	{
		$response = array();
	    $validation = $replyPost -> validate($request);

	    try {

			if ($validation -> fails()){
				throw new \Exception($validation -> errors() -> first());
			}

			$data['reply'] = $this -> dispatch(New ReplyPostCommand($replyPost -> finalfields($request)));

			return response() -> view('web.response.submitreply', $data);

	    }catch (\Exception $e){

			return "Failed to post your reply. Please try again.";
	    }
	}

	public function fetchReplies(Request $request)
	{
	    try {

			$data['replies'] = $this -> dispatch(New FetchRepliesCommand($request));

			return response() -> view('web.response.replies', $data);

	    }catch (\Exception $e){

			return "Failed to fetch replies. Please try again.";
	    }
	}

	public function upVote(UpVotePostValidation $upVote, Request $request)
	{
		$validation = $upVote -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

	    	$gp['post_id'] = $request -> input('post_id');
			$give_point = $this -> dispatch(New UpVotePostCommand($gp));
			$response['status'] = 'success';
			$response['message'] = '0';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function deletePost(UpVotePostValidation $upVote, Request $request)
	{

	    try {

	    	$dp['post_id'] = $request -> input('post_id');
			$delete_post = $this -> dispatch(New DeletePostCommand($dp));

			if($delete_post){
				$response['status'] = 'success';
				$response['message'] = '0';
			}else{
				$response['status'] = 'failed';
				$response['message'] = 'Sorry, Cannot able to delete post';
			}


	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}
}