<?php namespace App\Http\Controllers\Api\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Commands\Manager\Register as RegisterMember;
use App\Commands\Manager\Login as LoginMember;
use App\Commands\Manager\AddFriend;
use App\Commands\Manager\ApproveRequest;
use App\Commands\Manager\DeclineRequest;
use App\Commands\Manager\FlagOnline;
use App\Commands\Manager\SendMessage;
use App\Commands\Manager\PrepareTeam;
use App\Commands\Manager\CreateTeam;
use App\Commands\Manager\TradePlayers;
use App\Commands\Manager\ChangePassword;
use App\Commands\Manager\ChangeFavorites;
use App\Commands\Manager\UploadAvatar;
use App\Commands\Manager\ManageBadges;
use App\Commands\Manager\MakeExpressBet;
use App\Commands\Manager\ClaimExpressBetPrize;
use App\Commands\Manager\PurchaseProduct;
use App\Commands\Manager\ForgotPassword;
use App\Commands\Manager\RestrainManager;
use App\Commands\Manager\ResetPassword;
use App\Commands\Manager\ClaimReferalPrize;
use App\Commands\Manager\OpenGift;

use App\Http\Validations\Manager\Registration as ManagerRegistrationValidation;
use App\Http\Validations\Manager\AddFriend as AddFriendValidation;
use App\Http\Validations\Manager\ApproveRequest as ApproveRequestValidation;
use App\Http\Validations\Manager\DeclineRequest as DeclineRequestValidation;
use App\Http\Validations\Manager\SendMessage as SendMessageValidation;
use App\Http\Validations\Manager\CreateFranchise as CreateFranchiseValidation;
use App\Http\Validations\Manager\TradePlayers as TradePlayersValidation;
use App\Http\Validations\Manager\ChangePassword as ChangePasswordValidation;
use App\Http\Validations\Manager\ChangeFavorites as ChangeFavoritesValidation;
use App\Http\Validations\Manager\UploadAvatar as UploadAvatarValidation;
use App\Http\Validations\Manager\ManageBadges as ManageBadgesValidation;
use App\Http\Validations\Manager\MakeGameBet as MakeGameBetValidation;
use App\Http\Validations\Manager\ClaimExpressBetPrize as ClaimExpressBetPrizeValidation;
use App\Http\Validations\Manager\PurchaseProduct as PurchaseProductValidation;
use App\Http\Validations\Manager\ForgotPassword as ForgotPasswordValidation;
use App\Http\Validations\Manager\ResetPassword as ResetPasswordValidation;
use App\Http\Validations\Manager\RestrainManager as RestrainManagerValidation;
use App\Http\Validations\Manager\ClaimReferalPrize as ClaimReferalPrizeValidation;

use App\Repositories\ManagerRepository;
use App\Repositories\MessageRepository;

use Input;

class ManagerController extends Controller {

	public function __construct(){

	}

	public function register(ManagerRegistrationValidation $managerRegistration, Request $request)
	{
		$response = array();
	    $validation = $managerRegistration -> validate($request);

	    try {

			if ($validation -> fails()){
				throw new \Exception($validation -> errors() -> first());
			}

			$team = ($request -> has('team')) ? $request -> input('team') : null;

			$create_member = $this -> dispatch(New RegisterMember($managerRegistration -> finalfields($request), $team));

			if(!$create_member){
				throw new \Exception('Failed to create account, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'Member Successfully registered';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function login(Request $request){

		try {

			$login['email'] = $request -> input('email');
			$login['password'] = $request -> input('password');
			$login['remember'] = $request -> input('remember');

			$login_member = $this -> dispatch(New LoginMember($login));

			if(!$login_member['success'])
			{
				throw new \Exception($login_member['message']);
			}

			$response['status'] = 'success';
			$response['message'] = '0';
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function forgotPassword(ForgotPasswordValidation $forgotPassword, Request $request){

		$validation = $forgotPassword -> validate($request);

		try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$forgot_password = $this -> dispatch(New ForgotPassword($request));

			if(!$forgot_password){
				throw new \Exception('Failed to request forgot password, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'Way to go, Please check your email for a message containing your password reset link';
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function resetPassword(ResetPasswordValidation $resetPassword, Request $request)
	{
		$validation = $resetPassword -> validate($request);

		try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['password'] = $request -> input('newpassword');
			$details['email'] = $request -> input('email');
			$details['token'] = $request -> input('token');

			$reset_password = $this -> dispatch(New ResetPassword($details));

			if(!$reset_password){
				throw new \Exception('Failed to request forgot password, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'Password was changed successfully, You can login now with your new password';
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function changePassword(ChangePasswordValidation $changePassword, Request $request)
	{
		$response = array();
	    $validation = $changePassword -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$change_password = $this -> dispatch(New ChangePassword($request));

			if(!$change_password){
				throw new \Exception('Failed to change password, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'Your password was changed successfully';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function changeFavorites(ChangeFavoritesValidation $changeFavorites, Request $request)
	{
		$response = array();
	    $validation = $changeFavorites -> validate($request);

	    try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$change_favorites = $this -> dispatch(New ChangeFavorites($request));

			if(!$change_favorites){
				throw new \Exception('Failed to change favorites, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'Your changes was updated successfully';

	    }
	    catch (\Exception $e)
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function tmpCreateTeam(Request $request, CreateFranchiseValidation $createFranchise)
	{
		$validation = $createFranchise -> validate($request);

		try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$team['name'] = $request -> input('name');
			$team['logo'] = $request -> input('logo');
			$team['picks'] = $request -> input('picks');

			$prepare_team = $this -> dispatch(New PrepareTeam($team));

			if(!$prepare_team['success'])
			{
				throw new \Exception($prepare_team['message']);
			}

			$response['status'] = 'success';
			$response['tag'] = $prepare_team['tag'];
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function createTeam(Request $request, CreateFranchiseValidation $createFranchise)
	{
		$validation = $createFranchise -> validate($request);

		try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$team['name'] = $request -> input('name');
			$team['logo'] = $request -> input('logo');
			$team['picks'] = $request -> input('picks');

			$create_team = $this -> dispatch(New CreateTeam($team));

			if(!$create_team['success'])
			{
				throw new \Exception($create_team['message']);
			}

			$response['status'] = 'success';
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function tradePlayers(Request $request, TradePlayersValidation $tradePlayers)
	{
		$validation = $tradePlayers -> validate($request);

		try {

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$player['give'] = $request -> input('give');
			$player['get'] = $request -> input('get');

			$trade_players = $this -> dispatch(New TradePlayers($player));

			if(!$trade_players)
			{
				throw new \Exception('Unable to trade player, Please try again.');
			}

			$response['status'] = 'success';
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function addFriend(AddFriendValidation $addFriend, Request $request)
	{
		$validation = $addFriend -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$this -> dispatch(New AddFriend($request));
			
			$response['status'] = 'success';
			$response['message'] = '0';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function approveRequest(ApproveRequestValidation $approveRequest, Request $request)
	{
		$validation = $approveRequest -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$this -> dispatch(New ApproveRequest($request));
			
			$response['status'] = 'success';
			$response['message'] = '0';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function declineRequest(DeclineRequestValidation $declineRequest, Request $request)
	{
		$validation = $declineRequest -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$this -> dispatch(New DeclineRequest($request));
			
			$response['status'] = 'success';
			$response['message'] = '0';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function flagOnline()
	{
		$this -> dispatch(New flagOnline());
	}

	public function fetchFriends(ManagerRepository $manager)
	{
		$data['friends'] = $manager -> instance() -> approveFriends() -> paginate(4);
		return view('web.response.friends', $data);
	}

	public function fetchMessages(MessageRepository $message)
	{
		$data['render'] = 'page';
		$data['messages'] = $message -> lists($data['render']);
		return view('web.response.messages', $data);
	}

	public function fetchConversations($manager_slug, ManagerRepository $manager, MessageRepository $message)
	{
		$manager_id = $manager -> getManager('slug', $manager_slug) -> id;
		$data['conversations'] = $message -> conversations($manager_id);
		$data['latest_message_marker'] = true;
		return view('web.response.conversations', $data);
	}

	public function getMoreConversations(Request $request, MessageRepository $message)
	{
		$data['conversations'] = $message -> stream($request -> input('recipient'), $request -> input('pointer'));
		$data['latest_message_marker'] = false;
		return response() -> view('web.response.conversations', $data);
	}

	public function markMessageRead(MessageRepository $message, Request $request)
	{
		$message -> markMessageRead($request -> input('manager_id'));
	}

	public function sendMessage(SendMessageValidation $sendMessage, Request $request)
	{
		$validation = $sendMessage -> validate($request);

		try{

			if ($validation -> fails()){
				throw new \Exception($validation -> errors() -> first());
			}

			$this -> dispatch(New SendMessage($request));
			
			$response['status'] = 'success';
			$response['message'] = '0';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function uploadAvatar(UploadAvatarValidation $uploadAvatar)
	{
		$file = Input::file('file');
		$validation = $uploadAvatar -> validate($file);

		try{

			if ($validation -> fails()){
				throw new \Exception($validation -> errors() -> first());
			}

			$avatar = $this -> dispatch(New UploadAvatar($file));
			
			$response['status'] = 'success';
			$response['avatar'] = $avatar;
			$response['message'] = 'Your avatar was successfully updated';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function manageBadges(ManageBadgesValidation $manageBadges, Request $request)
	{
		$validation = $manageBadges -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$this -> dispatch(New ManageBadges($request));
			
			$response['status'] = 'success';
			$response['message'] = 'Badges was successfully saved';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function makeGameBet(MakeGameBetValidation $makeGameBet, Request $request)
	{
		$validation = $makeGameBet -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$make_express_bet = $this -> dispatch(New MakeExpressBet($request));
			
			if(!$make_express_bet)
			{
				throw new \Exception('Theres an error making a bet, Please try again.');
			}

			$response['status'] = 'success';
			$response['message'] = 'Your bet has been submitted successfully';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function claimExpressBetPrize(ClaimExpressBetPrizeValidation $claimExpressBetPrize, Request $request)
	{
		$validation = $claimExpressBetPrize -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}
			
			$claim_express_bet_prize = $this -> dispatch(New ClaimExpressBetPrize($request));
			
			if(!$claim_express_bet_prize)
			{
				throw new \Exception('Theres an error claiming bet prize, Please try again.');
			}
			
			$response['status'] = 'success';
			$response['message'] = 'Express bet prize was claimed successfully';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function claimReferalPrize(ClaimReferalPrizeValidation $claimReferalPrizeValidation, Request $request)
	{
		$validation = $claimReferalPrizeValidation -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}
			
			$claim_referal_prize = $this -> dispatch(New ClaimReferalPrize($request));
			
			if(!$claim_referal_prize)
			{
				throw new \Exception('Theres an error claiming referal prize, Please try again.');
			}
			
			$response['status'] = 'success';
			$response['message'] = 'Referal prize was claimed successfully';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function purchaseProduct(PurchaseProductValidation $purchaseProduct, Request $request)
	{
		$validation = $purchaseProduct -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}
			
			$purchase_product = $this -> dispatch(New PurchaseProduct($request));
			
			if(!$purchase_product)
			{
				throw new \Exception('Theres an error purchasing product, Please try again.');
			}
			
			$response['status'] = 'success';
			$response['message'] = 'Congratulations, New product was purchased successfully';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function restrainManager(RestrainManagerValidation $restrainManager, Request $request)
	{
		$validation = $restrainManager -> validate($request);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$restrain_manager = $this -> dispatch(New RestrainManager($request));
			
			if(!$restrain_manager)
			{
				throw new \Exception('Theres an error restraining manager, Please try again.');
			}
			
			$response['status'] = 'success';
			$response['message'] = 'Manager was restrain successfully.';

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	public function openGift(ManagerRepository $manager)
	{
		$validation = $this -> validateOpeningGift($manager);

		try{

			if ($validation['fails']){
				throw new \Exception($validation['message']);
			}

			$details['gift'] = $this -> randomGift();

			$open_gift = $this -> dispatch(New OpenGift($details));
			
			if(!$open_gift['success'])
			{
				throw new \Exception('Theres an error opening your gift, Please try again later.');
			}
			
			$response['status'] = 'success';
			$response['message'] = "You've got " . number_format($open_gift['received']) . " credit's for todays gift. You can get another gift by logging in everyday. Merry Christmas and Happy New Year :)";

		}
		catch (\Exception $e) 
		{
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
		}

		return response() -> json($response);
	}

	private function validateOpeningGift($manager)
	{
		$validation = array();

		if($manager -> canOpenGift()){
			$validation['fails'] = false;
		}else{
			$validation['fails'] = true;
			$validation['message'] = 'You already claimed your gift for today, You can get another gift in another day';
		}

		return $validation;
	}

	private function randomGift()
	{
            $credits_arr = array(
				500,
				500,
				500,
				500,
				500,
				750,
				750,
				750,
				900,
				900,
				1100,
				1100,
				1500,
            );

            $credits_ran_arr = array_rand($credits_arr, 2);

           return $credits_arr[$credits_ran_arr[0]];
	}




}