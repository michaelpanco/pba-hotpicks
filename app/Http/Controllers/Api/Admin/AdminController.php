<?php namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Commands\Admin\Login as LoginAdmin;
use Illuminate\Http\Request;

use App\Repositories\ManagerRepository;
use App\Repositories\TeamRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\GameRepository;
use App\Repositories\PerformanceRepository;
use App\Repositories\LeagueRepository;
use App\Repositories\ExpressBetRepository;
use App\Repositories\ProductRepository;
use App\Repositories\AnnouncementRepository;
use App\Repositories\SettingRepository;

use App\Commands\Admin\UpdateMostSignedPlayers;
use App\Commands\Admin\TopManagers;
use App\Commands\Admin\RichestManagers;

class AdminController extends Controller {

	public function __construct(){

	}

	public function login(Request $request){

		try {

			$login['email'] = $request -> input('email');
			$login['password'] = $request -> input('password');
			$login['remember'] = $request -> input('remember');

			$login_admin = $this -> dispatch(New LoginAdmin($login));

			if(!$login_admin['success'])
			{
				throw new \Exception($login_admin['message']);
			}

			$response['status'] = 'success';
			$response['message'] = '0';
	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateTeamProfile(Request $request, TeamRepository $team)
	{
		try {
			
			$details['id'] = $request -> input('id');
			$details['name'] = $request -> input('name');
			$details['slug'] = $request -> input('slug');
			$details['manager'] = $request -> input('manager');
			$details['coach'] = $request -> input('coach');
			$details['assistant_coaches'] = $request -> input('assistant_coaches');
			$details['joined'] = $request -> input('joined');
			$details['titles'] = $request -> input('titles');
			$details['win'] = $request -> input('win');
			$details['loss'] = $request -> input('loss');
			$details['logo'] = $request -> input('logo');

			$update_team = $team -> update($details);

			if(!$update_team){
				throw new \Exception('Failed to update team');
			}

			$response['status'] = 'success';
			$response['message'] = 'Team details was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);

	}

	public function createPlayer(Request $request, PlayerRepository $player)
	{
		try {

			$details['firstname'] = $request -> input('firstname');
			$details['lastname'] = $request -> input('lastname');
			$details['slug'] = $request -> input('slug');
			$details['team_id'] = $request -> input('team_id');
			$details['position'] = $request -> input('position');
			$details['jersey'] = $request -> input('jersey');
			$details['birthday'] = $request -> input('birthday');
			$details['height'] = $request -> input('height');
			$details['school'] = $request -> input('school');
			$details['avatar'] = $request -> input('avatar');
			$details['salary'] = $request -> input('salary');

			$create_player = $player -> create($details);

			if(!$create_player['status']){
				throw new \Exception('Failed to create player');
			}

			$response['status'] = 'success';
			$response['player_id'] = $create_player['player_id'];
			$response['message'] = 'Player was created successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updatePlayerProfile(Request $request, PlayerRepository $player)
	{
		try {
			$details['id'] = $request -> input('id');
			$details['firstname'] = $request -> input('firstname');
			$details['lastname'] = $request -> input('lastname');
			$details['slug'] = $request -> input('slug');
			$details['position'] = $request -> input('position');
			$details['jersey'] = $request -> input('jersey');
			$details['team_id'] = $request -> input('team_id');
			$details['birthday'] = $request -> input('birthday');
			$details['school'] = $request -> input('school');
			$details['height'] = $request -> input('height');
			$details['avatar'] = $request -> input('avatar');
			$details['status'] = $request -> input('status');
			$details['salary'] = $request -> input('salary');

			$update_player = $player -> update($details);

			if(!$update_player){
				throw new \Exception('Failed to update team');
			}

			$response['status'] = 'success';
			$response['message'] = 'Player details was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateManagerProfile(Request $request, ManagerRepository $manager)
	{
		try {
			$details['id'] = $request -> input('id');
			$details['firstname'] = $request -> input('firstname');
			$details['lastname'] = $request -> input('lastname');
			$details['slug'] = $request -> input('slug');
			$details['gender'] = $request -> input('gender');
			$details['status'] = $request -> input('status');
			$details['credits'] = $request -> input('credits');
			$details['restrain_until'] = $request -> input('restrain_until');
			$details['birthday'] = $request -> input('birthday');
			$details['privilege'] = $request -> input('privilege');
			$details['favteam'] = $request -> input('favteam');
			$details['fav2ndteam'] = $request -> input('fav2ndteam');
			$details['favplayer'] = $request -> input('favplayer');
			$details['favmatchup'] = $request -> input('favmatchup');

			$update_manager = $manager -> update($details);

			if(!$update_manager){
				throw new \Exception('Failed to update manager');
			}

			$response['status'] = 'success';
			$response['message'] = 'Manager details was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateLeagueProfile(Request $request, LeagueRepository $league)
	{
		try {

			$details['id'] = $request -> input('id');
			$details['name'] = $request -> input('name');
			$details['desc'] = $request -> input('desc');
			$details['host'] = $request -> input('host');
			$details['sequence'] = $request -> input('sequence');
			$details['status'] = $request -> input('status');
			$details['date_created'] = $request -> input('date_created');

			$update_league = $league -> updateDetails($details);

			if(!$update_league){
				throw new \Exception('Failed to update league');
			}

			$response['status'] = 'success';
			$response['message'] = 'League was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function createGame(Request $request, GameRepository $game)
	{
		try {

			$details['team_x_id'] = $request -> input('team_x_id');
			$details['team_y_id'] = $request -> input('team_y_id');
			$details['venue'] = $request -> input('venue');
			$details['season'] = $request -> input('season');
			$details['conference'] = $request -> input('conference');
			$details['game_no'] = $request -> input('game_no');
			$details['cluster_no'] = $request -> input('cluster_no');
			$details['game_type'] = $request -> input('game_type');
			$details['sequence'] = $request -> input('sequence');
			$details['datetime'] = $request -> input('datetime');

			$create_game = $game -> create($details);

			if(!$create_game){
				throw new \Exception('Failed to create game');
			}

			$response['status'] = 'success';
			$response['message'] = 'Game was created successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateGameDetails(Request $request, GameRepository $game)
	{
		try {
			
			$details['id'] = $request -> input('id');
			$details['team_x_id'] = $request -> input('team_x_id');
			$details['team_y_id'] = $request -> input('team_y_id');
			$details['winner'] = $request -> input('winner');
			$details['score'] = $request -> input('score');
			$details['venue'] = $request -> input('venue');
			$details['season'] = $request -> input('season');
			$details['conference'] = $request -> input('conference');
			$details['game_no'] = $request -> input('game_no');
			$details['cluster_no'] = $request -> input('cluster_no');
			$details['game_type'] = $request -> input('game_type');
			$details['sequence'] = $request -> input('sequence');
			$details['datetime'] = $request -> input('datetime');

			$update_game = $game -> update($details);

			if(!$update_game){
				throw new \Exception('Failed to update team');
			}

			$response['status'] = 'success';
			$response['message'] = 'Game details was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateExpressBet(Request $request, ExpressBetRepository $expressBet)
	{
		try {
			
			$details['id'] = $request -> input('id');
			$details['description'] = $request -> input('description');
			$details['option'] = $request -> input('option');
			$details['limit'] = $request -> input('betlimit');
			$details['won'] = $request -> input('won');
			$details['gamedate'] = $request -> input('gamedate');
			$details['expiry'] = $request -> input('expiry');

			$update_express_bet = $expressBet -> update($details);

			if(!$update_express_bet){
				throw new \Exception('Failed to update express bet');
			}

			$response['status'] = 'success';
			$response['message'] = 'Express bet details was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function createExpressBet(Request $request, ExpressBetRepository $expressBet)
	{
		try {

			$details['description'] = $request -> input('description');
			$details['option'] = serialize([1 => $request -> input('option1'), 2 => $request -> input('option2')]);
			$details['limit'] = $request -> input('betlimit');
			$details['won'] = 0;
			$details['gamedate'] = $request -> input('gamedate');
			$details['expiry'] = $request -> input('expiry');

			$create_express_bet = $expressBet -> create($details);

			if(!$create_express_bet['status']){
				throw new \Exception('Failed to create express bet');
			}

			$response['status'] = 'success';
			$response['express_bet_id'] = $create_express_bet['express_bet_id'];
			$response['message'] = 'Express bet was created successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function createGamePerformance(Request $request, PerformanceRepository $gamePerformance)
	{
		try {
			
			$performance['game_id'] = $request -> input('game_id');
			$performance['player_id'] = $request -> input('player_id');
			$performance['team_id'] = $request -> input('team_id');
			$performance['opponent'] = $request -> input('opponent');
			$performance['points'] = $request -> input('points', 0);
			$performance['assists'] = $request -> input('assists', 0) ;
			$performance['rebounds'] = $request -> input('rebounds', 0);
			$performance['steals'] = $request -> input('steals', 0);
			$performance['blocks'] = $request -> input('blocks', 0);
			$performance['turnovers'] = $request -> input('turnovers', 0);
			$performance['win_game'] = $request -> input('win_game', 0);

	        $fantasy_points = (($performance['points'] * 2) + ($performance['rebounds'] * 2) + ($performance['assists'] * 3) + ($performance['steals'] * 3) + ($performance['blocks'] * 3) + ($performance['win_game'] * 10) + (0 - $performance['turnovers'])) * 2;

	        $performance['fantasy_points'] = $fantasy_points;

			$create_performance = $gamePerformance -> create($performance);

			if(!$create_performance){
				throw new \Exception('Failed to create performance');
			}

			$response['status'] = 'success';
			$response['message'] = 'Game performance was created successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function publishGameResult(Request $request, PerformanceRepository $performance)
	{
		try {
			
			$game_id = $request -> input('id');

			$publish_game = $performance -> publish($game_id);

			if(!$publish_game){
				throw new \Exception('Failed to update team');
			}

			$response['status'] = 'success';
			$response['message'] = 'Game was published successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function createProduct(Request $request, ProductRepository $product)
	{
		try {

			$details['name'] = $request -> input('name');
			$details['type'] = $request -> input('type');
			$details['product_id'] = $request -> input('product_id');
			$details['image'] = $request -> input('image');
			$details['price'] = $request -> input('price');

			$create_product = $product -> create($details);

			if(!$create_product['status']){
				throw new \Exception('Failed to create product');
			}

			$response['status'] = 'success';
			$response['message'] = 'Product was created successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateProduct(Request $request, ProductRepository $product)
	{
		try {
			
			$details['id'] = $request -> input('id');
			$details['name'] = $request -> input('name');
			$details['image'] = $request -> input('image');
			$details['type'] = $request -> input('type');
			$details['product_id'] = $request -> input('product_id');
			$details['price'] = $request -> input('price');

			$update_product = $product -> update($details);

			if(!$update_product){
				throw new \Exception('Failed to update product');
			}

			$response['status'] = 'success';
			$response['message'] = 'Product details was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function createAnnouncement(Request $request, AnnouncementRepository $announcement)
	{
		try {

			$details['text'] = $request -> input('text');
			$details['order'] = $request -> input('order');

			$create_announcement = $announcement -> create($details);

			if(!$create_announcement['status']){
				throw new \Exception('Failed to create product');
			}

			$response['status'] = 'success';
			$response['message'] = 'Announcement was created successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateAnnouncement(Request $request, AnnouncementRepository $announcement)
	{
		try {
			
			$details['id'] = $request -> input('id');
			$details['text'] = $request -> input('text');
			$details['order'] = $request -> input('order');

			$update_announcement = $announcement -> update($details);

			if(!$update_announcement){
				throw new \Exception('Failed to update announcement');
			}

			$response['status'] = 'success';
			$response['message'] = 'Announcement was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function deleteAnnouncement(Request $request, AnnouncementRepository $announcement)
	{
		try {
			
			$details['id'] = $request -> input('id');

			$delete_announcement = $announcement -> delete($details);

			if(!$delete_announcement){
				throw new \Exception('Failed to update announcement');
			}

			$response['status'] = 'success';
			$response['message'] = 'Announcement was deleted successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateSettings(Request $request, SettingRepository $setting)
	{
		try {
			
			$details['NOTIFIER_POLLING_INTERVAL'] = $request -> input('ninterval');
			$details['FLAG_ONLINE_POLLING_INTERVAL'] = $request -> input('finterval');
			$details['CONVERSATION_POLLING_INTERVAL'] = $request -> input('cinterval');
			$details['SEQUENCE'] = $request -> input('sequence');
			$details['INITIAL_SALARY_CAP'] = $request -> input('initsalary');
			$details['MINIMUM_LEAGUE_PARTICIPANTS'] = $request -> input('minparticipants');
			$details['MAXIMUM_LEAGUE_PARTICIPANTS'] = $request -> input('maxparticipants');
			$details['SEASON'] = $request -> input('season');
			$details['ALLOW_TRADE'] = $request -> input('allow_trade');
			$details['DISTRIBUTE_REWARD'] = $request -> input('distribute_reward');
			$details['BEGIN_LEAGUE'] = $request -> input('begin_league');

			$update_setting = $setting -> update($details);

			if(!$update_setting){
				throw new \Exception('Failed to update settings');
			}

			$response['status'] = 'success';
			$response['message'] = 'Settings was updated successfully';

	    }
	    catch (\Exception $e) 
	    {
			$response['status'] = 'failed';
			$response['message'] = $e -> getMessage();
	    }

		return response() -> json($response);
	}

	public function updateStatistics()
	{
		$this -> dispatch(New TopManagers());
		$this -> dispatch(New RichestManagers());
		//$this -> dispatch(New UpdateMostSignedPlayers());
		$response['status'] = 'success';
		$response['message'] = 'Statatistics update was applied';
		return response() -> json($response);
	}

}