<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Validations\Manager\Registration as ManagerRegistrationValidation;
use App\Http\Validations\Manager\EmailConfirmation;

use App\Models\Manager;

use App\Commands\Manager\ConfirmRegistration;
use App\Commands\Manager\CreateFranchise;

use App\Repositories\GameRepository;
use App\Repositories\TeamRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\PerformanceRepository;
use App\Repositories\FranchiseRepository;
use App\Repositories\LeagueRepository;
use App\Repositories\ManagerRepository;

use App\Http\Helpers\Helper;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

use Setting;
use Auth;
use Redis;
use Session; 
use Image;

class MainController extends Controller {

	public function __construct(){

	}

	public function index(GameRepository $game, PerformanceRepository $performance, TeamRepository $team, Request $request, ManagerRepository $manager)
	{
		if ($request -> has('r')) {
		    $request->session()->put('referal', $request->input('r'));
		}

		//$data['most_signed_players'] = array_reverse(Redis::zrange('laravel:most_signed_players', 0,10));
		$data['top_managers'] = array_slice(array_reverse(Redis::zrange('laravel:top_managers', 0,49)),0,10,true);
		$data['richest_managers'] = array_slice(array_reverse(Redis::zrange('laravel:richest_managers', 0,49)),0,10,true);
		$data['featured_games'] = $game -> featured();

		$last_games = collect($data['featured_games'][2]['games']) -> keys() -> all();

		$data['ranks'] = array(
				'1st',
				'2nd',
				'3rd',
				'4th',
				'5th',
				'6th',
				'7th',
				'8th',
				'9th',
				'10th',
		);

		if(isset($data['featured_games'][3])){
			$data['more_games'] = true;
			$data['game_start'] = $data['featured_games'][3]['game_start'][0];
		}else{
			$data['more_games'] = false;
		}

		$data['featured_players'] = $performance -> featured($last_games)-> sortByDesc('fantasy_points') -> take(5) -> reverse();

		$data['teams'] = $team -> lists();

		// delete this after event

		$data['can_open_gift'] = (Auth::check() && $manager -> canOpenGift()) ? true : false;


		return view('web.index', $data);
	}

	public function top_50()
	{
		$data['top_managers'] = array_reverse(Redis::zrange('laravel:top_managers', 0,49));
		$data['my_own_id'] = (Auth::check()) ? Auth::user()->id : 0;
		return view('web.top_50', $data);
	}

	public function richest_50()
	{
		$data['richest_managers'] = array_reverse(Redis::zrange('laravel:richest_managers', 0,49));
		$data['my_own_id'] = (Auth::check()) ? Auth::user()->id : 0;
		return view('web.richest_50', $data);
	}

	public function register(Request $request, FranchiseRepository $franchise)
	{
		if(Auth::check()){
			return redirect()->route('home');
		}

		$data['team'] = null;
		if ($request->has('team')) {
			$franchise_request = $franchise -> tmpDetailsByTag($request -> input('team'));
			if($franchise_request){
				$data['team'] = $franchise_request;
			}
		}
		return view('web.register', $data);
	}

	public function facebookCallback(Facebook $facebook, Manager $manager)
	{
	    // Obtain an access token.
	    try {
	        $token = $facebook -> getAccessTokenFromRedirect();
	    } catch (Facebook\Exceptions\FacebookSDKException $e) {
	        dd($e->getMessage());
	    }

	    // Access token will be null if the user denied the request
	    // or if someone just hit this URL outside of the OAuth flow.
	    if (! $token) {
	        // Get the redirect helper
	        $helper = $facebook -> getRedirectLoginHelper();

	        if (! $helper->getError()) {
	            abort(403, 'Unauthorized action.');
	        }

	        // User denied the request
	        dd(
	            $helper->getError(),
	            $helper->getErrorCode(),
	            $helper->getErrorReason(),
	            $helper->getErrorDescription()
	        );
	    }

	    if (! $token->isLongLived()) {
	        // OAuth 2.0 client handler
	        $oauth_client = $facebook -> getOAuth2Client();

	        // Extend the access token.
	        try {
	            $token = $oauth_client->getLongLivedAccessToken($token);
	        } catch (Facebook\Exceptions\FacebookSDKException $e) {
	            dd($e->getMessage());
	        }
	    }

	    $facebook -> setDefaultAccessToken($token);

	    // Save for later
	    Session::put('fb_user_access_token', (string) $token);

	    // Get basic info on the user from Facebook.
	    try {
	        $response = $facebook -> get('/me?fields=id,first_name,last_name,gender,email,birthday');
	    } catch (Facebook\Exceptions\FacebookSDKException $e) {
	        dd($e->getMessage());
	    }

	    // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
	    $facebook_user = $response->getGraphUser();

	    if(isset($facebook_user['email'])){
		    $manager_details = $manager -> where('email', $facebook_user['email']) -> first();

		    if($manager_details === null)
		    {
		        $filename  = $facebook_user['id'] . '.jpg';
		        $manager_avatar_dir = '/assets/img/avatars/' . date('dY', strtotime(date('Y-m-d')));
		        $complete_avatar_url = $manager_avatar_dir . $filename;

		        $path = public_path($complete_avatar_url);
				Image::make('https://graph.facebook.com/' . $facebook_user['id'] . '/picture?width=110&height=110') -> save($path);

				$facebook_user['avatar'] = $complete_avatar_url;

				$facebook_user['ip_address'] = $this -> _get_client_ip();
				$facebook_user['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
				$facebook_user['reg_type'] = 'FCBOOK';
				$facebook_user['credits'] = 250;

				$manager_details = $manager::createOrUpdateGraphNode($facebook_user);
		    }

		    

		    Auth::login($manager_details);

		    return redirect('/');


		}else{
			echo "Invalid facebook email address. <a href=\"" . url() . "\">Return to homepage</a>";
		}
	    //$pathd = public_path('/assets/img/avatars/012015/rrrrr.jpg');
		//Image::make('https://graph.facebook.com/10207744826398968/picture?width=100&height=110') -> save($pathd);


	    //$facebook_user['avatar'] = 'asdasdas';

		//dd($facebook_user);
	    // Create the user if it does not exist or update the existing entry.
	    // This will only work if you've added the SyncableGraphNodeTrait to your User model.
	    //dd($facebook_user);
	}

	private function _get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}		

	public function createFranchise(PlayerRepository $player, FranchiseRepository $franchise)
	{	
		$data['players'] = $player -> lists();
		$data['initial_salary_cap'] = Setting::get('INITIAL_SALARY_CAP');
		$data['franchise_logos'] = $franchise -> logos();
		return view('web.create_franchise', $data);
	}

	public function franchiseProfile($franchise_id, FranchiseRepository $franchise)
	{
		$data['team'] = $franchise -> details($franchise_id);
		$data['franchise'] = $franchise_id;
		return view('web..managers.franchise_profile', $data);
	}

	public function leagues(LeagueRepository $league, Request $request){
		$data['leagues'] = $league -> lists($request);
		$data['search'] = $request -> input('search');

		$data['can_create'] = false;
		$data['own_league'] = false;

		$auth_user = Auth::user();

		if(Auth::check() && $auth_user -> franchise != null && $auth_user -> franchise -> league == null)
		{
			$data['can_create'] = true;
		}

		if(Auth::check() && isset($auth_user -> franchise -> league))
		{
			$data['own_league'] = $auth_user -> franchise -> league -> id;
		}
		
		return view('web.leagues', $data);
	}

	public function leagueProfile($league_id, LeagueRepository $league, TeamRepository $team, Request $request)
	{
		$auth_user = Auth::user();

		$data['auth_user_franchise_id'] = (Auth::check() && $auth_user -> franchise != null) ? $auth_user -> franchise -> id : 0;

		$data['teams'] = $team -> lists();
		$data['league'] = $league -> details($league_id);

		if($data['league']){

			$data['can_join'] = false;
			$data['can_leave'] = false;

			if(Auth::check() && $data['league'] -> ready == false && ($auth_user -> franchise == null || $auth_user -> franchise -> league == null))
			{
				$data['can_join'] = true;
			}

			if($data['league'] -> ready == false)
			{
				$data['can_leave'] = true;
			}

			$i = 1;

			foreach($data['league'] -> participants as $participant)
			{
				$data['league_participants'][$i]['participant_id'] = $participant -> id;
				$data['league_participants'][$i]['franchise_logo'] = $participant -> icon -> filename;
				$data['league_participants'][$i]['franchise_name'] = $participant -> name;
				$data['league_participants'][$i]['franchise_manager_slug'] = $participant -> manager -> slug;
				$data['league_participants'][$i]['franchise_manager_avatar'] = $participant -> manager -> avatar;
				$data['league_participants'][$i]['franchise_manager_fullname'] = $participant -> manager -> fullname;
				$data['league_participants'][$i]['franchise_fantasy_points'] = $participant -> statistic -> sum('fantasy_points');
				$i++;
			}
			
			$data['league_participants'] = collect($data['league_participants']) -> sortByDesc('franchise_fantasy_points');

			$auth_user_franchise = (isset($auth_user -> franchise)) ? $auth_user -> franchise : 0;

			if(Auth::check() 
				&& isset($auth_user_franchise) 
					&& isset($auth_user_franchise -> league_id) 
						&& $league_id == $auth_user_franchise -> league_id
						 || (Auth::user() && Auth::user()->privilege == 'administrator')){

				$data['nofooter'] = true;
				return view('web.league_profile_participant', $data);
			}

			return view('web.league_profile_guest', $data);
		}

		abort(404);
	}

	public function rules()
	{
		return view('web.rules');
	}

	public function teams(TeamRepository $team)
	{
		$data['teams'] = $team -> lists();
		return view('web.teams', $data);
	}

	public function players(PlayerRepository $player)
	{
		$data['players'] = $player -> lists();
		return view('web.players', $data);
	}

	public function playerProfile($player_slug, PlayerRepository $player, PerformanceRepository $performance, TeamRepository $team)
	{
		$data['player'] = $player -> details($player_slug);

		if($data['player']){
			$data['performances'] = $performance -> playerPerformance($data['player']->id);
			$statistic = $performance -> playerPercentage($data['player']->id);
			$data['total_fantasy_points'] = (isset($statistic -> total_fantasy_points)) ? round($statistic -> total_fantasy_points) : 0;
			$data['avg_points'] = (isset($statistic -> avg_points)) ? round($statistic -> avg_points) : 0;
			$data['avg_rebounds'] = (isset($statistic -> avg_rebounds)) ? round($statistic -> avg_rebounds) : 0;
			$data['avg_assists'] = (isset($statistic -> avg_assists)) ? round($statistic -> avg_assists) : 0;
			$data['avg_steals'] = (isset($statistic -> avg_steals)) ? round($statistic -> avg_steals) : 0;
			$data['avg_blocks'] = (isset($statistic -> avg_blocks)) ? round($statistic -> avg_blocks) : 0;

			$data['teams'] = $team -> lists();
			return view('web.player_profile', $data);
		}

		abort(404);
	}

	public function teamProfile($team_slug, TeamRepository $team, GameRepository $game, PerformanceRepository $performance)
	{
		$data['team'] = $team -> details($team_slug);

		if($data['team']){

			$data['asst_coaches'] = explode(',', $data['team'] -> assistant_coaches);
			$games = collect($game -> matchups($data['team'] -> id));

			$performances = $performance -> teamPerformance($data['team'] -> players);

			$data['upcoming_games'] = $games -> where('final', false);
			$data['game_results'] = $games -> where('final', true);

			$data['points_leader'] = $performances -> sortByDesc('avg_points') -> first();
			$data['assists_leader'] = $performances -> sortByDesc('avg_assists') -> first();
			$data['rebounds_leader'] = $performances -> sortByDesc('avg_rebounds') -> first();
			$data['steals_leader'] = $performances -> sortByDesc('avg_steals') -> first();
			$data['blocks_leader'] = $performances -> sortByDesc('avg_blocks') -> first();

			return view('web.team_profile', $data);
		}

		abort(404);
	}

	public function games(GameRepository $game, TeamRepository $team, $team_slug='all', $outcome='all')
	{
		$teams = $team -> lists();
		$team_id = 'all';
		$data['filtered_team'] = '';

		if($team_slug != 'all')
		{
			if($teams -> where('slug', $team_slug) -> isEmpty())
			{
				abort(404);
			}

			if(!in_array($outcome, ['all','win','loss']))
			{
				abort(404);
			}

			$team_id = $teams -> where('slug', $team_slug) -> first() -> id;
			$data['filtered_team'] = $teams -> where('slug', $team_slug) -> first() -> name;
		}
		$data['teams'] = $teams;
		$data['games'] = $game -> lists($team_id, $outcome);
		return view('web.games', $data);
	}

	public function gameResult($game_id, PerformanceRepository $performance, GameRepository $game)
	{
		$game_performance = $performance -> lists($game_id);

		if(count($game_performance) > 0){
			$unique = $game_performance->unique('team_id');
			$team = $unique->values()->all();

			$team_x_id = $team[0] -> team_id;
			$team_y_id = $team[1] -> team_id;

			$data['game'] = $game -> details($game_id);
			$data['scores'] = explode('-',  $data['game'] -> score);
			$data['team_x_performance'] = $game_performance -> where('team_id', $team_x_id) -> sortByDesc('fantasy_points');
			$data['team_y_performance'] = $game_performance -> where('team_id', $team_y_id) -> sortByDesc('fantasy_points');

			return view('web.result', $data);
		}

		abort(404);
	}

	public function verifyEmail($manager_id, $confirmation_code, EmailConfirmation $emailConfirmation, Request $request, FranchiseRepository $franchise)
	{
		$manager['manager_id'] = $manager_id;
		$manager['confirmation_code'] = $confirmation_code;

	    try{

	    	$confirmed = $this -> dispatch(New ConfirmRegistration($manager));

	    	// Don't confuse dude, this is just for testing purposes. :)
	    	if (app() -> environment() == "testing")
	    	{
	    		return 'success';
	    	}

			if ($confirmed && $request -> has('team')) {

				$franchise  = $franchise -> tmpDetailsByID($request -> input('team'));

				$details['name'] = $franchise -> name;
				$details['manager_id'] = $manager_id;
				$details['logo_id'] = $franchise -> logo_id;

				$picks = [];

				foreach($franchise -> players as $player){
					array_push($picks, $player -> id);
				}

				$details['picks'] = $picks;

			    $this -> dispatch(New CreateFranchise($details));
			}

			if($confirmed){
				Auth::loginUsingId($manager['manager_id']);
			}
	    	
			return view('web.landings.verified');
	    } 
	    catch (\Exception $e) 
	    {
	    	$data['response_title'] = 'Request Failed';
	    	$data['response_heading'] = 'Account verification failed';
	    	$data['response_body'] = $e -> getMessage();

	    	return view('web.landings.generic', $data);
	    }
	}

	public function passwordReset(ManagerRepository $manager, Request $request)
	{
		$details['email'] = $request -> input('email');
		$details['token'] = $request -> input('token');

		$request_exist = $manager -> passwordResetRequestExist($details);

		if($request_exist)
		{
			return view('web.landings.password_reset');
		}

		abort(404);
	}

	public function termsAndCondition()
	{
		return view('web.terms_and_condition');
	}

	public function disclaimer()
	{
		return view('web.disclaimer');
	}

	public function privacy()
	{
		return view('web.privacy');
	}
	public function contact()
	{
		return view('web.contact');
	}

}
