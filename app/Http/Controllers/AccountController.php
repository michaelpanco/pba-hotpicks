<?php namespace App\Http\Controllers;

use Auth;
use Redirect;
use App\Repositories\ChannelRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\PostRepository;
use App\Repositories\ManagerRepository;
use App\Repositories\FranchiseRepository;
use App\Repositories\TeamRepository;
use App\Repositories\ExpressBetRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

use Session;

class AccountController extends Controller {

	public function __construct(){

	}

	public function index(ChannelRepository $channel, TeamRepository $team)
	{
		$data['teams'] = $team -> lists();
		$data['team_channels'] = $channel -> teams();
		return view('web.auth.dashboard', $data);
	}

	public function managerProfile($manager_slug, ManagerRepository $manager_repository, FranchiseRepository $franchise_repository)
	{
		$data['manager'] = $manager_repository -> getManager('slug', $manager_slug);

		if($data['manager']){
			$data['team'] = $franchise_repository -> manager($data['manager'] -> id);
			$params['account_id'] = (Auth::check()) ? Auth::user() -> id : 0;
			$params['manager_id'] = $data['manager'] -> id;
			$data['relationship'] = $manager_repository -> managerRelationship($params);
			return view('web.managers.profile', $data);
		}

		abort(404);
	}

	public function messages(TeamRepository $team)
	{
		$data['teams'] = $team -> lists();
		return view('web.auth.messages', $data);
	}

	public function conversation($manager_slug, ManagerRepository $manager, Request $request, TeamRepository $team)
	{	
		if(Auth::user() -> slug == $manager_slug)
		{
			return redirect()->route('dashboard');	
		}

		$manager_details = $manager -> getManager('slug', $manager_slug);

		if($manager_details){

		$data['manager_fullname'] = $manager_details -> firstname . ' ' . $manager_details -> lastname;
		$data['recipient'] = $manager_details -> id;
		$data['manager_slug'] = $manager_slug;
		$data['unread'] = $request -> input('unread');
		$data['teams'] = $team -> lists();
		return view('web.auth.conversation', $data);

		}

		abort(404);
	}

	public function friends(TeamRepository $team)
	{
		$data['teams'] = $team -> lists();
		return view('web.auth.friends', $data);
	}

	public function referals(TeamRepository $team)
	{
		$data['teams'] = $team -> lists();
		return view('web.auth.referals', $data);
	}

	public function trade($player_id, PlayerRepository $player, FranchiseRepository $franchise)
	{
		$details['franchise_id'] = Auth::user() -> franchise -> id;
		$details['player_id'] = $player_id;

		$franchise_player_position = $franchise -> playerPosition($details);
		
		$data['player_id'] = $player_id;
		$data['player'] = $player -> detailsByID($player_id);
		$data['player_position'] = $franchise_player_position;
		$data['players'] = $player -> lists($franchise_player_position);
		$data['can_trade'] = true;

		$franchise_players = $franchise -> players($details['franchise_id']);
		$franchise_player_info = $franchise_players -> where('player_id', (int) $player_id) -> first();

		if(!$franchise_player_info)
		{
			return 'Player not existed in your team';
		}

		$date_hired = $franchise_player_info -> date_hired;
		$date_hire_obj = new \DateTime($date_hired);
		$date_now_obj = new \DateTime(date('Y-m-d'));
		$interval = $date_hire_obj -> diff($date_now_obj);
		
		if($interval -> days < 10)
		{
			$data['can_trade'] = false;
		}

		switch ($franchise_player_position) {

			case 1:
			$position_label = 'Point Guard';
			$allowed_position = '["PG","PG/SG"]';
			break;

			case 2:
			$position_label = 'Shooting Guard';
			$allowed_position = '["PG/SG","SG","SG/SF"]';
			break;

			case 3:
			$position_label = 'Small Forward';
			$allowed_position = '["SG/SF","SF","SF/PF"]';
			break;

			case 4:
			$position_label = 'Power Forward';
			$allowed_position = '["SF/PF","PF","PF/C"]';
			break;

			case 5:
			$position_label = 'Center';
			$allowed_position = '["PF/C","C"]';
			break;
		}

		$data['position_label'] = $position_label;
		$data['allowed_position'] = $allowed_position;

		return view('web.auth.trade', $data);
	}

	public function expressBet(ExpressBetRepository $expressBet, TeamRepository $team)
	{
		$data['latestBets'] = $expressBet -> latest();

		$latest_bets_arr = [];

		foreach($data['latestBets'] as $latest){
			array_push($latest_bets_arr, $latest -> id);
		}

		$my_bets = $expressBet -> myBets(Auth::user() -> id);

		$my_4_bets = clone $my_bets;
		$my_all_bets = clone $my_bets;

		$data['myLatestBets'] = $my_4_bets -> orderBy('id', 'desc') -> take(4) -> lists('express_bet_id');
		$data['myLatestBetList'] = $my_bets -> where('datetime', '<', date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +30 days'))) -> get();

		$data['myAllBets'] = $my_all_bets -> with('details') -> whereNotIn('express_bet_id', $latest_bets_arr) -> orderBy('id', 'desc') -> paginate(10);
		$data['teams'] = $team -> lists();
		return view('web.auth.expressbet', $data);
	}

	public function shop(ProductRepository $product, TeamRepository $team)
	{
		$data['products'] = $product -> lists();
		$data['account_credits'] = Auth::user() -> credits;
		$data['teams'] = $team -> lists();
		return view('web.auth.shop', $data);
	}

	public function settings(TeamRepository $team, PlayerRepository $player)
	{
		$data['teams'] = $team -> lists();
		$data['players'] = $player -> lists() -> sortBy('firstname');

		$manager_fav_matchup = Auth::user() -> fav_match_up;

		$data['matchup1'] = '';
		$data['matchup2'] = '';

		if($manager_fav_matchup){
			$matchup = explode('|', $manager_fav_matchup);
			$data['matchup1'] = $matchup[0];
			$data['matchup2'] = $matchup[1];
		}

		return view('web.auth.settings', $data);
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('/');
	}
}
