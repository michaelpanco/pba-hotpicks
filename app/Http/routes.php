<?php

require app_path('Http/Prepare.php');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

require app_path('Http/Routes/Manager.php');

Route::group(['prefix' => 'api/manager/'], function(){

	require app_path('Http/Routes/Api/Manager.php');

});

require app_path('Http/Routes/Admin.php');

Route::group(['prefix' => 'api/admin/'], function(){

	require app_path('Http/Routes/Api/Admin.php');

});