<?php namespace App\Http\Helpers;

use App\Models\Setting as SettingModel;

class Setting {

	protected $model;

	public function get($field)
	{
		$this -> model = new \App\Models\Setting;
		return $this -> model -> where('field', $field) -> first() -> value;
	}
}