<?php

use \Carbon\Carbon;

class Helper {

	public static function canUpVote($post_points_details)
	{
		foreach($post_points_details as $detail){
			if($detail -> manager_id == Auth::user() -> id){
				return false;
			}
		}

		return true;
	}

	public static function addFriendButton()
	{
		return '<a href="" class="btn btn-flat-blue btn-flat-blue-small mr5"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Friend</a>';
	}



	public static function lastActivity($date)
	{
		if((strtotime(date('Y-m-d h:i:s')) - strtotime($date)) < 604800)
		{
			$last_activity = Carbon::createFromTimeStamp(strtotime($date)) -> diffForHumans();
		}else{
			
			$last_activity = date('F j, Y g:i a', strtotime($date));
		}
		return $last_activity;
	}

	public static function carbonDate($timestamp)
	{
		if(date("m-d-Y") == date("m-d-Y", $timestamp))
		{
		    $date_label = 'Today ' . date('g:i a', $timestamp);
		}else if(date("m-d-Y", strtotime("-1 day")) == date("m-d-Y", $timestamp)){
		    $date_label = 'Yesterday ' . date('g:i a', $timestamp);
		}else{
		    $date_label = date('F j, Y g:i a', $timestamp);
		}
	    return $date_label;
	}

	public static function getLabelPosition($position)
	{
		switch ($position) {

			case 1:
			$label_position = 'Guard';	
			break;

			case 12:
			$label_position = 'Guard';	
			break;

			case 2:
			$label_position = 'Guard';	
			break;

			case 23:
			$label_position = 'Guard/Forward';	
			break;

			case 3:
			$label_position = 'Forward';	
			break;

			case 34:
			$label_position = 'Forward';	
			break;

			case 4:
			$label_position = 'Forward';	
			break;

			case 45:
			$label_position = 'Forward/Center';	
			break;

			case 5:
			$label_position = 'Center';	
			break;	

			default:
			$label_position = 'Unknown';	
			break;
		}

		return $label_position;
	}

	public static function getExactPosition($position)
	{
		switch ($position) {

			case 1:
			$label_position = 'PG';	
			break;

			case 12:
			$label_position = 'PG/SG';	
			break;

			case 2:
			$label_position = 'SG';	
			break;

			case 23:
			$label_position = 'SG/SF';	
			break;

			case 3:
			$label_position = 'SF';	
			break;

			case 34:
			$label_position = 'SF/PF';	
			break;

			case 4:
			$label_position = 'PF';	
			break;

			case 45:
			$label_position = 'PF/C';	
			break;

			case 5:
			$label_position = 'C';	
			break;

			default:
			$label_position = 'NA';	
			break;
		}

		return $label_position;
	}

	public static function ordinal($number)
	{
	    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
	    if ((($number % 100) >= 11) && (($number%100) <= 13)){
	        return $number. 'th';
	    }else{
	        return $number. $ends[$number % 10];
		}
	}

}

?>
