<?php

class Render {

	public static function addFriendButton($params)
	{
		switch ($params['relationship']) {

			case 'stranger':
				$link = '<a href="" class="add-friend btn btn-flat-blue btn-flat-blue-small mr5" ng-click="addFriend('. $params['manager_id'] .')" style="width:130px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Friend</a>';
				break;

			case 'friend':
				$link = '<a href="" class="remove-friend btn btn-flat-blue btn-flat-blue-small mr5" disabled><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Friend</a>';
				break;

			case 'pending':
				$link = '<a href="" class="btn btn-flat-blue btn-flat-blue-small disabled mr5" style="width:130px;">Pending</a>';
				break;

			case 'waiting':
				$link = '<a href="" ng-click="approveRequestViaProfile(' . $params['manager_id'] . ')" class="btn btn-flat-blue btn-flat-blue-small mr5 acceptRequestViaProfile' . $params['manager_id'] . '" style="width:130px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Accept</a>';
				break;

			default:
				$link = '';
				break;
		}

		return $link;
	}

	public static function specialContent($content)
	{
		$contents = explode("-", $content);

		switch($contents[1])
		{
			case 'join':
				$content = "<span class='cgreen'>has joined the league</span>";
			break;

			case 'leave':
				$content = "<span class='cred'>has leaved the league</span>";
			break;

			case 'start':
				$content = "<span class='bold'>has started the league</span>";
			break;
		}

		return $content;
	}

	public static function managerStatus($status)
	{
		switch($status)
		{
			case 'ACTV':
				$content = '<span class="label label-success">Active</span>';
			break;

			case 'PNDG':
				$content = '<span class="label label-warning">Pending</span>';
			break;

			default:
				$content = '<span class="label label-danger">Disabled</span>';
			break;
		}

		return $content;
	}
}