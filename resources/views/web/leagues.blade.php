@extends('web.layouts.default')

@section('title', 'Leagues - PinoyTenPicks')

@section('css')
@parent<link href="<% asset('assets/css/leagues.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="LeagueGuest" class="content-full" ng-controller="LeagueGuest">
	<div class="leagues-wrapper mb20">

    	<div class="row mb20">
	    	<div class="col-md-6">
	    		<?php if($own_league){ ?>
	    		<a href="<% route('league_profile', $own_league) %>" class="btn btn-default"><span class="glyphicon glyphicon-tower" aria-hidden="true"></span> My League</a>
	    		<?php } ?>
	    		<?php if($can_create){ ?>
	    		<a href="" class="btn btn-default" data-toggle="modal" data-target=".modal-create-league"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create League</a>
	    		<?php } ?>
	    	</div>
	    	<div class="col-md-6">
				<div class="search-mod">
					<div class="input-group mb20" style="display:inline">
						<form action="<% route('leagues') %>" method="GET">
							<input type="text" placeholder="League" name="search" class="form-control" value="<% $search %>" style="height:auto;width: 225px;" />
							<span class="input-group-btn">
								<input type="submit" class="btn btn-default" value="Search" />
							</span>
						</form>
			        </div>
		    	</div>
	    	</div>
    	</div>

        <div class="clearfix"></div>
        <?php if(!$leagues -> isEmpty()){ ?>
			<?php foreach($leagues as $league){ ?>
			<div class="item">
				<div class="panel panel-primary">
					<div class="panel-heading"><% $league -> name %></div>
					<div class="panel-body">
						<div class="mb10">
							<div class="row">
								<div class="col-md-8 cgray f12">
									<?php if($league -> password && !$league -> ready){ ?>
									<span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Protected
									<?php } ?>
								</div>
								<div class="col-md-4 text-right">
									<?php if($league -> ready){ ?>
									<span class="label label-primary">Started</span>
									<?php }else{ ?>
									<span class="label label-success">Available</span>
									<?php } ?>
									
								</div>
							</div>
						</div>
						<div class="host"><span class="cgray">Host :</span> <a href="<% route('profile', $league -> creator -> slug) %>"><% $league -> creator -> fullname %></a></div>
						<div class="participant mb10"><span class="cgray">Participants :</span> <% $league -> participants -> count() %></div>
						<div class="desc mb10 f12">
							<?php if(mb_strlen($league -> desc) > 120){ ?>
								<% substr($league -> desc, 0, 120) %>...
							<?php }else{ ?>
								<% $league -> desc %>
							<?php } ?>
						</div>
						<div class="text-center">
							<a class="btn btn-flat-blue btn-small" href="<% route('league_profile', $league -> id) %>">Visit</a>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php }else{ ?>
				<div class="text-center cred mb20 f16">No league found</div>
			<?php } ?>
		<div class="clearfix"></div>
		<div class="text-center"><?= $leagues->render() ?></div>
	</div>
	

@include('web.modals.create_league')	
</div>



@endsection

@section('jsdependency')

@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/leagueguest.js') %>"></script>
@endsection

@section('jsinline')
<script type="text/javascript">
</script>


@endsection