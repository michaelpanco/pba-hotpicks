@extends('web.layouts.whitebg')

@section('title', 'Terms and Condition - PinoyTenPicks')
@section('description', 'Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.')

@section('content')
<div class="content-full standard-page">
<h1>Terms and Condition</h1>
<p class="dgray">Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>
<p class="mb20 dgray">By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
<p class="bold mb20">The use of this website is subject to the following terms of use:</p>

<ul class="list mb20">
	<li class="dgray">The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
	<li class="dgray">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
	<li class="dgray">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
	<li class="dgray">This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
	<li class="dgray">All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.</li>
	<li class="dgray">Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li>
	<li class="dgray">From time to time this website may also include links third-party websites or services that are not owned or controlled by PinoyTenPicks. We have no responsibility for the content of the linked website(s).</li>
	<li class="dgray">We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</li>
</ul>

<p class="bold">Our Terms and Condition may change from time to time and all updates will be posted on this page.</p>
</div>
@endsection

