@extends('web.layouts.whitebg')

@section('title', 'Rules - PinoyTenPicks')
@section('description', 'Playing PinoyTenPicks is very easy, just create a franchise and pick your preferred 10 players and start manage it in your own hands.')

@section('content')
<div class="content-full standard-page">
<h1>How to Play</h1>

<h2 class="allcaps">Creating Franchise</h2>

<p class="mb20 dgray">Creating your own franchise is very easy, You can create your own team just by going to the 'Create your team' link located at the top menu. Inside the page you can decide what name and logo you prefered for your own team. You can pick 10 players under your franchise.</p>


<p class="mb20">10 Players should compose of :</p>


<ul class="list mb20">
	<li class="dgray">2 PointGuard</li>
	<li class="dgray">2 ShootingGuard</li>
	<li class="dgray">2 SmallForward</li>
	<li class="dgray">2 PowerForward</li>
	<li class="dgray">2 Center</li>
</ul>

<p class="mb20 dgray">When creating a franchise, you will be given an initial of 30,000 credits to form your own franchise, each player has different salary, of course those awesome players will be given a high amount of salary so it’s very important to decide which player you will pick under the given budget and make a better choice to compete against different franchises.</p>

<hr/>
<h2 class="allcaps">Trading Players</h2>

<p class="mb20 dgray">Trading is very important specially if you want to upgrade your lineup to compete with other team. You can make a trade by going to your franchise page and select those player you want to trade for a better player you think that can boost your team.</p>

<p class="bold">Trading Restriction</p>

<p class="nomargin dgray">- Let Player A is your current player</p>
<p class="mb20 dgray">- Let Player B is the player you want for a trade</p>


<ul class="list mb20">
	<li class="dgray">If the Player A's  salary  is equivalent or more than the salary of Player B, you can make a direct trade. But if Player A's salary is less than the salary of Player B then you should pay the excess salary of Player B.</li>
	<li class="dgray">You can't trade a player less than 10 days since hired, You can only trade your player after 10 days.</li>
	<li class="dgray">Trading players only allowed during elimination, You are not allowed to make any changes in your lineup after it.</li>
	<li class="dgray">You can only trade your player for the player with the same position. e.g. trading your PG to a PG or PG/SG position. Trading a player having different position is not allowed.</li>
</ul>

<hr/>

<h2 class="allcaps">Joining/Creating League</h2>

<p class="mb20 dgray">If you have your franchise already, you can now join or create your own league to enjoy playing with others.</p>
<p class="bold">League Restriction</p>

<ul class="list mb20">
	<li class="dgray">League must be minimum of 10 participants to start and limited to 30 participants.</li>
	<li class="dgray">You cannot leave your league when it’s already started, you can only leave your league after the conference has ended.</li>
	<li class="dgray">You can’t join the league when it’s already started</li>
	<li class="dgray">League creator can perform the following actions :</li>
		<ul>
			<li class="dgray">Start the league</li>
			<li class="dgray">Kick any participants during recruitment process</li>
			<li class="dgray">Update the league description</li>
			<li class="dgray">Can put a league password for privacy</li>
		</ul>
</ul>

<hr/>

<h2 class="allcaps">Updating the League</h2>

<p class="dgray">Updating the league is a process of applying all the game result in the league, You can update your league any time but changes may only apply when the PBA game results was applied to the system. Any member of the league can update the league.</p>

<hr/>

<h2 class="allcaps">Player Fantasy Points</h2>

<p class="mb20 dgray">Player fantasy points will depends on the player performance of the game. When the player showed a good performance to the game, the better fantasy points will be given.</p>

<p class="dgray">Player Fantasy points computation as follows : </p>

<p class="bold">Merits</p>

<ul class="list mb20 lnm">
	<li class="dgray">Points x 2</li>
	<li class="dgray">Rebounds x 2</li>
	<li class="dgray">Assists x 3</li>
	<li class="dgray">Steals x 3</li>
	<li class="dgray">Blocks x 3</li>
	<li class="dgray" style="margin-bottom:20px !important">Winning Game x 10</li>
	<li class="dgray">Player Fantasy Points = (Points + Rebounds + Assists + Steals + Blocks + Winning Game) x 2</li>
</ul>

<p class="bold">Demerits</p>

<ul class="list mb20 lnm">
	<li class="dgray">Player Fantasy Points - Turnovers</li>
</ul>

<hr/>

<h2 class="allcaps">Earning Credits</h2>

<p class="mb20 dgray">Game credit is very important specially if you want to strengthen your lineup and trade for a high caliber player that needs to pay extra credit. Credit could be also use to buy some special stuff like franchise logo and badges.   There are 2 different ways right now to earn credits.</p>

<ul class="list mb20 lnm">
	<li class="dgray">You can earn credits base on the fantasy points earned by your players. If your Player A earns 120 fantasy points you will earn 120 credits as well.</li>
	<li class="dgray">You can also earn credits by playing the express bet. Express Bet is a betting game that based on PBA games.</li>
</ul>
<p>Note : In-game cash is not transferable into a real cash. You can only use credits just for the sake of the game</p>
<hr/>

<h2 class="allcaps">Rewards</h2>

<p class="mb20">After the conference, Top 3 players in the league will be rewarded</p>

<ul class="list mb20 lnm">
	<li class="dgray">1st 25,000 Credits + Champion Badge</li>
	<li class="dgray">2nd 15,000 Credits</li>
	<li class="dgray">3rd 10,000 Credits</li>
</ul>



</div>
@endsection

