<?php if($requests -> count() > 0){ ?>
	<?php foreach($requests as $request){ ?>
	<div class="fr-lists">
		<img width="45" class="mr5 img-circle" src="<% $request -> avatar %>">
		<a href="<% route('profile', $request -> slug) %>"><% $request -> fullname %></a>
		<div class="request-control requestControl<% $request -> id %>">
			<a href="" class="ctrl btn btn-flat-blue btn-small pull-right declineRequest<% $request -> id %>" ng-click="declineRequest(<% $request -> id %>)">Decline</a>
			<a href="" class="ctrl btn btn-flat-blue btn-small pull-right mr10 acceptRequest<% $request -> id %>" ng-click="approveRequest(<% $request -> id %>)">Accept</a>
		</div>
	</div>
	<?php } ?>
<?php }else{ ?>
<div class="text-center" style="padding:10px;">No friend request</div>
<?php } ?>