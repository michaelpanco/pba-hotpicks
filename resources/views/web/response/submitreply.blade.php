<div class="items mb5">
	<div class="reply-avatar fleft"><img width="52" src="<?= url(Auth::user() -> avatar) ?>" class="mr5 img-circle" /></div>
	<div class="reply-content fleft text-left">
		<p class="mb5">
			<a href=""><?= Auth::user() -> firstname ?> <?= Auth::user() -> lastname ?></a>
			<span class="intervaltime f10 cgray">Just Now</span>
		</p>
		<p><?= $reply -> content ?></p>
		<div class="controls text-right mb5 f12">
			<span class="cgreen mr5" ng-init="post<?= $reply -> id ?>point=<% $reply -> points -> count() %>">
				<ng-pluralize count="post<?= $reply -> id ?>point" when="{'0': '', '1': '1 Point', 'other': '{{post<?= $reply -> id ?>point}} points'}">
				</ng-pluralize>
			</span>
			<?php if(Helper::canUpVote($reply -> points)){ ?>
			<a href="" ng-hide="hideUpVote<?= $reply -> id ?>" ng-click="upVote(<?= $reply -> id ?>)" class="f16 vmiddle up-vote up-vote-<?= $reply -> id ?>"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span></a>
			<?php } ?>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
