<?php if($notifications -> count() > 0){ ?>
	<?php foreach($notifications as $notification){ ?>
	<?php $active_flag = ($notification -> seen === 0) ? 'new-notice-entry' : '' ?>
	<div class="notification-lists cgray <?= $active_flag ?>">
		<% $notification -> details %>
	</div>
	<?php } ?>
<?php }else{ ?>
<div class="text-center" style="padding:10px;">No notifications available</div>
<?php } ?>