<div id="post-132" class="channel-post new-item-transition">
	<div class="manager fleft">
		<div class="manager text-center mb10"><img width="64" src="<?= url($post -> manager -> avatar) ?>" class="mr5 img-circle" /></div>
		<div class="badges">
			<?php foreach($post -> manager -> badges('ENBL') -> take(3) -> get() as $badge){ ?>
				<img width="28" src="<% $badge -> src %>" alt="<% $badge -> name %>" title="<% $badge -> name %>" />
			<?php } ?>
		</div>
	</div>
	<div class="message fleft">
		<p class="mb5">
			<?php if($post -> manager -> league_leader){ ?><span title="League Leader" class="glyphicon glyphicon-star cyellow" aria-hidden="true"></span><?php } ?>
			<a href="<?= route('profile', Auth::user() -> slug) ?>"><?= Auth::user() -> firstname ?> <?= Auth::user() -> lastname ?></a><span class="intervaltime f10 cgray">Just Now<span></p>
		<p class="postcontent"><% $post -> content %></p>

		<div class="controls text-right mb5 f12">
			<?php if ($post -> replies -> count() > 0){ ?>
				<a href="" class="mr10" ng-click="toggleReplyComment(<?= $post -> id ?>)">
					<% $post -> replies -> count() %> <% $replies = ($post -> replies -> count() == 1) ? 'Reply' : 'Replies' %>
				</a>
			<?php }else{ ?>
				<a href="" class="mr10" ng-click="post<?= $post -> id ?> = !post<?= $post -> id ?>">
					<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
					Write Reply
				</a>
			<?php } ?>

			<span class="cgreen mr5" ng-init="post<?= $post -> id ?>point=<% $post -> points -> count() %>">
				<ng-pluralize count="post<?= $post -> id ?>point" when="{'0': '', '1': '1 Point', 'other': '{{post<?= $post -> id ?>point}} points'}">
				</ng-pluralize>
			</span>
			<?php if(Helper::canUpVote($post -> points)){ ?>
			<a href="" ng-hide="hideUpVote<?= $post -> id ?>" ng-click="upVote(<?= $post -> id ?>)" class="f16 vmiddle up-vote up-vote-<?= $post -> id ?>"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span></a>
			<?php } ?>
		</div>

		<div class="reply mb10 post-reply-<?= $post -> id ?>" ng-show="post<?= $post -> id ?>">
			<div compile="p<?= $post -> id ?>r" class="post-replies mb5"></div>
			<textarea ng-model="replyContentPost<?= $post -> id ?>" class="mb10 form-control replyinput" placeholder="Write your reply"></textarea>
			<a ng-click="replyPost(<?= $post -> id ?>, replyContentPost<?= $post -> id ?>)" href="" class="field trigger btn btn-flat-blue btn-small pull-right">
				<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> Reply
			</a>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>