<?php if($messages -> count() > 0){ ?>
	<?php if($render == 'page'){ ?><div class="messageWrapper"><?php } ?>
		<?php foreach($messages as $message){ ?>
			<?php 
			$recipient_name = ($message -> sender == $account -> id) ? $message -> recipient_firstname . ' ' . $message -> recipient_lastname  : $message -> sender_firstname . ' ' . $message -> sender_lastname;
			$recipient_avatar = ($message -> sender == $account -> id) ? $message -> recipient_avatar  :  $message -> sender_avatar;
			$recipient_slug = ($message -> sender == $account -> id) ? $message -> recipient_slug  :  $message -> sender_slug;
			$flag = ($message -> read == 0 && $message -> recipient == $account -> id) ? 'unread' : 'read';
			$action = ($message -> read == 0 && $message -> recipient == $account -> id) ? '?unread=1' : '';
			$sent = ($message -> sender == $account -> id) ? '<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>' : '';
			$num_char = ($render == 'page') ? 200 : 50;
			$message_text = (strlen($message -> message) >= $num_char) ? mb_substr($message -> message, 0, $num_char) . '...' : $message -> message;
			?>
			<div class="item">
				<a class="<?= $flag ?>" href="<% route('conversation', ['manager_slug' => $recipient_slug]) %><% $action %>">
					<div class="left">
						<img width="55" class="img-circle" src="<?= url($recipient_avatar) ?>" />
					</div>
					<div class="right">
						<div class="msg-details">
							<div class="left"><?= $recipient_name ?></div>
							<?php if($render == 'page'){ ?>
							<div class="right"><?= date('M j, Y g:i A', strtotime($message -> created_at)) ?></div>
							<?php } ?>
							<div class="clearfix"></div>
						</div>
						<div class="msg"><?= $sent ?><?= $message_text ?></div>
					</div>
					<div class="clearfix"></div>
				</a>
			</div>
		<?php } ?>
		</div>
	<?php if($render == 'page'){ ?></div><?php } ?>
	<div class="text-right" style="padding-right:15px;"><?= (new App\Pagination\MessagesPagination($messages))->render() ?></div>
<?php }else{ ?>
<div style="padding-top: 10px;text-align: center;">You don't have any message</div>
<?php } ?>