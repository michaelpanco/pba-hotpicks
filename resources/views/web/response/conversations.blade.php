<?php if($conversations -> count() > 0){ ?>
	<?php $counter = 1; ?>
	<?php foreach($conversations as $conversation){ ?>

		<?php
		$messageID = $conversation -> id;
		$conversationID = $conversation -> conversation_id;
		$recipient_name = ($conversation -> sender == $account -> id) ? $conversation -> recipient_firstname . ' ' . $conversation -> recipient_lastname  : $conversation -> sender_firstname . ' ' . $conversation -> sender_lastname;
		$recipient_avatar = $conversation -> sender_avatar;
		$sender_slug = $conversation -> sender_slug;
		$message_text = (strlen($conversation -> message) >= 200) ? mb_substr($conversation -> message, 0, 200) . '...' : $conversation -> message;
		$mtype = ($conversation -> sender == $account -> id) ? 'sender' : 'recipient';
		$mleft = ($conversation -> sender == $account -> id) ? 'right' : 'left';
		$mright = ($conversation -> sender == $account -> id) ? 'left' : 'right';
		?>

		<?php if($counter == 1){ ?>
		<% $messageID %>
		/*super#0291873645#master*/
		<?php } ?>

		<div id="msg-<% $messageID %>" class="<?= $mtype ?> mb15">
			<div class="<?= $mleft ?>">
				<a href="<% route('profile', $sender_slug) %>"><img width="55" class="mr5 img-circle" src="<?= url($recipient_avatar) ?>"></a>
			</div>
			<div class="<?= $mright ?>">
				<div class="msg">
					<% $message_text %>
				</div>
				<div class="datetime cgray">
					<span class="glyphicon glyphicon-time" aria-hidden="true"></span><?= Helper::carbonDate(strtotime($conversation -> created_at)) ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php $latestMessageMarker = $messageID ?>
		<?php $counter++; ?>

	<?php } ?>

	<span ng-init="conversationID='<% $conversationID %>'">
	<input type="hidden" id="conversationIDval" value="<% $conversationID %>">
	/*super#0291873645#master*/
	<% $messageID %>

<?php }else{ ?>
0
/*super#0291873645#master*/
<div style="padding: 10px 0;text-align:center" class="cgray" ng-hide="hideNoPostShow">You don't have any conversation</div>
<input type="hidden" id="conversationIDval" value="<?= str_random(10) ?>">/*super#0291873645#master*/0
<?php } ?>