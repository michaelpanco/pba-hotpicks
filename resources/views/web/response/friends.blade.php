<?php if($friends -> count() > 0){ ?>
	<div class="friendPageWrapper">
	<?php foreach($friends as $friend){ ?>
		<div class="item">

			<div class="left">
				<img width="60" class="mr5 img-circle" src="<% url($friend -> avatar) %>">
			</div>

			<div class="right">
				<a href="<% route('profile', ['manager_slug' => $friend -> slug]) %>" class="manager"><% $friend -> fullname %></a>
				<?php if((strtotime(date('Y-m-d h:i:s')) - strtotime($friend -> last_online)) < 120){ ?>
					<span class="cgreen">Online</span>
				<?php }else{ ?>
					<span class="cgray">
						Last online <% Helper::lastActivity($friend -> last_online) %>
					</span>
				<?php } ?>

			</div>

			<div class="cleafix"></div>

		</div>
	<?php } ?>
	</div>
	<div class="text-right"><?= (new App\Pagination\FriendsPagination($friends))->render() ?></div>
<?php }else{ ?>
<div style="font-size: 16px;padding-top: 70px;text-align: center;">You don't have any friends. Go on and make friends. :)</div>
<?php } ?>
