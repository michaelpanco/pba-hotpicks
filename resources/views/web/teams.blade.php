@extends('web.layouts.whitebg')

@section('title', 'PBA Teams - PinoyTenPicks')


@section('css') @parent @stop

<?php if(Auth::check()){ ?>
@parent

@stop
<?php } ?>
@section('content')
<div class="content-full text-center">
	<div class="team-wrapper">
	<?php foreach($teams as $team){ ?>
	<div class="item">
		<div style="float:left;margin-right:10px">
		<a href="<% route('team_profile', $team -> slug) %>"><img src="<% $team -> logo %>"></a>
		</div>
		<div style="float:left" class="text-left">
			<div><a href="<% route('team_profile', $team -> slug) %>"><% $team -> name %></a></div>
			<div><span class="cgray">Manager :</span> <% $team -> manager %></div>
			<div><span class="cgray">Coach :</span> <% $team -> coach %></div>
			<?php if($team -> titles > 0){ ?>
			<div><span class="cgray">Titles :</span> <% $team -> titles %></div>
			<?php } ?>
			<div><span class="cgray">Joined :</span> <% $team -> joined %></div>
		</div>
	</div>
	<?php } ?>
	</div>
</div>
@endsection

