@extends('web.layouts.default')

@section('title', 'PBA Players - PinoyTenPicks')

@section('css') @parent <link href="<% asset('assets/css/players.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent

@stop
<?php } ?>
@section('content')
<div id="player-wrapper">
<?php foreach($players as $player){ ?>
	<div class="item">
		<div style="float:left;margin-right:10px;">
			<a href="<% route('player_profile', $player -> slug) %>">
				<img width="60" src="<% $player -> avatar %>" class="img-circle">
			</a>
		</div>
		<div style="float:left">
			<div><a href="<% route('player_profile', $player -> slug) %>"><% $player -> firstname %> <% $player -> lastname %></a></div>
			<div><span class="cgray">Height :</span> <% $player -> height %></div>
			<div><span class="cgray">School :</span> <% $player -> school %></div>
			<div><span class="cgray">Position :</span> <?= Helper::getLabelPosition($player -> position) ?></div>
			<div><span class="cgray">Salary :</span> <% number_format($player -> salary) %></div>
			<div><span class="cgray">Team :</span> <a href="<% route('team_profile', $player -> team -> slug) %>"><% $player -> team -> name %></a></div>
		</div>
		<div class="clearfix"></div>
	</div>
<?php } ?>
</div>
@endsection