@extends('web.layouts.single')

@section('title')<% $manager -> fullname %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/profile.css') %>" rel="stylesheet">
@stop

@section('content')
<div class="mb10">
@include('web.ads.upper_header_image')
</div>
<div class="panel panel-default manager-profile" ng-controller="Profile">
	<div class="panel-body">
		<div class="row mb20">
			<div class="col-xs-3 text-center">
				<div class="manager text-center mb10"><img src="<?= url($manager -> avatar) ?>" class="mr5 img-circle avatar" /></div>
				<div class="badges">
					<?php foreach($manager -> badges() -> get() as $badge){ ?>
						<img class="mb5" src="<% $badge -> src %>" alt="<% $badge -> name %>" title="<% $badge -> name %>" />
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-9" style="padding-right:0;">
				<div class="stats mb20">
					<div class="fleft item">
						<div class="label f14 text-center">Credit</div>
						<div class="count cgreen text-center"><% number_format($manager -> credits) %></div>
					</div>
					<div class="fleft item">
						<div class="label f14 text-center">Friends</div>
						<div class="count cgreen text-center"><% $manager -> friends -> count() %></div>
					</div>
					<div class="fleft item">
						<div class="label f14 text-center">Badges</div>
						<div class="count cgreen text-center"><% $manager -> badges -> count() %></div>
					</div>
					<div class="fright">
						<?= Render::addFriendButton(["relationship" => $relationship, "manager_id" => $manager -> id]) ?>
						<?php if(Auth::check() && $manager -> id != Auth::user() -> id){ ?>
						<a href="<% route('conversation', $manager -> slug) %>" class="btn btn-flat-blue btn-flat-blue-small">
							<span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Message
						</a>
						<?php } ?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="name mb10">
					<h1 style="margin: 0 0 10px;font-size:22px">
					<?php if($manager -> league_leader){ ?><span title="League Leader" class="glyphicon glyphicon-star profileStar" aria-hidden="true"></span><?php } ?>
					<% $manager -> fullname %> <?php if(Auth::check() && $account -> privilege == 'administrator'){ ?><a href="<% route('admin_manager_details', $manager -> id) %>" target="_blank" class="btn btn-info">View Profile</a><?php } ?> <span>Member Since <% date('F Y', strtotime($manager -> date_registered)) %></span>
					</h1>
				</div>
				
				<div class="basic">
					<?php if($manager -> fav_team != "" || $manager -> fav_2nd_team != "" || $manager -> fav_player != "" || ($manager -> fav_match_up != "" && $manager -> fav_match_up != "|")){ ?>
						<div class="mb5 f12 bold cgray">Favorites</div>
						<?php if($manager -> fav_team != ""){ ?>
						<div class="mb5"><span class="field">PBA Team</span><span class="value"><% $manager -> fav_team %></span></div>
						<?php } ?>
						<?php if($manager -> fav_player != ""){ ?>
						<div class="mb5"><span class="field">PBA Player</span><span class="value"><% $manager -> fav_player %></span></div>
						<?php } ?>
						<?php if($manager -> fav_team != "" && $manager -> fav_2nd_team != "" && $manager -> fav_team != $manager -> fav_2nd_team){ ?>
						<div class="mb5"><span class="field">Second PBA Team</span><span class="value"><% $manager -> fav_2nd_team %></span></div>
						<?php } ?>
						<?php if($manager -> fav_match_up != ""){ ?>
						<?php $matchup = explode('|', $manager -> fav_match_up) ?>
						<div class="mb5"><span class="field">Team Match-Up</span><span class="value"><% $matchup[0] %> <span class="cgray">vs</span> <% $matchup[1] %></span></div>
						<?php } ?>
					<?php }else{ ?>
					No more details available
					<?php } ?>

					<?php if(Auth::check() && in_array($account -> privilege, ['administrator', 'moderator'])){ ?>
					<div style="margin-top:10px">
						<?php if($manager -> restrain_until != "" && date('Y-m-d') < $manager -> restrain_until){ ?>
							<div class="alert alert-danger text-center" style="width:350px" role="alert">Restrain until <% date('F j, Y', strtotime($manager -> restrain_until)) %></div>
						<?php }else{ ?>
							<a href="" class="btn btn-danger" data-toggle="modal" data-target=".modal-restrain"><span aria-hidden="true" class="glyphicon glyphicon-minus-sign"></span> Restrain</a>
						<?php } ?>
					</div>
					<?php } ?>

				</div>
			</div>
		</div>
		<hr/>
		<div class="manager-franchise">
			<?php if(isset($team -> players)){ ?>
			<div class="mb20">
				<div class="info">
					<div class="fleft"><img class="mb10 mr10" src="<% asset('assets/img/franchise_logos/64/' . $team -> icon -> filename) %>"></div>
					<div class="fleft">
						<div class="team mb5"><% $team -> name %></div>
						<div class="value mb5"><span class="cgray">Franchise Value :</span> <% number_format($team -> players -> sum('salary')) %></div>
						<?php if($team -> league_id){ ?>
						<div class="value mb5"><span class="cgray">League :</span> <a href="<% route('league_profile', $team -> league -> id) %>"><% $team -> league -> name %></a></div>
						<?php } ?>
					</div>
					<div class="clearfix"></div>
				</div>

			</div>
			<div class="players">
				<?php foreach($team -> players as $player){ ?>
				<div class="player text-center">
					<div class="inner">
						<a href="<% route('player_profile', $player -> slug) %>"><img width="60" class="img-circle mb10" src="<% asset($player -> avatar) %>" /></a>
						<div class="f12"><a href="<% route('player_profile', $player -> slug) %>"><% substr($player -> firstname, 0, 1) %>. <% $player -> lastname %></a></div>
						<div class=""><span class="cgray">Salary :</span> <% number_format($player -> salary) %></div>
						<div class=""><span class="cgray">Fantasy :</span> <span class="cgreen" style="font-size:14px"><% number_format($team -> statistic -> where('player_id', $player -> id) -> first() -> fantasy_points) %></span></div>
						<?php $date_hired = $team -> statistic -> where('player_id', $player -> id) -> first() -> date_hired ?>
						<div class="mb10 f12"><span class="cgray">Hired :</span><br/> <% date('M j, Y', strtotime($date_hired)) %></div>
					</div>
					<div class="f12"><% Helper::getLabelPosition($player -> position) %></div>
				</div>
				<?php } ?>
			</div>
			<?php }else{ ?>
				<div class="text-center" style="padding: 0 0 15px;">Team not available</div>
			<?php } ?>
		</div>
	</div>

@include('web.modals.restrain')	

</div>

@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/profile.js') %>"></script>
@endsection