@extends('web.layouts.single')

@section('title')<% $team -> name . ' - PinoyTenPicks' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/franchise_profile.css') %>" rel="stylesheet">
@stop

@section('content')
<div class="mb10">
@include('web.ads.upper_header_image')
</div>
<div class="panel panel-default franchise-profile" ng-controller="Profile">
	<div class="panel-body">
		<div class="manager-franchise">
			<?php if(isset($team -> players)){ ?>
			<div class="mb15">
				<div class="info mb10">
					<div class="fleft mr10"><img class="mb10 mr10" src="<% asset('assets/img/franchise_logos/64/' . $team -> icon -> filename) %>"></div>
					<div class="fleft">
						<div class="team mb10" style="font-size:24px;"><% $team -> name %></div>
						<div><span class="ffield">Value :</span> <% number_format($team -> players -> sum('salary')) %></div>
						<?php if($team -> league){ ?>
						<div><span class="ffield">League :</span> <a href=""><% $team -> league -> name %></a></div>
						<?php } ?>
						<div><span class="ffield">Created :</span> <% date('F j, Y', strtotime($team -> created_at)) %></div>
						<div>
							<span class="ffield">Owner :</span> <a href="<% route('profile', $team -> manager -> slug) %>">
							<img width="50" src="<% asset($team -> manager -> avatar) %>" class="img-circle avatar"> <% $team -> manager -> fullname %></a></div>
						
					</div>
					<div class="clearfix"></div>
				</div>
<?php if(isset($account) && $account -> franchise && $franchise == $account -> franchise -> id){ ?>
<span class="cgray">* You can only perform trade after 10 days since you hired your player.</span>
<?php } ?>
			</div>
			<div class="players">
				<?php foreach($team -> players as $player){ ?>
				<div class="player text-center">
					<div class="inner">
						<a href="<% route('player_profile', $player -> slug) %>"><img width="60" class="img-circle mb10" src="<% asset($player -> avatar) %>" /></a>
						<div class="f12"><a href="<% route('player_profile', $player -> slug) %>"><% substr($player -> firstname, 0, 1) %>. <% $player -> lastname %></a></div>
						<div class=""><span class="cgray">Salary :</span> <% number_format($player -> salary) %></div>
						<div class=""><span class="cgray">Fantasy :</span> <span class="cgreen" style="font-size:14px"><% number_format($team -> statistic -> where('player_id', $player -> id) -> first() -> fantasy_points) %></span></div>
						<?php $date_hired = $team -> statistic -> where('player_id', $player -> id) -> first() -> date_hired ?>
						<div class="mb10 f12"><span class="cgray">Hired :</span><br/> <% date('M j, Y', strtotime($date_hired)) %></div>
						
						<?php
							$date_hire_obj = new \DateTime($date_hired);
							$date_now_obj = new \DateTime(date('Y-m-d'));
							$interval = $date_hire_obj -> diff($date_now_obj);
							$trade_status = ($interval -> days < 10) ? 'disabled' : '';
							$button_type = ($interval -> days < 10) ? 'btn-default' : 'btn-primary';
						?>
						<?php if(isset($account) && $account -> franchise && $franchise == $account -> franchise -> id){ ?>
						<div><a href="<% route('trade', $player -> id) %>" class="btn <% $button_type %> btn-sm <% $trade_status %>">Trade</a></div>
						<?php } ?>
					</div>
					<div class="f12"><% Helper::getLabelPosition($player -> position) %></div>
				</div>
				<?php } ?>
			</div>
			<?php }else{ ?>
				<div class="text-center" style="padding: 0 0 15px;">Team not available</div>
			<?php } ?>
		</div>
	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/profile.js') %>"></script>
@endsection