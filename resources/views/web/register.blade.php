@extends('web.layouts.default')

@section('title')<% 'Register Account - PinoyTenPicks' %>@endsection
@section('description', 'To start playing, you must first register by filling out the fields. After registration you can create your own team.')


@section('css') @parent @stop

@section('content')
<div class="content-left fleft">
	<h1>Register</h1>
	<p class="mb20">To start playing, you must first register by filling out the fields. <?php if(!$team){ ?>After registration you can create your own team.<?php } ?></p>

	<div class="register-manager">
		<div class="row mb20">
			<div class="col-xs-6">
				<?php if($team){ ?>
				<div class="register-team" ng-init="team=<% $team -> id %>">
					<span class=" mr10">Team : </span>
					<img width="28" class="mr5" src="/assets/img/franchise_logos/32/<% $team -> icon -> filename %>"> <% $team -> name %>
				</div>
				<?php } ?>
				<input ng-model="register.firstname" type="text" class="field form-control mb10" placeholder="Firstname" />
				<input ng-model="register.lastname" type="text" class="field form-control mb20" placeholder="Lastname" />
				<p class="text-right">Password should at least 6 characters</p>
				<input ng-model="register.password" type="password" class="field form-control mb10" placeholder="Password" />
				<input ng-model="register.confirm_password" type="password" class="field form-control mb10" placeholder="Confirm Password" />
			</div>
			<div class="col-xs-6">
				<input ng-model="register.email" type="text" class="field form-control mb20" placeholder="Email" />
				<div class="row" style="margin-bottom:25px">
					<div class="col-xs-2">Gender</div>
					<div class="col-xs-10">
						<div class="radio nomargin" ng-init="register.gender='male'">
							<label><input ng-model="register.gender" type="radio" class="field" value="male">Male</label>
							<label><input ng-model="register.gender" type="radio" class="field" value="female">Female</label>
						</div>
					</div>
				</div>

				<p>Date of Birth</p>

				<div class="mb10">

					<!-- Month -->
					<div class="btn-group mr5">
						<button type="button" class="field btn btn-default" style="width:130px">{{selectedMonthLabel || 'Month'}}</button>
						<button type="button" class="field btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<?php for ($i = 1; $i < 13; ++$i) { ?>
							<?php $month_label = date("F", mktime(0, 0, 0, $i, 10)); ?>
							<li><a href="" ng-click="selectRegisterDOBmonth('<?= $i ?>', '<?= $month_label ?>')"><?= $month_label ?></a></li>
							<?php } ?>			
						</ul>
					</div>

					<!-- Day -->
					<div class="btn-group mr5">
						<button type="button" class="field btn btn-default" style="width:70px">{{selectedDay || 'Day'}}</button>
						<button type="button" class="field btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu" role="menu" style="height: 290px;overflow-y: scroll;width: 90px;min-width:90px">
							<?php for ($i = 1; $i < 32; ++$i) { ?>
							<li><a href="" ng-click="selectRegisterDOBday('<?= $i ?>')"><?= $i ?></a></li>
							<?php } ?>
						</ul>
					</div>

					<!-- Year -->
					<div class="btn-group">
						<button type="button" class="field btn btn-default" style="width:100px">{{selectedYear || 'Year'}}</button>
						<button type="button" class="field btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu" role="menu" style="height: 290px;overflow-y: scroll;width: 120px;min-width:120px">
							<?php for ($i = 2004; $i > 1950; $i--) { ?>
							<li><a href="" ng-click="selectRegisterDOByear('<?= $i ?>')"><?= $i ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="pull-right">
					{!! app('captcha')->display(); !!}
				</div>
			</div>
		</div>

		<p>By Registering to this site, you acknowledge, understand and further agree that you will observe and be willing to be bound by following <a href="<% route('terms_and_condition') %>">terms and conditions</a>.</p>

		<div class="text-right"><a href="" class="trigger field btn btn-flat-blue" ng-click="registerManager(register)">Register</a></div>
	</div>
</div>
@endsection

@section('sidebar')
<div class="content-right fleft">
	<div class="why-register mb20">
		<h4 class="mb20 fwnormal">Why Register?</h4>
		
		<div class="row mb10">
			<div class="col-xs-3"><img width="50" src="<% asset('assets/img/icon/ball_circle.png') %>" /></div>
			<div class="col-xs-9">
				<p class="mb5">Manage your own team</p>
				<p class="f12 cgray">Pick your desired players and improve each game.</p>
			</div>
		</div>

		<div class="row mb10">
			<div class="col-xs-3"><img width="50" src="<% asset('assets/img/icon/friends_circle.png') %>" /></div>
			<div class="col-xs-9">
				<p class="mb5">Challenge your friends</p>
				<p class="f12 cgray">Show what you've got and play competetively.</p>
			</div>
		</div>

		<div class="row mb10">
			<div class="col-xs-3"><img width="50" src="<% asset('assets/img/icon/interact_circle.png') %>" /></div>
			<div class="col-xs-9">
				<p class="mb5">Socialize with other manager</p>
				<p class="f12 cgray">Interact, reconcile and enjoy relationship together.</p>
			</div>
		</div>

		<div class="row mb10">
			<div class="col-xs-3"><img width="50" src="<% asset('assets/img/icon/badge_circle.png') %>" /></div>
			<div class="col-xs-9">
				<p class="mb5">Collect badges</p>
				<p class="f12 cgray">Unlock every badges and outshine your profile account.</p>
			</div>
		</div>

		<div class="row mb10">
			<div class="col-xs-3"><img width="50" src="<% asset('assets/img/icon/champion_circle.png') %>" /></div>
			<div class="col-xs-9">
				<p class="mb5">Win Championships</p>
				<p class="f12 cgray">Gain the fame and earn the championship rewards.</p>
			</div>
		</div>
	</div>

</div>
@endsection