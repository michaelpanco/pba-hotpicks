@extends('web.layouts.whitebg')

@section('title')<% $league -> name . ' - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/leagues.css') %>" rel="stylesheet">
@stop

@section('content')

<div class="content-left league-participan-left fleft" ng-controller="LeagueChannel">

	<div class="row">
		<div class="col-md-10">
			<h1 class="league-name"><% $league -> name %></h1>
			<div class="description cgray mb10" ng-init="leagueDesc='<% $league -> desc %>'">
				<div ng-hide="editDesc" ng-cloak>
					<span ng-bind="leagueDesc"></span>
					<?php if($account -> id == $league -> host){ ?>
					<a ng-click="editDesc = !editDesc" class="" href=""> Edit</a>
					<?php } ?>
				</div>
				<?php if($account -> id == $league -> host){ ?>
				<div class="edit-league-desc" ng-show="editDesc" ng-init="leagueDesc='<% $league -> desc %>'">
					<textarea class="form-control mb10" ng-model="leagueDesc"></textarea>
					<a class="field trigger btn btn-flat-blue btn-small pull-right" ng-click="updateLeagueDesc()" href="">Save</a>
					<a class="field btn btn-flat-blue btn-small pull-right mr5" href="" ng-click="editDesc = !editDesc">Cancel</a>
				</div>
				<?php } ?>
				<div class="clearfix"></div>
			</div>
			<?php if(!$league -> ready && $account -> id == $league -> host){ ?>
 			<div class="password-container mb20" style="width:290px;">
				<div class="edit-league-password" ng-show="editPassword" ng-init="leaguePassword='<% $league -> password %>'">
					<input type="text" class="form-control" ng-model="leaguePassword" />
					<div class="mb10 cgray f12">Put empty if you want to disable password</div>
					<a class="field trigger btn btn-flat-blue btn-small pull-right" ng-click="updateLeaguePassword()" href="">Update</a>
					<a class="field btn btn-flat-blue btn-small pull-right mr5" href="" ng-click="editPassword = !editPassword">Cancel</a>
				</div>
				<a ng-show="!editPassword" ng-click="editPassword = !editPassword" href="" class="btn btn-default btn-sm">Password</a>
				<span class="clearfix"></span>
			</div>
			<?php } ?>
			<div class="host mb10">Host : <a href="<% route('profile', $league -> creator -> slug) %>"><% $league -> creator -> fullname %></a></div>
		</div>
		<div class="col-md-2">
			<?php if(!$league -> ready){ ?>
				<?php if($account -> id == $league -> host){ ?>
 				<div class="start-container mb10">
					<a ng-click="startLeague()" href="" class="field trigger btn btn-flat-blue btn-small pull-right">Start</a>
					<span class="clearfix"></span>
				</div>
				<div class="dismiss-container">
					<a ng-click="dismissLeague()" href="" class="field trigger btn btn-flat-blue btn-small pull-right">Dismiss</a>
					<span class="clearfix"></span>
				</div>
				<?php }else{ ?>
				<div class="leave-container">
					<a ng-click="leaveLeague()" href="" class="field trigger btn btn-flat-blue btn-small pull-right">Leave</a>
					<span class="clearfix"></span>
				</div>
				<?php } ?>
			<?php }else{ ?>
			<div class="update-league-container">
				<a ng-click="updateLeague()" href="" class="field trigger btn btn-flat-blue btn-small pull-right">Update</a>
				<span class="clearfix"></span>
			</div>
			<?php } ?>
		</div>
	</div>

	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Team</th>
				<th>Owner</th>
				<?php if($league -> ready){ ?><th>Fantasy Points</th><?php } ?>
				<?php if(!$league -> ready && $account -> id == $league -> host){ ?><th></th><?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; ?>
			<?php foreach($league_participants as $participant){ ?>
			<tr class="franchise-id-<% $participant['participant_id'] %>">
				<td><?php if($league -> ready){ ?><% Helper::ordinal($i) %><?php }else{ ?><% $i %><?php } ?></td>
				<td>
				<a href="<% route('franchise_profile', $participant['participant_id']) %>">
					<img width="32" src="/assets/img/franchise_logos/64/<% $participant['franchise_logo'] %>" class="mr5"> <% $participant['franchise_name'] %>
				</a>
			</td>
			<td>
				<a href="<% route('profile', $participant['franchise_manager_slug']) %>">
					<img width="32" class="mr5 img-circle avatar" src="<% url($participant['franchise_manager_avatar']) %>"> <% $participant['franchise_manager_fullname'] %>
				</a>
			</td>
			<?php if($league -> ready){ ?><td class="cgreen" style="padding-left:60px"><% $participant['franchise_fantasy_points'] %></td><?php } ?>
			<?php if(!$league -> ready && $account -> id == $league -> host){ ?>
				<td class="text-right"><a href="" class="btn btn-sm btn-danger" ng-click="kickTeam('<% $participant['franchise_manager_fullname'] %>',<% $participant['participant_id'] %>)">Kick</a></td>
			<?php } ?>
			</tr>
			<?php $i++; ?>
			<?php } ?>
		</tbody>
	</table>
</div>
@endsection

@section('sidebar')
<div id="LeagueChannel" class="content-right league-participan-right fleft" ng-controller="LeagueChannel">
	<div class="status-post" ng-init="selectLeagueChannel(<% $league -> channel -> id %>)">
		<textarea spellcheck="false" placeholder="Share your thoughts" class="field form-control mb10 ng-pristine ng-untouched ng-valid" ng-model="postContent"></textarea>
		<a ng-click="createPost()" href="" class="field trigger btn btn-flat-blue btn-small pull-right">Post</a>
	</div>
	<div class="clearfix"></div>
	<div id="league-channel-content" class="league-channel-wrapper tab-content" compile="leagueChannelContent"></div>
	<div class="posts-loader"></div>
</div>
@endsection

@section('jsdependency')

@endsection

@section('jscustom')
<script src="<% asset('assets/js/vendor/nanoscroller.min.js') %>"></script>
<script type="text/javascript">
	var league_id = <% $league -> id %>;
	var league_host = <% $league -> host %>;
	var league_channel_id = <% $league -> channel -> id %>;
</script>
<script src="<% asset('assets/js/angular/league.js') %>"></script>
@endsection

@section('jsinline')
<script type="text/javascript">
$(window).scroll(function(){
	if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
		loadMorePosts();
	}
});
</script>
@endsection