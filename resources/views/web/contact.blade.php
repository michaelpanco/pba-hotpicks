@extends('web.layouts.default')

@section('title')<% 'Contact Us - PinoyTenPicks' %>@endsection

@section('content')
<div class="content-full standard-page">
<h1>Contact Us</h1>
<p>We're happy to answer any questions you have or provide you with an estimate. Just send us a message to 
<script type="text/javascript"><!--
function gen_mail_to_link(lhs,rhs,subject)
{
document.write("<a href=\"mailto");
document.write(":" + lhs + "@");
document.write(rhs + "?subject=" + subject + "\">" + lhs + "@" + rhs + "<\/A>"); } 
// -->
</script>
<script type="text/javascript"><!-- 
  gen_mail_to_link('contact','pinoytenpicks.com','Inquiry')
// -->
</script>


</p>
</div>
@endsection

