@extends('web.layouts.default')

@section('title')<% 'Create your own team - PinoyTenPicks' %>@endsection

@section('css') @parent <link href="<% asset('assets/css/create_franchise.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent

@stop
<?php } ?>
@section('content')

<div ng-controller="CreateTeam" class="create-team">
	<div class="col-md-12">

		<?php if(Auth::check() && $account -> franchise){ ?>
		<div class="alert alert-warning" role="alert" ng-init="hasTeam=true">
			<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> You already have a team, you can't create another one.
		</div>
		<?php } ?>

		<div class="row mb20">
			<div class="col-md-3">
				<span class="mr10 f16 {{salaryClass}}"><span class="cgray">Total Salary :</span> {{totalPickAmount() | number}}/<% number_format($initial_salary_cap) %></span>
			</div>
			<div class="col-md-6">
				<a href="" class="btn btn-default mr5" data-toggle="modal" data-target=".modal-change-team-logo">
					<img width="24" class="mr5" src="/assets/img/franchise_logos/32/{{selectedTeamLogo.filename}}" /> Change Logo
				</a>
				
				<input type="text" maxlength="26" placeholder="Name of your team" ng-model="name" class="team-name field form-control">
				
			</div>
			<div class="col-md-3 text-right">
				<a ng-click="createTeam()" class="trigger field btn btn-flat-blue" href=""><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Create Team</a>
			</div>
		</div>

		<div class="alert alert-warning" ng-show="limitReached" role="alert">
			<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> You've reached the salary limit. Please adjust your picks
		</div>
			<div class="alert alert-info" role="alert" style="font-size:16px">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  You can <strong>DRAG</strong> your desired player and <strong>DROP</strong> to your team's player position below.
			</div>

		<div class="pick-wrapper">

			<div class="pick-item" ng-repeat="picker in pickLists">
				<span class="cwhite">{{picker.type}}</span>
				<div class="pick-players" dnd-list="picker.dndlist" dnd-disable-if="picker.dndlist.length >= 1" dnd-allowed-types="picker.allowed_position">
			    	<div class="player-item text-center" ng-repeat="pick in picker.dndlist" post-render 
			            dnd-draggable="pick"
			            dnd-type="pick.position"
			            dnd-disable-if="pick.position == 'unknown'"
			            dnd-moved="picker.dndlist.splice($index, 1)"
			            dnd-effect-allowed="move">

			    		<img class="mb5 img-circle" width="45" src="{{pick.avatar}}">
			    		<div class="f10" compile="pick.name"></div>
			    		<div class="f10 cgray">{{pick.position}}</div>
			    		<div class="f10"><span class="cgray">Salary : </span>{{pick.salary | number}}</div>
			    	</div>
				</div>
			</div>

		</div>

	</div>


	<div class="row">
		<div class="col-md-12">

			<div class="row">
				<div class="col-md-6">
					<input class="form-control mb10" placeholder="Search Player" ng-model="search.name">
				</div>
				<div class="col-md-6 text-right">

					<div class="btn-group" data-toggle="buttons">
					  <label class="btn btn-primary active" ng-click="search.position=''">
					    <input type="radio" checked> All
					  </label>
					  <label class="btn btn-primary" ng-click="search.position='PG'">
					    <input type="radio"> PG
					  </label>
					  <label class="btn btn-primary" ng-click="search.position='SG'">
					    <input type="radio"> SG
					  </label>
					  <label class="btn btn-primary" ng-click="search.position='SF'">
					    <input type="radio"> SF
					  </label>
					  <label class="btn btn-primary" ng-click="search.position='PF'">
					    <input type="radio"> PF
					  </label>
					  <label class="btn btn-primary" ng-click="search.position='C'">
					    <input type="radio"> C
					  </label>
					</div>
				</div>
			</div>


			<div id="players-container" class="nano mb20">
			    <div class="nano-content" dnd-list="players">
			    	<div class="player-item text-center" ng-repeat="player in players | filter:search" post-render 
			            dnd-draggable="player"
			            dnd-type="player.position"
			            dnd-disable-if="player.position == 'unknown'"
			            dnd-effect-allowed="move">

			    		<img class="mb5 img-circle" width="45" src="{{player.avatar}}">
			    		<div class="f10" compile="player.name"></div>
			    		<div class="f10 cgray">{{player.position}}</div>
			    		<div class="f10"><span class="cgray">Salary : </span>{{player.salary | number : fractionSize}}</div>
			    	</div>
			    </div>
			</div>

		</div>
	</div>


	<div class="modal modal-change-team-logo">
	    <div class="modal-dialog" style="top: 70px;">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Select your Team logo</h4>
	            </div>
	            <div class="modal-body">
	            	<div class="franchise-logos">
		                <div class="item" ng-repeat="logo in franchiseLogos" ng-click="selectFranchiseLogo(logo.id, logo.filename)">
		                	<img width="55" src="/assets/img/franchise_logos/64/{{logo.filename}}" />
		                </div>
	            	</div>
	            </div>

	        </div>
	    </div>
	</div>


</div>


@endsection


@section('jsdependency')
<script>angular_dependency.push('dndLists');</script>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/createteam.js') %>"></script>
<script src="<% asset('assets/js/vendor/nanoscroller.min.js') %>"></script>
<script src="<% asset('assets/js/vendor/angular-drag-and-drop-lists.js') %>"></script>
@endsection



@section('jsinline')
<script>
var initial_salary_limit = <% $initial_salary_cap %>;
$('.nano').nanoScroller({ alwaysVisible: true });

angular.element(document).ready(function() {
	
	var appElement = document.querySelector('[ng-app=pbahotpicks]');
	var $scope = angular.element(appElement).scope();

	$scope.$apply(function() {

	    $scope.players = [
	    	<?php foreach($players as $player){ ?>
	    	{id: <% $player -> id %>, name: "<% $player -> fullname %>", avatar: "<% $player -> avatar %>", position: "<% Helper::getExactPosition($player -> position) %>", salary: <% $player -> salary %>},
            <?php } ?>
	    ];

	    $scope.selectedTeamLogo = {id:1, filename: 'star_red.png'};

	    $scope.franchiseLogos = [
	    	<?php foreach($franchise_logos as $logo){ ?>
	    	{id: <% $logo -> id %>, filename: "<% $logo -> filename %>"},
            <?php } ?>
	    ];

	    $scope.picks = {pg1:[],sg1:[],sf1:[],pf1:[],c1:[],pg2:[],sg2:[],sf2:[],pf2:[],c2:[]}

	    $scope.pickLists = [
	    	{type:"Point Guard", dndlist:$scope.picks.pg1, allowed_position: ["PG","PG/SG"]},
	    	{type:"Shooting Guard", dndlist:$scope.picks.sg1, allowed_position: ["PG/SG","SG","SG/SF"]},
	    	{type:"Small Forward", dndlist:$scope.picks.sf1, allowed_position: ["SG/SF","SF","SF/PF"]},
	    	{type:"Power Forward", dndlist:$scope.picks.pf1, allowed_position: ["SF/PF","PF","PF/C"]},
	    	{type:"Center", dndlist:$scope.picks.c1, allowed_position: ["PF/C","C"]},
	    	{type:"Point Guard", dndlist:$scope.picks.pg2, allowed_position: ["PG","PG/SG"]},
	    	{type:"Shooting Guard", dndlist:$scope.picks.sg2, allowed_position: ["PG/SG","SG","SG/SF"]},
	    	{type:"Small Forward", dndlist:$scope.picks.sf2, allowed_position: ["SG/SF","SF","SF/PF"]},
	    	{type:"Power Forward", dndlist:$scope.picks.pf2, allowed_position: ["SF/PF","PF","PF/C"]},
	    	{type:"Center", dndlist:$scope.picks.c2, allowed_position: ["PF/C","C"]}
	    ];
  		
	});
});
</script>
@endsection