<div class="modal modal-login">
    <div class="modal-dialog" style="top: 70px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-lock"></span> Account Login</h4>
            </div>
            <div class="modal-body">
                <div class="login-body login-manager">
                    <form ng-submit="loginManager(login)" autocomplete="off">

                        <div class="alert alert-danger text-center" ng-show="loginAlert" role="alert">{{errMessage}}</div>

                        <input type="text" ng-model="login.email" class="form-control field mb10" placeholder="Email">
                        <input type="password" ng-model="login.password" class="form-control field mb10" placeholder="Password">

                        <div class="row mb10">
                            <div class="col-xs-6"><label><input type="checkbox" ng-model="login.remember" ng-init="login.remember=false"> Remember me</label></div>
                            <div class="col-xs-6 text-right">
                                <a href="" ng-click="showForgotPasswordForm()">Forgot Password?</a>
                            </div>
                        </div>
                        <input type="submit" style="display:none;" value="Login">
                        <a href="" ng-click="loginManager(login)" class="field trigger btn btn-flat-blue mb10" style="width:100%">Login</a>
                        <a href="<% Facebook::getLoginUrl(['email','user_birthday']) %>" class="facebook-login btn btn-flat-blue mb10" style="width:100%;background-color:#3b5998;border:none;color:#fff">
                            <img src="<% asset('assets/img/facebook_login.png') %>" style="margin-right:10px;" />Login with Facebook
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>