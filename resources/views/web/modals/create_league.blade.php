<div class="modal modal-create-league">
    <div class="modal-dialog" style="top: 70px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create League</h4>
            </div>
            <div class="modal-body">
                <div class="create-league-body create-league">
                    <form ng-submit="createLeague(league)" autocomplete="off">

                        <div class="alert alert-danger" ng-show="leagueAlert" role="alert">{{errMessage}}</div>

                        <input type="text" ng-model="league.name" maxlength="28" class="form-control field mb10" placeholder="League name">
                        <textarea ng-model="league.description" maxlength="255" class="form-control field mb10" style="height:100px" placeholder="League description"></textarea>
                        <input type="text" ng-model="league.password" maxlength="16" class="form-control field mb5" placeholder="League password">
                        <div class="mb10 cgray f12 mb20">Keep password empty to disable password protection</div>
                        <input type="submit" style="display:none;" value="Login">
                        <a href="" ng-click="createLeague(league)" class="field trigger btn btn-flat-blue mb10" style="width:100%">Create League</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>