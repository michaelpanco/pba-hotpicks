<div class="modal modal-restrain">
    <div class="modal-dialog" style="top: 70px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-minus-sign"></span> Restrain Manager</h4>
            </div>
            <div class="modal-body">
                <div class="restrain-manager-body restrain-manager">
                    <form ng-submit="restrainManager()" autocomplete="off">

                        <p class="cgray text-center">Restraining manager will unable the manager to create post within a given period of time</p>
                        <input type="hidden" ng-model="restrainManagerID" ng-init="restrainManagerID=<% $manager -> id %>" >
						<div class="btn-group mb10">
							<button type="button" class="btn btn-default field" style="width:200px" ng-bind="selectedNoDays || 'No. of days'"></button>
							<button type="button" class="btn btn-default field dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="" ng-click="selectedNoDays='1 Day';restrainDays=1">1 Day</a></li>
								<li><a href="" ng-click="selectedNoDays='3 Days';restrainDays=3">3 Days</a></li>
								<li><a href="" ng-click="selectedNoDays='7 Days';restrainDays=7">7 Days</a></li>
								<li><a href="" ng-click="selectedNoDays='30 Days';restrainDays=30">30 Days</a></li>
							</ul>
						</div>

                        <textarea ng-model="restrainReason" maxlength="255" class="form-control field mb10" style="height:100px" placeholder="Restrain reason"></textarea>
                        <input type="submit" style="display:none;" value="Login">
                        <a href="" ng-click="restrainManager()" class="field trigger btn btn-flat-blue mb10" style="width:100%">Restrain</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>