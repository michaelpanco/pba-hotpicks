<div class="pba-team-standing">
	<div class="standing-header text-center">2015-2016 Philippine Cup Standings</div>
	<table class="ptp-table">
	      <thead>
	        <tr>
	          <th></th>
	          <th>Teams</th>
	          <th>W</th>
	          <th>L</th>
	        </tr>
	      </thead>
	      <tbody>
	      	<?php $i=1; ?>
	      	<?php foreach($teams -> sortByDesc('win') as $team){ ?>
	        <tr>
	          <th scope="row"><% $i %></th>
	          <td><a href="<% route('team_profile', $team -> slug) %>"><% $team -> name %></a></td>
	          <td><a href="<% route('games', $team -> slug) %>/win"><% round($team -> win) %></a></td>
	          <td><a href="<% route('games', $team -> slug) %>/loss"><% $team -> loss %></a></td>
	        </tr>
	        <?php $i++ ?>
	      	<?php } ?>
	      </tbody>
	</table>
</div>