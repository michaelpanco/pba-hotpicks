<!DOCTYPE html>
<html lang="en" ng-app="pbahotpicks">
<head>
	<meta charset="utf-8">
	<title>@yield('title', 'PBA Fantasy League - PinoyTenPicks')</title>
	<meta name="description" content="@yield('description', 'Pinoy Fantasy League based on the PBA (Philippine Basketball Association). Be a manager and choose your PBA players and challenge your friends in private leagues.')" />
	@section('nofollow')@show
	<link rel="shortcut icon" href="<% asset('assets/img/favicon.png') %>" type="image/x-icon">
	@section('css')<link href="<% asset('assets/css/bootstrap.min.css') %>" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Cabin+Condensed' rel='stylesheet' type='text/css'>
	<link href="<% asset('assets/css/bootstrap-theme.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/vendor.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/override.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/main.css') %>" rel="stylesheet">
	@show
	
</head>
<body id="PBAHotpicks" ng-controller="Guest">

<nav class="navbar main-navbar navbar-fixed-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-7 left">
				<?php if(Auth::check()){ ?>
				<a href="<?= URL::to('/') ?>">
					<img width="50" class="mt5 fleft mr15" src="<% asset('assets/img/pinoytenpicks_small.png') %>" alt="PinoyTenPicks">
				</a>
				<ul class="nav navbar-nav mr15">
					<li><a href="<?= URL::to('/') ?>">Home</a></li>
					<li><a href="<% route('leagues') %>">Leagues</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							PBA <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu" style="right:0">
							<li><a href="<% route('teams') %>">Teams</a></li>
							<li><a href="<% route('players') %>">Players</a></li>
							<li><a href="<% route('games') %>">Games</a></li>		
						</ul>
					</li>
				</ul>
				<p class="fleft mt15 f14">Credits : <?= number_format($account -> credits) ?></p>
				<?php }else{ ?>
				<h1 class="fleft f16 secondary-logo"><a href="/">PBA Fantasy League</a></h1>
				<ul class="nav navbar-nav">
					<li><a href="<% route('create_franchise') %>">Create your Team</a></li>
					<li><a href="<% route('leagues') %>">Leagues</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							PBA <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu" style="right:0">
							<li><a href="<% route('teams') %>">Teams</a></li>
							<li><a href="<% route('players') %>">Players</a></li>
							<li><a href="<% route('games') %>">Games</a></li>		
						</ul>
					</li>
					<li><a href="">How to Play</a></li>
				</ul>
				<?php } ?>
			</div>
			<div class="col-xs-5 text-right right">
				<?php if(Auth::check()){ ?>
				<a href="/dashboard" class="btn btn-azure mr5" style="margin-top:8px">
					<span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard
				</a>
				<div id="navbar" class="navbar-collapse collapse pull-right">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<img width="30" src="<?= url($account -> avatar) ?>" class="mr5 img-circle" />
								 <?= $account -> fullname ?> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu" style="right:0">
								<li><a href="/manager/<% $account -> slug %>"><span class="glyphicon glyphicon-globe mr5" aria-hidden="true"></span> Profile</a></li>
								<li><a href="<% route('settings') %>"><span class="glyphicon glyphicon-cog mr5" aria-hidden="true"></span> Settings</a></li>
								<li class="divider"></li>
								<li><a href="<% route('logout') %>"><span class="glyphicon glyphicon-log-out mr5" aria-hidden="true"></span> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<?php }else{ ?>
				<div style="margin-top:8px">
					<a href="" class="btn btn-azure mr5" data-toggle="modal" data-target=".modal-login">
						<span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Account Login
					</a>
					<a href="/register" class="btn btn-azure">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Create Account
					</a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</nav>

<div class="container" style="margin-top:10px;">

	<div class="centered-content">
		@yield('content')
	</div>

</div>

@include('web.modals.login')

<!-- Inject Angular Dependency -->
<script>var angular_dependency = ['oitozero.ngSweetAlert'];</script>
@section('jsdependency')@show

<!-- Scripts -->
<script src="<% asset('assets/js/jquery.min.js') %>"></script>
<script src="<% asset('assets/js/bootstrap.min.js') %>"></script>

<script src="<% asset('assets/js/angular/minified.js') %>"></script>
<script src="<% asset('assets/js/angular/primary.js') %>"></script>
<script src="<% asset('assets/js/angular/guest.js') %>"></script>

<script src="<% asset('assets/js/vendor/jquery.marquee.js') %>"></script>
<script src="<% asset('assets/js/vendor/sweet-alert.js') %>"></script>
@section('jscustom')@show

@section('jsinline')@show
@include('web.modules.analytics')
</body>
</html>