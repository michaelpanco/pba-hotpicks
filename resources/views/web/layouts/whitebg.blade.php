<!DOCTYPE html>
<html lang="en" ng-app="pbahotpicks">
<head>
	<meta charset="utf-8">
	<title>@yield('title', 'PBA Fantasy League - PinoyTenPicks')</title>
	<meta name="description" content="@yield('description', 'Pinoy Fantasy League based on the PBA (Philippine Basketball Association). Be a manager and choose your PBA players and challenge your friends in private leagues.')" />
	<link rel="shortcut icon" href="<% asset('assets/img/favicon.png') %>" type="image/x-icon">
@section('css')
	<link href='https://fonts.googleapis.com/css?family=Cabin+Condensed' rel='stylesheet' type='text/css'>
	<link href="<% asset('assets/css/bootstrap.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/bootstrap-theme.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/vendor.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/override.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/main.css') %>" rel="stylesheet">
	@show
</head>
<?php $add_class = (isset($nofooter)) ? 'nomargin' : '' ?>
<?php if(Auth::check()){ ?><body class="auth <% $add_class %>" ng-controller="Guest"><?php }else{ ?><body ng-controller="Guest"><?php } ?>

<nav class="navbar main-navbar navbar-fixed-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-7 left">
				<h1 class="fleft f16 secondary-logo"><a href="/">Pinoy Fantasy League</a></h1>
				<ul class="nav navbar-nav">
					<?php if(Auth::check() && $account -> franchise){ ?>
					
					<?php  }else{ ?>
					<li><a href="<% route('create_franchise') %>">Create your Team</a></li>
					<?php } ?>
					<li><a href="<% route('leagues') %>">Leagues</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							PBA <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu" style="right:0">
							<li><a href="<% route('teams') %>">Teams</a></li>
							<li><a href="<% route('players') %>">Players</a></li>
							<li><a href="<% route('games') %>">Games</a></li>		
						</ul>
					</li>
					<li><a href="<% route('how_to_play') %>">Rules</a></li>
				</ul>
			</div>
			<div class="col-xs-5 text-right right">
				<?php if(Auth::check()){ ?>
				<a href="/dashboard" class="btn btn-azure mr5" style="margin-top:8px">
					<span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard
				</a>
				<div id="navbar" class="navbar-collapse collapse pull-right">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<img width="30" src="<?= url($account -> avatar) ?>" class="mr5 img-circle" />
								 <?= $account -> fullname ?> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu" style="right:0">
								<li><a href="/manager/<% $account -> slug %>"><span class="glyphicon glyphicon-globe mr5" aria-hidden="true"></span> Profile</a></li>
								<li><a href="<% route('settings') %>"><span class="glyphicon glyphicon-cog mr5" aria-hidden="true"></span> Settings</a></li>
								<li class="divider"></li>
								<li><a href="<% route('logout') %>"><span class="glyphicon glyphicon-log-out mr5" aria-hidden="true"></span> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<?php }else{ ?>
				<a href="" class="btn btn-azure mr5 login-link" data-toggle="modal" data-target=".modal-login">
					<span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Account Login
				</a>
				<a href="<% route('register') %>" class="btn btn-azure">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Create Account
				</a>
				<?php } ?>
			</div>
		</div>
	</div>
</nav>

<div class="container mb20 main-header">
	<div class="row">
		<div class="col-xs-4 left">
			<a href="/">
				<img src="<% asset('assets/img/pinoytenpicks.png') %>" alt="PinoyTenPicks">
			</a>
		</div>
		<div class="col-xs-8 text-right right">
			@include('web.ads.upper_header_image')
		</div>
	</div>
</div>

<div class="announcements mb10">
	<div class="container">
		<div class="row">
			<div class="col-xs-2"><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Announcements</div>
			<div class="col-xs-10">
				<div class="marquee">
					<marquee behavior="scroll" scrollamount="1" direction="left">
						<?php foreach(DB::table('announcements') -> orderBy('order', 'asc') -> get() as $announcement){ ?>
						<span style="margin-right:30px;"><span style="margin-right:10px;color:#ed9700" class="glyphicon glyphicon-bookmark" aria-hidden="true"></span><?= $announcement -> text ?></span>
						<?php } ?>
					</marquee>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container" style="background:#fff;padding:30px">
	@yield('content')
	@yield('sidebar')
	<div class="clearfix"></div>
</div>
<?php if(!isset($nofooter)){ ?>


<div class="footer cwhite">


	<div class="container">

		<div class="clearfix"><div>

		<div class="menu-row fleft">
			<div class="head bold">Account</div>
			<ul>
				<?php if(Auth::check()){ ?>
				<li><a href="<% route('home') %>">Login</a></li><?php }else{ ?><li><a href="" data-toggle="modal" data-target=".modal-login">Login</a></li><?php } ?>
				<li><a href="<% route('register') %>">Register</a></li>
				<li><a href="<% route('create_franchise') %>">Create your Team</a></li>
			</ul>
		</div>

		<div class="menu-row fleft">
			<div class="head bold">PBA</div>
			<ul>
				<li><a href="<% route('teams') %>">Teams</a></li>
				<li><a href="<% route('players') %>">Players</a></li>
				<li><a href="<% route('games') %>">Games</a></li>	
			</ul>
		</div>

		<div class="menu-row fleft">
			<div class="head bold">Guidelines</div>
			<ul>
				<li><a href="<% route('how_to_play') %>">How to play</a></li>
			</ul>
		</div>

		<div class="menu-row fleft">
			<div class="head bold">Regulation</div>
			<ul>
				<li><a href="<% route('privacy') %>">Privacy Policy</a></li>
				<li><a href="<% route('terms_and_condition') %>">Terms and Condition</a></li>
				<li><a href="<% route('disclaimer') %>">Disclaimer</a></li>
			</ul>
		</div>

		<div class="menu-row fleft">
			<div class="head bold">Connect</div>
			<ul>
				<li><a href="<% route('contact') %>">Contact Us</a></li>
				<li><a href="https://www.facebook.com/PinoyTenPicks" rel="nofollow" target="_blank">Facebook</a></li>
			</ul>
		</div>

		<div class="clearfix"></div>


		<hr class="footer-hr" />
		<div class="row">
			<div class="footer-links col-xs-1">
				<img src="<% asset('assets/img/pinoytenpicks_gray.png') %>" alt="PinoyTenPicks">
			</div>
			<div class="col-xs-11" style="padding-left:50px;padding-top:15px;">
				PinoyTenPicks is a Pinoy Fantasy League based on the PBA (Philippine Basketball Association). Be a manager and choose your PBA players and challenge your friends in private leagues. PinoyTenPicks is a website created for basketball fans and it is not affiliated with the PBA or any of its properties, teams, players, and organizations.                        
			</div>
		</div>
	</div>
</div>
<?php } ?>

@include('web.modals.login')
@include('web.modals.forgot_password')

<!-- Web JS Global Config -->
<script>
	window.flag_online_polling_interval = <?= config('app.flag_online_polling_interval') ?>;
	var angular_dependency = ['oitozero.ngSweetAlert'];
</script>
@section('jsdependency')@show

<?php if(Auth::check()){ ?><script>window.auth = true;</script><?php }else{ ?><script>window.auth = false;</script><?php } ?>

<!-- Scripts -->
<script src="<% asset('assets/js/jquery.min.js') %>"></script>
<script src="<% asset('assets/js/bootstrap.min.js') %>"></script>
<script src="<% asset('assets/js/angular/minified.js') %>"></script>
<script src="<% asset('assets/js/angular/primary.js') %>"></script>
<script src="<% asset('assets/js/angular/guest.js') %>"></script>
<script src="<% asset('assets/js/vendor/jquery.marquee.js') %>"></script>
<script src="<% asset('assets/js/vendor/sweet-alert.js') %>"></script>
<script src="<% asset('assets/js/vendor/storage.min.js') %>"></script>
@section('jscustom')@show

@section('jsinline')@show
<?php if(Request::input('action') == 'login'){ ?>
<script type="text/javascript">
$( ".login-link" ).trigger( "click" );
</script>
<?php } ?>
@include('web.modules.analytics')
</body>
</html>