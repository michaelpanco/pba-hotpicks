<!DOCTYPE html>
<html lang="en" ng-app="pbahotpicks">
<head>
	<meta charset="utf-8">
	<title>@yield('title', 'PBA Fantasy League - PinoyTenPicks')</title>
	<meta name="description" content="@yield('description', 'Pinoy Fantasy League based on the PBA (Philippine Basketball Association). Be a manager and choose your PBA players and challenge your friends in private leagues.')" />
	<link rel="shortcut icon" href="<% asset('assets/img/favicon.png') %>" type="image/x-icon">
	@section('css')<link href="<% asset('assets/css/bootstrap.min.css') %>" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Cabin+Condensed' rel='stylesheet' type='text/css'>
	<link href="<% asset('assets/css/bootstrap-theme.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/vendor.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/override.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/main.css') %>" rel="stylesheet">
	@show
	
</head>
<body id="PBAHotpicks" ng-controller="Auth">

<nav class="navbar main-navbar navbar-fixed-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 left">
				<a href="<?= URL::to('/') ?>">
					<img width="50" class="mt5 fleft mr15" src="<% asset('assets/img/pinoytenpicks_small.png') %>" alt="PinoyTenPicks">
				</a>
				<ul class="nav navbar-nav mr15">
					<li><a href="<?= URL::to('/') ?>">Home</a></li>
					<li><a href="<% route('leagues') %>">Leagues</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							PBA <span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu" style="right:0">
							<li><a href="<% route('teams') %>">Teams</a></li>
							<li><a href="<% route('players') %>">Players</a></li>
							<li><a href="<% route('games') %>">Games</a></li>		
						</ul>
					</li>
				</ul>
				<p class="fleft mt15 f14">Credits : <?= number_format($account -> credits) ?></p>
			</div>
			<div class="col-xs-6 text-right right">
				<div id="navbar" class="navbar-collapse collapse pull-right">
					<ul class="nav navbar-nav">
						<li class="nav-not navbar-notice">
							<!-- Notices -->
							<a role="button" data-toggle="dropdown" href="" ng-click="fetchNotifications()">
								<span class="glyphicon glyphicon-bell" aria-hidden="true" style="font-size:18px"></span>
								<span class="counter" ng-show="notificationsCountShow" ng-bind="notificationsCount"></span>
							</a>
							<ul class="dropdown-menu notice notices" role="menu">
								<div class="notice-heading">
									<div class="left fleft bold">Notifications</div>
									<div class="right fleft">&nbsp;</div>
									<div class="clearfix"></div>
							    </div>
							    <li class="divider nomargin"></li>
								<div class="notice-wrapper nano" >
									<div class="nano-content">
										<div compile="noticeNotificationsContent"></div>
									</div>
								</div>
							    <li class="divider nomargin"></li>
							    <div class="notice-footer text-center">&nbsp;</div>
							</ul>
						</li>
						<li class="nav-msg navbar-notice">
							<!-- Messages -->
							<a role="button" data-toggle="dropdown" href="" ng-click="fetchLatestMessages()">
								<span class="glyphicon glyphicon-comment" aria-hidden="true" style="font-size:18px"></span>
								<span class="counter" ng-show="newMessagesCountShow" ng-bind="newMessagesCount"></span>
							</a>
							<ul class="dropdown-menu notice messages" role="menu">
								<div class="notice-heading">
									<div class="left fleft bold">Messages</div>
									<div class="right fleft">&nbsp;</div>
									<div class="clearfix"></div>
							    </div>
							    <li class="divider nomargin"></li>
								<div class="notice-wrapper nano">
									<div class="nano-content">
										<div id="noticeMessageWrapper" compile="noticeMessagesContent"></div>
									</div>
								</div>
							    <li class="divider nomargin"></li>
							    <div class="notice-footer text-center">
							    	<a href="<% route('messages') %>">View all</a>
							    </div>
							</ul>
						</li>
						<li class="nav-req navbar-notice" style="margin-right:30px">
							<!-- Requests -->
							<a role="button" data-toggle="dropdown" href="" ng-click="fetchRequests()">
								<span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size:18px"></span>
								<span class="counter" ng-show="requestsCountShow" ng-bind="requestsCount"></span>
							</a>
							<ul class="dropdown-menu notice requests" role="menu">
								<div class="notice-heading">
									<div class="left fleft bold">Friend Requests</div>
									<div class="right fleft">&nbsp;</div>
									<div class="clearfix"></div>
							    </div>
							    <li class="divider nomargin"></li>
								<div class="notice-wrapper nano" >
									<div class="nano-content">
										<div compile="noticeRequestsContent"></div>
									</div>
								</div>
							    <li class="divider nomargin"></li>
							    <div class="notice-footer text-center">
							    	&nbsp;
							    </div>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<img width="30" src="<?= url($account -> avatar) ?>" class="mr5 img-circle" />
								 <?= $account -> fullname ?> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu" style="right:0">
								<li><a href="/manager/<% $account -> slug %>"><span class="glyphicon glyphicon-globe mr5" aria-hidden="true"></span> Profile</a></li>
								<li><a href="<% route('settings') %>"><span class="glyphicon glyphicon-cog mr5" aria-hidden="true"></span> Settings</a></li>
								<li class="divider"></li>
								<li><a href="<% route('logout') %>"><span class="glyphicon glyphicon-log-out mr5" aria-hidden="true"></span> Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</nav>

<div class="announcements mb15">
	<div class="container">
		<div class="row">
			<div class="col-xs-2"><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> Announcements</div>
			<div class="col-xs-10">
				<div class="marquee">
					<marquee behavior="scroll" scrollamount="1" direction="left">
						<?php foreach(DB::table('announcements') -> orderBy('order', 'asc') -> get() as $announcement){ ?>
						<span style="margin-right:30px;"><span style="margin-right:10px;color:#ed9700" class="glyphicon glyphicon-bookmark" aria-hidden="true"></span><?= $announcement -> text ?></span>
						<?php } ?>
					</marquee>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">

	<div class="menu-control fleft mr15">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="avatar mb10 text-center">
					<img width="80" src="<?= url($account -> avatar) ?>" class="mr5 img-circle" />
					<a href="/manager/<% $account -> slug %>" class="fwbold"><?= $account -> fullname ?></a>
				</div>
				<?php if(isset($account -> privilege)){ ?>
				<div class="text-center f12 cgray mb10" style="margin-top:-5px"><% ucfirst($account -> privilege) %></div>
				<?php } ?>
				<div class="ft-pins">
					<?php foreach($account -> badges('ENBL') -> take(3) -> get() as $badge){ ?>
						<img src="<% $badge -> src %>" alt="<% $badge -> name %>" title="<% $badge -> name %>" />
					<?php } ?>
				</div>
				<div class="essential-pages list-menu mb10">
					<ul>
						<li><a href="<% route('dashboard') %>"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard</a></li>
						<?php if($account -> franchise){ ?>
			    		<?php $franchise_name = $account -> franchise -> name; ?>
			    		<?php $franchise_name_label = strlen($franchise_name) > 18 ? substr($franchise_name, 0, 16) . ' ...' : $franchise_name;  ?>
						<li><a href="<% route('franchise_profile', $account -> franchise -> id) %>"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> <% $franchise_name_label %></a></li>
						<?php }else{ ?>
						<li><a href="<% route('create_franchise') %>"><span style="color:orange" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Create Team</a></li>
						<?php } ?>
						<?php if($account -> franchise && $account -> franchise -> league){ ?>
			    		<?php $franchise_league_name = $account -> franchise -> league -> name ; ?>
			    		<?php $franchise_league_name_label = strlen($franchise_league_name) > 18 ? substr($franchise_league_name, 0, 16) . ' ...' : $franchise_league_name;  ?>
						<li><a href="<% route('league_profile', $account -> franchise -> league -> id) %>"><span class="glyphicon glyphicon-tower" aria-hidden="true"></span> <% $franchise_league_name_label %></a></li>
						<?php }else{ ?>
						<li><a href="<% route('leagues') %>"><span style="color:orange" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Join League</a></li>
						<?php } ?>
						<li><a href="<% route('messages') %>"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Messages</a></li>
						<li><a href="<% route('friends') %>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Friends <span ng-show="accountFriends">(<span ng-init="onlineFriends=0" ng-bind="onlineFriends"></span>/<span ng-init="accountFriends=<% $account -> approveFriends() -> count() %>" ng-bind="accountFriends"></span>)</span></a></li>
						<li><a href="<% route('referals') %>"><span class="glyphicon glyphicon-registration-mark" aria-hidden="true"></span> Referals</a></li>
					</ul>
				</div>
				<div class="gamecenter-pages list-menu">
					<span class="heading mb10 f12">GAMECENTER</span>
					<ul>
						<li><a href="<% route('expressbet') %>"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Express Bet</a></li>
						<li><a href="<% route('shop') %>"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Shop</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content fleft mr15">
		@yield('content')
	</div>

	<div class="right-sidebar fleft">
		<div class="panel panel-default">
			<div class="panel-body">
@include('web.ads.side_box')
<div class="mb10">
@include('web.modules.standings')
</div>
@include('web.ads.side_box2')
			</div>
		</div>
	</div>

</div>

<!-- Web JS Global Config -->
<script>window.notifier_polling_interval = <?= config('app.notifier_polling_interval') ?></script>
<script>window.flag_online_polling_interval = <?= config('app.flag_online_polling_interval') ?></script>
<script>window.conversation_polling_interval = <?= config('app.conversation_polling_interval') ?></script>

<!-- Inject Angular Dependency -->
<script>angular_dependency = ['oitozero.ngSweetAlert'];</script>
@section('jsdependency')@show

<!-- Scripts -->
<script src="<% asset('assets/js/jquery.min.js') %>"></script>
<script src="<% asset('assets/js/bootstrap.min.js') %>"></script>
<script src="<% asset('assets/js/angular/minified.js') %>"></script>
<script src="<% asset('assets/js/angular/primary.js') %>"></script>
<script src="<% asset('assets/js/angular/auth.js') %>"></script>
<script src="<% asset('assets/js/vendor/jquery.marquee.js') %>"></script>
<script src="<% asset('assets/js/vendor/sweet-alert.js') %>"></script>
<script src="<% asset('assets/js/vendor/nanoscroller.min.js') %>"></script>
@section('jscustom')@show

@section('jsinline')@show
<script type="text/javascript">
$('.nano').nanoScroller();
</script>
@include('web.modules.analytics')
</body>
</html>