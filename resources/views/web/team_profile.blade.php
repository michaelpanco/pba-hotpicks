@extends('web.layouts.default')

@section('title')<% $team -> name . ' - PinoyTenPicks ' %>@endsection

@section('css') @parent <link href="<% asset('assets/css/team.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent

@stop
<?php } ?>
@section('content')
<div class="content-full">
	<div class="team-profile-wrapper mb20">
		<div class="row upper-team-wrapper">
			<div class="col-md-6">

				<div class="row">

					<div class="col-md-3"><img class="mb20" src="<% $team -> logo %>"></div>
					<div class="col-md-9">
						<h1 class="f28 mb5" style="margin: 0 0 10px;"><% $team -> name %></h1>
						<div><span class="cgray">Joined :</span> <% $team -> joined %></div>
						<?php if($team -> titles > 0){ ?>
						<div><span class="cgray">Titles :</span> <% $team -> titles %></div>
						<?php } ?>
						<div><span class="cgray">Current Standings :</span> <% round($team -> win) %>-<% $team -> loss %></div>
					</div>
				</div>
			</div>

			<div class="col-md-6">

				<?php if($points_leader){ ?>
				<div style="float:right;">
					<div class="f18 mb10" style="margin: 0 0 10px;">Team Leaders</div>

					<div class="leader-items">
						<div>
							<a href="<% route('player_profile', $points_leader -> player -> slug) %>">
								<img width="45" class="mb5 img-circle" src="<% $points_leader -> player -> avatar %>">
							</a>
						</div>
						<div>
							<a href="<% route('player_profile', $points_leader -> player -> slug) %>">
								<% substr($points_leader -> player -> firstname,0,1) %>. <% $points_leader -> player -> lastname %>
							</a>
						</div>
						<div class="f12"><% round($points_leader -> avg_points) %> PPG</div>
					</div>

					<div class="leader-items">
						<div>
							<a href="<% route('player_profile', $assists_leader -> player -> slug) %>">
								<img width="45" class="mb5 img-circle" src="<% $assists_leader -> player -> avatar %>">
							</a>
						</div>
						<div>
							<a href="<% route('player_profile', $assists_leader -> player -> slug) %>">
								<% substr($assists_leader -> player -> firstname,0,1) %>. <% $assists_leader -> player -> lastname %>
							</a>
						</div>
						<div class="f12"><% round($assists_leader -> avg_assists) %> APG</div>
					</div>

					<div class="leader-items">
						<div>
							<a href="<% route('player_profile', $rebounds_leader -> player -> slug) %>">
								<img width="45" class="mb5 img-circle" src="<% $rebounds_leader -> player -> avatar %>">
							</a>
						</div>
						<div>
							<a href="<% route('player_profile', $rebounds_leader -> player -> slug) %>">
								<% substr($rebounds_leader -> player -> firstname,0,1) %>. <% $rebounds_leader -> player -> lastname %>
							</a>
						</div>
						<div class="f12"><% round($rebounds_leader -> avg_rebounds) %> RPG</div>
					</div>

					<div class="leader-items">
						<div>
							<a href="<% route('player_profile', $steals_leader -> player -> slug) %>">
								<img width="45" class="mb5 img-circle" src="<% $steals_leader -> player -> avatar %>">
							</a>
						</div>
						<div>
							<a href="<% route('player_profile', $steals_leader -> player -> slug) %>">
								<% substr($steals_leader -> player -> firstname,0,1) %>. <% $steals_leader -> player -> lastname %>
							</a>
						</div>
						<div class="f12"><% round($steals_leader -> avg_steals) %> SPG</div>
					</div>

					<div class="leader-items">
						<div>
							<a href="<% route('player_profile', $blocks_leader -> player -> slug) %>">
								<img width="45" class="mb5 img-circle" src="<% $blocks_leader -> player -> avatar %>">
							</a>
						</div>
						<div>
							<a href="<% route('player_profile', $blocks_leader -> player -> slug) %>">
								<% substr($blocks_leader -> player -> firstname,0,1) %>. <% $blocks_leader -> player -> lastname %>
							</a>
						</div>
						<div class="f12"><% round($blocks_leader -> avg_blocks) %> BPG</div>
					</div>

				</div>

				<?php } ?>
			</div>
		</div>

		<div class="row ">
			<div class="col-md-3 management-left">
				<div class="mb20" style="border-bottom: 1px solid #f1f1f1;padding-bottom: 20px">

					<div class="f18 mb20">Management</div>
					<div class="mb10"><span class="cgray">Manager :</span> <% $team -> manager %></div>
					<div class="mb10"><span class="cgray">Coach :</span> <% $team -> coach %></div>
					<div class="mb10"><span class="cgray">Asst. Coaches :</span></div>
					<?php foreach($asst_coaches as $coach){ ?>
					<div><% $coach %></div>
					<?php } ?>

				</div>

				<div class="mb20">
					<?php if($upcoming_games -> count() > 0){ ?>
					<div class="f18 mb20">Upcoming Games</div>
					
					<?php foreach($upcoming_games as $game){ ?>
					<div class="mb5"><?= date('F j, Y - l g:i A', strtotime($game['datetime'])); ?></div>
					<div class="mb20" style="padding-left:20px">
						<span class="cgray">vs</span> 
						<a href="<% route('team_profile', $game['opponent_slug']) %>"><% $game['opponent_team'] %></a>
					</div>
					<?php } ?>
					<?php } ?>

					<div class="text-center">
					<a class="result-btn btn btn-flat-blue" href="<% route('games', $team -> slug) %>">
						View All Games
					</a>
					</div>

				</div>


			</div>

			<div class="col-md-9 team-roster-wrapper">

				<div class="f18 mb10">Team Roster</div>

				<div class="team-player-wrapper mb20">

					<?php foreach($team -> players as $player){ ?>
					<div class="item">
						<div class="left">
							<a href="<% route('player_profile', $player -> slug) %>">
								<img width="60" class="img-circle" src="<% $player -> avatar %>">
							</a>
						</div>
						<div class="right">
							<div><a href="<% route('player_profile', $player -> slug) %>"><% $player -> firstname %> <% $player -> lastname %></a></div>
							<div><span class="cgray">Height :</span> <% $player -> height %></div>
							<div><span class="cgray">Position :</span> <?= Helper::getLabelPosition($player -> position) ?></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php } ?>

					<div class="clearfix"></div>
				</div>


				<div class="f18 mb20 text-center cred">Game Result</div>
				<table class="table table-bordered game-result">

					<thead>
						<tr>
							<th>Opponent</th>
							<th>Venue</th>
							<th>Date and Time</th>
							<th>Game Type</th>
							<th>Score</th>
							<th>Outcome</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($game_results as $game){ ?>
						<tr>
							<td><span class="cgray">vs</span> <a href="<% route('team_profile', $game['opponent_slug']) %>"><% $game['opponent_team'] %></a></td>
							<td><% $game['venue'] %></td>
							<td><?= date('M j, Y - D g:i A', strtotime($game['datetime'])); ?></td>
							<td><% $game['game_type'] %></td>
							<td><a href="<% route('game_result', $game['game_id']) %>"><% $game['score'] %></a></td>
							<?php if($game['outcome'] == 'win'){ ?>
							<td class="text-center"><span class="label label-success">won</span></td>
							<?php }else{ ?>
							<td class="text-center"><span class="label label-danger">loss</span></td>
							<?php } ?>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

