@extends('web.layouts.default')

@section('css') @parent @stop

<?php if(Auth::check()){ ?>
@parent
  <link href="<% asset('assets/css/member.css') %>" rel="stylesheet">
@stop
<?php } ?>
@section('content')

<?php if($can_open_gift){ ?>
<div class="mb10" style='background: #fff url("/assets/img/cballs.png") repeat-x scroll 0 0;padding-top: 45px;'>
	<div class="row">
		<div class="col-md-5 text-right"><img width="150" src="<% asset('assets/img/gift.gif') %>" /></div>
		<div class="col-md-7">
			<div style="font-size:20px;margin-top:20px;margin-bottom:5px;font-weight:bold">Hi <% $account -> fullname %>, You received your gift for today :)</div>
			<div class="mb10"><a ng-click="openGift()" class="open-gift btn btn-success btn-lg" href="">Open it now!</a></div>
			<div class="cgray">This event is available until January 1, 2016</div>
		</div>
	</div>
</div>
<?php } ?>

<div class="how-to-play-container">


<div class="row">
	<div class="col-xs-3">
		<div class="labeltext"><img src="<% asset('assets/img/smile.png') %>" style="margin-right:10px;">How to Play</div>
	</div>
	<div class="col-xs-3">
		<div class="row">
			<div class="col-xs-2 bold" style="font-size:48px;color:#ed9700;">
				<div>1</div>
			</div>
			<div class="col-xs-10">
				<div>Build your own team</div>
				<p class="cgray">Choose your 10 players to form your team.</p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="row">
			<div class="col-xs-2 bold" style="font-size:48px;color:#ed9700;">
				<div>2</div>
			</div>
			<div class="col-xs-10">
				<div>Challenge different managers</div>
				<p class="cgray">Join any leagues and compete each game together.</p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="row">
			<div class="col-xs-2 bold" style="font-size:48px;color:#ed9700;">
				<div>3</div>
			</div>
			<div class="col-xs-10">
				<div>Become the champion</div>
				<p class="cgray">Get the championship and earn great prizes.</p>
			</div>
		</div>
	</div>

</div>




</div>

<div class="clearfix"></div>
<div class="content-left fleft">
	<!-- Begin Featured Player -->
	<div class="ft-player">
		<?php foreach($featured_players as $featured){ ?>
		<div class="player fleft">
			<div class="fleft">
				<a href="<% route('player_profile', $featured -> player -> slug) %>">
					<img width="55" class="img-circle" src="<% $featured -> player -> avatar %>" />
				</a>
			</div>
			<div class="fright">
				<a href="<% route('player_profile', $featured -> player -> slug) %>"><% substr($featured -> player -> firstname,0,1) %>. <% $featured -> player -> lastname %></a>
				<div class="fpoints cgreen"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span><% $featured -> fantasy_points %></div>
				<div class="opponent f12">vs 
					<a href="<% route('team_profile', $featured -> adversary -> slug) %>"><% substr($featured -> adversary -> name,0,10) %>...</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php } ?>
		<div class="clearfix"></div>
	</div>
	<!-- Begin PBA Schedule -->
	<div class="pba-schedule mb10">
		<?php if($featured_games){ ?>
			<?php $b = 1; ?>
			<?php foreach($featured_games as $games){ ?>
				<?php $fbg = ($b==4) ? 'bgGreen' : 'bgWhite' ?>
				<div class="schedule fleft <% $fbg %>" style="height:200px;">
					<div class="conference text-center"><% $games['conference'] %></div>
					<div class="date text-center cgray f12"><% $games['date'] %></div>
					<div class="gametype text-center f12 cred mb5 text-uppercase"><% $games['game_type'] %></div>
					<?php $i = 0; ?>
					<?php foreach($games['games'] as $game){ ?>
						<div class="matchup mb5">
							<div class="team1 fleft text-center">
								<a href="<% route('team_profile', $game['team_x_slug']) %>">
								<img class="<% $game['result_team_x'] %>" width="50" src="<% asset($game['team_x_img']) %>" />
								</a>
							</div>
							<div class="vs-time fleft">
								<div class="f12 text-center cgray">VS</div>
								<div class="time text-center f12"><% $game['time'] %></div>
								<?php if($game['score']){ ?>
								<div class="text-center f12"><a href="<% route('game_result', $game['game_id']) %>"><% $game['score'] %></a></div>
								<?php } ?>
							</div>
							<div class="team2 fleft text-center">
								<a href="<% route('team_profile', $game['team_y_slug']) %>">
								<img class="<% $game['result_team_y'] %>" width="50" src="<% asset($game['team_y_img']) %>" />
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<?php if($i == 0){ ?><hr class="nomargin mb5 hrdash"><?php } ?>
						<?php $i++; ?>
					<?php } ?>

				</div>
				<?php $b++; ?>
			<?php } ?>
		<?php }else{ ?>
			<div style="margin-top:10px;">No PBA Schedule available</div>
		<?php } ?>
	</div>

	<!-- Begin Featured Manager and Most Signed Players -->
	<div class="ft-bundle">
		<div class="managers fleft">
			<div class="panel panel-default" style="border-radius:0">
				<div class="panel-heading" style="background-color:#e54c4c;color:#fff;border-radius:0;border:none;">
					<div class="row">
						<div class="col-md-6"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Top Active Managers</div>
						<div class="col-md-6 text-right">Fantasy Points</div>
					</div>
				</div>
				<table class="table">
					<tbody>
						<?php $y = 0; ?>
						<?php foreach($top_managers as $manager){ ?>
						<?php $manager_details = json_decode($manager, true); ?>
						<tr>
							<td class="manager" >
								<div class="fleft cgray" style="font-size:18px;margin-right:10px;width:25px">
									<% $ranks[$y] %>
								</div>
								<div class="left fleft">
									<a href="<% route('profile', $manager_details['slug']) %>">
										<img width="48" class="img-circle" src="<% asset($manager_details['avatar']) %>" />
									</a>
								</div>
								<div class="right fleft">
									<div><a href="<% route('profile', $manager_details['slug']) %>"><% $manager_details['name'] %></a></div>
									<div class="cgray">Team : <a href="<% route('franchise_profile', $manager_details['franchise_id']) %>"><% $manager_details['franchise'] %></a></div>
									<div class="cgray">League : <a href="<% route('league_profile', $manager_details['league_id']) %>"><% $manager_details['league_name'] %></a></div>
								</div>
								<div class="clearfix"></div>
							</td>
							<td class="cgreen f16"><% number_format($manager_details['total_fantasy_points']) %></td>
						</tr>
						<?php $y++; ?>
						<?php } ?>
						<tr>
							<td colspan="2" style="border-top:1px solid #ddd;text-align:center;padding: 20px 0;">
								<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <a href="<% route('top_50_managers') %>">View top 50 Active Managers</a>
							</td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>

		<div class="signed-players fleft">
			<div class="panel panel-default" style="border-radius:0">
				<div class="panel-heading" style="background-color:#e54c4c;color:#fff;border-radius:0;border:none;">
					<div class="row">
						<div class="col-md-6"><span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span>  Richest Active Managers</div>
						<div class="col-md-6 text-right">Credits</div>
					</div>
				</div>
				<table class="table">
					<tbody>
						<?php $x = 0; ?>
						<?php foreach($richest_managers as $richest_manager){ ?>
						<?php $manager_details = json_decode($richest_manager, true); ?>
						<tr>
							<td class="manager" >
								<div class="fleft cgray" style="font-size:18px;margin-right:10px;width:25px">
									<% $ranks[$x] %>
								</div>
								<div class="left fleft" style="margin-right:20px">
									<a href="<% route('profile', $manager_details['slug']) %>">
										<img width="48" class="img-circle" src="<% asset($manager_details['avatar']) %>" />
									</a>
								</div>
								<div class="right fleft">
									<div><a href="<% route('profile', $manager_details['slug']) %>"><% $manager_details['name'] %></a></div>
									<div class="cgray">Team : <a href="<% route('franchise_profile', $manager_details['franchise_id']) %>"><% $manager_details['franchise'] %></a></div>
									<div class="cgray">League : <a href="<% route('league_profile', $manager_details['league_id']) %>"><% $manager_details['league_name'] %></a></div>
								</div>
								<div class="clearfix"></div>
							</td>
							<td class="f16 text-center"><% number_format($manager_details['credits']) %></td>
						</tr>
						<?php $x++; ?>
						<?php } ?>
						<tr>
							<td colspan="2" style="border-top:1px solid #ddd;text-align:center;padding: 20px 0;">
								<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <a href="<% route('richest_50_managers') %>">View richest 50 Active Managers</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="clearfix"></div>
	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/vendor/jquery.countdown.min.js') %>"></script>
@endsection

@section('jsinline')
<?php if($more_games){ ?>
<script type="text/javascript">
	$("#next-matchup").countdown("<% $game_start %>", function(event) {
		var format = '';
		if(event.offset.days > 0){

			if(event.offset.days > 0){
				format = '<span class="b">%d</span> days <span class="b">%H</span> Hours <span class="b">%M</span> Minutes <span class="b">%S</span> Seconds';
			}else{
				format = '<span class="b">%d</span> day <span class="b">%H</span> Hours <span class="b">%M</span> Minutes <span class="b">%S</span> Seconds';
			}
		}else{
			if(event.offset.hours > 0){
				format = '<span class="b">%H</span> Hours <span class="b">%M</span> Minutes <span class="b">%S</span> Seconds';
			}else{
				format = '<span class="b">%M</span> Minutes <span class="b">%S</span> Seconds';
			}
		}
		$(this).html(event.strftime(format));
	});
</script>
<?php } ?>
@endsection

@section('sidebar')
	<div class="content-right fleft">
	<?php if($more_games){ ?>
	<div class="match-container">
		<div class="match-title cred">Next Matchup</div>
		<div id="next-matchup"></div>
	</div>
	<?php } ?>
	@include('web.ads.side_box')
	@include('web.modules.standings')
	</div>
@endsection