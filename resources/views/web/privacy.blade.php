@extends('web.layouts.whitebg')

@section('title', 'Privacy Policy - PinoyTenPicks')
@section('description', 'This privacy policy discloses the privacy practices for www.pinoytenpicks.com. This privacy policy applies solely to information collected by this web site.')


@section('content')
<div class="content-full standard-page">
<h1>Privacy Policy</h1>
<p class="dgray">This privacy policy discloses the privacy practices for www.pinoytenpicks.com. This privacy policy applies solely to information collected by this web site. It will notify you of the following:</p>
<ul class="list mb20">

	<li class="dgray">What personally identifiable information is collected from you through the web site, how it is used and with whom it may be shared.</li>
	<li class="dgray">What choices are available to you regarding the use of your data.</li>
	<li class="dgray">The security procedures in place to protect the misuse of your information.</li>
	<li class="dgray">How you can correct any inaccuracies in the information.</li>
</ul>
<p class="bold">Information Collection, Use, and Sharing</p>

<p class="dgray">We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p>
<p class="dgray">We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request.</p>
<p class="dgray">Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>

<p class="bold">Your Access to and Control Over Information</p>
<p class="dgray">You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p>

<ul class="list mb20">

	<li class="dgray">See what data we have about you, if any. </li>
	<li class="dgray">Change/correct any data we have about you.</li>
	<li class="dgray">Have us delete any data we have about you.</li>
	<li class="dgray">Express any concern you have about our use of your data.</li>
</ul>

<p class="bold">Security</p>

<p class="dgray">We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p>
<p class="dgray">While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>

<p class="bold">Our Privacy Policy may change from time to time and all updates will be posted on this page.</p>
<p>If you feel that we are not abiding by this privacy policy, you should <a href="<% route('contact') %>">contact us</a> immediately.</p>
</div>
@endsection

