@extends('web.layouts.auth')

@section('title')<% 'Express Bet - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="ExpressBet" class="panel panel-default" ng-controller="ExpressBet">
	<div class="panel-body">
		<h1><span aria-hidden="true" class="glyphicon glyphicon-credit-card"></span> Express Bet</h1>
		<?php foreach($latestBets as $bet){ ?>
		<div class="current-bet mb20">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="cgray">Game Date :</span> <% date('l, d-M-Y g:i A', strtotime($bet -> gamedate)) %></div>
				<div class="panel-body bet-container-<% $bet -> id %>">
					<div class="mb10"><% $bet -> description %></div>
					<?php $myLatestBetsArray = ($myLatestBetList -> count() > 0) ? $myLatestBets -> toArray() : [] ?>
					<?php if(in_array($bet -> id, $myLatestBetsArray)){ ?>
						<?php $bet_details = $myLatestBetList -> where('express_bet_id', $bet -> id) -> first(); ?>

						<div class="mb10">
						<?php $options = unserialize($bet -> option) ?>
						<?php foreach($options as $key => $value){ ?>
		                <div><% $value %> <span class="cgray f12" style="padding-left:5px">(<% $bet -> bets -> where('bet', $key) -> count() %> Total bets)</span></div>
		                <?php } ?>
		            	</div>

						<div class="mb5"><span class="cgray">Your bet :</span> <% unserialize($bet -> option)[$bet_details -> bet] %></div>
						<div class="mb5"><span class="cgray">Bet amount :</span> <% number_format($bet_details -> amount) %></div>
						<div><span class="cgray">Winning amount :</span> <% number_format($bet_details -> amount * 2) %></div>
					<?php }else{ ?>

						<?php if(strtotime($bet -> expiry) > strtotime(date('Y-m-d H:i:s'))){ ?>
						<div class="mb20">
							<?php $options = unserialize($bet -> option) ?>
							<?php foreach($options as $key => $value){ ?>
			                <label><input type="radio" ng-change="betNo<% $bet -> id %>Val='<% $value %>'" class="field" name="betNo<% $bet -> id %>" ng-model="betNo<% $bet -> id %>" value="<% $key %>" style="margin-right:10px" /> <% $value %></label> <span class="cgray f12" style="padding-left:5px">(<% $bet -> bets -> where('bet', $key) -> count() %> Total bets)</span><br/>
			                <?php } ?>
			            </div>
			            <div class="cgray mb20">
			            	<p class="mb20">Bet Limit : <span style="color:#333"><% number_format($bet -> limit) %></span></p>
			            	Amount Bet : <input type="text" ng-model="betNo<% $bet -> id %>Amount" class="form-control field" style="display:inline;width:250px" placeholder="Enter amount range 100 - <% $bet -> limit %>">
			            	<a href="" class="field trigger btn btn-flat-blue btn-small" ng-click="submitBet(betNo<% $bet -> id %>Val, <% $bet -> id %>, betNo<% $bet -> id %>, betNo<% $bet -> id %>Amount)">Submit</a>
			            </div>
			            <div class="cgray f12">* The betting for this game is open until <% date('l, d-M-Y g:i A', strtotime($bet -> expiry)) %></div>
			            <?php }else{ ?>
						<div>
							<?php $options = unserialize($bet -> option) ?>
							<div class="mb20">
							<?php foreach($options as $key => $value){ ?>
			                <label class="cgray"><% $value %></label><br/>
			                <?php } ?>
			            	</div>
			                <div class="cgray f12">* Betting for this game was already closed</div>
			            </div>
			            <?php } ?>

		            <?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="bet-lists">
			<?php foreach($myAllBets as $mybet){ ?>
			<div class="item well claimbet-container-<% $mybet -> express_bet_id %>">
				
				<% $mybet -> details -> description %>
					
				<hr class="hrgamebet"/>
				<div class="row mb10">
					<div class="col-md-6">
						<span class="cgray">Date : </span><% date('F j, Y', strtotime($mybet -> datetime)) %>
					</div>
					<div class="col-md-6">
						<span class="cgray">Bet : </span><% unserialize($mybet -> details -> option)[$mybet -> bet] %>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<span class="cgray">Bet amount : </span><% number_format($mybet -> amount) %>
					</div>
					<div class="col-md-6">
						<span class="cgray">Outcome : </span>
						<?php if($mybet -> bet == $mybet -> details -> won){ ?>
						<span class="label label-success">Won</span>
						<?php }else{ ?>
						<span class="label label-danger">Loss</span>
						<?php } ?>
					</div>
				</div>
				<?php if($mybet -> bet == $mybet -> details -> won && $mybet -> claimed == 0){ ?>
				<hr class="hrgamebet"/>
				<a href="" ng-click="claimExpressBetPrice(<% $mybet -> express_bet_id %>)" class="field trigger btn btn-flat-blue" style="margin: 20px auto 0;width:170px;display:block">Claim <% number_format($mybet -> amount * 2) %></a>
				<?php } ?>
			</div>
			<?php } ?>
			<div class="text-center"><?= $myAllBets -> render() ?></div>
		</div>

	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/express-bet.js') %>"></script>
@endsection