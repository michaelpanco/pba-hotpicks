@extends('web.layouts.auth')

@section('title')<% 'Friends - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="friends" class="panel panel-default">
	<div class="panel-body">
		<h1><span aria-hidden="true" class="glyphicon glyphicon-user"></span> Friends</h1>
		<div id="friendsContent" compile="friendsContent"></div>
	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/friends.js') %>"></script>
@endsection

@section('jsinline')
<script angular.element(document).ready(function() {ype="text/javascript">
angular.element(document).ready(function() {
	loadFriends();
});
</script>
@endsection