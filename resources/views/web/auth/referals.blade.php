@extends('web.layouts.auth')

@section('title')<% 'Referals - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="Referals" ng-controller="Referals" class="panel panel-default">
	<div class="panel-body text-center">
		<h1>Invite Friends and earn Credits</h1>
		<img class="mb10" width="90" src="<% asset('assets/img/referals.png') %>" alt="Refer a Friend">

		<p class="mb20">Help grow our PinoyTenPicks community and earn free credits, You can earn 5,000 credits when you refer 10 people to join here in PinoyTenPicks.</p>

		<p class="mb20">You can invite your friends by sharing the provided link</p>

		<div class="well">http://www.pinoytenpicks.com/?r=<% $account -> id %></div>
		<?php if($account -> referals -> count() >= 10 && $account -> claimed_referal_prize == 0){ ?>
		<div class="mb20">
			<div class="mb20 cgreen">Awesome, you can now claim your credits</div>
			<a class="submit btn btn-flat-blue btn-small" ng-click="claimReferalPrize()" style="width:200px"  href="">
				Claim 5,000 Credits
			</a>
		</div>
		<?php }else{ ?>
		<div class="mb20">
			<a class="submit btn btn-flat-blue btn-small disabled" ng-click="claimReferalPrize()"  style="width:200px"  href="">
				<?php if($account -> claimed_referal_prize == 1){ ?>
					Claimed
				<?php }else{ ?>
					Claim 5,000 Credits
				<?php } ?>
			</a>
		</div>
		<?php } ?>
		<?php if($account -> referals -> count() > 0 ){ ?>
		<p class="mb20 bold">Successful Referals</p>
		<ul class="list-group">
		<?php foreach($account -> referals -> take(10) as $referal){ ?>
		<li class="list-group-item"><a href="<% route('profile', $referal -> slug) %>"><% $referal -> fullname %></a></li>
		<?php } ?>
		</ul>
		<?php } ?>

		<?php if($account -> referals -> count() < 10 ){ ?>
		You need <% 10 - $account -> referals -> count() %> referals to claimed your 5,000 credits
		<?php } ?>




	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/referals.js') %>"></script>
@endsection