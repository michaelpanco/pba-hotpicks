@extends('web.layouts.auth')

@section('title')<% 'Shop - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
<link href="<% asset('assets/css/shop.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="Shop" class="panel panel-default" ng-controller="Shop">
	<div class="panel-body shop-wrapper">
		<h1><span aria-hidden="true" class="glyphicon glyphicon-shopping-cart"></span> Shop</h1>
		<p class="bold f12">PBA Team Badges</p>
		<div class="panel-body badges-shop-wrapper">

			<?php foreach($products -> where('type', 'BDGE') as $badge){ ?>
			<div class="item product-item-<% $badge -> id %>">

				<div class="row">
					<div class="col-md-2">
						<img src="<% $badge -> img %>" />
					</div>
					<div class="col-md-10">
						<div class="mb5"><% $badge -> name %></div>
						<?php if($account_credits < $badge -> price){ ?>
						<div class="mb5 cred"><span class="cgray">Price : </span> <% number_format($badge -> price) %></div>
						<div class="">
							<a style="width:100px;display:block" class="btn btn-flat-blue disabled" href="">Purchase</a>
						</div>
						<?php }else{ ?>
						<div class="mb5 cgreen"><span class="cgray">Price : </span> <% number_format($badge -> price) %></div>
						<div class="">
							<a style="width:100px;display:block" class="field trigger btn btn-flat-blue" ng-click="purchaseProduct('<% $badge -> name %>', '<% $badge -> type %>', <% $badge -> id %>, <% $badge -> product_id %>)" href="">Purchase</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>

		<p class="bold f12">Franchise Icons</p>
		<div class="panel-body badges-shop-wrapper">

			<?php foreach($products -> where('type', 'FICO') as $fico){ ?>
			<div class="item product-item-<% $fico -> id %>">

				<div class="row">
					<div class="col-md-2">
						<img src="<% $fico -> img %>" />
					</div>
					<div class="col-md-10">
						<div class="mb5"><% $fico -> name %></div>

						<?php if($account_credits < $fico -> price){ ?>
						<div class="mb5 cred"><span class="cgray">Price : </span> <% number_format($fico -> price) %></div>
						<div class="">
							<a style="width:100px;display:block" class="btn btn-flat-blue disabled" href="">Purchase</a>
						</div>
						<?php }else{ ?>
						<div class="mb5 cgreen"><span class="cgray">Price : </span> <% number_format($fico -> price) %></div>
						<div class="">
							<a style="width:100px;display:block" class="field trigger btn btn-flat-blue" ng-click="purchaseProduct('<% $fico -> name %>', '<% $fico -> type %>', <% $fico -> id %>, <% $fico -> product_id %>)" href="">Purchase</a>
						</div>
						<?php } ?>

					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="text-center mb20">More items to come soon...</div>
	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/shop.js') %>"></script>
@endsection