@extends('web.layouts.single')

@section('title')<% 'Settings - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/settings.css') %>" rel="stylesheet">
@stop

@section('content')

<div class="panel panel-default settings-container" ng-controller="Settings">
	<div class="panel-body">
		<h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Settings</h1>

		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

			<!-- Change Password -->
			<div class="panel panel-default">
				<div class="panel-heading" role="tab">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#changePassword" aria-expanded="true" aria-controls="changePassword">
							Change Password
						</a>
					</h4>
				</div>
				<div id="changePassword" class="panel-collapse collapse in" role="tabpanel">
					<div class="panel-body">


					<form autocomplete="off" ng-submit="changePassword(password)" class="change-password" style="width:350px;margin:0 auto;">


                        <input type="password" placeholder="Enter your current password" class="form-control field mb10" ng-model="password.current">
                        <input type="password" placeholder="Enter your new password" class="form-control field mb10" ng-model="password.new">
                        <input type="password" placeholder="Confirm new password" class="form-control field mb10" ng-model="password.confirm">
                        <input type="submit" value="Login" style="display:none;">
                        <a style="width:100%" class="field trigger btn btn-flat-blue mb10" ng-click="changePassword(password)" href="">Change Password</a>

                    </form>

					</div>
				</div>
			</div>

			<!-- Change Favorites -->
			<div class="panel panel-default">
				<div class="panel-heading" role="tab">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#editFavorites" aria-expanded="true" aria-controls="editFavorites">
							Update Favorites
						</a>
					</h4>
				</div>
				<div id="editFavorites" class="panel-collapse collapse" role="tabpanel">
					<div class="panel-body">

						<form autocomplete="off" ng-submit="changePassword(password)" class="change-favorites">
						
							<!-- Favorite Team -->
							<div class="row mb10">
								<div class="col-md-3 cgray">
									Favorite Team
								</div>
								<div class="col-md-9" ng-init="selectedFavTeamLbl='<% $account -> fav_team %>'">
									<div class="btn-group">
										<button type="button" class="btn btn-default" style="width:200px">{{selectedFavTeamLbl || 'No Favorite Team'}}</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu">
											<li><a href="" ng-click="selectedFavTeamLbl='No Favorite Team';selectedFavTeamVal=999">No Favorite Team</a></li>
											<li role="separator" class="divider"></li>
											<?php foreach($teams as $team){ ?>
											<li><a href="" ng-click="selectedFavTeamLbl='<% $team -> name %>';selectedFavTeamVal=<% $team -> id %>"><% $team -> name %></a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>

							<!-- Favorite Player -->
							<div class="row mb10">
								<div class="col-md-3 cgray">
									Favorite Player
								</div>
								<div class="col-md-9" ng-init="selectedFavPlayerLbl='<% $account -> fav_player %>'">
									<div class="btn-group">
										<button type="button" class="btn btn-default" style="width:200px">{{selectedFavPlayerLbl || 'No Favorite Player'}}</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" style="overflow-y:scroll;height:300px">
											<li><a href="" ng-click="selectedFavPlayerLbl='No Favorite Player';selectedFavPlayerVal=999">No Favorite Player</a></li>
											<li role="separator" class="divider"></li>
											<?php foreach($players as $player){ ?>
											<li><a href="" ng-click="selectedFavPlayerLbl='<% $player -> fullname %>';selectedFavPlayerVal=<% $player -> id %>"><% $player -> fullname %></a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
							
							<!-- 2nd Favorite Team -->
							<div class="row mb10">
								<div class="col-md-3 cgray">
									2nd Favorite Team
								</div>
								<div class="col-md-9" ng-init="selected2ndFavTeamLbl='<% $account -> fav_2nd_team %>'">
									<div class="btn-group">
										<button type="button" class="btn btn-default" style="width:200px">{{selected2ndFavTeamLbl || 'No 2nd Favorite Team'}}</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" style="overflow-y:scroll;height:240px">
											<li><a href="" ng-click="selected2ndFavTeamLbl='No 2nd Favorite Team';selected2ndFavTeamVal=999">No 2nd Favorite Team</a></li>
											<li role="separator" class="divider"></li>
											<?php foreach($teams as $team){ ?>
											<li><a href="" ng-click="selected2ndFavTeamLbl='<% $team -> name %>';selected2ndFavTeamVal=<% $team -> id %>"><% $team -> name %></a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>

							<!-- Favorite Team matchup -->
							<div class="row mb20">
								<div class="col-md-3 cgray">
									Favorite Matchup
								</div>
								<div class="col-md-9">

									<div class="btn-group" style="margin-right:10px;" ng-init="currentMatchupA='<% $matchup1 %>'">
										<button type="button" class="btn btn-default" style="width:200px">{{selectedFavMatchupALbl || (currentMatchupA || 'Select a Team')}}</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" style="overflow-y:scroll;height:220px">
											<li><a href="" ng-click="selectTeamMatchupA('', 999)">No Prefered Matchup</a></li>
											<li role="separator" class="divider"></li>
											<?php foreach($teams as $team){ ?>
											<li><a href="" ng-click="selectTeamMatchupA('<% $team -> name %>', <% $team -> id %>)"><% $team -> name %></a></li>
											<?php } ?>
										</ul>
									</div>

									<span class="cgray">vs</span>

									<div class="btn-group" style="margin-left:10px;" ng-init="currentMatchupB='<% $matchup2 %>'">
										<button type="button" class="btn btn-default" style="width:205px">{{selectedFavMatchupBLbl || (currentMatchupB || 'Select a Team')}}</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" style="overflow-y:scroll;height:220px">
											<li><a href="" ng-click="selectTeamMatchupB('', 999)">No Prefered Matchup</a></li>
											<li role="separator" class="divider"></li>
											<?php foreach($teams as $team){ ?>
											<li><a href="" ng-click="selectTeamMatchupB('<% $team -> name %>', <% $team -> id %>)"><% $team -> name %></a></li>
											<?php } ?>
										</ul>
									</div>

								</div>
							</div>

							<a href="" ng-click="saveFavorites()" class="field trigger btn btn-flat-blue pull-right" style="width:230px">Save Changes</a>

						</form>

					</div>
				</div>
			</div>

			<!-- Change Avatar -->
			<div class="panel panel-default">
				<div class="panel-heading" role="tab">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#changeAvatar" aria-expanded="true" aria-controls="changeAvatar">
							Upload Avatar
						</a>
					</h4>
				</div>
				<div id="changeAvatar" class="panel-collapse collapse" role="tabpanel">
					<div class="panel-body text-center upload-avatar">
						<p class="cgray f12 text-center">You can upload image 110px by 110px (jpeg/png) to change your avatar</p>
						<div class="row mb10">
							<div class="col-md-12 text-center">
								<img width="80" class="img-circle" src="<% $account -> avatar %>">
							</div>
						</div>
						<div class="row mb10">
							<div class="col-md-12 text-center">

								<div style="position:relative;width:125px;margin:0 auto;">
									<a class='field btn btn-default mb5' href='javascript:;'>
										Select Image
										<input type="file" accept="image/*" file-model="avatar" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' size="40"  onchange='$("#upload-file-info").html($(this).val());'>
									</a>
									&nbsp;
								</div>

								<span class='label label-info' id="upload-file-info"></span>

							</div>
						</div>
						<a style="width:130px" class="field trigger btn btn-flat-blue" ng-click="uploadAvatar()" href="">Upload</a>
					</div>
				</div>
			</div>

			<!-- Update Badges -->
			<div class="panel panel-default">
				<div class="panel-heading" role="tab">
					<h4 class="panel-title">
						<a role="updateBadges" data-toggle="collapse" data-parent="#accordion" href="#updateBadges" aria-expanded="true" aria-controls="changeAvatar">
							Manage Badges
						</a>
					</h4>
				</div>
				<div id="updateBadges" class="panel-collapse collapse" role="tabpanel">
					<div class="panel-body update-badges">

						<?php if($account -> badges() -> count() > 0){ ?>

						<p class="cgray f12 mb10">You can only activate 3 badges</p>

						<?php foreach($account -> badges() -> withPivot('status') -> get() as $badge){ ?>

					        <div class="form-group">
					        	<?php $active = ($badge -> pivot -> status == 'ENBL') ? 'true' : 'false' ?>

					            <input type="checkbox" name="badges" ng-init="badges.badge<% $badge -> id %>=<% $active %>" ng-model="badges.badge<% $badge -> id %>" id="badge-id-<% $badge -> id %>" autocomplete="off" />

					            <div class="btn-group">
					                <label for="badge-id-<% $badge -> id %>" class="btn btn-primary" style="width:45px">
					                    <span class="glyphicon glyphicon-ok"></span>
					                    <span> </span>
					                </label>
					                <label for="fancy-checkbox-primary" class="btn btn-default badge-label active">
					                    <img width="17" class="mr5" title="<% $badge -> name %>" alt="<% $badge -> name %>" src="<% $badge -> src %>"> <% $badge -> name %>
					                </label>
					            </div>
					        </div>

						<?php } ?>
						<span class="clearfix"></span>
						<a style="width:130px;margin-top:10px" class="field trigger btn btn-flat-blue pull-right" ng-click="updateBadges()" href="">Save</a>

						<?php }else{ ?>
						<p class="cgray f12 mb10">You don't have any badges</p>
						<?php } ?>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/settings.js') %>"></script>
@endsection