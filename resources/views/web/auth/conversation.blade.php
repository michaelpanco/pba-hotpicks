@extends('web.layouts.auth')

@section('title')<% 'Conversation - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="conversation" class="panel panel-default" ng-controller="Conversation">
	<div class="panel-body">
		<div class="controls">
			<div class="btn-group mb15" role="group" aria-label="...">
				<a href="<% route('messages') %>" class="btn btn-default"><span aria-hidden="true" class="glyphicon glyphicon-comment"></span> Back to Messages</a>
				<a href="" class="disabled btn btn-default"><% $manager_fullname %></a>
			</div>
		</div>
		<div id="conversationWrapper" class="nano">
			<div class="nano-content">
				<div class="conversations-loader"></div>
				<div id="conversationsContent" ng-init="messageRecipient=<% $recipient %>"></div>
			</div>
		</div>
		<div class="composer">
			<textarea class="field form-control mb10" ng-disabled="disableMessage" ng-model="message" placeholder="Write your message"></textarea>
			<a href="" ng-click="sendMessage(<% $recipient %>, '<% $manager_slug %>')" class="submit btn btn-flat-blue btn-small pull-right">Send</a>
		</div>
	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/conversation.js') %>"></script>
@endsection

@section('jsinline')
<script type="text/javascript">
angular.element(document).ready(function() {
	loadConversations('<% $manager_slug %>');
	<?php if($unread == 1){ ?>markMessageRead(<% $recipient %>);<?php } ?>
});
$('.nano').nanoScroller();

$(".nano").bind("scrolltop", function(e){
	loadMoreConversations();
});
</script>
@endsection