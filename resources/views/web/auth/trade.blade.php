@extends('web.layouts.default')

@section('title')<% 'Trade - PinoyTenPicks ' %>@endsection

@section('css') @parent <link href="<% asset('assets/css/trade.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent

@stop
<?php } ?>
@section('content')

<div id="Trade" ng-controller="Trade" class="trade">

	<div class="row">
		<div class="col-md-12">

			<?php if(!$can_trade){ ?>
				<div class="alert alert-warning" style="margin-top:10px;" role="alert">
					<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> You are only allowed to trade player after 10 days from the date when the player was hired.
				</div>
			<?php }else{ ?>
				<div class="row">
					<div class="col-md-6">
						<div class="btn-group" role="group" aria-label="..." style="margin-right:15px">
						  <a href="<% route('franchise_profile', $account -> franchise -> id) %>" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <% $account -> franchise -> name %></a>
						  <a href="" class="btn btn-default disabled">Trade </a>
						</div>
						<span class="mr10 f16 {{salaryClass}}" ng-init="accountCredits='<?= $account -> credits ?>'"><span class="cgray">Credits : </span><span ng-class="creditClass" ng-bind="accountCredits | number"></span></span>
					</div>
					<div class="col-md-6 text-right trade-players">
						<a ng-click="tradePlayers()" class="trigger field btn btn-flat-blue" href=""><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Trade</a>
					</div>
				</div>

				<div class="alert alert-warning" style="margin-top:10px;display:none" role="alert" ng-show="creditTradeError">
					<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> You don't have enough credits to perform this trade.
				</div>

				<div class="trade-contaner row mb20">
					<div class="col-md-5 text-center">

						<div class="text-right" style="float:right;width:75px" ng-show="overAmountTrade">
							<div ng-class="creditClass" style="font-size:16px;margin-top:50px">+ <span ng-bind="additionalAmount | number"></div>
						</div>

						<div class="pick-item ng-scope" style="float:right">
							<span class="cwhite"><% $position_label %></span>
							<div class="pick-players">
						    	<div class="player-item text-center ng-scope" draggable="true">
						    		<img width="45" src="<% $player -> avatar %>" class="mb5 img-circle">
						    		<div compile="pick.name" class="f10"><span class="ng-scope"><% $player -> fullname %></span></div>
						    		<div class="f10 cgray"><% Helper::getExactPosition($player_position) %></div>
						    		<div class="f10"><span class="cgray">Salary : </span><span ng-bind="playerSalary=<% $player -> salary %> | number"></div>
						    	</div>
							</div>
						</div>

					</div>

					<div class="col-md-2 text-center">
						<span class="glyphicon glyphicon-resize-horizontal" style="font-size:42px;margin-top:40px;color:#4385c7" aria-hidden="true"></span>
					</div>

					<div class="col-md-5">

						<div class="pick-item" ng-repeat="picker in pickLists">
							<span class="cwhite">{{picker.type}}</span>
							<div class="pick-players" dnd-list="picker.dndlist" dnd-disable-if="picker.dndlist.length >= 1" dnd-allowed-types="picker.allowed_position">
						    	<div class="player-item text-center" ng-repeat="pick in picker.dndlist" post-render 
						            dnd-draggable="pick"
						            dnd-type="pick.position"
						            dnd-disable-if="pick.position == 'unknown'"
						            dnd-moved="picker.dndlist.splice($index, 1)"
						            dnd-effect-allowed="move">
						    		<img class="mb5 img-circle" width="45" src="{{pick.avatar}}">
						    		<div class="f10" compile="pick.name"></div>
						    		<div class="f10 cgray">{{pick.position}}</div>
						    		<div class="f10"><span class="cgray">Salary : </span>{{pick.salary | number}}</div>
						    	</div>
							</div>
						</div>

					</div>
				</div>

				<div id="players-container" class="nano mb20">
				    <div class="nano-content" dnd-list="players">
				    	<div class="player-item text-center" ng-repeat="player in players | filter:search" post-render 
				            dnd-draggable="player"
				            dnd-type="player.position"
				            dnd-moved="players.splice($index, 1)"
				            dnd-disable-if="player.position == 'unknown'"
				            dnd-effect-allowed="move">

				    		<img class="mb5 img-circle" width="45" src="{{player.avatar}}">
				    		<div class="f10" compile="player.name"></div>
				    		<div class="f10 cgray">{{player.position}}</div>
				    		<div class="f10"><span class="cgray">Salary : </span>{{player.salary | number : fractionSize}}</div>
				    	</div>
				    </div>
				</div>
			<?php } ?>
		</div>
	</div>

</div>

@endsection

@section('jsdependency')
<script>
	angular_dependency.push('dndLists');
	var account_credits = <?= $account -> credits ?>;
	var account_franchise_id = <?= $account -> franchise -> id ?>;
	var player_id = <?= $player_id ?>;
</script>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/trade.js') %>"></script>
<script src="<% asset('assets/js/vendor/nanoscroller.min.js') %>"></script>
<script src="<% asset('assets/js/vendor/angular-drag-and-drop-lists.js') %>"></script>
@endsection

@section('jsinline')
<script>

$('.nano').nanoScroller({ alwaysVisible: true });

angular.element(document).ready(function() {
	
	var appElement = document.querySelector('[ng-app=pbahotpicks]');
	var $scope = angular.element(appElement).scope();

	$scope.$apply(function() {

	    $scope.players = [
	    	<?php foreach($players as $player){ ?>
		    	<?php if($player -> id != $player_id){ ?>
		    	{id: <% $player -> id %>, name: "<% $player -> fullname %>", avatar: "<% $player -> avatar %>", position: "<% Helper::getExactPosition($player -> position) %>", salary: <% $player -> salary %>},
	            <?php } ?>
            <?php } ?>
	    ];

	    $scope.picks = {pg1:[],sg1:[],sf1:[],pf1:[],c1:[],pg2:[],sg2:[],sf2:[],pf2:[],c2:[]}

	    $scope.pickLists = [

	    	{type: '<% $position_label %>', dndlist:$scope.picks.pg1, allowed_position: "<% $allowed_position %>"},

	    ];
  		
	});
});
</script>
@endsection