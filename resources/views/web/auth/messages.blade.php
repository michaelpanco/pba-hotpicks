@extends('web.layouts.auth')

@section('title')<% 'Messages - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="messages" class="panel panel-default">
	<div class="panel-body">
		<h1><span aria-hidden="true" class="glyphicon glyphicon-comment"></span> Messages</h1>
		<div id="messagesContent" compile="messagesContent"></div>
	</div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/angular/messages.js') %>"></script>
@endsection

@section('jsinline')
<script angular.element(document).ready(function() {ype="text/javascript">
angular.element(document).ready(function() {
	loadMessages();
});
</script>
@endsection