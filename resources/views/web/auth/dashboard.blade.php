@extends('web.layouts.auth')

@section('title')<% 'Dashboard - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/auth.css') %>" rel="stylesheet">
@stop

@section('content')
<div id="Dashboard" class="panel panel-default" ng-controller="Dashboard">
	<div class="panel-body">
		<div class="status-post">
			<textarea ng-model="postContent" class="field form-control mb10" placeholder="Share your thoughts @{{currentChannel}}" spellcheck="false"></textarea>
			<a class="field trigger btn btn-flat-blue btn-small pull-right" href="" ng-click="createPost()">Post</a>
		</div>
		<div class="clearfix"></div>
		<div class="channels" data-example-id="togglable-tabs" role="tabpanel">
			<ul id="channelTab" role="tablist" class="nav nav-tabs">
				<li ng-class="{active: channel.selected}" ng-repeat="channel in featuredChannels" role="presentation">
					<a data-toggle="tab" href="" ng-click="selectChannel(channel)" ng-cloak>{{channel.name}}</a>
				</li>
				<li class="dropdown" role="presentation">
					<a data-toggle="dropdown" class="dropdown-toggle" href="">More <span class="caret"></span></a>
					<ul role="menu" class="dropdown-menu">
						<li ng-repeat="channel in concealedChannels | orderBy:'name'">
							<a href="" ng-click="tabToFront(channel)" ng-cloak>{{channel.name}}</a>
						</li>
					</ul>
				</li>
			</ul>

			<div id="channel-content" class="channel-wrapper tab-content" compile="channelContent"></div>
			<div class="posts-loader"></div>

		</div>
	</div>
</div>
@endsection

@section('jsdependency')
<script>window.angular_dependency.push('ngStorage');</script>
@endsection

@section('jscustom')
<script src="<% asset('assets/js/vendor/storage.min.js') %>"></script>
<script src="<% asset('assets/js/angular/dashboard.js') %>"></script>
@endsection

@section('jsinline')
<script type="text/javascript">
angular.element(document).ready(function() {
	
	var appElement = document.querySelector('[ng-app=pbahotpicks]');
	var $scope = angular.element(appElement).scope();

	$scope.primarySelectedChannel = window.primarySelectedChannel;
	$scope.secondarySelectedChannel = window.secondarySelectedChannel;

	$scope.$apply(function() {

    	$scope.featuredChannels = [
    		{id:1, name:'Public'},
    		<?php if($account -> franchise && $account -> franchise -> league){ ?>
    		<?php $league_name = $account -> franchise -> league -> name; ?>
    		<?php $league_channel_name = strlen($league_name) > 18 ? substr($league_name, 0, 16) . ' ...' : $league_name;  ?>
    		{id: <% $account -> franchise -> league -> channel -> id %>, name: '<% $league_channel_name %>'}
    		<?php } ?>
    	];

    	$scope.featuredChannels.push($scope.primarySelectedChannel, $scope.secondarySelectedChannel);

    	$scope.concealedChannels = [
			<?php foreach($team_channels as $channel){ ?>
			{id: <% $channel -> id %>, name: '<% $channel -> name %>'},
			<?php } ?>
		];
  
    	var primary_index = $scope.concealedChannels.map(function(e) { return e.id; }).indexOf($scope.primarySelectedChannel.id);
    	$scope.concealedChannels.splice(primary_index, 1);
    	
    	var secondary_index = $scope.concealedChannels.map(function(e) { return e.id; }).indexOf($scope.secondarySelectedChannel.id);
  		$scope.concealedChannels.splice(secondary_index, 1);
  		
	});

	$( "#channelTab li a:first" ).trigger( "click" );
});
</script>

<script type="text/javascript">
$(window).scroll(function(){
	if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
		loadMorePosts();
	}
});
</script>
@endsection