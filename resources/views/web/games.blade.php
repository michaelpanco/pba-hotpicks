@extends('web.layouts.whitebg')

@section('title', 'PBA Games - PinoyTenPicks')

@section('css') @parent <link href="<% asset('assets/css/games.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent
  <link href="<% asset('assets/css/member.css') %>" rel="stylesheet">
@stop
<?php } ?>
@section('content')
<div class="content-left fleft" style="width:790px">

	<?php if($games){ ?>
	<!-- Split button -->
	<div class="btn-group mb20" ng-init="selectedTeamFilter='<% $filtered_team %>'" style="margin-left: 120px;">
		<button type="button" class="btn btn-default" style="width:230px">{{selectedTeamFilter || 'All Teams'}}</button>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="caret"></span>
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu">
			<?php foreach($teams as $team){ ?>
			<li><a ng-click="selectedTeamFilter='<% $team -> name %>'" href="<% route('games', $team -> slug) %>"><% $team -> name %></a></li>
			<?php } ?>
			<li role="separator" class="divider"></li>
			<li><a href="<% route('games') %>">All Teams</a></li>
		</ul>
	</div>

	<div id="pba-games">
		<?php foreach($games as $game){ ?>
		<div class="item">
			<div class="date"><% $game['date'] %></div>
			<div class="matchup">
				<div class="venue"><% $game['venue'] %> - <% $game['game_type'] %></div>
				<?php foreach($game['games'] as $matchup){ ?>
					<div class="game"><span class="time"><% $matchup['time'] %></span>
						<a href="<% route('team_profile', $matchup['team_x_slug']) %>">
							<img class="<% $matchup['result_team_x'] %>" width="70" style="margin-right:10px" src="<% asset($matchup['team_x_img']) %>">
						</a>
						<?php if($matchup['team_x_score']){ ?><span class="score"><% $matchup['team_x_score'] %></span><?php } ?>
						<span class="cgray vs">vs</span> 
						<?php if($matchup['team_y_score']){ ?><span class="score"><% $matchup['team_y_score'] %></span><?php } ?>
						<a href="<% route('team_profile', $matchup['team_y_slug']) %>">
							<img class="<% $matchup['result_team_y'] %>" width="70" style="margin-left:10px" src="<% asset($matchup['team_y_img']) %>">
						</a>
					</div><?php if($matchup['score']){ ?><a href="<% route('game_result', $matchup['game_id']) %>" class="result-btn btn btn-flat-blue btn-small">Result</a><?php } ?>
					<div class="clearfix"></div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php }else{ ?>
		<div style="margin-top:10px;">No result found</div>
	<?php } ?>
</div>
@endsection

@section('sidebar')
	<div class="content-right fleft">
	@include('web.ads.side_box')
	@include('web.modules.standings')
	</div>
@endsection