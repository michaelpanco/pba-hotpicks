@extends('web.layouts.whitebg')

@section('title')<% $game -> team_x -> name . ' vs. ' . $game -> team_y -> name  %>@endsection

@section('css') @parent <link href="<% asset('assets/css/results.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent

@stop
<?php } ?>
@section('content')
<div id="result-wrapper">

<div class="mb20">
<span class="mr15"><span class="cgray">Game No.</span> <% $game -> game_no %></span>
<span class="mr15"><span class="cgray">Game Type :</span> <span class="label label-primary"><% $game -> game_type %></span></span>
<span class="mr15"><span class="cgray">Venue :</span> <% $game -> venue %></span>
<span class="mr15"><span class="cgray">Date and Time : </span> <% date('F j, Y - l g:i A', strtotime($game -> datetime)) %></span>
</div>

<div class="row mb10">

	<div class="col-md-6">

		<div class="result-header mb10">
			<div class="row">
				<div class="col-md-6">
					<a href="<% route('team_profile', $game -> team_x -> slug) %>">
						<img src="<% $game -> team_x -> logo %>" title="<% $game -> team_x -> name %>" />
					</a>
				</div>
				<div class="col-md-6 text-right">
					<span class="score"><% $scores[0] %></span>
				</div>
			</div>
		</div>

		<div class="result-statistic">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Players</th>
						<th>PTS</th>
						<th>REB</th>
						<th>AST</th>
						<th>STL</th>
						<th>BLK</th>
						<th>TO</th>
						<th class="text-center"><span aria-hidden="true" class="nomargin glyphicon glyphicon-star cyellow"></span></th>
					</tr>
				</thead>
		      	<tbody>
			      	<?php foreach($team_x_performance as $player){ ?>
			        <tr>
			          <td>
			          	<img width="24" class="img-circle mr5" src="<% $player -> player -> avatar %>"> 
			          	<a href="<% route('player_profile', $player -> player -> slug) %>">
			          		<% $player -> player -> firstname %> <% $player -> player -> lastname %>
			          	</a>
			          </td>
			          <td><% $player -> points %></td>
			          <td><% $player -> rebounds %></td>
			          <td><% $player -> assists %></td>
			          <td><% $player -> steals %></td>
			          <td><% $player -> blocks %></td>
			          <td><% $player -> turnovers %></td>
			          <td class="cgreen"><% $player -> fantasy_points %></td>
			        </tr>
			      	<?php } ?>
				</tbody>
		    </table>
		</div>
	</div>

	<div class="col-md-6">
		<div class="result-header mb10">
			<div class="row">
				<div class="col-md-6">
					<span class="score"><% $scores[1] %></span>
				</div>
				<div class="col-md-6 text-right">
					<a href="<% route('team_profile', $game -> team_y -> slug) %>">
						<img src="<% $game -> team_y -> logo %>" title="<% $game -> team_y -> name %>" />
					</a>
				</div>
			</div>
		</div>

		<div class="result-statistic">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Players</th>
						<th>PTS</th>
						<th>REB</th>
						<th>AST</th>
						<th>STL</th>
						<th>BLK</th>
						<th>TO</th>
						<th class="text-center"><span aria-hidden="true" class="nomargin glyphicon glyphicon-star cyellow"></span></th>
					</tr>
				</thead>
		      	<tbody>
			      	<?php foreach($team_y_performance as $player){ ?>
			        <tr>
			          <td>
			          	<img width="24" class="img-circle mr5" src="<% $player -> player -> avatar %>"> 
			          	<a href="<% route('player_profile', $player -> player -> slug) %>">
			          		<% $player -> player -> firstname %> <% $player -> player -> lastname %>
			          	</a>
			          </td>
			          <td><% $player -> points %></td>
			          <td><% $player -> rebounds %></td>
			          <td><% $player -> assists %></td>
			          <td><% $player -> steals %></td>
			          <td><% $player -> blocks %></td>
			          <td><% $player -> turnovers %></td>
			          <td class="cgreen"><% $player -> fantasy_points %></td>
			        </tr>
			      	<?php } ?>
				</tbody>
		    </table>
		</div>
	</div>
</div>

</div>
@endsection