<!DOCTYPE html>
<html lang="en" ng-app="pbahotpicks">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reset Password</title>
	<link rel="shortcut icon" href="<% asset('assets/img/favicon.png') %>" type="image/x-icon">
	<link href="<% asset('assets/css/bootstrap.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/bootstrap-theme.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/override.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/main.css') %>" rel="stylesheet">
</head>
<body style="background-color: #f5f5f5;padding-top:30px;margin-bottom:0px;" ng-controller="Reset">

	<div class="container">
		<div class="panel panel-default" style="margin-top:50px;margin: 25px auto;width: 500px;">
			<div class="panel-body" style=" padding: 20px 30px;">
				<div class="row">
					<div class="col-lg-12 reset-password">
						<h1 class="text-center" style="font-size: 22px;font-weight: normal;margin: 20px 0 30px;color: #155aa1;">Reset Password</h1>
						<form ng-submit="resetPassword(reset)" autocomplete="off" style="width:350px;margin:0 auto;">
							<div ng-class="resetAlertType" ng-show="resetPasswordAlert" role="alert" ng-bind="alertMessage"></div>

							<div class="text-center" ng-show="passwordResetSuccessfull">
								<p class="mb20"><span style="font-size:48px;" class="glyphicon glyphicon-ok-circle cgreen" aria-hidden="true"></span></p>
								<div class="mb20">Your password was changed successfully, You can now <a href="<% URL::to('/?action=login') %>">login</a> your account with your new password</div>
							</div>
							<div ng-hide="passwordResetSuccessfull">
								<input type="password" ng-model="reset.newpassword" class="form-control mb10" placeholder="Enter your new password" >
								<input type="password" ng-model="reset.renewpassword" class="form-control mb10" placeholder="Confirm your new password" >
								<input type="hidden" ng-model="reset.email" ng-init="reset.email='<% Request::input('email') %>'">
								<input type="hidden" ng-model="reset.token" ng-init="reset.token='<% Request::input('token') %>'">
								<input type="submit" style="display:none;" value="Submit">
								<a style="width:100%" class="field trigger btn btn-flat-blue mb10" ng-click="resetPassword(reset)" href="">Submit</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Web JS Global Config -->
<script>
	var angular_dependency = [];
</script>
<script src="<% asset('assets/js/jquery.min.js') %>"></script>
<script src="<% asset('assets/js/angular/minified.js') %>"></script>
<script src="<% asset('assets/js/angular/primary.js') %>"></script>
<script src="<% asset('assets/js/angular/reset.js') %>"></script>


</body>
</html>