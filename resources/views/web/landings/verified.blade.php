<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Account Activated</title>
	<link rel="shortcut icon" href="<% asset('assets/img/favicon.png') %>" type="image/x-icon">
	<link href="<% asset('assets/css/bootstrap.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/bootstrap-theme.min.css') %>" rel="stylesheet">
</head>
<body style=" background-color: #f5f5f5;">

	<div class="container">
		<div class="panel panel-default" style="margin-top:50px">
			<div class="panel-body" style=" padding: 20px 30px;">
				<div class="row">
					<div class="col-lg-12">
						<h1 style="font-size: 22px;font-weight: normal;margin: 10px 0 20px;color: #155aa1;">Welcome to PinoyTenPicks</h1>
						<p><span class="glyphicon glyphicon-ok" aria-hidden="true" style="margin-right:5px;font-size:24px;color:#40b322"></span> Congratulations! Your account has now been activated. You will be redirected shortly to the main page.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
	setTimeout("location.href = '<% URL::to('/') %>';", 3000);
</script>

</body>
</html>