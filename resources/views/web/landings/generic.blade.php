<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $response_title ?></title>
	<link rel="shortcut icon" href="<% asset('assets/img/favicon.png') %>" type="image/x-icon">
	<link href="<% asset('assets/css/bootstrap.min.css') %>" rel="stylesheet">
	<link href="<% asset('assets/css/bootstrap-theme.min.css') %>" rel="stylesheet">
</head>
<body style=" background-color: #f5f5f5;">

	<div class="container" style="margin-top:50px">

		<div class="panel panel-default">
			<div class="panel-heading"><?= $response_heading ?></div>
			<div class="panel-body">
				<?= $response_body ?>
			</div>
		</div>

	</div>
	
</body>
</html>