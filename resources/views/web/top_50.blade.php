@extends('web.layouts.whitebg')

@section('title', 'Top 50 Active Managers - PinoyTenPicks')

@section('content')
<div class="content-full standard-page text-center">
<h1>Top 50 Active Managers</h1>
<?php
 
function addOrdinalNumberSuffix($num) {
	if (!in_array(($num % 100),array(11,12,13))){
		switch ($num % 10) {
			case 1:  return $num.'st';
			case 2:  return $num.'nd';
			case 3:  return $num.'rd';
		}
	}
	return $num.'th';
}
 
?>

<?php $i = 1; ?>

<table class="table table-bordered" style="width:600px;margin:0 auto;">
	<thead>
		<tr>
			<th class="text-center">Rank</th>
			<th>Managers</th>
			<th class="text-center">Fantasy Points</th>
		</tr>	
	</thead>
	<tbody>
		<?php foreach($top_managers as $manager){ ?>
		<?php $manager_details = json_decode($manager, true); ?>
		<tr class="<% ($manager_details['id'] == $my_own_id) ? 'success' : 'crow' %>">
			<td class="text-center"><% addOrdinalNumberSuffix($i) %></td>
			<td class="text-left"><a href="<% route('profile', $manager_details['slug']) %>"><img width="24" class="img-circle" src="<% asset($manager_details['avatar']) %>" /> <% $manager_details['name'] %></a></td>
			<td class="text-center cgreen f16"><% number_format($manager_details['total_fantasy_points']) %></td>
		</tr>

		<?php $i++; ?>

		<?php } ?>

	</tbody>
</table>


</div>
@endsection

