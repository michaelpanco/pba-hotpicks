@extends('web.layouts.whitebg')

@section('title')<% $league -> name . ' - PinoyTenPicks ' %>@endsection

@section('css')
@parent<link href="<% asset('assets/css/leagues.css') %>" rel="stylesheet">
@stop

@section('content')

<div id="LeagueGuest" class="content-left fleft" ng-controller="LeagueGuest" style="width:790px">

	<div class="mb20">
		<div class="row">
			<div class="col-md-8"><h1 class="league-name"><% $league -> name %></h1></div>
			<div class="col-md-4 text-right join-container">
				<?php if($can_join){ ?>
				<?php ?>
				<?php if($league -> password){ ?>
				<input type="text" class="form-control mb10" ng-model="leaguePassword" placeholder="Please enter league password" >
				<?php } ?>
				<a ng-click="joinLeague()" href="" class="field trigger btn btn-flat-blue btn-small">Join</a>
				<?php } ?>
			</div>
		</div>
		
		<div class="description cgray mb20"><% $league -> desc %></div>
		<div class="host mb10">Host : <a href="<% route('profile', $league -> creator -> slug) %>"><% $league -> creator -> fullname %></a></div>
	</div>

	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Team</th>
				<th>Owner</th>
				<?php if($league -> ready){ ?><th>Fantasy Points</th><?php } ?>
			</tr>
		</thead>


		<tbody>
			<?php $i = 1; ?>
			<?php foreach($league_participants as $participant){ ?>
			<tr>
				<td><?php if($league -> ready){ ?><% Helper::ordinal($i) %><?php }else{ ?><% $i %><?php } ?></td>
				<td>
				<a href="<% route('franchise_profile', $participant['participant_id']) %>">
					<img width="32" src="/assets/img/franchise_logos/64/<% $participant['franchise_logo'] %>" class="mr5"> <% $participant['franchise_name'] %>
				</a>
			</td>
			<td>
				<a href="<% route('profile', $participant['franchise_manager_slug']) %>">
					<img width="32" class="mr5 img-circle avatar" src="<% url($participant['franchise_manager_avatar']) %>"> <% $participant['franchise_manager_fullname'] %>
				</a>
			</td>
			<?php if($league -> ready){ ?><td class="cgreen" style="padding-left:60px"><% $participant['franchise_fantasy_points'] %></td><?php } ?>
			</tr>
			<?php $i++; ?>
			<?php } ?>
		</tbody>

	</table>

</div>
@endsection

@section('sidebar')
	<div class="content-right fleft">
	@include('web.modules.standings')
	</div>
@endsection

@section('jsdependency')

@endsection

@section('jscustom')
<script type="text/javascript">
var franchise_id = <% $auth_user_franchise_id %>;
var league_id = <% $league -> id %>;
</script>
<script src="<% asset('assets/js/angular/leagueguest.js') %>"></script>
@endsection

@section('jsinline')
<script type="text/javascript">
</script>
@endsection