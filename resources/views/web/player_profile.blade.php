@extends('web.layouts.whitebg')

@section('title')<% $player -> fullname %>@endsection
@section('description', 'This privacy policy discloses the privacy practices for www.pinoytenpicks.com. This privacy policy applies solely to information collected by this web site.')


@section('css') @parent <link href="<% asset('assets/css/player_profile.css') %>" rel="stylesheet"> @stop

<?php if(Auth::check()){ ?>
@parent
  <link href="<% asset('assets/css/member.css') %>" rel="stylesheet">
@stop
<?php } ?>
@section('content')
<div class="content-left fleft" style="width:790px">

<div class="row mb20">
	<div class="col-md-2 text-center"><img src="<% $player -> avatar %>" class="img-circle"></div>
	<div class="col-md-10">

		<div class="row mb20">
			<div class="col-md-6">
				<h1 class="f28" style="margin: 0 0 10px;"><% $player -> fullname %> <span class="cgray">#<% $player -> jersey %></span></h1>
				<div class="mb10 f16"><?= Helper::getLabelPosition($player -> position) ?> - <a href="<% route('team_profile', $player -> team -> slug) %>"><% $player -> team -> name %></a></div>
				<div><span class="cgray">Height :</span> <% $player -> height %></div>
				<div><span class="cgray">School :</span> <% $player -> school %></div>
			</div>
			<div class="col-md-6 text-right f16" style="padding-top:45px">
				<div class="mb5"><span class="cgray">Fantasy Salary :</span> <% number_format($player -> salary) %></div>
				<div><span class="cgray">Total Fantasy Points :</span> <% number_format($total_fantasy_points) %></div>
			</div>
		</div>
		<div class="f16">
			<span class="mr10 cgray">STATS</span>
			<span class="mr10"><% $avg_points %> PPG</span>
			<span class="mr10"><% $avg_rebounds %> RPG</span>
			<span class="mr10"><% $avg_assists %> APG</span>
			<span class="mr10"><% $avg_steals %> SPG</span>
			<span class="mr10"><% $avg_blocks %> BPG</span>
		</div>

	</div>
</div>

<?php if($performances -> count() > 0){ ?>
<p class="f18">Performance</p>
<div class="row performance">
	<div class="col-md-12">

		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Opponent</th>
					<th>PTS</th>
					<th>REB</th>
					<th>AST</th>
					<th>STL</th>
					<th>BLK</th>
					<th>TO</th>
					<th class="text-center"><span class="nomargin glyphicon glyphicon-star cyellow" aria-hidden="true"></span></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($performances as $game){ ?>
				<tr>
					<td><span class="cgray">vs</span> <a href="<% route('team_profile', $game -> adversary -> slug) %>"><% $game -> adversary -> name %></a></td>
					<td><% $game -> points %></td>
					<td><% $game -> rebounds %></td>
					<td><% $game -> assists %></td>
					<td><% $game -> steals %></td>
					<td><% $game -> blocks %></td>
					<td><% $game -> turnovers %></td>
					<td class="cgreen"><% $game -> fantasy_points %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

	</div>
</div>
<?php }else{ ?>
	<div class="nogame">No game played</div>
<?php } ?>

</div>
@endsection

@section('sidebar')
	<div class="content-right fleft">
	@include('web.modules.standings')
	</div>
@endsection