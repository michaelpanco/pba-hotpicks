@extends('web.layouts.whitebg')

@section('title', 'Disclaimer - PinoyTenPicks')
@section('description', 'All content provided on this site is for informational purposes only. The owner of this site makes no representations as to the accuracy or completeness of any information on this site or found by following any link on this site.')


@section('content')
<div class="content-full standard-page">
<h1>Disclaimer</h1>

<ul>
<li class="mb20 dgray">All content provided on this site is for informational purposes only. The owner of this site makes no representations as to the accuracy or completeness of any information on this site or found by following any link on this site. The owner will not be liable for any errors or omissions in this information nor for the availability of this information. The owner will not be liable for any losses, injuries, or damages from the display or use of this information.</li>
<li class="mb20 dgray">PinoyTenPicks creates no claim or credit for images featured on our site unless otherwise noted. All visual content is copyrighted to its respectful owners and we make every effort to link back to original content whenever possible. If you own rights to any of the images, and do not wish them to appear here, please <a href="<% route('contact') %>">contact us</a> and they will be promptly remove.</li>
<li class="mb20 dgray">In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.</li>
<li class="mb20 dgray">
Through this website you are able to link to other websites which are not under the control of PinoyTenPicks. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.
</li>
<li class="mb20 dgray">Every effort is made to keep the website up and running smoothly. However, PinoyTenPicks takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</li>
<li class="mb20 dgray">This is not a gambling or betting website that involves real money. Our betting feature involves virtual currency that you can win and use it for game items and improve your own team. You cannot convert your virtual currency into a real money.</li>
</ul>

</div>
@endsection

