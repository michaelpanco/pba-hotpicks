@extends('web.layouts.single')

@section('title')<% 'Page not found' %>@endsection

@section('nofollow', '<meta name="robots" content="noindex, nofollow">
')
@section('description', 'Page not found')

@section('css')
@parent<link href="<% asset('assets/css/profile.css') %>" rel="stylesheet">
@stop

@section('content')
<style type="text/css">
body{
	background-color: #fff;
	text-align: center;
}
</style>
<p style="font-size:30px;margin-top:20px;margin-bottom:20px">Oops, the page you are looking for cannot be found.</p>
<img src="<% asset('assets/img/notfound.png') %>">

@endsection
