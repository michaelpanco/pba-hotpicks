@extends('admin.layouts.auth')

@section('content')
<div class="row update-team-profile-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-6">
		        <h1  style="font-size:20px;"><img width="72" src="<% $team -> logo %>" style="margin-right:10px" /> <% $team -> name %></h1>
		    </div>
		    <div class="col-lg-6 text-right" style="padding-top:20px">
		        <a href="<% route('admin_players') %>?team=<% $team -> id %>&selectedTeam=<% $team -> name %>" class="btn btn-lg btn-info"><i class="fa fa-male fa-fw"></i> Players</a>
		        <a href="" class="btn btn-lg btn-info"><i class="fa fa-cube fa-fw"></i> Games</a>
		    </div>
		</div>
	</div>

    <div class="mb10" >
    	<div class="col-lg-6" ng-init="teamID=<% $team -> id %>">
    		<p class="cgray f12 mb5">Team Name</p>
    		<input type="text" class="form-control mb10 field" placeholder="Name" ng-model="teamName" ng-init="teamName='<% $team -> name %>'" />

    		<p class="cgray f12 mb5">Team Slug</p>
    		<input type="text" class="form-control mb10 field" placeholder="Slug" ng-model="teamSlug" ng-init="teamSlug='<% $team -> slug %>'" />

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Team Manager</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Manager" ng-model="teamManager" ng-init="teamManager='<% $team -> manager %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Team Coach</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Coach" ng-model="teamCoach" ng-init="teamCoach='<% $team -> coach %>'" />
    			</div>
    		</div>

    		<p class="cgray f12 mb5">Team Asst. Coaches</p>
    		<textarea class="form-control mb10 field" style="height:100px" spellcheck="false" placeholder="Asst. Coaches" ng-model="teamAsstCoach" ng-init="teamAsstCoach='<% $team -> assistant_coaches %>'"></textarea>

    	</div>
    	<div class="col-lg-6">

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Year Joined</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Joined" ng-model="teamJoined" ng-init="teamJoined='<% $team -> joined %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Titles</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Titles" ng-model="teamTitles" ng-init="teamTitles='<% $team -> titles %>'" />
    			</div>
    		</div>


    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Current Win</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Win" ng-model="teamWin" ng-init="teamWin='<% $team -> win %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Current Loss</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Loss" ng-model="teamLoss" ng-init="teamLoss='<% $team -> loss %>'" />
    			</div>
    		</div>

    		<p class="cgray f12 mb5">Logo Source</p>
    		<input type="text" class="form-control mb10 field" placeholder="Logo" ng-model="teamLogo" ng-init="teamLogo='<% $team -> logo %>'" />
    	</div>

    </div>
    <div class="col-lg-12">
        <a href="" style="width:150px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateTeamProfile()">Update Team</a>
    </div>
</div>
@endsection