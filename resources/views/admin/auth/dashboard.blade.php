@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="row admin-title-header">
            <div class="col-md-6"><h1 style="font-size:20px;"><i class="fa fa-dashboard fa-fw"></i> Dashboard</h1></div>
            <div class="col-md-6 text-right"><a href="" ng-click="updateStatistics()" class="btn btn-default btn-lg update-statistic"><i class="fa fa-cogs fa-fw"></i> Update Statistic</a></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><% $new_managers %></div>
                        <div>New Managers</div>
                    </div>
                </div>
            </div>
            <a href="<% route('admin_new_managers_lists') %>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-child fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><% $active_managers %></div>
                        <div>Active Managers</div>
                    </div>
                </div>
            </div>
            <a href="<% route('admin_active_managers_lists') %>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comment fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><% $new_posts %></div>
                        <div>New Posts</div>
                    </div>
                </div>
            </div>
            <a href="<% route('admin_new_posts_lists') %>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-trophy fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><% $new_leagues %></div>
                        <div>New Leagues</div>
                    </div>
                </div>
            </div>
            <a href="<% route('admin_new_leagues_lists') %>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart fa-fw"></i> Manager Registration Report

            </div>
            <div class="panel-body">
                <div id="morris-area-chart"></div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-pie-chart fa-fw"></i> Summary
            </div>
            
            <div class="panel-body">
                <div class="list-group">
                    <a href="" class="list-group-item">
                        <i class="fa fa-user fa-fw"></i> Total Members
                        <span class="pull-right text-muted small"><% $total_accounts %>
                        </span>
                    </a>
                    <a href="" class="list-group-item">
                        <i class="fa fa-comment fa-fw"></i> Total Posts
                        <span class="pull-right text-muted small"><% $total_posts %>
                        </span>
                    </a>
                    <a href="" class="list-group-item">
                        <i class="fa fa-envelope fa-fw"></i> Total Messages
                        <span class="pull-right text-muted small"><% $total_messages %>
                        </span>
                    </a>
                    <a href="" class="list-group-item">
                        <i class="fa fa-trophy fa-fw"></i> Total Leagues
                        <span class="pull-right text-muted small"><% $total_leagues %>
                        </span>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('jscustom')
<script src="<% asset('admin-assets/components/raphael/raphael-min.js') %>"></script>
<script src="<% asset('admin-assets/components/morrisjs/morris.min.js') %>"></script>


<script type="text/javascript">
$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [
        <?php foreach($chart_data as $key => $val){ ?>
        {
            dates: '<% $key %>',
            registration: <% $val %>
        },
        <?php } ?>

        ],
        xkey: 'dates',
        ykeys: ['registration'],
        labels: ['Registration'],
        pointSize: 1,
        hideHover: 'auto',
        resize: true,

    });

});

</script>


@endsection