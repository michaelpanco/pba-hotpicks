@extends('admin.layouts.auth')

@section('content')
<div class="row update-game-details-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-12">
		        <h1  style="font-size:14px;">
					<img width="64" src="<% $details -> team_x -> logo %>" /> <% $details -> team_x -> name %> <span class="cgray" style="margin:0 10px">vs</span> <% $details -> team_y -> name %> <img width="64" src="<% $details -> team_y -> logo %>" />
		    </div>
		</div>
	</div>

    <div class="mb10" >
    	<div class="col-lg-7" ng-init="gameID=<% $details -> id %>">

    		<div class="row mb10">
    			<div class="col-md-6">
					<!-- Split button -->
					<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedTeamXID=<% $details -> team_x -> id %>;selectedXTeam='<% $details -> team_x -> name %>'">
						<button type="button" class="btn btn-default field" ng-bind="selectedXTeam" style="width:227px">Select Team</button>
						<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu">
							<?php foreach($teams as $team){ ?>
							<li><a href="" ng-click="selectedXTeam='<% $team -> name %>';selectedTeamXID=<% $team -> id %>"><% $team -> name %></a></li>
							<?php } ?>
						</ul>
					</div>
    			</div>
    			<div class="col-md-6">
					<!-- Split button -->
					<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedTeamYID=<% $details -> team_y -> id %>;selectedYTeam='<% $details -> team_y -> name %>'">
						<button type="button" class="btn btn-default field" ng-bind="selectedYTeam" style="width:227px">Select Team</button>
						<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu">
							<?php foreach($teams as $team){ ?>
							<li><a href="" ng-click="selectedYTeam='<% $team -> name %>';selectedTeamYID=<% $team -> id %>"><% $team -> name %></a></li>
							<?php } ?>
						</ul>
					</div>
    			</div>
    		</div>

    		<div class="row mb10">
    			<div class="col-md-6">
    				<p class="cgray f12 mb5">Winner</p>

    				<?php $team_winner_id = ($details -> team_winner) ? $details -> team_winner -> id : 'false' ?>
    				<?php $team_winner = ($details -> team_winner) ? $details -> team_winner -> name : 'false' ?>

					<!-- Split button -->
					<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedTeamWinnerID=<% $team_winner_id %>;selectedTeamWinner='<% $team_winner %>'">
						<button type="button" class="btn btn-default field" ng-bind="selectedTeamWinner || 'Select Game Winner'" style="width:227px">Select Game Winner</button>
						<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu">
							<?php foreach($teams as $team){ ?>
							<li><a href="" ng-click="selectedTeamWinner='<% $team -> name %>';selectedTeamWinnerID=<% $team -> id %>"><% $team -> name %></a></li>
							<?php } ?>
							<li role="separator" class="divider"></li>
							<li><a href="" ng-click="selectedTeamWinner='No Winner';selectedTeamWinnerID=''">No Winner</a></li>
						</ul>
					</div>

    			</div>
    			<div class="col-md-6">
		    		<p class="cgray f12 mb5">Score</p>
		    		<input type="text" class="form-control field" placeholder="Score" ng-model="gameScore" ng-init="gameScore='<% $details -> score %>'" />
    			</div>
    		</div>

    		<div class="row mb10">
    			<div class="col-md-6">
    				<p class="cgray f12 mb5">Game Venue</p>
					<!-- Split button -->
					<div class="btn-group mr5" ng-hide="otherVenue" style="display:inline;float:left" ng-init="gameVenue='<% $details -> venue %>'">
						<button type="button" class="btn btn-default field" ng-bind="gameVenue" style="width:227px">Select Game Venue</button>
						<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="" ng-click="gameVenue='Smart Araneta Coliseum'">Smart Araneta Coliseum</a></li>
							<li><a href="" ng-click="gameVenue='Mall of Asia Arena'">Mall of Asia Arena</a></li>
							<li><a href="" ng-click="gameVenue='Cuneta Astrodome'">Cuneta Astrodome</a></li>
							<li><a href="" ng-click="gameVenue='Ynares Center'">Ynares Center</a></li>
							<li><a href="" ng-click="gameVenue='PhilSports Arena'">PhilSports Arena</a></li>
							<li><a href="" ng-click="gameVenue='Philippine Arena'">Philippine Arena</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="" ng-click="gameVenue='';otherVenue=true">Others</a></li>
						</ul>
					</div>
                    <input type="text" ng-show="otherVenue" ng-model="gameVenue" class="form-control field mb10" placeholder="Game Venue">
    			</div>
    			<div class="col-md-6"></div>
    		</div>

    		<div class="row">
    			<div class="col-md-6">
		    		<p class="cgray f12 mb5">Game No.</p>
		    		<input type="text" class="form-control field" placeholder="Game No." ng-model="gameNo" ng-init="gameNo='<% $details -> game_no %>'" />
    			</div>
    			<div class="col-md-6">
		    		<p class="cgray f12 mb5">Cluster No.</p>
		    		<input type="text" class="form-control field" placeholder="Cluster No." ng-model="gameClusterNo" ng-init="gameClusterNo='<% $details -> cluster_no %>'" />
    			</div>
    		</div>

    	</div>
    	<div class="col-lg-5">
			<!-- Split button -->
			<div class="btn-group mr5 mb10" ng-hide="otherGameConference" style="display:inline;float:left" ng-init="gameConference='<% $details -> conference %>'">
				<button type="button" class="btn btn-default field" ng-bind="gameConference || 'Select Game Conference'" style="width:238px">Select Game Confrence</button>
				<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="" ng-click="gameConference='PBA Philippine Cup'">PBA Philippine Cup</a></li>
					<li><a href="" ng-click="gameConference='Commissioners Cup'">Commissioners Cup</a></li>
					<li><a href="" ng-click="gameConference='Governors Cup'">Governors Cup</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="" ng-click="gameConference='';otherGameConference=true">Others</a></li>
				</ul>
			</div>

            <input type="text" ng-show="otherGameConference" ng-model="gameConference" class="form-control field mb10" placeholder="Conference">

            <span class="clearfix"></span>

            <p class="cgray f12 mb5">Game Type</p>
			<!-- Split button -->
			<div class="btn-group mr5 mb10" ng-hide="otherGameType" style="display:inline;float:left" ng-init="gameType='<% $details -> game_type %>'">
				<button type="button" class="btn btn-default field" ng-bind="gameType || 'Select Game Type'" style="width:238px">Select Game Type</button>
				<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="" ng-click="gameType='Elimination'">Elimination</a></li>
					<li><a href="" ng-click="gameType='Quarterfinals'">Quarterfinals</a></li>
					<li><a href="" ng-click="gameType='Semi Finals'">Semi Finals</a></li>
					<li><a href="" ng-click="gameType='Finals'">Finals</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="" ng-click="gameType='';otherGameType=true">Others</a></li>
				</ul>
			</div>

            <input type="text" ng-show="otherGameType" ng-model="gameType" class="form-control field mb10" placeholder="Game Type">

            <span class="clearfix"></span>

            <div class="row mb10">
            	<div class="col-md-6">
		    		<p class="cgray f12 mb5">Season</p>
		    		<input type="text" class="form-control field" placeholder="Season" ng-model="gameSeason" ng-init="gameSeason='<% $details -> season %>'" />
            	</div>
            	<div class="col-md-6">
		    		<p class="cgray f12 mb5">Sequence</p>
		    		<input type="text" class="form-control field" placeholder="Sequence" ng-model="gameSequence" ng-init="gameSequence='<% $details -> sequence %>'" />
            	</div>
            </div>

            <span class="clearfix"></span>
            <p class="cgray f12 mb5">Date and Time</p>
            <input type="text" class="form-control field" placeholder="Date and Time" ng-model="gameDateTime" ng-init="gameDateTime='<% $details -> datetime %>'" />

    	</div>

    </div>

    <div class="col-lg-12 text-right" style="margin-top:20px">
        <a href="" style="width:150px" class="field trigger btn btn-lg btn-primary mb20" ng-click="updateGameDetails()">Update Game</a>
    </div>

</div>
@endsection