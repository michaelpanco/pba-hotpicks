@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header" style="font-size:20px;"><i class="fa fa-cube fa-fw"></i> PBA Games</h1>

        <div class="row">

	        <div class="col-md-8">
		        <form class="mb20" method="GET" action="/<% env('ADMIN_SLUG') %>/games">

		        	<input type="hidden" name="team" value="{{selectedTeamID}}" />
		        	<input type="hidden" name="selectedTeam" value="{{selectedTeam}}" />

					<!-- Split button -->
					<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedTeamID=<% Request::input('team', 'false') %>;selectedTeam='<% Request::input('selectedTeam', '') %>'">
						<button type="button" class="btn btn-default" ng-bind="selectedTeam || 'Select Team'" style="width:220px">Select Team</button>
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu">
							<?php foreach($teams -> sortByDesc('win') as $team){ ?>
							<li><a href="" ng-click="selectedTeam='<% $team -> name %>';selectedTeamID=<% $team -> id %>"><% $team -> name %></a></li>
							<?php } ?>
							<li role="separator" class="divider"></li>
							<li><a href="" ng-click="selectedTeam='All Team';selectedTeamID=''">All Team</a></li>
						</ul>
					</div>

					<input type="submit" class="btn btn-primary" value="Search">
					<div class="clearfix"></div>
		        </form>
	        </div>
	        <div class="col-md-4 text-right">
	        	<a href="" class="btn btn-primary" data-toggle="modal" data-target=".modal-create-game"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Game</a>
	        </div>

    	</div>

        <?php if($games -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Game ID</th>
					<th>Matchup</th>
					<th>Game Type</th>
					<th>Date and Time</th>
					<th>Score</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($games as $game){ ?>
				<tr>
					<td><a href="<% route('admin_game_details', $game -> id) %>"><% $game -> game_no %></a></td>
					<td>
						<a href="<% route('admin_team_profile', $game -> team_x_id) %>"><% $game -> team_x -> name %></a> 
						vs 
						<a href="<% route('admin_team_profile', $game -> team_y_id) %>"><% $game -> team_y -> name %></a> 
					</td>
					<td><% $game -> game_type %></td>
					<td><% date('M j, Y g:i a', strtotime($game -> datetime)) %></td>
					<td>
					<?php if($game -> score){ ?>
					<a href="<% route('admin_game_result', $game -> id) %>"><% $game -> score %></a>
					<?php } ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="text-right"><?= $games -> render() ?></div>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@include('admin.modals.create_game')
@endsection