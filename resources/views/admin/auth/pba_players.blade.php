@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
    	<div class="row admin-title-header">
    		<div class="col-md-6"><h1 style="font-size:20px;"><i class="fa fa-ioxhost fa-fw"></i> PBA Players</h1></div>
    		<div class="col-md-6 text-right">
    			<a href="" class="btn btn-primary mb10" style="margin-top:10px" data-toggle="modal" data-target=".modal-create-player"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Player</a>
    		</div>
    	</div>
    	
        <form class="mb20" method="GET" action="/<% env('ADMIN_SLUG') %>/players" style="margin-top:20px">

        	<input type="hidden" name="team" value="{{selectedTeamID}}" />
        	<input type="hidden" name="selectedTeam" value="{{selectedTeam}}" />
        	<input type="hidden" name="position" value="{{selectedPlayerPositionID}}" />
        	<input type="hidden" name="selectedPlayerPosition" value="{{selectedPlayerPosition}}" />

			<input name="name" type="text" style="width:200px;display:inline;float:left" value="<% Input::input('name') %>" class="form-control mr5" value="" placeholder="Search Player Name">
			<!-- Split button -->
			<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedTeamID=<% Input::input('team', 'false') %>;selectedTeam='<% Input::input('selectedTeam') %>'">
				<button type="button" class="btn btn-default" ng-bind="selectedTeam || 'Select Team'" style="width:200px">Select Team</button>
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<?php foreach($teams as $team){ ?>
					<li><a href="" ng-click="selectedTeam='<% $team -> name %>';selectedTeamID=<% $team -> id %>"><% $team -> name %></a></li>
					<?php } ?>
					<li role="separator" class="divider"></li>
					<li><a href="" ng-click="selectedTeam='All Team';selectedTeamID=''">All Team</a></li>
				</ul>
			</div>
			<!-- Split button -->
			<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedPlayerPositionID=<% Input::input('position', 'false') %>;selectedPlayerPosition='<% Input::input('selectedPlayerPosition') %>'">
				<button type="button" class="btn btn-default" ng-bind="selectedPlayerPosition || 'Select Position'" style="width:200px">Select Position</button>
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="" ng-click="selectedPlayerPosition='Point Guard';selectedPlayerPositionID=1">Point Guard</a></li>
					<li><a href="" ng-click="selectedPlayerPosition='Shooting Guard';selectedPlayerPositionID=2">Shooting Guard</a></li>
					<li><a href="" ng-click="selectedPlayerPosition='Small Forward';selectedPlayerPositionID=3">Small Forward</a></li>
					<li><a href="" ng-click="selectedPlayerPosition='Power Forward';selectedPlayerPositionID=4">Power Forward</a></li>
					<li><a href="" ng-click="selectedPlayerPosition='Center';selectedPlayerPositionID=5">Center</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="" ng-click="selectedPlayerPosition='All Position';selectedPlayerPositionID=''">All Position</a></li>
				</ul>
			</div>
			<input name="min" value="<% Input::input('min') %>" type="text" style="width:100px;display:inline;float:left" class="form-control mr5" value="" placeholder="Min Salary"> 
			<input name="max" value="<% Input::input('max') %>" type="text" style="width:100px;display:inline;float:left" class="form-control mr5" value="" placeholder="Max Salary">

			<input type="submit" class="btn btn-primary" value="Search">
			<div class="clearfix"></div>
        </form>

        <?php if($players -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Team</th>
					<th>Position</th>
					<th>Salary</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($players as $player){ ?>
				<tr>
					<td>
						<a href="<% route('admin_player_profile', $player -> id) %>">
						<?php if($player -> avatar){ ?>
							<img width="32" class="img-circle mr5" src="<% $player -> avatar %>"> 
						<?php }else{ ?>
							<img width="32" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
						<?php } ?>
						<% $player -> lastname %>, <% $player -> firstname %></a></td>
					<td><a href="<% route('admin_team_profile', $player -> team_id) %>"><% $player -> team -> name %></a></td>
					<td><?= Helper::getLabelPosition($player -> position) ?></td>
					<td class="cgreen"><% number_format($player -> salary) %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@include('admin.modals.create_player')
@endsection