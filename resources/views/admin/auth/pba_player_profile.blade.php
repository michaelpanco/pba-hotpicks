@extends('admin.layouts.auth')

@section('content')
<div class="row update-player-profile-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-12">
		        <h1  style="font-size:20px;">

					<?php if($player -> avatar){ ?>
						<img width="72" class="img-circle mr5" src="<% $player -> avatar %>">
					<?php }else{ ?>
						<img width="72" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
					<?php } ?>

		        	<% $player -> fullname %></h1>
		    </div>
		</div>
	</div>

    <div class="mb10" >
    	<div class="col-lg-6" ng-init="playerID=<% $player -> id %>">

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Firstname</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Firstname" ng-model="playerFirstname" ng-init="playerFirstname='<% $player -> firstname %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Lastname</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Lastname" ng-model="playerLastname" ng-init="playerLastname='<% $player -> lastname %>'" />
    			</div>
    		</div>

    		<p class="cgray f12 mb5">Player Slug</p>
    		<input type="text" class="form-control mb10 field" placeholder="Slug" ng-model="playerSlug" ng-init="playerSlug='<% $player -> slug %>'" />

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Position</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Position" ng-model="playerPosition" ng-init="playerPosition='<% $player -> position %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Jersey</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Jersey" ng-model="playerJersey" ng-init="playerJersey='<% $player -> jersey %>'" />
    			</div>
    		</div>


    		<p class="cgray f12 mb5">Team</p>
			<!-- Split button -->
			<div class="btn-group mb10" ng-init="selectedTeam='<% $player -> team -> name %>';selectedTeamID=<% $player -> team_id %>">
				<button type="button" class="btn btn-default field" style="min-width:190px" ng-bind="selectedTeam"></button>
				<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<?php foreach($teams as $team){ ?>
					<li><a href="" ng-click="selectedTeam='<% $team -> name %>';selectedTeamID=<% $team -> id %>"><% $team -> name %></a></li>
					<?php } ?>
				</ul>
			</div>

    	</div>
    	<div class="col-lg-6">

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Birthday</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Birthday" ng-model="playerBirthday" ng-init="playerBirthday='<% $player -> birthday %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">School</p>
		    		<input type="text" class="form-control mb10 field" placeholder="School" ng-model="playerSchool" ng-init="playerSchool='<% $player -> school %>'" />
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Height</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Height" ng-model="playerHeight" ng-init="playerHeight='<% $player -> height %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Avatar</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Avatar" ng-model="playerAvatar" ng-init="playerAvatar='<% $player -> avatar %>'" />
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Status</p>
					<!-- Split button -->
					<div class="btn-group mb10" ng-init="selectedStatus='<% $player -> status %>'">
						<button type="button" class="btn btn-default field" style="width:190px" ng-bind="selectedStatus"></button>
						<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="" ng-click="selectedStatus='ACTV'">ACTV</a></li>
							<li><a href="" ng-click="selectedStatus='DSBL'">DSBL</a></li>
						</ul>
					</div>
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Salary</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Salary" ng-model="playerSalary" ng-init="playerSalary='<% $player -> salary %>'" />
    			</div>
    		</div>

    	</div>

    </div>
    <div class="col-lg-12">
        <a href="" style="width:150px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updatePlayerProfile()">Update Player</a>
    </div>

</div>
@endsection