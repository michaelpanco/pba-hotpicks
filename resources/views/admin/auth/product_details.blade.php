@extends('admin.layouts.auth')

@section('content')
<div class="row update-product-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-12">
		        <h1  style="font-size:20px;"><img width="32" class="mr5" src="<% $product -> img %>"> <% $product -> name %></h1>
		    </div>
		</div>
	</div>

    <div class="mb10" >
    	<div class="col-lg-12" ng-init="productID=<% $product -> id %>">
    		<p class="cgray f12 mb5">Name</p>
    		<input type="text" class="form-control mb10 field" placeholder="Name" ng-model="productName" ng-init="productName='<% $product -> name %>'" />

    		<p class="cgray f12 mb5">Image</p>
    		<input type="text" class="form-control mb10 field" placeholder="Image" ng-model="productImage" ng-init="productImage='<% $product -> img %>'" />

    		<div class="row">
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Type</p>
		    		<input type="text" class="form-control mb10 field" maxlength="4" placeholder="Type" ng-model="productType" ng-init="productType='<% $product -> type %>'" />
    			</div>
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Product ID</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Product ID" ng-model="productProductID" ng-init="productProductID='<% $product -> product_id %>'" />
    			</div>
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Price</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Price" ng-model="productPrice" ng-init="productPrice='<% $product -> price %>'" />
    			</div>
    		</div>

    	</div>


    </div>
    <div class="col-lg-12">
        <a href="" style="width:200px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateProduct()">Update Product</a>
    </div>
</div>
@endsection