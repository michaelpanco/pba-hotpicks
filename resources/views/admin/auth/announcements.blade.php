@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
    	<div class="row admin-title-header">
    		<div class="col-md-8"><h1 style="font-size:20px;"><i class="fa fa-bullhorn fa-fw"></i> Announcements</h1></div>
    		<div class="col-md-4 text-right" style="padding-top:10px">
    			<a href="" class="btn btn-primary mb10" data-toggle="modal" data-target=".modal-create-announcement"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Announcement</a>
    		</div>
    	</div>

        <?php if($announcements -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Text</th>
					<th>Order</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($announcements as $announcement){ ?>
				<tr>
					<td><a href="<% route('admin_announcement_details', $announcement -> id) %>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
					<td><% $announcement -> text %></td>
					<td><% $announcement -> order %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>

    </div>
</div>

@include('admin.modals.create_announcement')

@endsection