@extends('admin.layouts.auth')

@section('content')
<div class="row update-game-details-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-8">
		        <h1  style="font-size:14px;">
					<img width="64" src="<% $details -> team_x -> logo %>" style="margin-right:10px" /> <% $details -> team_x -> name %> <span class="f18 cgreen" style="padding:0px 10px"><% explode('-', $details -> score)[0] %></span><span class="cgray" style="margin:0 10px">vs</span><span class="f18 cgreen" style="padding:0px 10px"><% explode('-', $details -> score)[1] %></span> <% $details -> team_y -> name %> <img style="margin-left:10px" width="64" src="<% $details -> team_y -> logo %>" />
		    </div>
		    <div class="col-lg-4 text-right">
		    	<a href="<% route('admin_game_details', $details -> id) %>" class="btn btn-default"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit Game</a>
		    	<a href="" ng-click="publishGameResult(<% $details -> id %>)" class="publish btn btn-primary"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Publish</a>
		    </div>
		</div>
	</div>

	<div class="col-lg-6">
		<a data-target=".modal-team-x-performance" data-toggle="modal" href="" class="mb10" style="display:block"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Team-X Performance</a>
		<table class="table table-bordered">
			<thead>
				<tr>
		            <th class="text-center">Players</th>
		            <th class="text-center">PTS</th>
		            <th class="text-center">REB</th>
		            <th class="text-center">AST</th>
		            <th class="text-center">STL</th>
		            <th class="text-center">BLK</th>
		            <th class="text-center">TO</th>
		            <th class="text-center"><span class="nomargin glyphicon glyphicon-star cyellow" aria-hidden="true"></span></th>
				</tr>
	        </thead>
	        <tbody>
	        	<?php foreach($results -> where('team_id', $details -> team_x -> id) -> all() as $result){ ?>
				<tr>
					<?php $win = ($result -> win_game == 1) ? '*' : '' ?>
		            <td><?php if($result -> ready == 1){ ?><img width="24" src="<% $result -> player -> avatar %>" class="img-circle mr5"><?php } ?> <% $result -> player -> lastname %><% $win %></td>
		            <td class="text-center"><% $result -> points %></td>
		            <td class="text-center"><% $result -> rebounds %></td>
		            <td class="text-center"><% $result -> assists %></td>
		            <td class="text-center"><% $result -> steals %></td>
		            <td class="text-center"><% $result -> blocks %></td>
		            <td class="text-center"><% $result -> turnovers %></td>
		            <td class="text-center"><a href=""><% $result -> fantasy_points %></a></td>
				</tr>
				<?php } ?>
			</tbody>
	    </table>

	</div>
	<div class="col-lg-6">
		<a data-target=".modal-team-y-performance" data-toggle="modal" href="" class="mb10 pull-right" style="display:block"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Team-Y Performance</a>
		<table class="table table-bordered">
			<thead>
				<tr>
		            <th class="text-center">Players</th>
		            <th class="text-center">PTS</th>
		            <th class="text-center">REB</th>
		            <th class="text-center">AST</th>
		            <th class="text-center">STL</th>
		            <th class="text-center">BLK</th>
		            <th class="text-center">TO</th>
		            <th class="text-center"><span class="nomargin glyphicon glyphicon-star cyellow" aria-hidden="true"></span></th>
				</tr>
	        </thead>
	        <tbody>
	        	<?php foreach($results -> where('team_id', $details -> team_y -> id) -> all() as $result){ ?>
				<tr>
		            <td><?php if($result -> ready == 1){ ?><img width="24" src="<% $result -> player -> avatar %>" class="img-circle mr5"><?php } ?> <% $result -> player -> lastname %></td>
		            <td class="text-center"><% $result -> points %></td>
		            <td class="text-center"><% $result -> rebounds %></td>
		            <td class="text-center"><% $result -> assists %></td>
		            <td class="text-center"><% $result -> steals %></td>
		            <td class="text-center"><% $result -> blocks %></td>
		            <td class="text-center"><% $result -> turnovers %></td>
		            <td class="text-center"><a href=""><% $result -> fantasy_points %></a></td>
				</tr>
				<?php } ?>
			</tbody>
	    </table>

	</div>
</div>
@include('admin.modals.add_team_x_performance')
@include('admin.modals.add_team_y_performance')
@endsection