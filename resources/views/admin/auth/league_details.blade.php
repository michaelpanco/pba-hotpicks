@extends('admin.layouts.auth')

@section('content')
<div class="row update-league-profile-admin">

	<div class="col-lg-10">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-12">
		        <h1  style="font-size:20px;"><% $league -> name %></h1>
		    </div>
		</div>
	</div>

    <div class="col-lg-2" style="padding-top:25px">
    	<a target="_blank" href="<% route('league_profile', $league -> id) %>" class="btn btn-info">View Public League</a>
    </div>

    <div class="mb10" >
    	<div class="col-lg-7 mb20" ng-init="leagueID=<% $league -> id %>">

    		<div class="row">
    			<div class="col-lg-12">
		    		<p class="cgray f12 mb5">Name</p>
		    		<input type="text" class="form-control mb10 field" placeholder="League Name" ng-model="leagueName" ng-init="leagueName='<% $league -> name %>'" />
    			</div>
    		</div>

    		<p class="cgray f12 mb5">Description</p>
    		<textarea class="form-control mb10 field" ng-model="leagueDesc" ng-init="leagueDesc='<% $league -> desc %>'" spellcheck="false"></textarea>

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Host</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Host" ng-model="leagueHost" ng-init="leagueHost='<% $league -> host %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Sequence</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Sequence" ng-model="leagueSequence" ng-init="leagueSequence='<% $league -> sequence %>'" />
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Status</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Status" ng-model="leagueStatus" ng-init="leagueStatus='<% $league -> ready %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Date Created</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Date Created" ng-model="leagueDateCreated" ng-init="leagueDateCreated='<% $league -> created_at %>'" />
    			</div>
    		</div>


    		<p class="cgray f12 mb10">Creator</p>
    		<div class="">
                <a href="<% route('admin_manager_details', $league -> creator -> id) %>">
                    <img class="img-circle mr5" width="32" src="<% $league -> creator -> avatar %>"> <% $league -> creator -> fullname %>
                </a>
            </div>



        	<a href="" style="width:170px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateLeagueProfile()">Update League</a>


    	</div>

    	<div class="col-lg-5">
    		<p class="cgray f12 mb20">Participants</p>

	    	<?php foreach($league -> participants as $participant){ ?>
	    		<div class="mb10">
                    <a href="<% route('admin_manager_details', $participant -> manager -> id) %>">
                        <img class="img-circle mr5" width="32" src="<% $participant -> manager -> avatar %>"> <% $participant -> manager -> fullname %> 
                    </a>
                </div>
	    	<?php } ?>


    	</div>

    </div>



</div>
@endsection