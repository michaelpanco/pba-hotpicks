@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">

    	<div class="row admin-title-header">
    		<div class="col-md-12"><h1 style="font-size:20px;"><i class="fa fa-ioxhost fa-fw"></i> PBA Teams</h1></div>
    	</div>
        
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Team</th>
					<th>Coach</th>
					<th>Win</th>
					<th>Loss</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($teams -> sortByDesc('win') as $team){ ?>
				<tr>
					<td><a href="<% route('admin_team_profile', $team -> id) %>"><% $team -> name %></a></td>
					<td><% $team -> coach %></td>
					<td><% round($team -> win) %></td>
					<td><% $team -> loss %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

    </div>
</div>
@endsection