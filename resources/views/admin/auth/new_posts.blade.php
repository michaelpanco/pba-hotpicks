@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
    	<div class="row admin-title-header" style="border:none">
    		<div class="col-md-6"><h1 style="font-size:20px;"><i class="fa fa-comment fa-fw"></i> New Posts</h1></div>
    		<div class="col-md-6 text-right"><a style="margin-top:10px" href="<% route('admin_new_posts_lists') %>" class="btn btn-primary"><i class="fa fa-refresh fa-fw"></i> Refresh</a></div>
    	</div>

        <?php if($posts -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Manager</th>
					<th>Post</th>
					<th>Created at</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($posts as $post){ ?>
				<tr>
					<td style="width:220px">
						<a href="<% route('admin_manager_details', $post -> manager -> id) %>">
						<?php if($post -> manager -> avatar){ ?>
							<img width="32" class="img-circle mr5" src="<?= url($post -> manager -> avatar) ?>"> 
						<?php }else{ ?>
							<img width="32" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
						<?php } ?>
						<% $post -> manager -> lastname %>, <% $post -> manager -> firstname %></a>
					</td>
					<td><% $post -> content %></td>
					<td style="width:130px" class="cgray"><% $post -> created_at -> diffForHumans() %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@endsection