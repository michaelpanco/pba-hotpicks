@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
    	<div class="row admin-title-header">
    		<div class="col-md-8"><h1 style="font-size:20px;"><i class="fa fa-shopping-cart fa-fw"></i> Products</h1></div>
    		<div class="col-md-4 text-right" style="padding-top:10px">
    			<a href="" class="btn btn-primary mb10" data-toggle="modal" data-target=".modal-create-product"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Product</a>
    		</div>
    	</div>
        

        <?php if($products -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Product ID</th>
					<th>Price</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($products as $product){ ?>
				<tr>
					<td><a href="<% route('admin_product_details', $product -> id) %>" ><img width="32" class="mr5" src="<% $product -> img %>"> <% $product -> name %></a></td>
					<td><% $product -> type %></td>
					<td><% $product -> product_id %></td>
					<td><% number_format($product -> price) %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@include('admin.modals.create_product')

@endsection