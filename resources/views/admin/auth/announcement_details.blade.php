@extends('admin.layouts.auth')

@section('content')
<div class="row update-announcement-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-6">
		        <h1  style="font-size:20px;">Announcement #<% $announcement -> id %></h1>
		    </div>
		    <div class="col-lg-6 text-right">
		        <a href="" ng-click="deleteAnnouncement(<% $announcement -> id %>)" class="btn btn-danger btn-lg field"><span class="glyphicon glyphicon-trash mr5" aria-hidden="true"></span> Delete</a>
		    </div>
		</div>
	</div>

    <div class="mb10" >
    	<div class="col-lg-12" ng-init="announcementID=<% $announcement -> id %>">
    		<p class="cgray f12 mb5">Text</p>
    		<input type="text" class="form-control mb10 field" placeholder="Name" ng-model="announcementText" ng-init='announcementText="<% $announcement -> text %>"' />

    		<p class="cgray f12 mb5">Order</p>
    		<input type="text" class="form-control mb10 field" placeholder="Slug" ng-model="announcementOrder" ng-init="announcementOrder='<% $announcement -> order %>'" />

    	</div>
    </div>
    <div class="col-lg-12">
        <a href="" style="width:230px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateAnnouncement()">Update Announcement</a>
    </div>
</div>
@endsection