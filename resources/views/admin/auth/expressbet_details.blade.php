@extends('admin.layouts.auth')

@section('content')
<div class="row update-express-bet-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-12">
		        <h1  style="font-size:20px;"><% $expressbet -> description %></h1>
		    </div>
		</div>
	</div>

    <div class="mb10" >
    	<div class="col-lg-12" ng-init="betID=<% $expressbet -> id %>">
    		<p class="cgray f12 mb5">Description</p>
    		<input type="text" class="form-control mb10 field" placeholder="Name" ng-model="betDescription" ng-init="betDescription='<% $expressbet -> description %>'" />

    		<p class="cgray f12 mb5">Bet Array Option</p>
    		<input type="text" class="form-control mb10 field" placeholder="Slug" ng-model="betArrayOption" ng-init="betArrayOption='<% $expressbet -> option %>'" />

    		<div class="row">
    			<div class="col-lg-3">
		    		<p class="cgray f12 mb5">Winner</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Winner" ng-model="betWinner" ng-init="betWinner='<% $expressbet -> won %>'" />
    			</div>
    			<div class="col-lg-3">
		    		<p class="cgray f12 mb5">Game Date</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Game Date" ng-model="betGameDate" ng-init="betGameDate='<% $expressbet -> gamedate %>'" />
    			</div>
                <div class="col-lg-3">
                    <p class="cgray f12 mb5">Bet Limit</p>
                    <input type="text" class="form-control mb10 field" placeholder="Bet Limit" ng-model="expressBetLimit" ng-init="expressBetLimit='<% $expressbet -> limit %>'" />
                </div>
    			<div class="col-lg-3">
		    		<p class="cgray f12 mb5">Expiry</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Expiry" ng-model="betGameExpiry" ng-init="betGameExpiry='<% $expressbet -> expiry %>'" />
    			</div>
                
    		</div>

    	</div>


    </div>
    <div class="col-lg-12">
        <a href="" style="width:200px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateExpressBet()">Update Express Bet</a>
    </div>

    <div class="col-lg-12" style="margin-bottom:10px">
        <p  style="font-size:20px;">Bet Entries</p>
   </div>

    <?php $bet_options = unserialize($expressbet -> option ); ?>
    <?php foreach($bet_options as $key => $val){ ?>
    <div class="col-lg-6">
        <div class="mb10"><span class="cred"><% $val %></span> - <% $expressbet -> bets -> where('bet', $key) -> count() %></div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Manager</th>
                    <th class="text-center">Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($expressbet -> bets -> where('bet', $key) as $ebet){ ?>
                <tr>
                <td><a target="_blank" href="<% route('profile', $ebet -> manager -> slug) %>"><%  $ebet -> manager -> fullname %></a></td>
                <td class="text-center"><% number_format($ebet -> amount) %></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php } ?>
</div>
@endsection