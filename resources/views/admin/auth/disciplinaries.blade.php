@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
    	<div class="row admin-title-header" style="margin-bottom:20px">
    		<div class="col-md-12"><h1 style="font-size:20px;"><i class="fa fa-legal fa-fw"></i> Disciplinaries</h1></div>
    	</div>

        <?php if($disciplinaries -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Officer</th>
					<th>Manager</th>
					<th>Type</th>
					<th>Description</th>
					<th>Date and Time</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($disciplinaries as $disciplinary){ ?>
				<tr>
					<td style="width:200px">
						<a href="<% route('admin_manager_details', $disciplinary -> officer -> id) %>">
						<?php if($disciplinary -> officer -> avatar){ ?>
							<img width="32" class="img-circle mr5" src="<% $disciplinary -> officer -> avatar %>"> 
						<?php }else{ ?>
							<img width="32" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
						<?php } ?>
						<% $disciplinary -> officer -> lastname %>, <% $disciplinary -> officer -> firstname %></a>
					</td>

					<td style="width:200px">
						<a href="<% route('admin_manager_details', $disciplinary -> manager -> id) %>">
						<?php if($disciplinary -> manager -> avatar){ ?>
							<img width="32" class="img-circle mr5" src="<% $disciplinary -> manager -> avatar %>"> 
						<?php }else{ ?>
							<img width="32" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
						<?php } ?>
						<% $disciplinary -> manager -> lastname %>, <% $disciplinary -> manager -> firstname %></a>
					</td>

					<td><% $disciplinary -> status %></td>
					<td><% $disciplinary -> description %></td>
					<td style="width:170px"><% date('M j, Y g:i a', strtotime($disciplinary -> created_at)) %></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="text-right"><?= $disciplinaries -> render() ?></div>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@endsection