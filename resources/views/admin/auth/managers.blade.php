@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
    	<div class="row admin-title-header">
    		<div class="col-md-12"><h1 style="font-size:20px;"><i class="fa fa-users fa-fw"></i> Managers</h1></div>
    	</div>

        <form class="mb20" method="GET" action="/<% env('ADMIN_SLUG') %>/managers" style="margin-top:20px">

			<input name="name" type="text" style="width:200px;display:inline;float:left" value="<% Input::input('name') %>" class="form-control mr5" value="" placeholder="Search Manager Name">

			<input type="submit" class="btn btn-primary" value="Search">
			<div class="clearfix"></div>
        </form>

        <?php if($managers -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Credits</th>
					<th>Date Registered</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($managers as $manager){ ?>
				<tr>
					<td>
						<a href="<% route('admin_manager_details', $manager -> id) %>">
						<?php if($manager -> avatar){ ?>
							<img width="32" class="img-circle mr5" src="<?= url($manager -> avatar) ?>"> 
						<?php }else{ ?>
							<img width="32" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
						<?php } ?>
						<% $manager -> lastname %>, <% $manager -> firstname %></a>
					</td>

					<td><% $manager -> email %></td>
					<td class="cgreen"><% number_format($manager -> credits) %></td>
					<td><% date('F j, Y', strtotime($manager -> date_registered)) %></td>
					<td><?= Render::managerStatus($manager -> status) ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="text-right"><?= $managers -> render() ?></div>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@endsection