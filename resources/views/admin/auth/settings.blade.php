@extends('admin.layouts.auth')

@section('content')
<div class="row update-setting-admin">

	<div class="col-lg-12" style="margin-bottom:20px">
    	<div class="row admin-title-header">
    		<div class="col-md-8"><h1 style="font-size:20px;"><i class="fa fa-cog fa-fw"></i> Settings</h1></div>
    	</div>
	</div>

    <div class="mb10" >
    	
		<?php foreach($settings as $key => $val){ ?>
		<div class="col-lg-6">
			<p class="cgray f12 mb5"><% str_replace('_', ' ', $key) %></p>
			<input type="text" ng-model="setting.<% strtolower(substr($key,0,4)) %>" class="form-control mb10 field" placeholder="Name" ng-init="setting.<% strtolower(substr($key,0,4)) %>='<% $val %>'" />
		</div>
		<?php } ?>

    </div>
    <div class="col-lg-12">
        <a href="" style="width:230px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateSettings(setting)">Update Settings</a>
    </div>
</div>
@endsection