@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">

    	<div class="row admin-title-header">
    		<div class="col-md-8"><h1 style="font-size:20px;"><i class="fa fa-credit-card fa-fw"></i> Express Bets</h1></div>
    		<div class="col-md-4 text-right" style="padding-top:10px">
    			<a href="" class="btn btn-primary mb10" data-toggle="modal" data-target=".modal-create-express-bet"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Express Bet</a>
    		</div>
    	</div>
        
        <?php if($expressbets -> count() > 0){ ?>
		<div class="bets-wrapper">

			<?php foreach($expressbets as $bet){ ?>

			<?php if($bet -> won){ ?>
			<div class="item" style="background:#F5F5F5">
			<?php }else{ ?>
			<div class="item">
			<?php } ?>


				<div class="row mb5">
					<div class="col-md-2 cgray">Description</div>
					<div class="col-md-10"><% $bet -> description %></div>
				</div>
				<div class="row mb5">
					<div class="col-md-2 cgray">Options</div>
					<div class="col-md-10">
						<?php foreach(unserialize($bet -> option) as $option){ ?>
							<span class="label label-default"><% $option %></span>
						<?php } ?>
					</div>
				</div>
				<div class="row mb5">
					<div class="col-md-2 cgray">Game Date</div>
					<div class="col-md-10"><% date('F j, Y - l g:i A', strtotime($bet -> gamedate)) %></div>
				</div>
				<div class="row mb5">
					<div class="col-md-2 cgray">Expiry Date</div>
					<div class="col-md-10">
						<?php if(strtotime($bet -> expiry) > strtotime(date('Y-m-d H:i:s'))){ ?>
						<span class="cgreen">
						<?php }else{ ?>
						<span class="cred">
						<?php } ?>
						<% date('F j, Y - l g:i A', strtotime($bet -> expiry)) %>
						</span>
					</div>
				</div>

				<div class="row mb5">
					<div class="col-md-2 cgray">Bet Limit</div>
					<div class="col-md-10"><% number_format($bet -> limit) %></div>
				</div>

				<div class="row mb5">
					<div class="col-md-2 cgray">Total Bets</div>
					<div class="col-md-10"><% $bet -> entries -> count() %></div>
				</div>

				<?php if($bet -> won){ ?>
				<div class="row mb5">
					<div class="col-md-2 cgray">Winner</div>
					<div class="col-md-10"><span class="label label-primary"><% unserialize($bet -> option)[$bet -> won] %></span></div>
				</div>
				<?php } ?>

				<div class="row mb5">
					<div class="col-md-2">&nbsp;</div>
					<div class="col-md-10"><a href="<% route('admin_expressbet_details', $bet -> id) %>">View / Edit Details</a></div>
				</div>

			</div>
			<?php } ?>

		</div>

		<div class="text-right"><?= $expressbets -> render() ?></div>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@include('admin.modals.create_express_bet')

@endsection