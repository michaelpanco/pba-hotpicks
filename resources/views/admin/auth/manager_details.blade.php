@extends('admin.layouts.auth')

@section('content')
<div class="row update-manager-profile-admin">

	<div class="col-lg-12">
		<div class="row" style="padding-top:20px;margin-bottom:20px">
		    <div class="col-lg-7">
		        <h1  style="font-size:20px;">

					<?php if($manager -> avatar){ ?>
						<img width="72" class="img-circle mr5" src="<?= url($manager -> avatar) ?>">
					<?php }else{ ?>
						<img width="72" class="img-circle mr5" src="/assets/img/players/avatars/no_player_avatar.jpg">
					<?php } ?>

		        	<% $manager -> fullname %>

			    	<?php foreach($manager -> badges as $badge){ ?>
						<img class="mb5" src="<% $badge -> src %>" alt="<% $badge -> name %>" title="<% $badge -> name %>" />
			    	<?php } ?>
		        </h1>

		        
		    </div>

		    <div class="col-lg-5 text-right" style="padding-top:20px">
		    	<a target="_blank" href="<% route('profile', $manager -> slug) %>" class="btn btn-info">View Public Profile</a>
		    	<a target="_blank" href="<% route('proxy_login', $manager -> id) %>" class="btn btn-info">Login as <% $manager -> fullname %></a>
		    </div>

		</div>
	</div>

	<div class="col-lg-12 mb20">
		<p><span class="cgray">IP Address :</span> <% $manager -> ip_address %></p>
		<p><span class="cgray">Browser :</span> <% $manager -> user_agent %></p>
		<p><span class="cgray">User Type :</span> <% $manager -> reg_type %></p>
		<p><span class="cgray">Email :</span> <% $manager -> email %></p>
		<p><span class="cgray">Last Online :</span> <% date('F j, Y g:i a', strtotime($manager -> last_online)) %></p>
		<p><span class="cgray">Email :</span> <% $manager -> email %></p>


		<?php if($team && $team -> league_id){ ?>
		<div class="value mb5"><span class="cgray">League :</span> <a href="<% route('league_profile', $team -> league -> id) %>"><% $team -> league -> name %></a></div>
		<?php } ?>


	</div>

    <div class="mb10" >
    	<div class="col-lg-7 mb20" ng-init="managerID=<% $manager -> id %>">

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Firstname</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Firstname" ng-model="managerFirstname" ng-init="managerFirstname='<% $manager -> firstname %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Lastname</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Lastname" ng-model="managerLastname" ng-init="managerLastname='<% $manager -> lastname %>'" />
    			</div>
    		</div>

    		<p class="cgray f12 mb5">Manager Slug</p>
    		<input type="text" class="form-control mb10 field" placeholder="Slug" ng-model="managerSlug" ng-init="managerSlug='<% $manager -> slug %>'" />

    		<div class="row">
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Gender</p>
		    		<input type="text" class="form-control mb10 field" maxlength="1" placeholder="Gender" ng-model="managerGender" ng-init="managerGender='<% $manager -> gender %>'" />
    			</div>
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Status</p>
		    		<input type="text" class="form-control mb10 field" maxlength="4" placeholder="Status" ng-model="managerStatus" ng-init="managerStatus='<% $manager -> status %>'" />
    			</div>
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Credits</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Credits" ng-model="managerCredits" ng-init="managerCredits='<% $manager -> credits %>'" />
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Birthday</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Birthday" ng-model="managerBirthday" ng-init="managerBirthday='<% $manager -> birthday %>'" />
    			</div>
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Privilege</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Privilege" ng-model="managerPrivilege" ng-init="managerPrivilege='<% $manager -> privilege %>'" />
    			</div>
    			<div class="col-lg-4">
		    		<p class="cgray f12 mb5">Restrain until</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Restrain until" ng-model="managerRestrainUntil" ng-init="managerRestrainUntil='<% $manager -> restrain_until %>'" />
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Fav Team</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Fav Team" ng-model="managerFavTeam" ng-init="managerFavTeam='<% $manager -> fav_team %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Fav Player</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Fav Player" ng-model="managerFavPlayer" ng-init="managerFavPlayer='<% $manager -> fav_player %>'" />
    			</div>
    		</div>

    		<div class="row mb20">
    			<div class="col-lg-6">
					<p class="cgray f12 mb5">Fav 2nd Team</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Fav 2nd Team" ng-model="managerFav2ndTeam" ng-init="managerFav2ndTeam='<% $manager -> fav_2nd_team %>'" />
    			</div>
    			<div class="col-lg-6">
		    		<p class="cgray f12 mb5">Fav Matchup</p>
		    		<input type="text" class="form-control mb10 field" placeholder="Fav Matchup" ng-model="managerFavMatchup" ng-init="managerFavMatchup='<% $manager -> fav_match_up %>'" />
    			</div>
    		</div>

        	<a href="" style="width:170px" class="field trigger btn btn-lg btn-primary mb20 pull-right" ng-click="updateManagerProfile()">Update Manager</a>


    	</div>

    	<div class="col-lg-5">
    		<?php if($manager -> franchise){ ?>
		    	<h3 class="mb20"><img src="/assets/img/franchise_logos/32/<% $manager -> franchise -> icon -> filename %>" /> <% $manager -> franchise -> name %> <span class="cgray" style="margin-left:10px"><% number_format($manager -> franchise -> players -> sum('salary')) %></span></h3>

		    	<?php foreach($manager -> franchise -> players as $player){ ?>
		    		<div class="mb10"><img class="img-circle mr5" width="32" src="<% $player -> avatar %>"> <% $player -> fullname %>  <span class="cgray"><% Helper::getExactPosition($player -> pivot -> position) %></span><span class="cgreen" style="margin-left:10px"><% number_format($player -> salary) %></span></div>
		    	<?php } ?>

	    	<?php }else{ ?>
	    		<p class="text-center cred">No Franchise yet</p>
	    	<?php } ?>
    	</div>

    </div>



</div>
@endsection