@extends('admin.layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-12">
        
    	<div class="row admin-title-header">
    		<div class="col-md-12"><h1 style="font-size:20px;"><i class="fa fa-th-list fa-fw"></i> Leagues</h1></div>
    	</div>

        <form class="mb20" method="GET" action="/<% env('ADMIN_SLUG') %>/leagues" style="margin-top:20px">

			<input name="search" type="text" style="width:200px;display:inline;float:left" value="<% Input::input('search') %>" class="form-control mr5" value="" placeholder="Search League Name">

			<input type="submit" class="btn btn-primary" value="Search">
			<div class="clearfix"></div>
        </form>

        <?php if($leagues -> count() > 0){ ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Host</th>
					<th>Date Created</th>
					<th>Participants</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($leagues as $league){ ?>
				<tr>
					<td><a href="<% route('admin_league_details', $league -> id) %>"><% $league -> name %></a></td>
					<td><% $league -> creator -> fullname %></td>
					<td><% date('F j, Y', strtotime($league -> created_at)) %></td>
					<td><% $league -> participants -> count() %></td>
					<td><?= ($league -> ready == 1) ? '<span class="label label-primary">Started</span>' : '<span class="label label-warning">Waiting</span>' ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="text-right"><?= $leagues -> render() ?></div>
		<?php }else{ ?>
		<div class="cred text-center f18" style="margin-top:50px">No result found</div>
		<?php } ?>
    </div>
</div>

@endsection