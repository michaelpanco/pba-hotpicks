<!DOCTYPE html>
<html lang="en" ng-app="pbahotpicksadmin">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>PTP Administrator</title>
		<link rel="shortcut icon" href="<% asset('admin-assets/img/favicon.png') %>" type="image/x-icon">
		<link rel="stylesheet" href="<% asset('admin-assets/components/bootstrap/dist/css/bootstrap.min.css') %>" type="text/css">
        <link rel="stylesheet" href="<% asset('admin-assets/components/metisMenu/dist/metisMenu.min.css') %>" type="text/css">
        <link rel="stylesheet" href="<% asset('admin-assets/dist/css/sb-admin-2.css') %>" type="text/css">
        <link rel="stylesheet" href="<% asset('admin-assets/dist/css/sweet-alert.css') %>" type="text/css">
        <link rel="stylesheet" href="<% asset('admin-assets/components/morrisjs/morris.css') %>" type="text/css">
		<link rel="stylesheet" href="<% asset('assets/css/main.css') %>" type="text/css">
		<link rel="stylesheet" href="<% asset('admin-assets/dist/css/auth.css') %>" type="text/css">
        <link rel="stylesheet" href="<% asset('admin-assets/components/font-awesome/css/font-awesome.min.css') %>" type="text/css">
</head>
<body id="AdminAuth" ng-controller="PTPAdmistrator">
	<nav class="navbar main-navbar navbar-fixed-top">
		<div class="container">
            <div class="row">
                <div class="col-lg-6"><h1><i class="fa fa-lock fa-fw"></i> PTP Administrator</h1></div>
                <div class="col-lg-6 text-right">
                    <h1><i class="fa fa-user fa-fw"></i> <span style="margin-right:20px;"><% $account -> fullname %></span> 
                        <a href="<% route('home') %>" target="_blank" style="margin-right:20px;" class="adminlink2"><i class="fa fa-share-square-o fa-fw"></i>View Website</a>
                        <a href="<% route('admin_logout') %>" class="adminlink2"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                    </h1>
                </div>
            </div>
            
        </div>
	</nav>

    <div id="wrapper">

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse" style="">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="<% route('admin_dashboard') %>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-dribbble fa-fw"></i> PBA<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<% route('admin_teams') %>"><i class="fa fa-ioxhost fa-fw"></i> Teams</a></li>
                            <li><a href="<% route('admin_players') %>"><i class="fa fa-male fa-fw"></i> Players</a></li>
                            <li><a href="<% route('admin_games') %>"><i class="fa fa-cube fa-fw"></i> Games</a></li>
                        </ul>
                    </li>
                    <li><a href="<% route('admin_managers') %>"><i class="fa fa-users fa-fw"></i> Managers</a></li>
                    <li><a href="<% route('admin_leagues') %>"><i class="fa fa-th-list fa-fw"></i> Leagues</a></li>
                    <li><a href="<% route('admin_expressbets') %>"><i class="fa fa-credit-card fa-fw"></i> Express Bets</a></li>
                    <li><a href="<% route('admin_products') %>"><i class="fa fa-shopping-cart fa-fw"></i> Products</a></li>
                    <li><a href="<% route('admin_announcements') %>"><i class="fa fa-bullhorn fa-fw"></i> Announcements</a></li>
                    <li><a href="<% route('admin_disciplinaries') %>"><i class="fa fa-legal fa-fw"></i> Disciplinaries</a></li>
                    <li><a href="<% route('admin_settings') %>"><i class="fa fa-cog fa-fw"></i> Settings</a></li>
                </ul>
            </div>
        </div>

        <div id="page-wrapper">@yield('content')</div>

    </div>

<script>
var angular_dependency = ['oitozero.ngSweetAlert'];
var admin_slug = "<% env('ADMIN_SLUG') %>";
</script>
<!-- Scripts -->
<script src="<% asset('admin-assets/components/jquery/dist/jquery.min.js') %>"></script>
<script src="<% asset('admin-assets/components/bootstrap/dist/js/bootstrap.min.js') %>"></script>
<script src="<% asset('admin-assets/components/metisMenu/dist/metisMenu.min.js') %>"></script>
<script src="<% asset('admin-assets/components/angularjs/minified.js') %>"></script>
<script src="<% asset('admin-assets/dist/js/angular/primary.js') %>"></script>
<script src="<% asset('admin-assets/dist/js/angular/app.js?nocache=1') %>"></script>
<script src="<% asset('admin-assets/dist/js/sb-admin-2.js') %>"></script>
<script src="<% asset('assets/js/vendor/sweet-alert.js') %>"></script>
@section('jscustom')@show

</body>
</html>