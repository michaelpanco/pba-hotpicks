<!DOCTYPE html>
<html lang="en" ng-app="pbahotpicksadmin">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>PTP Administrator</title>
		<link rel="shortcut icon" href="<% asset('admin-assets/img/favicon.png') %>" type="image/x-icon">
		<link rel="stylesheet" href="<% asset('admin-assets/components/bootstrap/dist/css/bootstrap.min.css') %>" type="text/css">
		<link rel="stylesheet" href="<% asset('admin-assets/dist/css/sweet-alert.css') %>" type="text/css">
		<link rel="stylesheet" href="<% asset('assets/css/main.css') %>" type="text/css">
		<link rel="stylesheet" href="<% asset('admin-assets/dist/css/login.css') %>" type="text/css">
	</head>
<body ng-controller="PTPAdmistrator">
	<nav class="navbar main-navbar navbar-fixed-top">
		<div class="container">
			<h1>Welcome to PTP Administrator</h1>
		</div>
	</nav>
	<div class="container login-admin" style="margin-top:100px;">
		<form class="form-signin" role="form" ng-submit="loginAdmin(login)" >
			<div class="text-center" style="margin-bottom:20px;">
				<img src="<% asset('assets/img/pinoytenpicks.png') %>" alt="PinoyTenPicks" />
			</div>
			<div class="alert alert-danger" ng-show="loginAlert" role="alert">{{errMessage}}</div>
			<input type="text" name="email" class="form-control" ng-model="login.email" placeholder="Email">
			<input type="password" name="password" class="form-control" ng-model="login.password" placeholder="Password">
			<input type="submit" value="Login" style="display:none;">
			<a href="" class="btn btn-lg btn-primary btn-block field trigger" ng-click="loginAdmin(login)">Login</a>
		</form>
	</div>

<script>
var angular_dependency = ['oitozero.ngSweetAlert'];
var admin_slug = "<% env('ADMIN_SLUG') %>";
</script>

<!-- Scripts -->
<script src="<% asset('admin-assets/components/jquery/dist/jquery.min.js') %>"></script>
<script src="<% asset('admin-assets/components/angularjs/minified.js') %>"></script>
<script src="<% asset('admin-assets/dist/js/angular/primary.js') %>"></script>
<script src="<% asset('admin-assets/dist/js/angular/app.js') %>"></script>

<script src="<% asset('admin-assets/js/sweetalert/sweet-alert.js') %>"></script>

</body>
</html>