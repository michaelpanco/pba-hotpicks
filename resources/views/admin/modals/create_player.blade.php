<div class="modal modal-create-player">
    <div class="modal-dialog" style="top: 70px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Player</h4>
            </div>
            <div class="modal-body">
                <div class="create-league-body create-league">
                    <form ng-submit="createGame()" class="create-player-admin">

                        <div class="alert alert-danger" ng-show="leagueAlert" role="alert">{{errMessage}}</div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<input type="text" ng-model="playerFirstname" class="form-control field" placeholder="Firstname">
                        	</div>
                        	<div class="col-md-6">
                        		<input type="text" ng-model="playerLastname" class="form-control field" placeholder="Lastname">
                        	</div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<input type="text" ng-model="playerSlug" class="form-control field" placeholder="Slug">
                        	</div>
                        	<div class="col-md-6">
								<!-- Split button -->
								<div class="btn-group mr5" style="display:inline;float:left">
									<button type="button" class="btn btn-default" ng-bind="selectedTeam || 'Select Team'" style="width:239px">Select Team</button>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu">
										<?php foreach($teams as $team){ ?>
										<li><a href="" ng-click="selectedTeam='<% $team -> name %>';selectedTeamID=<% $team -> id %>"><% $team -> name %></a></li>
										<?php } ?>
									</ul>
								</div>
                        	</div>
                        </div>

                        <div class="row">
                        	<div class="col-md-3"><input type="text" ng-model="playerPosition" class="form-control field mb10" placeholder="Position"></div>
                        	<div class="col-md-3"><input type="text" ng-model="playerJersey" class="form-control field mb10" placeholder="Jersey"></div>
                        	<div class="col-md-3"><input type="text" ng-model="playerBirthday" class="form-control field mb10" placeholder="Birthday"></div>
                        	<div class="col-md-3"><input type="text" ng-model="playerHeight" class="form-control field mb10" placeholder="Height"></div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<input type="text" ng-model="playerSchool" class="form-control field" placeholder="School">
                        	</div>
                        	<div class="col-md-6">
                        		<input type="text" ng-model="playerAvatar" class="form-control field" placeholder="Avatar">
                        	</div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		&nbsp;
                        	</div>
                        	<div class="col-md-6">
                        		<input type="text" ng-model="playerSalary" class="form-control field" placeholder="Salary">
                        	</div>
                        </div>

                        <input type="submit" style="display:none;" value="Create Player">
                        <a href="" ng-click="createPlayer()" class="field trigger btn btn-primary btn-lg mb10" style="width:100%">Create Player</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>