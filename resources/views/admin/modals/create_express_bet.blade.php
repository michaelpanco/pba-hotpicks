<div class="modal modal-create-express-bet">
    <div class="modal-dialog" style="top: 60px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Express Bet</h4>
            </div>
            <div class="modal-body">
                <div class="create-express-bet-body create-express-bet">
                    <form ng-submit="createExpressBet()" class="create-express-bet-admin">

                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<textarea class="form-control field" ng-model="expressBetDescription" spellcheck="false" ng-init="expressBetDescription='Who do you think will win between xxxxxx and yyyyyy?'"></textarea>
                        	</div>
                        </div>

						<p class="cgray mb5 f12">Express bet options</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="expressBetOption1" class="form-control field" placeholder="Option 1">
                        	</div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="expressBetOption2" class="form-control field" placeholder="Option 2">
                        	</div>
                        </div>

                        <p class="cgray mb5 f12">Bet Limit</p>
                        <div class="row mb10">
                            <div class="col-md-12">
                                <input type="text" ng-init="expressBetLimit=999" ng-model="expressBetLimit" class="form-control field" >
                            </div>
                        </div>

                        <p class="cgray mb5 f12">Game Date</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="expressBetGameDate" ng-init="expressBetGameDate='<% date('Y-m-d') %> 12:00:00'" class="form-control field" placeholder="Game Date">
                        	</div>
                        </div>

                        <p class="cgray mb5 f12">Expiry</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="expressBetGameExpiry" ng-init="expressBetGameExpiry='<% date('Y-m-d') %> 12:00:00'" class="form-control field" placeholder="Game Date">
                        	</div>
                        </div>

                        <input type="submit" style="display:none;" value="Create Player">
                        <a href="" ng-click="createExpressBet()" class="field trigger btn btn-primary btn-lg mb10" style="width:100%">Create Express Bet</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>