<div class="modal modal-create-game">
    <div class="modal-dialog" style="top: 70px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Game</h4>
            </div>
            <div class="modal-body">
                <div class="create-league-body create-league">
                    <form ng-submit="createGame()" class="create-game-admin">

                        <div class="alert alert-danger" ng-show="leagueAlert" role="alert">{{errMessage}}</div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<input type="hidden" ng-model="selectedXTeamID" />
								<!-- Split button -->
								<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedXTeamID=<% Request::input('team', 'false') %>;selectedTeamX='<% Request::input('selectedTeam', '') %>'">
									<button type="button" class="btn btn-default field" ng-bind="selectedTeamX || 'Select Team'" style="width:238px">Select Team</button>
									<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu">
										<?php foreach($teams -> sortByDesc('win') as $team){ ?>
										<li><a href="" ng-click="selectedTeamX='<% $team -> name %>';selectedXTeamID=<% $team -> id %>"><% $team -> name %></a></li>
										<?php } ?>
									</ul>
								</div>

                        	</div>
                        	<div class="col-md-6">
                        		<input type="hidden" ng-model="selectedYTeamID" />
								<!-- Split button -->
								<div class="btn-group mr5" style="display:inline;float:left" ng-init="selectedYTeamID=<% Request::input('team', 'false') %>;selectedTeamY='<% Request::input('selectedTeam', '') %>'">
									<button type="button" class="btn btn-default field" ng-bind="selectedTeamY || 'Select Team'" style="width:238px">Select Team</button>
									<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu">
										<?php foreach($teams -> sortByDesc('win') as $team){ ?>
										<li><a href="" ng-click="selectedTeamY='<% $team -> name %>';selectedYTeamID=<% $team -> id %>"><% $team -> name %></a></li>
										<?php } ?>
									</ul>
								</div>

                        	</div>
                        </div>


                        <div class="row">
                        	<div class="col-md-6">

								<!-- Split button -->
								<div class="btn-group mr5 mb10" ng-hide="otherVenue" style="display:inline;float:left">
									<button type="button" class="btn btn-default field" ng-bind="gameVenue || 'Select Game Venue'" style="width:238px">Select Game Venue</button>
									<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="" ng-click="gameVenue='Smart Araneta Coliseum'">Smart Araneta Coliseum</a></li>
										<li><a href="" ng-click="gameVenue='Mall of Asia Arena'">Mall of Asia Arena</a></li>
										<li><a href="" ng-click="gameVenue='Cuneta Astrodome'">Cuneta Astrodome</a></li>
										<li><a href="" ng-click="gameVenue='Ynares Center'">Ynares Center</a></li>
										<li><a href="" ng-click="gameVenue='PhilSports Arena'">PhilSports Arena</a></li>
										<li><a href="" ng-click="gameVenue='Philippine Arena'">Philippine Arena</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="" ng-click="gameVenue='';otherVenue=true">Others</a></li>
									</ul>
								</div>
		                        <input type="text" ng-show="otherVenue" ng-model="gameVenue" class="form-control field mb10" placeholder="Game Venue">

                        	</div>
                        	<div class="col-md-6">

								<!-- Split button -->
								<div class="btn-group mr5 mb10" ng-hide="otherGameConference" style="display:inline;float:left">
									<button type="button" class="btn btn-default field" ng-bind="gameConference || 'Select Game Conference'" style="width:238px">Select Game Confrence</button>
									<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="" ng-click="gameConference='PBA Philippine Cup'">PBA Philippine Cup</a></li>
										<li><a href="" ng-click="gameConference='Commissioners Cup'">Commissioners Cup</a></li>
										<li><a href="" ng-click="gameConference='Governors Cup'">Governors Cup</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="" ng-click="gameConference='';otherGameConference=true">Others</a></li>
									</ul>
								</div>

		                        <input type="text" ng-show="otherGameConference" ng-model="gameConference" class="form-control field mb10" placeholder="Conference">

                        	</div>
                        </div>

                        <div class="row">
                        	<div class="col-md-3"><input type="text" ng-model="gameNo" class="form-control field mb10" placeholder="Game No."></div>
                        	<div class="col-md-3"><input type="text" ng-model="gameCluster" class="form-control field mb10" placeholder="Cluster"></div>
                        	<div class="col-md-3"><input ng-init="gameSeason=<% Setting::get('SEASON') %>" type="text" ng-model="gameSeason" class="form-control field mb10" placeholder="Season"></div>
                        	<div class="col-md-3"><input ng-init="gameSequence=<% Setting::get('SEQUENCE') %>" type="text" ng-model="gameSequence" class="form-control field mb10" placeholder="Sequence"></div>
                        </div>

						<!-- Split button -->
						<div class="btn-group mr5 mb10" ng-hide="otherGameType" style="display:inline;float:left">
							<button type="button" class="btn btn-default field" ng-bind="gameType || 'Select Game Type'" style="width:238px">Select Game Type</button>
							<button type="button" class="btn btn-default dropdown-toggle field" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="" ng-click="gameType='Elimination'">Elimination</a></li>
								<li><a href="" ng-click="gameType='Quarterfinals'">Quarterfinals</a></li>
								<li><a href="" ng-click="gameType='Semi Finals'">Semi Finals</a></li>
								<li><a href="" ng-click="gameType='Finals'">Finals</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="" ng-click="gameType='';otherGameType=true">Others</a></li>
							</ul>
						</div>

                        <input type="text" ng-show="otherGameType" ng-model="gameType" maxlength="16" class="form-control field mb10" placeholder="Game Type">

                        <input type="text" ng-model="gameDateTime" maxlength="16" class="form-control field mb10" placeholder="Date and Time">

                        <input type="submit" style="display:none;" value="Create Game">
                        <a href="" ng-click="createGame()" class="field trigger btn btn-primary btn-lg mb10" style="width:100%">Create Game</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>