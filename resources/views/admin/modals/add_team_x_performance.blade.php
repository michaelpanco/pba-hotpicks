<div class="modal modal-team-x-performance">
    <div class="modal-dialog" style="top: 20px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><img width="64" src="<% $details -> team_x -> logo %>" style="margin-right:20px" /> Performance</h4>
            </div>
            <div class="modal-body">
                <div class="create-performance-body create-performance">
                    <form ng-submit="createPerformance(performanceX)" class="create-performance-admin">

                        <div class="row mb10">
                        	<div class="col-md-6" ng-init="performanceX.gameID=<% $details -> id %>;performanceX.opponent=<% $details -> team_y_id %>;performanceX.teamID=<% $details -> team_x_id %>">

								<!-- Split button -->
								<div class="btn-group mr5" style="display:inline;float:left">
									<button type="button" class="btn btn-default" ng-bind="performanceX.selectedPlayer || 'Select Player'" style="width:239px">Select Player</button>
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" style="height:400px;overflow-y:scroll">
										<?php foreach($team_x_players as $player){ ?>
										<li><a href="" ng-click="performanceX.selectedPlayer='<% $player -> lastname %>, <% $player -> firstname %>';performanceX.playerID=<% $player -> id %>"><% $player -> lastname %>, <% $player -> firstname %></a></li>
										<?php } ?>
									</ul>
								</div>


                        	</div>
                        	<div class="col-md-6">
                        		 <label>Win : <input type="checkbox" ng-init="performanceX.winGame=<% $team_x_outcome %>" ng-model="performanceX.winGame" ng-true-value="1" ng-false-value="0"></label>
                        	</div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<p class="cgray f12 mb5">Points</p>
                        		<input type="text" ng-model="performanceX.playerPoints" class="form-control field" placeholder="Points">
                        	</div>
                        	<div class="col-md-6">
                        		<p class="cgray f12 mb5">Rebounds</p>
                        		<input type="text" ng-model="performanceX.playerRebounds" class="form-control field" placeholder="Rebounds">
                        	</div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<p class="cgray f12 mb5">Assists</p>
                        		<input type="text" ng-model="performanceX.playerAssists" class="form-control field" placeholder="Assists">
                        	</div>
                        	<div class="col-md-6">
                        		<p class="cgray f12 mb5">Steals</p>
                        		<input type="text" ng-model="performanceX.playerSteals" class="form-control field" placeholder="Steals">
                        	</div>
                        </div>

                        <div class="row mb10">
                        	<div class="col-md-6">
                        		<p class="cgray f12 mb5">Blocks</p>
                        		<input type="text" ng-model="performanceX.playerBlocks" class="form-control field" placeholder="Blocks">
                        	</div>
                        	<div class="col-md-6">
                        		<p class="cgray f12 mb5">Turnovers</p>
                        		<input type="text" ng-model="performanceX.playerTurnovers" class="form-control field" placeholder="Turnovers">
                        	</div>
                        </div>

                        <input type="submit" style="display:none;" value="Create Player">
                        <a href="" ng-click="createPerformance(performanceX)" class="field trigger btn btn-primary btn-lg mb10" style="width:100%">Add Performance</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>