<div class="modal modal-create-announcement">
    <div class="modal-dialog" style="top: 60px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Announcement</h4>
            </div>
            <div class="modal-body">
                <div class="create-announcement-body create-announcement">
                    <form ng-submit="createAnnouncement()" class="create-announcement-admin">
                    	<p class="cgray mb5 f12">Announcement Text</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<textarea class="form-control field" ng-model="announcementText" spellcheck="false" ng-init="expressBetDescription='Who do you think will win between xxxxxx and yyyyyy?'"></textarea>
                        	</div>
                        </div>

						<p class="cgray mb5 f12">Announcement Order</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" maxlength="2" ng-model="announcementOrder" class="form-control field">
                        	</div>
                        </div>

                        <input type="submit" style="display:none;" value="Create Player">
                        <a href="" ng-click="createAnnouncement()" class="field trigger btn btn-primary btn-lg mb10" style="width:100%">Create Announcement</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>