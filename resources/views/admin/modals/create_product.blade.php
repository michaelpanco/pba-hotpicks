<div class="modal modal-create-product">
    <div class="modal-dialog" style="top: 60px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Product</h4>
            </div>
            <div class="modal-body">
                <div class="create-product-body create-product">
                    <form ng-submit="createExpressBet()" class="create-product-admin">

						<p class="cgray mb5 f12">Product name</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="productName" class="form-control field" placeholder="Product name">
                        	</div>
                        </div>

                        <div class="row mb10">

                        	<div class="col-md-12">
                        		<p class="cgray mb5 f12">Product type</p>
								<!-- Split button -->
								<div class="btn-group mr5" style="display:inline;float:left">
									<button type="button" class="btn btn-default field" ng-bind="selectedProductTypeLabel || 'Select Product Type'" style="width:239px">Select Product Type</button>
									<button type="button" class="btn btn-default field dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu">
										
										<li><a href="" ng-click="selectedProductTypeLabel='Badge';selectedProductType='BDGE'">Badge</a></li>
										<li><a href="" ng-click="selectedProductTypeLabel='Franchise Icon';selectedProductType='FICO'">Franchise Icon</a></li>
									</ul>
								</div>
                        	</div>
                        </div>


                        <p class="cgray mb5 f12">Product ID</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="productID" class="form-control field" placeholder="Product ID">
                        	</div>
                        </div>

                        <p class="cgray mb5 f12">Product Image</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="productImage" class="form-control field" placeholder="Product Image">
                        	</div>
                        </div>

                        <p class="cgray mb5 f12">Product Price</p>
                        <div class="row mb10">
                        	<div class="col-md-12">
                        		<input type="text" ng-model="productPrice" class="form-control field" placeholder="Product Price">
                        	</div>
                        </div>

                        <input type="submit" style="display:none;" value="Create Player">
                        <a href="" ng-click="createProduct()" class="field trigger btn btn-primary btn-lg mb10" style="width:100%">Create Product</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>