You received this email because someone requested a password for this email address. To reset your password, please click the URL below. If it's not clickable, please copy and paste the URL into your browser's address bar.
<br/><br/>
<a href="<% config('app.base_url') %>password-reset?email=<% $email %>&token=<% $token %>">
	<% config('app.base_url') %>password-reset?email=<% $email %>&token=<% $token %>
</a>
<br/><br/>
If you were not the one to make this recovery request, you can safely ignore this email and nothing will be done to your account.
<br/><br/>
Cheers,<br/>
PinoyTenPicks Team