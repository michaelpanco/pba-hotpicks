Hi <?= $fullname ?>,<br/><br/>

Thank you for registering at PinoyTenPicks, you are on your way.<br/><br/>

To complete your registration you may click on this link or copy and paste it in your browser<br/>
<a href="<% config('app.base_url') %>confirm/manager/<% $manager_id %>/confirmation/<% $confirmation_code %><% $team %>">
	<% config('app.base_url') %>confirm/manager/<% $manager_id %>/confirmation/<% $confirmation_code %><% $team %>
</a>
<br/><br/>

If you have any question or concerns, please contact us at contact@pinoytenpicks.com<br/><br/>

Cheers,<br/>
PinoyTenPicks Team