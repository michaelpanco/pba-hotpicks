circularPreloader = function() {
	return '<img height="20" src="/assets/img/submit_preloader.gif">';
};

loadMoreMessagePreloader = function() {
	return '<img src="/assets/img/small_black_circular.gif">';
};

channelPreloader = function(){
	return '<img class="channel-preloader" src="/assets/img/channel_preloader.gif">';
};

givePointPreloader = function(){
	return '<img src="/assets/img/point_preloader.gif">';
};

validateEmail = function(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
};

$('.navbar-notice .dropdown-menu').click(function(e) {
    e.stopPropagation();
});