pbahotpicks.controller('LeagueGuest', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.joinLeague = function()
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".join-container .field").attr("disabled", true);
				$(".join-container .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
				league_password : $scope.leaguePassword,
			},

			url: "/api/manager/joinLeague",

			validation: function()
			{
				if (franchise_id === 0) {
					throw "You need to create your own franchise first before joining any league";
				}
			},

			success_callback: function(response_data)
			{
				$(".join-container .field").removeAttr("disabled");
				$(".join-container .trigger").html("Join");
				window.location = "../leagues/" + league_id;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Join Failed', response_data.message, 'warning');
				$(".join-container .field").removeAttr("disabled");
				$(".join-container .trigger").html("Join");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.league = [];

	$scope.createLeague = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$scope.loginAlert = false;
				$(".create-league .field").attr("disabled", true);
				$(".create-league .trigger").html(circularPreloader());
			},

			data: {
				name : params.name,
				description : params.description,
				password : params.password
			},

			url: "/api/manager/createLeague",

			validation: function(){
				if ((params.name === undefined || params.name === "")) {
					throw 'Please enter your league name';
				}
			    if (params.description === undefined || params.description.length < 5) {
			    	throw 'League description is too short';
			    }
			},

			success_callback: function(response_data){
				window.location = window.location.origin + '/leagues/' + response_data.message;
			},

			failed_callback: function(response_data){
				$scope.leagueAlert = true;
				$scope.errMessage = response_data.message;
				$(".create-league .field").removeAttr("disabled");
				$(".create-league .trigger").html("Create League");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

});