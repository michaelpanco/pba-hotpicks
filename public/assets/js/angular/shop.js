pbahotpicks.controller('Shop', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.purchaseProduct = function(productName, productType, itemID, productID)
	{
		$scope.requests = {

			pre_request: function(){
				$(".shop-wrapper .field").attr("disabled", true);
				$(".product-item-" + itemID + " .trigger").html(circularPreloader());
			},

			data: {
				item_id : itemID
			},

			url: "/api/manager/purchaseProduct",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".shop-wrapper .field").removeAttr("disabled");
				$(".product-item-" + itemID + " .trigger").html("Purchase");
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Warning', response_data.message, 'warning');
				$(".shop-wrapper .field").removeAttr("disabled");
				$(".product-item-" + itemID + " .trigger").html("Purchase");
			},

		}

		SweetAlert.swal({
			title: "Hey!",
			text: "Are you sure you want to purchase this item?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#45B549",
			confirmButtonText: "Purchase",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});

	};
});