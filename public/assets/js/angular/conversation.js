pbahotpicks.controller('Conversation', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.sendMessage = function(managerID, managerSlug)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".composer .submit").attr("disabled", true);
				$(".composer .submit").html(circularPreloader());
				$scope.disableMessage = true;
			},

			data: {
				recipient : managerID,
				conversation_id : $("#conversationIDval").val(),
				message : $scope.message
			},

			url: "/api/manager/sendMessage",

			validation: function()
			{
				if (($scope.message === undefined || $scope.message === ""))
				{
					throw "You didn't write any message";
				}
			},

			success_callback: function(response_data)
			{
				$(".composer .submit").removeAttr("disabled");
				$(".composer .submit").html("Send");
				$scope.disableMessage = false;
				$scope.reveal('conversationsContent', '/api/manager/fetchConversations/' + managerSlug, false, false);
				$scope.message = "";
				$scope.markMessageRead(managerID);

			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Sending Failed', response_data.message, 'warning');
				$(".composer .submit").removeAttr("disabled");
				$(".composer .submit").html("Send");
				$scope.disableMessage = false;
				$scope.markMessageRead(managerID);
			},


		}
		sendHttpRequest.generic($scope.requests);
	}

	$scope.markMessageRead = function(manager_id)
	{
		$scope.requests = {

			no_status : true,

			data: {
				manager_id : manager_id,
			},

			url: "/api/manager/markMessageRead",

			success_callback: function(response_data)
			{
				console.log('Message has been read');
			},

			failed_callback: function(response_data)
			{
				console.log('Message read error');
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadConversations = function(manager_slug)
	{
		$scope.conversationManagerSlug = manager_slug;
		$scope.requests = {

			no_status : true,
			method : 'GET',

			pre_request: function()
			{
				$("#conversationsContent").html(channelPreloader());
			},

			url: "/api/manager/fetchConversations/" + manager_slug,

			success_callback: function(response_data)
			{
				response_content = response_data.split("/*super#0291873645#master*/");
				$scope.lastConversationCursor = response_content[2];

				if(response_content[1] != 0)
				{
					$scope.conversationPointer = response_content[0]; // user for ponting the last msg
					$scope.conversationMarker = response_content[0]; // user for scroll cursor

					$("#conversationsContent").html(response_content[1]);
					$('.nano').nanoScroller();
					$(".nano").nanoScroller({ scroll: 'bottom' });

				}else{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No conversation available</div>";
					$(".posts-loader").html('');
				}

				setTimeout($scope.conversationCycle,  window.conversation_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadMoreConversations = function()
	{
		if(!$scope.noMoreConversationsFetching){
			$scope.haltConversationCycle = true;
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$(".conversations-loader").html(channelPreloader());
				},

				data: {
					recipient : $scope.messageRecipient,
					pointer : $scope.conversationPointer,
				},

				url: "/api/manager/getMoreConversations",

				validation: function()
				{

				},

				success_callback: function(response_data)
				{
					var container = "conversationsContent";

					response_content = response_data.split("/*super#0291873645#master*/");
					if(response_content[0] != 0)
					{
						$scope.conversationPointer = response_content[0];

						$("#" + container).prepend(response_content[1]);
						$('.nano').nanoScroller();
						$("#" + container).css('opacity', 1);
						$(".nano").nanoScroller({ scrollTo: $('#msg-' + $scope.conversationMarker) });
						$scope.conversationMarker = response_content[0];

					}else{
						$("#" + container).prepend("<div class='no-available-conversations'>No more messages</div>");
						$scope.noMoreConversationsFetching = true;
						$(".posts-loader").html('');
					}
					$(".conversations-loader").html('');
				},

				failed_callback: function(response_data)
				{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No more posts available</div>";
					$(".conversations-loader").html('');
				},
			}
			sendHttpRequest.generic($scope.requests);
		}
	}

	$scope.conversationCycle = function()
	{
		$scope.requests = {

			no_status : true,
			method : 'GET',

			url: "/api/manager/fetchConversations/" + $scope.conversationManagerSlug,

			success_callback: function(response_data)
			{
				response_content = response_data.split("/*super#0291873645#master*/");
				if(response_content[1] != 0)
				{
					if($scope.lastConversationCursor < response_content[2])
					{
						$("#conversationsContent").html(response_content[1]);
						$('.nano').nanoScroller();
						$(".nano").nanoScroller({ scroll: 'bottom' });
						$scope.lastConversationCursor = response_content[2];
					}

				}else{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No conversation available</div>";
					$(".posts-loader").html('');
				}

				polling.recursive($scope.conversationCycle,  window.conversation_polling_interval);
				
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

});

loadConversations = function(manager_slug)
{
	angular.element(document.getElementById('conversation')).scope().loadConversations(manager_slug);
}
markMessageRead = function(manager_slug)
{
	angular.element(document.getElementById('conversation')).scope().markMessageRead(manager_slug);
}
loadMoreConversations = function(manager_slug)
{
	angular.element(document.getElementById('conversation')).scope().loadMoreConversations();
}
conversationCycle = function(manager_slug)
{
	angular.element(document.getElementById('conversation')).scope().conversationCycle(manager_slug);
}