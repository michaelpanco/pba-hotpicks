pbahotpicks.controller('Guest', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.selectRegisterDOBmonth = function(monthValue, monthLabel)
	{
		$scope.selectedMonthLabel = monthLabel;
		$scope.selectedMonth = monthValue;
	};

	$scope.selectRegisterDOBday = function(dayValue)
	{
		$scope.selectedDay = dayValue;
	};

	$scope.selectRegisterDOByear = function(yearValue)
	{
		$scope.selectedYear = yearValue;
	};

	/*** REGISTRATION ***/

	$scope.register = [];

	$scope.registerManager = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$(".register-manager .field").attr("disabled", true);
				$(".register-manager .trigger").html(circularPreloader());
			},

			data: {
				firstname : params.firstname,
				lastname : params.lastname,
				password : params.password,
				password_confirmation : params.confirm_password,
				email : params.email,
				gender : params.gender,
				birth_month : $scope.selectedMonth,
				birth_day : $scope.selectedDay,
				birth_year : $scope.selectedYear,
				team : $scope.team,
				recaptcha : $scope.confirmRecaptcha,
			},

			url: "/api/manager/register",

			validation: function(){
				delete this.data['recaptcha']; // remove recaptcha temporarily
				delete this.data['team']; // remove team temporarily
				validation.required(this.data);
				this.data['recaptcha'] = $scope.confirmRecaptcha; // bring it back dude!
				this.data['team'] = $scope.team; // bring it back dude!
				validation.min('Firstname', this.data.firstname, 2);
				validation.min('Lastname', this.data.lastname, 2);
				validation.min('Password', this.data.password, 6);
				validation.email(this.data.email);
				validation.match('Password', this.data.password, this.data.password_confirmation);
			    if (this.data.recaptcha === undefined || this.data.recaptcha === "") {
			    	throw 'Please confirm reCAPTCHA anti-spam security.';
			    }
			    if(!/^[a-zA-Z]+$/.test(this.data.firstname)){
			    	throw 'The firstname may only contain letters.';
			    }
			    if(!/^[a-zA-Z]+$/.test(this.data.lastname)){
			    	throw 'The lastname may only contain letters.';
			    }
			},

			success_callback: function(response_data){
				SweetAlert.swal("Registration Successful", "Thank you for submitting your registration at PinoyTenPicks! You will automatically be login with your account", "success");
				$(".register-manager .field").removeAttr("disabled");
				$(".register-manager .trigger").html("Register");
				$scope.register = [];
				$scope.selectedYear = '';
				$scope.selectedMonthLabel = '';
				$scope.selectedMonth = '';
				$scope.selectedDay = '';
				setTimeout(function () {
					window.location = "/dashboard";
				}, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Registration Failed', response_data.message, 'warning');
				$(".register-manager .field").removeAttr("disabled");
				$(".register-manager .trigger").html("Register");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	/*** END REGISTRATION ***/
	
	/*** BEGIN LOGIN ***/

	$scope.login = [];

	$scope.loginManager = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$scope.loginAlert = false;
				$(".login-manager .field").attr("disabled", true);
				$(".login-manager .trigger").html(circularPreloader());
			},

			data: {
				email : params.email,
				password : params.password,
				remember : params.remember
			},

			url: "/api/manager/login",

			validation: function(){
				if ((params.email === undefined || params.email === "") || (params.password === undefined || params.password === "")) {
					throw 'Please enter your email and password.';
				}
			},

			success_callback: function(response_data){
				window.location = window.location.origin + '/dashboard';
			},

			failed_callback: function(response_data){
				$scope.loginAlert = true;
				$scope.errMessage = response_data.message;
				$scope.login.password = "";
				$(".login-manager .field").removeAttr("disabled");
				$(".login-manager .trigger").html("Login");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	/*** END LOGIN ***/

	$scope.showForgotPasswordForm = function()
	{
		$('.modal-login').modal('hide');
		$('.modal-forgot-password').modal('show');
		$scope.forgotSuccess = false;
		$scope.forgotPasswordAlert = false;
		$scope.forgotPasswordResetSuccess = false;
	}

	$scope.forgot = [];

	$scope.forgotPassword = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$scope.forgotPasswordAlert = false;
				$(".forgot-password .field").attr("disabled", true);
				$(".forgot-password .trigger").html(circularPreloader());
			},

			data: {
				email : params.email,
			},

			url: "/api/manager/forgotPassword",

			validation: function(){
				if(params.email == '' || params.email == null)
				{
					throw "Please enter your email address";
				}
				validation.email(this.data.email);
			},

			success_callback: function(response_data){
				$scope.forgotPasswordResetSuccess = true;
				$scope.forgotSuccess = true;
				$scope.forgotPasswordAlert = true;
				$scope.alertMessage = response_data.message;
				$scope.forgotAlertType = 'alert alert-success';
				$scope.forgot.email = '';
				$(".forgot-password .field").removeAttr("disabled");
				$(".forgot-password .trigger").html("Submit");
			},

			failed_callback: function(response_data){
				$scope.forgotPasswordAlert = true;
				$scope.alertMessage = response_data.message;
				$scope.forgotAlertType = 'alert alert-danger';
				$(".forgot-password .field").removeAttr("disabled");
				$(".forgot-password .trigger").html("Submit");
			},

		}

		sendHttpRequest.generic($scope.requests);
	}

	$scope.flagOnline = function()
	{
		$scope.requests = {

			no_status : true,

			url: "/api/manager/flagOnline",

			success_callback: function(response_data)
			{
				polling.recursive($scope.flagOnline, window.flag_online_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	//openGift
	$scope.openGift = function()
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".open-gift").attr("disabled", true);
				$(".open-gift").html("Opening your gift");
			},

			data: {
				xvim : 'xs3dsa'
			},

			url: "/api/manager/openGift",

			success_callback: function(response_data)
			{
				SweetAlert.swal("Congratulations", response_data.message, "success");
				$(".open-gift").html('Received');
			},

			failed_callback: function(response_data)
			{
				$(".open-gift").removeAttr("disabled");
				$(".open-gift").html('Open it now!');
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.onLoad = function()
	{
		if(window.auth){
			$scope.flagOnline();
		}
		
	};

	$scope.onLoad();
});

// Notice : If reCaptcha not working, this might due to the updating of the composer. if you update it, check the noCaptcha third party and look for the anhskobo/no-captcha/src/NoCaptcha.php and check if the callback is set, if not change it to setRecaptchaConfirm

setRecaptchaConfirm = function(response){
	
	pbahotpicks.controller('PBAHotpicksCtrl', function($scope) {
	    this.confirmRecaptcha = "true";
	});

  	var appElement = document.querySelector('[ng-app=pbahotpicks]');
    var $scope = angular.element(appElement).scope();

    $scope.$apply(function() {
        $scope.confirmRecaptcha = response;
    });
    
};

$( ".facebook-login" ).click(function() {
  $(this).addClass( "disabled" );
});
$().click