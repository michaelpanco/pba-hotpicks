pbahotpicks.controller('Referals', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.claimReferalPrize = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".submit").attr("disabled", true);
				$(".submit").html(circularPreloader());
			},

			data: {
				dh3ks : 'n37hofi8sd6s'
			},

			url: "/api/manager/claimReferalPrize",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".submit").removeAttr("disabled");
				$(".submit").html("Claim 5,000 Credits");

				setTimeout(function () {
					window.location = "/referals";
				}, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Warning', response_data.message, 'warning');
				$(".submit").removeAttr("disabled");
				$(".submit").html("Claim 5,000 Credits");
			},

		}
		sendHttpRequest.generic($scope.requests);
	};
});