pbahotpicks.controller('LeagueChannel', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.startLeague = function()
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".start-container .field").attr("disabled", true);
				$(".start-container .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
				league_desc : $scope.leagueDesc,
			},

			url: "/api/manager/startLeague",

			validation: function()
			{

			},

			success_callback: function(response_data)
			{
				$(".start-container .field").removeAttr("disabled");
				$(".start-container .trigger").html("Start");
				SweetAlert.swal("Success", "League has started", "success");
				window.location = "../leagues/" + league_id;

			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Start Failed', response_data.message, 'warning');
				$(".start-container .field").removeAttr("disabled");
				$(".start-container .trigger").html("Start");
			},

		}

		SweetAlert.swal({
			title: "Hey!",
			text: "You are about to start the league.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#45B549",
			confirmButtonText: "Start",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});
	};

	$scope.kickTeam = function(owner, franchise_id)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".franchise-id-" + franchise_id + " .btn").attr("disabled", true);
			},

			data: {
				franchise_id : franchise_id,
				league_id : league_id,
			},

			url: "/api/manager/kickTeam",

			success_callback: function(response_data)
			{
				$(".franchise-id-" + franchise_id).css('opacity', '0.10');
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Kick Failed', response_data.message, 'warning');
				$(".franchise-id-" + franchise_id + " .btn").removeAttr("disabled");
			},

		}

		SweetAlert.swal({
			title: "Hey!",
			text: "Are you sure you want to kick " + owner + "?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Kick",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});

	};

	$scope.updateLeagueDesc = function()
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".edit-league-desc .field").attr("disabled", true);
				$(".edit-league-desc .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
				league_desc : $scope.leagueDesc,
			},

			url: "/api/manager/updateLeagueDesc",

			validation: function()
			{
			    if ($scope.leagueDesc.length < 5) {
			    	throw 'League description is too short';
			    }
			},

			success_callback: function(response_data)
			{
				$(".edit-league-desc .field").removeAttr("disabled");
				$(".edit-league-desc .trigger").html("Update");
				SweetAlert.swal("Success", "League description has updated successfully", "success");
				$scope.editDesc = false;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Update Failed', response_data.message, 'warning');
				$(".edit-league-desc .field").removeAttr("disabled");
				$(".edit-league-desc .trigger").html("Update");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateLeaguePassword = function()
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".edit-league-password .field").attr("disabled", true);
				$(".edit-league-password .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
				league_password : $scope.leaguePassword,
			},

			url: "/api/manager/updateLeaguePassword",

			success_callback: function(response_data)
			{
				$(".edit-league-password .field").removeAttr("disabled");
				$(".edit-league-password .trigger").html("Update");
				SweetAlert.swal("Success", "League password has updated successfully", "success");
				$scope.editPassword = false;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Update Failed', response_data.message, 'warning');
				$(".edit-league-password .field").removeAttr("disabled");
				$(".edit-league-password .trigger").html("Update");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateLeague = function()
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".update-league-container .field").attr("disabled", true);
				$(".update-league-container .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
			},

			url: "/api/manager/updateLeague",

			validation: function()
			{

			},

			success_callback: function(response_data)
			{
				$(".update-league-container .field").removeAttr("disabled");
				$(".update-league-container .trigger").html("Update");
				window.location = "../leagues/" + league_id;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Update Failed', response_data.message, 'warning');
				$(".update-league-container .field").removeAttr("disabled");
				$(".update-league-container .trigger").html("Update");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.leaveLeague = function()
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".leave-container .field").attr("disabled", true);
				$(".leave-container .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
			},

			url: "/api/manager/leaveLeague",

			success_callback: function(response_data)
			{
				$(".leave-container .field").removeAttr("disabled");
				$(".leave-container .trigger").html("Leave");
				window.location = "../leagues";
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Leave Failed', response_data.message, 'warning');
				$(".leave-container .field").removeAttr("disabled");
				$(".leave-container .trigger").html("Leave");
			},

		}

		SweetAlert.swal({
			title: "Hey!",
			text: "Are you sure you want to leave the league?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Leave",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});
	};

	$scope.dismissLeague = function()
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".dismiss-container .field").attr("disabled", true);
				$(".dismiss-container .trigger").html(circularPreloader());
			},

			data: {
				league_id : league_id,
			},

			url: "/api/manager/dismissLeague",

			success_callback: function(response_data)
			{
				$(".dismiss-container .field").removeAttr("disabled");
				$(".dismiss-container .trigger").html("Dismiss");
				window.location = "../leagues";
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Dismiss Failed', response_data.message, 'warning');
				$(".dismiss-container .field").removeAttr("disabled");
				$(".dismiss-container .trigger").html("Dismiss");
			},

		}

		SweetAlert.swal({
			title: "Hey!",
			text: "Are you sure you want to dismiss the league?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Dismiss",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});
	};

	$scope.selectLeagueChannel = function(channel_id)
	{
		$scope.noMorePostsFetching = false;

		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$scope.leagueChannelContent = channelPreloader();
			},

			data: {
				channel : channel_id,
			},

			url: "/api/manager/getChannel",

			success_callback: function(response_data)
			{
				response_content = response_data.split("||||");
				$scope.leagueChannelContent = response_content[0];
				$scope.channelPostPointer = response_content[1];
			},

			failed_callback: function(response_data)
			{
				$scope.leagueChannelContent = "<div class='no-available-posts'>Content not available. Please refresh your page</div>";
				$scope.noMorePostsFetching = true;
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadMorePosts = function()
	{
		if(!$scope.noMorePostsFetching){

			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html(channelPreloader());
				},

				data: {
					channel : league_channel_id,
					pointer : $scope.channelPostPointer
				},

				url: "/api/manager/getMorePosts",

				validation: function()
				{

				},

				success_callback: function(response_data)
				{
					response_content = response_data.split("||||");
					if(response_content[1] != 0)
					{
						$scope.leagueChannelContent = $scope.leagueChannelContent + response_content[0];
						$scope.channelPostPointer = response_content[1];
						$scope.noMorePostsFetching = false;
						$(".posts-loader").html('');
					}else{
						$scope.leagueChannelContent = $scope.leagueChannelContent + "<div class='no-available-posts'>No more posts available</div>";
						$scope.noMorePostsFetching = true;
						$(".posts-loader").html('');
					}
				},

				failed_callback: function(response_data)
				{
					$scope.leagueChannelContent = $scope.leagueChannelContent + "<div class='no-available-posts'>No more posts available</div>";
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html('');
				},
			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.createPost = function()
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".status-post .field").attr("disabled", true);
				$(".status-post .trigger").html(circularPreloader());
			},

			data: {
				content : $scope.postContent,
				channel_id : league_channel_id
			},

			url: "/api/manager/createPost",

			validation: function()
			{
				if (($scope.postContent === undefined || $scope.postContent === "")) {
					throw "You didn't write any message to post";
				}
			},

			success_callback: function(response_data)
			{
				$scope.leagueChannelContent = response_data + $scope.leagueChannelContent;
				$(".status-post .field").removeAttr("disabled");
				$(".status-post .trigger").html("Post");
				$( ".channel-post" ).removeClass( "new-item-transition" )
				$scope.postContent = "";
				$scope.hideNoPostShow = true;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Posting Failed', response_data.message, 'warning');
				$(".status-post .field").removeAttr("disabled");
				$(".status-post .trigger").html("Post");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.toggleReplyComment = function(postID)
	{
		$scope['post' + postID] = !$scope['post' + postID];

		if($scope['post' + postID])
		{
			$scope.fetchReplies(postID);
		}

	};

	$scope.fetchReplies = function(postID)
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$scope['p' + postID + 'r'] = channelPreloader();
			},

			data: {
				parent_id : postID
			},

			url: "/api/manager/fetchReplies",

			success_callback: function(response_data)
			{
				$scope['p' + postID + 'r'] = response_data;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Status Failed', response_data.message, 'warning');
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Login");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.replyPost = function(postParent, replyContent)
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".post-reply-" + postParent + " .field").attr("disabled", true);
				$(".post-reply-" + postParent + " .trigger").html(circularPreloader());
			},

			data: {
				content : replyContent,
				parent_post : postParent,
				channel_id : league_channel_id
			},

			url: "/api/manager/replyPost",

			validation: function()
			{
				if ((replyContent === undefined || replyContent === ""))
				{
					throw "You didn't write any message reply";
				}
			},

			success_callback: function(response_data)
			{
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Reply");

				var current_replies = ($scope['p' + postParent + 'r'] === undefined) ? '' : $scope['p' + postParent + 'r'];

				$scope['p' + postParent + 'r'] = current_replies + response_data;
				$scope['replyContentPost' + postParent] = "";
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Reply Failed', 'Failed to reply post, Please try again', 'warning');
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Reply");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.upVote = function(postID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".up-vote-" + postID).html(givePointPreloader());
			},

			data: {
				post_id : postID
			},

			url: "/api/manager/upVotePost",

			success_callback: function(response_data)
			{
				$scope['post' + postID + 'point'] = $scope['post' + postID + 'point'] + 1;
				$(".up-vote-" + postID).html('<span class="glyphicon glyphicon-upload" aria-hidden="true">');
				$scope['hideUpVote' + postID] = true;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('UpVote Failed', response_data.message, 'warning');
				$(".up-vote-" + postID).html('<span class="glyphicon glyphicon-upload" aria-hidden="true">');
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

});

loadMorePosts = function()
{
	angular.element(document.getElementById('LeagueChannel')).scope().loadMorePosts();
}