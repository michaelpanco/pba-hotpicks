pbahotpicks.controller('ExpressBet', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.submitBet = function(selected, bet_id, bet, amount)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".bet-container-" + bet_id + " .field").attr("disabled", true);
				$(".bet-container-" + bet_id + " .trigger").html(circularPreloader());
			},

			data: {
				express_bet_id : bet_id,
				bet : bet,
				amount : amount,
			},

			url: "/api/manager/makeGameBet",

			validation: function()
			{
				if(bet == null || bet == undefined)
				{
					throw "Please select your bet for this item";
				}
				if(amount == null || amount == undefined)
				{
					throw "Please enter the amount bet";
				}
			},

			success_callback: function(response_data)
			{
				$(".bet-container-" + bet_id + " .field").removeAttr("disabled");
				$(".bet-container-" + bet_id + " .trigger").html("Submit");
				SweetAlert.swal("Success", response_data.message, "success");
				window.location = window.location.origin + '/express-bet';
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Bet Failed', response_data.message, 'warning');
				$(".bet-container-" + bet_id + " .field").removeAttr("disabled");
				$(".bet-container-" + bet_id + " .trigger").html("Submit");
			},
		}

		if(bet == null || bet == undefined || amount == null || amount == undefined)
		{
			SweetAlert.swal('Bet Failed', 'Please choose your bet and enter your bet amount', 'warning');
		}else{
			SweetAlert.swal({
				title: "Hey!",
				text: "Are you sure you want to make a bet for " + selected +" amounting " + amount + "?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#45B549",
				confirmButtonText: "Yes",
				closeOnConfirm: true
			},
			function(isConfirm){
				if (isConfirm) {
					sendHttpRequest.generic($scope.requests);
				}
			});
		}
	};

	$scope.claimExpressBetPrice = function(bet_id)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".claimbet-container-" + bet_id + " .field").attr("disabled", true);
				$(".claimbet-container-" + bet_id + " .trigger").html(circularPreloader());
			},

			data: {
				express_bet_id : bet_id
			},

			url: "/api/manager/claimExpressBetPrize",

			success_callback: function(response_data)
			{
				$(".claimbet-container-" + bet_id + " .field").removeAttr("disabled");
				$(".claimbet-container-" + bet_id + " .trigger").html("Claim");
				SweetAlert.swal("Success", response_data.message, "success");
				window.location = window.location.origin + '/express-bet';
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Bet Failed', response_data.message, 'warning');
				$(".claimbet-container-" + bet_id + " .field").removeAttr("disabled");
				$(".claimbet-container-" + bet_id + " .trigger").html("Claim");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

});