pbahotpicks.controller('CreateTeam', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.centerSlot = ['C','PF/C'];
	$scope.powerForwardSlot = ['PF','PF/C'];
	$scope.smallForwardSlot = ['SF','SF/PF'];
	$scope.shootingGuardSlot = ['SG','SG/SF'];
	$scope.pointGuardSlot = ['PG','PG/SG'];

	$scope.$watch('consumeAmount', function(amount) {
		if(amount > window.initial_salary_limit){
			$scope.limitReached = true;
			$scope.salaryClass = 'cred';
		}else{
			$scope.limitReached = false;
			$scope.salaryClass = 'cgreen';
		}
		
	});

	$scope.consumeAmount = 0;

	$scope.totalPickAmount = function() {
	    var amount = 0;
	    angular.forEach($scope.picks, function(pick){
		    angular.forEach(pick, function(picker){
		        amount += picker.salary;
		    });
	    });
	    $scope.consumeAmount = amount;
	    return amount; 
	}

	$scope.selectFranchiseLogo = function(logo_id, logo_filename){
		$scope.selectedTeamLogo.id = logo_id;
		$scope.selectedTeamLogo.filename = logo_filename;
		$('.modal-change-team-logo').modal('hide');
	}

	$scope.totalPickCount = function(){
	    var count = 0;
	    angular.forEach($scope.picks, function(pick){
		    angular.forEach(pick, function(picker){
		        count += (picker) ? 1:0;
		    });
	    });
	    return count; 
	}

	$scope.picksAreUnique = function(arr){
	    var map = {}, i, size;

	    for (i = 0, size = arr.length; i < size; i++){
	        if (map[arr[i]]){
	            return false;
	        }

	        map[arr[i]] = true;
	    }

	    return true;
	}

	$scope.createTeam = function()
	{
		$scope.playerpicks = [];

		if(window.auth === true){
			$scope.dist = 'createTeam';
		}else{
			$scope.dist = 'tmpCreateTeam';
		}

		$scope.requests = {

			picks: function(){
				($scope.picks.pg1.length > 0) ? $scope.playerpicks.push($scope.picks.pg1[0].id) : '';
				($scope.picks.pg2.length > 0) ? $scope.playerpicks.push($scope.picks.pg2[0].id): '';
				($scope.picks.sg1.length > 0) ? $scope.playerpicks.push($scope.picks.sg1[0].id): '';
				($scope.picks.sg2.length > 0) ? $scope.playerpicks.push($scope.picks.sg2[0].id): '';
				($scope.picks.sf1.length > 0) ? $scope.playerpicks.push($scope.picks.sf1[0].id): '';
				($scope.picks.sf2.length > 0) ? $scope.playerpicks.push($scope.picks.sf2[0].id): '';
				($scope.picks.pf1.length > 0) ? $scope.playerpicks.push($scope.picks.pf1[0].id): '';
				($scope.picks.pf2.length > 0) ? $scope.playerpicks.push($scope.picks.pf2[0].id): '';
				($scope.picks.c1.length > 0) ? $scope.playerpicks.push($scope.picks.c1[0].id): '';
				($scope.picks.c2.length > 0) ? $scope.playerpicks.push($scope.picks.c2[0].id): '';
			},

			pre_request: function(){
				$(".create-team .field").attr("disabled", true);
				$(".create-team .trigger").html(circularPreloader());
				this.picks();

			},

			data: {
				name : $scope.name,
				logo : $scope.selectedTeamLogo.id,
				picks : $scope.playerpicks
			},

			url: "/api/manager/" + $scope.dist,

			validation: function(){

				if($scope.hasTeam){
					throw "You already have a team, you can't create another one";
				}
			    if ($scope.name === undefined || $scope.name === "") {
			    	throw 'Please enter the name of your team';
			    }
			    if ($scope.name.length < 5) {
			    	throw 'Team name must be at least 5 characters long';
			    }
			    if ($scope.name.length > 26) {
			    	throw 'Team name must not exceed 32 characters';
			    }
 			    var team_name_pattern = "/^[a-z0-9 ]+$/i";
			    if (! /^[a-z0-9 ]+$/i.test($scope.name)) {
					throw 'Team name must not contain special characters';
			    }
			    if($scope.totalPickCount() < 10){
			    	throw 'Please complete your 10 player lineup team';
			    }
				if(!$scope.picksAreUnique($scope.playerpicks)){
					throw 'You have duplicate player in your team';
				}

			},

			success_callback: function(response_data){
				SweetAlert.swal("Successful", "You team was created successfully", "success");
				$(".create-team .field").removeAttr("disabled");
				$(".create-team .trigger").html("Create Team");
				if(!window.auth){
					setTimeout(function () {
						window.location = "/register?team=" + response_data.tag;
					}, 1000);
				}
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Warning', response_data.message, 'warning');
				$(".create-team .field").removeAttr("disabled");
				$(".create-team .trigger").html("Create Team");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};



});