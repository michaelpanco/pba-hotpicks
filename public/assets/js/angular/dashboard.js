pbahotpicks.controller('Dashboard', function($scope, $localStorage, sendHttpRequest, SweetAlert, validation, polling){

    $scope.$storage = $localStorage.$default({
      psc: {id:2, name:'Alaska'},
      ssc: {id:3, name:'Barako Bull'},
    });

	$scope.hasStorage = function()
	{
	  try
	  {
	    localStorage.setItem('pbahotpicks', 'pba');
	    localStorage.removeItem('pbahotpicks');
	    return true;
	  } catch (exception) {
	    return false;
	  }
	};

    window.primarySelectedChannel = $scope.$storage.psc;
	window.secondarySelectedChannel = $scope.$storage.ssc;
	window.hasStorage = $scope.hasStorage();

	$scope.tabToFront = function(channel)
	{

		var ftChannelOne = $scope.featuredChannels[2];
		var ftChannelTwo = $scope.featuredChannels[3];
		var index = $scope.concealedChannels.indexOf(channel);

		$scope.featuredChannels[2] = channel;
		$scope.featuredChannels[3] = ftChannelOne;

		if($scope.hasStorage)
		{
			$scope.concealedChannels.splice(index, 1);  
			$scope.concealedChannels.push(ftChannelTwo);
		}

		$( "#channelTab li" ).removeClass("active")

		$scope.$storage.psc = channel;
		$scope.$storage.ssc = ftChannelOne;

		channel.selected = true;
		$scope.selectChannel(channel);
	};

	$scope.selectChannel = function(channel)
	{
		$scope.currentChannel = channel.name;
		$scope.currentChannelID = channel.id;
		$scope.noMorePostsFetching = false;

		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$scope.channelContent = channelPreloader();
			},

			data: {
				channel : channel.id,
			},

			url: "/api/manager/getChannel",

			success_callback: function(response_data)
			{
				response_content = response_data.split("||||");
				$scope.channelContent = response_content[0];
				$scope.channelPostPointer = response_content[1];
			},

			failed_callback: function(response_data)
			{
				$scope.channelContent = "<div class='no-available-posts'>Content not available. Please refresh your page</div>";
				$scope.noMorePostsFetching = true;
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadMorePosts = function()
	{
		if(!$scope.noMorePostsFetching){

			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html(channelPreloader());
				},

				data: {
					channel : $scope.currentChannelID,
					pointer : $scope.channelPostPointer
				},

				url: "/api/manager/getMorePosts",

				validation: function()
				{

				},

				success_callback: function(response_data)
				{
					response_content = response_data.split("||||");
					if(response_content[1] != 0)
					{
						$scope.channelContent = $scope.channelContent + response_content[0];
						$scope.channelPostPointer = response_content[1];
						$scope.noMorePostsFetching = false;
						$(".posts-loader").html('');
					}else{
						$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No more posts available</div>";
						$scope.noMorePostsFetching = true;
						$(".posts-loader").html('');
					}
				},

				failed_callback: function(response_data)
				{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No more posts available</div>";
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html('');
				},
			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.createPost = function()
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".status-post .field").attr("disabled", true);
				$(".status-post .trigger").html(circularPreloader());
			},

			data: {
				content : $scope.postContent,
				channel_id : $scope.currentChannelID
			},

			url: "/api/manager/createPost",

			validation: function()
			{
				if (($scope.postContent === undefined || $scope.postContent === "")) {
					throw "You didn't write any message to post";
				}
			},

			success_callback: function(response_data)
			{
				$scope.channelContent = response_data + $scope.channelContent;
				$(".status-post .field").removeAttr("disabled");
				$(".status-post .trigger").html("Post");
				$( ".channel-post" ).removeClass( "new-item-transition" )
				$scope.postContent = "";
				$scope.hideNoPostShow = true;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Posting Failed', response_data.message, 'warning');
				$(".status-post .field").removeAttr("disabled");
				$(".status-post .trigger").html("Post");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.toggleReplyComment = function(postID)
	{
		$scope['post' + postID] = !$scope['post' + postID];

		if($scope['post' + postID])
		{
			$scope.fetchReplies(postID);
		}

	};

	$scope.fetchReplies = function(postID)
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$scope['p' + postID + 'r'] = channelPreloader();
			},

			data: {
				parent_id : postID
			},

			url: "/api/manager/fetchReplies",

			success_callback: function(response_data)
			{
				$scope['p' + postID + 'r'] = response_data;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Status Failed', response_data.message, 'warning');
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Login");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.replyPost = function(postParent, replyContent)
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".post-reply-" + postParent + " .field").attr("disabled", true);
				$(".post-reply-" + postParent + " .trigger").html(circularPreloader());
			},

			data: {
				content : replyContent,
				parent_post : postParent,
				channel_id : $scope.currentChannelID
			},

			url: "/api/manager/replyPost",

			validation: function()
			{
				if ((replyContent === undefined || replyContent === ""))
				{
					throw "You didn't write any message reply";
				}
			},

			success_callback: function(response_data)
			{
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Reply");

				var current_replies = ($scope['p' + postParent + 'r'] === undefined) ? '' : $scope['p' + postParent + 'r'];

				$scope['p' + postParent + 'r'] = current_replies + response_data;
				$scope['replyContentPost' + postParent] = "";
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Reply Failed', 'Failed to reply post, Please try again', 'warning');
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Reply");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.upVote = function(postID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".up-vote-" + postID).html(givePointPreloader());
			},

			data: {
				post_id : postID
			},

			url: "/api/manager/upVotePost",

			success_callback: function(response_data)
			{
				$scope['post' + postID + 'point'] = $scope['post' + postID + 'point'] + 1;
				$(".up-vote-" + postID).html('<span class="glyphicon glyphicon-upload" aria-hidden="true">');
				$scope['hideUpVote' + postID] = true;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('UpVote Failed', response_data.message, 'warning');
				$(".up-vote-" + postID).html('<span class="glyphicon glyphicon-upload" aria-hidden="true">');
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.deletePost = function(postID, poster)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".delete-post-" + postID).html(givePointPreloader());
			},

			data: {
				post_id : postID
			},

			url: "/api/manager/deletePost",

			success_callback: function(response_data)
			{
				$(".post-control-" + postID).html('');
				$('#post-' + postID).css('opacity', '0.3');
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Delete post failed', response_data.message, 'warning');
				$(".delete-post-" + postID).html('Failed');
			},

		}
		SweetAlert.swal({
			title: "Deleting post!",
			text: "Are you sure you want to delete the post of " + poster + "?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Delete",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});
	};

	$scope.deleteReply = function(postID, poster)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".delete-reply-" + postID).html(givePointPreloader());
			},

			data: {
				post_id : postID
			},

			url: "/api/manager/deletePost",

			success_callback: function(response_data)
			{
				$(".reply-control-" + postID).html('');
				$('#reply-' + postID).css('opacity', '0.3');
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Delete reply failed', response_data.message, 'warning');
				$(".delete-reply-" + postID).html('Failed');
			},

		}
		SweetAlert.swal({
			title: "Deleting reply!",
			text: "Are you sure you want to delete the reply of " + poster + "?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Delete",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});
	};

});

loadMorePosts = function()
{
	angular.element(document.getElementById('Dashboard')).scope().loadMorePosts();
}