pbahotpicks.controller('Profile', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.addFriend = function(managerID)
	{
		$scope.requests = {

			pre_request: function(){
				$(".add-friend").attr("disabled", true);
				$(".add-friend").html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/addFriend",

			validation: function()
			{

			},

			success_callback: function(response_data)
			{
				$(".add-friend").html('Pending');
			},

			failed_callback: function(response_data)
			{
				$(".add-friend").removeAttr("disabled");
				$(".add-friend").html('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Friend');
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.approveRequestViaProfile = function(managerID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".acceptRequestViaProfile" + managerID).attr("disabled", true);
				$(".acceptRequestViaProfile" + managerID).html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/approveRequest",

			success_callback: function(response_data)
			{
				$(".acceptRequestViaProfile" + managerID).html('<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Friend');
			},

			failed_callback: function(response_data)
			{
				$(".acceptRequestViaProfile" + managerID).removeAttr("disabled");
				$(".acceptRequestViaProfile" + managerID).html("Accept");
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.restrainManager = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".restrain-manager .field").attr("disabled", true);
				$(".restrain-manager .trigger").html(circularPreloader());
			},

			data: {
				restrain_manager_id : $scope.restrainManagerID,
				no_of_days : $scope.restrainDays,
				restrain_reason : $scope.restrainReason,
			},

			url: "/api/manager/restrainManager",

			validation: function(){
				if ($scope.restrainDays === undefined || $scope.restrainDays === "") {
					throw 'Please choose the no. of days to restrain';
				}
				if ($scope.restrainReason === undefined || $scope.restrainReason === "") {
					throw 'Please enter the reason of restraining the manager';
				}
			},

			success_callback: function(response_data){
				$('.modal-restrain').modal('hide')
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".restrain-manager .field").removeAttr("disabled");
				$(".restrain-manager .trigger").html("Restrain");
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Restraining failed', response_data.message, 'warning');
				$(".restrain-manager .field").removeAttr("disabled");
				$(".restrain-manager .trigger").html("Restrain");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

});