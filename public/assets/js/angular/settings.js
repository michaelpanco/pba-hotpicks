pbahotpicks.controller('Settings', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	/*** Change Password ***/

	$scope.password = [];

	$scope.changePassword = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$(".change-password .field").attr("disabled", true);
				$(".change-password .trigger").html(circularPreloader());
			},

			data: {
				current_password : params.current,
				new_password : params.new,
				confirm_password : params.confirm
			},

			url: "/api/manager/changePassword",

			validation: function(){
				validation.required(this.data);
				validation.min('New Password', this.data.new_password, 6);
				validation.match('Password', this.data.new_password, this.data.confirm_password);

				if(params.current == params.new){
					throw 'New password is identical to the old one. Please enter a new password';
				}
			},

			success_callback: function(response_data){
				SweetAlert.swal("Change Password Successful", "Your password was changed successfully", "success");
				$(".change-password .field").removeAttr("disabled");
				$(".change-password .trigger").html("Change Password");
				$scope.password = [];
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Change Password Failed', response_data.message, 'warning');
				$(".change-password .field").removeAttr("disabled");
				$(".change-password .trigger").html("Change Password");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.selectTeamMatchupA = function(label, value)
	{
		$scope.selectedFavMatchupALbl = label;
		$scope.selectedFavMatchupAVal = value;

		if(value == 999){
			$scope.selectedFavMatchupBLbl = '';
			$scope.selectedFavMatchupBVal = 999;
		}

		if($scope.currentMatchupB != "")
		{
			$scope.selectedFavMatchupBLbl = '';
			$scope.currentMatchupA = "";
			$scope.currentMatchupB = "";
		}
	}

	$scope.selectTeamMatchupB = function(label, value)
	{
		$scope.selectedFavMatchupBLbl = label;
		$scope.selectedFavMatchupBVal = value;

		if(value == 999){
			$scope.selectedFavMatchupALbl = '';
			$scope.selectedFavMatchupAVal = 999;
		}

		if($scope.currentMatchupA != "")
		{
			$scope.selectedFavMatchupALbl = '';
			$scope.currentMatchupA = "";
			$scope.currentMatchupB = "";
		}
	}

	$scope.saveFavorites = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".change-favorites .field").attr("disabled", true);
				$(".change-favorites .trigger").html(circularPreloader());
			},

			data: {
				team : $scope.selectedFavTeamVal,
				player : $scope.selectedFavPlayerVal,
				team2 : $scope.selected2ndFavTeamVal,
				match1 : $scope.selectedFavMatchupAVal,
				match2 : $scope.selectedFavMatchupBVal,
			},

			url: "/api/manager/changeFavorites",

			validation: function(){
				
				if ($scope.selectedFavMatchupAVal != undefined || $scope.selectedFavMatchupBVal != undefined)
				{
					if(($scope.selectedFavMatchupAVal === undefined || $scope.selectedFavMatchupAVal === "") || ($scope.selectedFavMatchupBVal === undefined || $scope.selectedFavMatchupBVal === ""))
					{
						throw "Please complete the team match-up";
					}
				}

			},

			success_callback: function(response_data){
				SweetAlert.swal("Changes Saved", response_data.message, "success");
				$(".change-favorites .field").removeAttr("disabled");
				$(".change-favorites .trigger").html("Save Changes");
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Changes Failed', response_data.message, 'warning');
				$(".change-favorites .field").removeAttr("disabled");
				$(".change-favorites .trigger").html("Save Changes");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.uploadAvatar = function()
	{
        $scope.requests = {

			pre_request: function(){
				$(".upload-avatar .field").attr("disabled", true);
				$(".upload-avatar .trigger").html(circularPreloader());
			},

        	file : $scope.avatar,

        	url: "/api/manager/uploadAvatar",

			validation: function(){
				if($scope.avatar == undefined){
					throw "Please select an image to upload";
				}

			},

			success_callback: function(response_data){
				$scope.managerAvatar = response_data.avatar;
				SweetAlert.swal("Changes Saved", response_data.message, "success");
				$(".upload-avatar .field").removeAttr("disabled");
				$(".upload-avatar .trigger").html("Upload");
				
			   	setTimeout(function () {
			       window.location = "../settings";
			    }, 1000);


			},

			failed_callback: function(response_data){
				SweetAlert.swal('Changes Failed', response_data.message, 'warning');
				$(".upload-avatar .field").removeAttr("disabled");
				$(".upload-avatar .trigger").html("Upload");
			},
        }

        sendHttpRequest.upload($scope.requests);
	}

	$scope.updateBadges = function()
	{
		var enabled_badges = 0;
	    angular.forEach($scope.badges, function(value, key){
		    if(value){
		    	enabled_badges++;
		    }
	    });

		$scope.requests = {

			pre_request: function(){
				$(".update-badges .field").attr("disabled", true);
				$(".update-badges .trigger").html(circularPreloader());
			},

			data: {
				badges : $scope.badges
			},

			url: "/api/manager/manageBadges",

			validation: function(){
				if(enabled_badges > 3){
					throw "Only 3 badges can be activate";
				}
			},

			success_callback: function(response_data){
				SweetAlert.swal("Badge Saved", response_data.message, "success");
				$(".update-badges .field").removeAttr("disabled");
				$(".update-badges .trigger").html("Save");
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Badge Failed', response_data.message, 'warning');
				$(".update-badges .field").removeAttr("disabled");
				$(".update-badges .trigger").html("Save");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

});