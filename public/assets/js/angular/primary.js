var pbahotpicks = angular.module('pbahotpicks', angular_dependency);

circularPreloader = function() {
	return '<img height="20" src="/assets/img/submit_preloader.gif">';
};

loadMoreMessagePreloader = function() {
	return '<img src="/assets/img/small_black_circular.gif">';
};

channelPreloader = function(){
	return '<img class="channel-preloader" src="/assets/img/channel_preloader.gif">';
};

givePointPreloader = function(){
	return '<img src="/assets/img/point_preloader.gif">';
};

validateEmail = function(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
};

$('.navbar-notice .dropdown-menu').click(function(e) {
    e.stopPropagation();
});

pbahotpicks.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {

                return scope.$eval(attrs.compile);
            },
            function(value) {

                element.html(value);

                $compile(element.contents())(scope);
                $('.nano').nanoScroller();
            }
        );
    };
}]);

pbahotpicks.directive('postRender',['$timeout', function (timer) {
    return {
        link: function (scope, elem, attrs, ctrl) {
            timer(function () {
                    $('.nano').nanoScroller({preventPageScrolling: true})
                 }
                 , 0);
        }
    }
}]);

pbahotpicks.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

pbahotpicks.service('sendHttpRequest', ['$http', function($http){

	this.generic = function(requests)
	{
		if(requests["pre_request"]) requests["pre_request"]();
		
		try {
			
			if(requests["validation"]) requests["validation"]();

			var requests_data = (requests['data']) ? $.param(requests['data']) : '';
			var method_request = (requests['method']) ? requests['method'] : 'POST';

			$http({
				method : method_request,
				url : requests['url'],
				data : requests_data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {

				if(!requests["no_status"])
				{
					var response_status = data.status;

					if (response_status === 'success')
					{
						requests["success_callback"](data);

					}else{

						if(requests["failed_callback"]) requests["failed_callback"](data);
					}
				}else{
					requests["success_callback"](data);
				}
			}).error(function(data, status, headers, config) {
				var data = { message : "Generic Error" }
				if(requests["failed_callback"]) requests["failed_callback"](data);
			});

		} catch(err) {
			var data = {message : err}
			if(requests["failed_callback"]) requests["failed_callback"](data);
		}
	}

    this.upload = function(requests){

        var formdata = new FormData();
        formdata.append('file', requests['file']);

		if(requests["pre_request"]) requests["pre_request"]();
		
		try {
			
			if(requests["validation"]) requests["validation"]();

			$http.post(requests['url'], formdata, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
			}).success(function(data, status, headers, config) {

				if(!requests["no_status"])
				{
					var response_status = data.status;

					if (response_status === 'success')
					{
						requests["success_callback"](data);

					}else{

						if(requests["failed_callback"]) requests["failed_callback"](data);
					}
				}else{
					requests["success_callback"](data);
				}
			}).error(function(data, status, headers, config) {
				var data = { message : "Generic Error" }
				if(requests["failed_callback"]) requests["failed_callback"](data);
			});

		} catch(err) {
			var data = {message : err}
			if(requests["failed_callback"]) requests["failed_callback"](data);
		}

		/*

        $http.post(requests['url'], formdata, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){

        })
        .error(function(){

        });

		*/
    }

}]);

pbahotpicks.service('sendHttpFileRequest1', ['$http', function ($http) {


    this.uploadAvatar = function(file, uploadUrl){

        var formdata = new FormData();
        formdata.append('file', file);

        $http.post(uploadUrl, formdata, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){

        })
        .error(function(){

        });
    }

}]);

pbahotpicks.service('validation', function(){

	this.required = function(values)
	{
		$.each(values, function(key, val)
		{
		    if (val === undefined || val === "") {
		    	throw "The " + key.replace('_', ' ') + " field is required ";
		    }
		});
	}

	this.min = function(field, value, length)
	{
	     if(value.length < length){
	     	throw field + ' must be at least ' + length + ' characters long';
	     }
	}

	this.match = function(field, value1, value2)
	{
	    if (value1 != value2) {
	    	throw field + ' does not match';
	    }
	}

	this.email = function(value)
	{
	    if (!validateEmail(value)) {
	    	throw 'Invalid email address';
	    }
	}

});

pbahotpicks.service('polling', function(){
	
	this.recursive = function(polling_method, polling_interval)
	{
		setTimeout(function(){

			polling_method();

		}, polling_interval);
	}

});