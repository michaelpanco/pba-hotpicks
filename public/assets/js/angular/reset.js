pbahotpicks.controller('Reset', function($scope, sendHttpRequest, validation){

	$scope.reset = [];
	$scope.resetPassword = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$scope.resetPasswordAlert = false;
				$(".reset-password .field").attr("disabled", true);
				$(".reset-password .trigger").html(circularPreloader());
			},

			data: {
				newpassword : params.newpassword,
				renewpassword : params.renewpassword,
				email : params.email,
				token : params.token,
			},

			url: "/api/manager/resetPassword",

			validation: function(){
				if(this.data.newpassword == null || this.data.newpassword == undefined)
				{
					throw "Please enter your new password";
				}
				if(this.data.renewpassword == null || this.data.renewpassword == undefined)
				{
					throw "Please confirm your new password";
				}
				if(this.data.email == null || this.data.email == undefined)
				{
					throw "Invalid request";
				}
				if(this.data.token == null || this.data.token == undefined)
				{
					throw "Invalid request";
				}
				if(this.data.newpassword != this.data.renewpassword)
				{
					throw "Password doesn't match";
				}
				validation.min('Password', this.data.newpassword, 6);
			},

			success_callback: function(response_data){
				$scope.passwordResetSuccessfull = true;
				$(".reset-password .field").removeAttr("disabled");
				$(".reset-password .trigger").html("Submit");
			},

			failed_callback: function(response_data){
				$scope.resetPasswordAlert = true;
				$scope.alertMessage = response_data.message;
				$scope.resetAlertType = 'alert alert-danger';
				$(".reset-password .field").removeAttr("disabled");
				$(".reset-password .trigger").html("Submit");
			},

		}

		sendHttpRequest.generic($scope.requests);
	}

});