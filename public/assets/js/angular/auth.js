pbahotpicks.controller('Auth', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.fetchNotifications = function()
	{
		if(!$('.nav-not').hasClass('open')) 
		{
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noticeNotificationsContent = channelPreloader();
				},

				url: "/api/manager/fetchNotifications",

				success_callback: function(response_data)
				{
					$scope.noticeNotificationsContent = response_data;
					$scope.notificationsCountShow = false;
				},

				failed_callback: function(response_data)
				{
					SweetAlert.swal('Fetching Failed', response_data.message, 'warning');
				},

			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.fetchLatestMessages = function()
	{
		if(!$('.nav-msg').hasClass('open')) 
		{
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noticeMessagesContent = channelPreloader();
				},

				url: "/api/manager/fetchLatestMessages",

				success_callback: function(response_data)
				{
					$scope.noticeMessagesContent = response_data;
				},

				failed_callback: function(response_data)
				{
					SweetAlert.swal('Fetching Failed', response_data.message, 'warning');
				},

			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.fetchRequests = function()
	{
		if(!$('.nav-req').hasClass('open')) 
		{
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noticeRequestsContent = channelPreloader();
				},

				url: "/api/manager/fetchRequests",

				success_callback: function(response_data)
				{
					$scope.noticeRequestsContent = response_data;
				},

				failed_callback: function(response_data)
				{
					SweetAlert.swal('Fetching Failed', response_data.message, 'warning');
				},

			}

			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.approveRequest = function(managerID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".acceptRequest" + managerID).attr("disabled", true);
				$(".acceptRequest" + managerID).html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/approveRequest",

			success_callback: function(response_data)
			{
				$scope['hideRequest' + managerID] = true;
				$(".requestControl" + managerID).html('<p class="success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Accepted</p>');
				$scope.requestsCount--;
				$scope.accountFriends++;
			},

			failed_callback: function(response_data)
			{
				$(".acceptRequest" + managerID).removeAttr("disabled");
				$(".acceptRequest" + managerID).html("Accept");
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.declineRequest = function(managerID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".declineRequest" + managerID).attr("disabled", true);
				$(".declineRequest" + managerID).html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/declineRequest",

			success_callback: function(response_data)
			{
				$scope['hideRequest' + managerID] = true;
				$(".requestControl" + managerID).html('<p class="success"><span class="glyphicon glyphicon-exclamation-signye" aria-hidden="true"></span> Declined</p>');
				$scope.requestsCount--;
			},

			failed_callback: function(response_data)
			{
				$(".declineRequest" + managerID).removeAttr("disabled");
				$(".declineRequest" + managerID).html("Decline");
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.notifier = function()
	{
		$scope.requests = {

			no_status : true,

			url: "/api/manager/notifier",

			success_callback: function(response_data)
			{
				$scope.onlineFriends = response_data.onlineFriends;

				if(response_data.requests > 0){
					$scope.requestsCountShow = true;
					$scope.requestsCount = response_data.requests;
				}

				if(response_data.newMessages > 0){
					$scope.newMessagesCountShow = true;
					$scope.newMessagesCount = response_data.newMessages;
				}else{
					$scope.newMessagesCountShow = false;
				}

				if(response_data.notifications > 0){
					$scope.notificationsCountShow = true;
					$scope.notificationsCount = response_data.notifications;
				}
				
				polling.recursive($scope.notifier, window.notifier_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.flagOnline = function()
	{
		$scope.requests = {

			no_status : true,

			url: "/api/manager/flagOnline",

			success_callback: function(response_data)
			{
				polling.recursive($scope.flagOnline, window.flag_online_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.paginate = function(container, paginationURL, next, loading)
	{
		var next = (typeof next === 'undefined') ? false : next;
		var loading = (typeof loading === 'undefined') ? true : loading;

		$scope.requests = {

			no_status : true,
			method : 'GET',

			pre_request: function()
			{
				if(next==false && loading==true) $("#" + container).html(channelPreloader());
				if(next==true && loading==true) $("#" + container).css('opacity',0.5);
			},

			data: {
				manager : 'me',
			},

			url: paginationURL,

			success_callback: function(response_data)
			{
				$scope[container] = response_data;
				$("#" + container).css('opacity', 1);
			},

			failed_callback: function(response_data)
			{
				$scope[container] = 'Failed to fetch request';
				$("#" + container).css('opacity', 1);
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.reveal = function(container, paginationURL, next, loading)
	{
		var next = (typeof next === 'undefined') ? false : next;
		var loading = (typeof loading === 'undefined') ? true : loading;

		$scope.requests = {

			no_status : true,
			method : 'GET',

			pre_request: function()
			{
				if(next==false && loading==true) $("#" + container).html(channelPreloader());
				if(next==true && loading==true) $("#" + container).css('opacity',0.5);
			},

			data: {
				manager : 'me',
			},

			url: paginationURL,

			success_callback: function(response_data)
			{

				response_content = response_data.split("/*super#0291873645#master*/");
				if(response_content[1] != 0)
				{
					$scope.conversationPointer = response_content[0];
					$scope.conversationMarker = response_content[0];

					$("#" + container).html(response_content[1]);
					$('.nano').nanoScroller();
					$("#" + container).css('opacity', 1);
					$(".nano").nanoScroller({ scroll: 'bottom' });

				}else{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No conversation available</div>";
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html('');
				}
			},

			failed_callback: function(response_data)
			{
				$scope[container] = 'Failed to fetch request';
				$("#" + container).css('opacity', 1);
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.onLoad = function()
	{
		$scope.notifier();
		$scope.flagOnline();
	};

	$scope.onLoad();

	$scope.$watch('requestsCount', function(requestsCount) {
		if(requestsCount < 1){
			$scope.requestsCountShow = false;
		}
	});

});