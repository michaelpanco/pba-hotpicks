pbahotpicks.controller('Trade', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.origAccountCredits = account_credits;
	

	$scope.$watch('totalTradeAmount()', function(amount) {

		$scope.creditTradeError = false;
		$scope.creditClass = 'cgreen';
		
		if($scope.playerSalary < amount){
			$scope.overAmountTrade = true;
			$scope.additionalAmount = amount - $scope.playerSalary;
			$scope.accountCredits = $scope.accountCredits - $scope.additionalAmount;
		}else{
			$scope.overAmountTrade = false;
			$scope.accountCredits = $scope.origAccountCredits;
		}

		if($scope.accountCredits < 0){
			$scope.creditTradeError = true;
			$scope.creditClass = 'cred';
		}
		
	});

	$scope.consumeAmount = 0;

	$scope.totalTradeAmount = function() {
	    var amount = 0;
	    angular.forEach($scope.picks, function(pick){
		    angular.forEach(pick, function(picker){
		        amount += picker.salary;
		    });
	    });
	    $scope.consumeAmount = amount;
	    return amount; 
	}

	$scope.getPlayerID = function() {
		var get_player_id = null;
	    angular.forEach($scope.picks, function(pick){
		    angular.forEach(pick, function(picker){
		        get_player_id = picker.id;
		    });
	    });

	    return get_player_id; 
	}

	$scope.tradePlayers = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".trade-players .field").attr("disabled", true);
				$(".trade-players .trigger").html(circularPreloader());
				$('.pick-item').css('opacity', '0.4');
			},

			data: {
				give : player_id,
				get : $scope.getPlayerID(),
			},

			url: "/api/manager/tradePlayers",

			validation: function(){
				
			},

			success_callback: function(response_data){
				SweetAlert.swal("Successful", "You successfully trade players", "success");

			   setTimeout(function () {
			       window.location = window.location.origin + '/franchises/' + account_franchise_id;
			    }, 1000);

				$(".trade-players .field").removeAttr("disabled");
				$(".trade-players .trigger").html("Trade");
				$('.pick-item').css('opacity', '1');
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Warning', response_data.message, 'warning');
				$(".trade-players .field").removeAttr("disabled");
				$(".trade-players .trigger").html("Trade");
				$('.pick-item').css('opacity', '1');
			},

		}

		if($scope.getPlayerID() === null)
		{
			SweetAlert.swal('Incomplete', "Please select a player that you want to get for this trade", 'warning');
		}else{
			SweetAlert.swal({
				title: "Hey!",
				text: "You are about to trade this players. Do you want to proceed?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#45B549",
				confirmButtonText: "Proceed",
				closeOnConfirm: true
			},
			function(isConfirm){
				if (isConfirm) {
					sendHttpRequest.generic($scope.requests);
				}
			});
		}

	};



});