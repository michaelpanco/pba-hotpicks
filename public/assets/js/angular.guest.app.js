var pbahotpicks = angular.module('pbahotpicks', ['oitozero.ngSweetAlert']);

pbahotpicks.service('sendHttpRequest', ['$http', function($http){

	this.generic = function(requests)
	{
		if(requests["pre_request"]) requests["pre_request"]();
		
		try {
			
			if(requests["validation"]) requests["validation"]();

			var requests_data = (requests['data']) ? $.param(requests['data']) : '';

			$http({
				method : 'POST',
				url : requests['url'],
				data : requests_data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {

				if(!requests["no_status"])
				{
					var response_status = data.status;

					if (response_status === 'success')
					{
						requests["success_callback"](data);

					}else{

						if(requests["failed_callback"]) requests["failed_callback"](data);
					}
				}else{
					requests["success_callback"](data);
				}
			}).error(function(data, status, headers, config) {
				var data = { message : "Generic Error" }
				if(requests["failed_callback"]) requests["failed_callback"](data);
			});

		} catch(err) {
			var data = {message : err}
			if(requests["failed_callback"]) requests["failed_callback"](data);
		}
	}

}]);

pbahotpicks.service('validation', function(){

	this.required = function(values)
	{
		$.each(values, function(key, val) {
		    if (val === undefined || val === "") {
		    	throw "The " + key.replace('_', ' ') + " field is required ";
		    }
		});
	}

	this.min = function(field, value, length)
	{
	     if(value.length < length){
	     	throw field + ' must be at least ' + length + ' characters long';
	     }
	}

	this.match = function(field, value1, value2)
	{
	    if (value1 != value2) {
	    	throw field + ' does not match';
	    }
	}

	this.email = function(value)
	{
	    if (!validateEmail(value)) {
	    	throw 'Invalid email address';
	    }
	}

});

pbahotpicks.service('polling', function(){
	
	this.recursive = function(polling_method, polling_interval)
	{
		setTimeout(function(){

			polling_method();

		}, polling_interval);
	}

});

pbahotpicks.controller('PBAHotpicksCtrl', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.selectRegisterDOBmonth = function(monthValue, monthLabel)
	{
		$scope.selectedMonthLabel = monthLabel;
		$scope.selectedMonth = monthValue;
	};

	$scope.selectRegisterDOBday = function(dayValue)
	{
		$scope.selectedDay = dayValue;
	};

	$scope.selectRegisterDOByear = function(yearValue)
	{
		$scope.selectedYear = yearValue;
	};

	/*** REGISTRATION ***/

	$scope.register = [];

	$scope.registerManager = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$(".register-manager .field").attr("disabled", true);
				$(".register-manager .trigger").html(circularPreloader());
			},

			data: {
				firstname : params.firstname,
				lastname : params.lastname,
				password : params.password,
				password_confirmation : params.confirm_password,
				email : params.email,
				gender : params.gender,
				birth_month : $scope.selectedMonth,
				birth_day : $scope.selectedDay,
				birth_year : $scope.selectedYear,
				recaptcha : $scope.confirmRecaptcha,
			},

			url: "/api/manager/register",

			validation: function(){
				delete this.data['recaptcha']; // remove recaptcha temporarily
				validation.required(this.data);
				this.data['recaptcha'] = $scope.confirmRecaptcha; // bring it back dude!
				validation.min('Firstname', this.data.firstname, 2);
				validation.min('Lastname', this.data.lastname, 2);
				validation.min('Password', this.data.password, 6);
				validation.email(this.data.email);
				validation.match('Password', this.data.password, this.data.password_confirmation);
			    if (this.data.recaptcha === undefined || this.data.recaptcha === "") {
			    	throw 'Please confirm reCAPTCHA anti-spam security.';
			    }
			},

			success_callback: function(response_data){
				SweetAlert.swal("Registration Successful", "Thank you for submitting your registration! A confirmation email has been sent to your email address to verify your account.", "success");
				$(".register-manager .field").removeAttr("disabled");
				$(".register-manager .trigger").html("Register");
				$scope.register = [];
				$scope.selectedYear = '';
				$scope.selectedMonthLabel = '';
				$scope.selectedMonth = '';
				$scope.selectedDay = '';

			},

			failed_callback: function(response_data){
				SweetAlert.swal('Registration Failed', response_data.message, 'warning');
				$(".register-manager .field").removeAttr("disabled");
				$(".register-manager .trigger").html("Register");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	/*** END REGISTRATION ***/
	
	/*** BEGIN LOGIN ***/

	$scope.login = [];

	$scope.loginManager = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$scope.loginAlert = false;
				$(".login-manager .field").attr("disabled", true);
				$(".login-manager .trigger").html(circularPreloader());
			},

			data: {
				email : params.email,
				password : params.password,
				remember : params.remember
			},

			url: "/api/manager/login",

			validation: function(){
				if ((params.email === undefined || params.email === "") || (params.password === undefined || params.password === "")) {
					throw 'Please enter your email and password.';
				}
			},

			success_callback: function(response_data){
				window.location = window.location.origin + '/dashboard';
			},

			failed_callback: function(response_data){
				$scope.loginAlert = true;
				$scope.errMessage = response_data.message;
				$scope.login.password = "";
				$(".login-manager .field").removeAttr("disabled");
				$(".login-manager .trigger").html("Login");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	/*** END LOGIN ***/

	$scope.addFriend = function(managerID)
	{
		$scope.requests = {

			pre_request: function(){
				$(".add-friend").attr("disabled", true);
				$(".add-friend").html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/addFriend",

			validation: function()
			{

			},

			success_callback: function(response_data)
			{
				$(".add-friend").html('Pending');
			},

			failed_callback: function(response_data)
			{
				$(".add-friend").removeAttr("disabled");
				$(".add-friend").html('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Friend');
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.approveRequestViaProfile = function(managerID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".acceptRequestViaProfile" + managerID).attr("disabled", true);
				$(".acceptRequestViaProfile" + managerID).html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/approveRequest",

			success_callback: function(response_data)
			{
				$(".acceptRequestViaProfile" + managerID).html('<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Friend');
			},

			failed_callback: function(response_data)
			{
				$(".acceptRequestViaProfile" + managerID).removeAttr("disabled");
				$(".acceptRequestViaProfile" + managerID).html("Accept");
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.flagOnline = function()
	{
		$scope.requests = {

			no_status : true,

			url: "/api/manager/flagOnline",

			success_callback: function(response_data)
			{
				polling.recursive($scope.flagOnline, window.flag_online_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.onLoad = function()
	{
		if(window.auth){
			$scope.flagOnline();
		}
		
	};

	$scope.onLoad();
});

// Notice : If reCaptcha not working, this might due to the updating of the composer. if you update it, check the noCaptcha third party and look for the anhskobo/no-captcha/src/NoCaptcha.php and check if the callback is set, if not change it to setRecaptchaConfirm

setRecaptchaConfirm = function(response){
	
	pbahotpicks.controller('PBAHotpicksCtrl', function($scope) {
	    this.confirmRecaptcha = "true";
	});

  	var appElement = document.querySelector('[ng-app=pbahotpicks]');
    var $scope = angular.element(appElement).scope();

    $scope.$apply(function() {
        $scope.confirmRecaptcha = response;
    });
    
};