var pbahotpicks = angular.module('pbahotpicks', window.angular_dependency);

pbahotpicks.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {

                return scope.$eval(attrs.compile);
            },
            function(value) {

                element.html(value);

                $compile(element.contents())(scope);
                $('.nano').nanoScroller();
            }
        );
    };
}]);

pbahotpicks.service('sendHttpRequest', ['$http', function($http){

	this.generic = function(requests)
	{
		if(requests["pre_request"]) requests["pre_request"]();
		
		try {
			
			if(requests["validation"]) requests["validation"]();

			var requests_data = (requests['data']) ? $.param(requests['data']) : '';
			var method_request = (requests['method']) ? requests['method'] : 'POST';

			$http({
				method : method_request,
				url : requests['url'],
				data : requests_data,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {

				if(!requests["no_status"])
				{
					var response_status = data.status;

					if (response_status === 'success')
					{
						requests["success_callback"](data);

					}else{

						if(requests["failed_callback"]) requests["failed_callback"](data);
					}
				}else{
					requests["success_callback"](data);
				}
			}).error(function(data, status, headers, config) {
				var data = { message : "Generic Error" }
				if(requests["failed_callback"]) requests["failed_callback"](data);
			});

		} catch(err) {
			var data = {message : err}
			if(requests["failed_callback"]) requests["failed_callback"](data);
		}
	}
}]);

pbahotpicks.service('validation', function(){

	this.required = function(values)
	{
		$.each(values, function(key, val)
		{
		    if (val === undefined || val === "") {
		    	throw "The " + key.replace('_', ' ') + " field is required ";
		    }
		});
	}

	this.min = function(field, value, length)
	{
	     if(value.length < length){
	     	throw field + ' must be at least ' + length + ' characters long';
	     }
	}

	this.match = function(field, value1, value2)
	{
	    if (value1 != value2) {
	    	throw field + ' does not match';
	    }
	}

	this.email = function(value)
	{
	    if (!validateEmail(value)) {
	    	throw 'Invalid email address';
	    }
	}

});

pbahotpicks.service('polling', function(){
	
	this.recursive = function(polling_method, polling_interval)
	{
		setTimeout(function(){

			polling_method();

		}, polling_interval);
	}

});

pbahotpicks.controller('PBAHotpicksCtrl', function($scope, $localStorage, sendHttpRequest, SweetAlert, validation, polling){

    $scope.$storage = $localStorage.$default({
      psc: {id:2, name:'Alaska'},
      ssc: {id:3, name:'Barako Bull'},
    });

	$scope.hasStorage = function()
	{
	  try
	  {
	    localStorage.setItem('pbahotpicks', 'pba');
	    localStorage.removeItem('pbahotpicks');
	    return true;
	  } catch (exception) {
	    return false;
	  }
	};

    window.primarySelectedChannel = $scope.$storage.psc;
	window.secondarySelectedChannel = $scope.$storage.ssc;
	window.hasStorage = $scope.hasStorage();

	$scope.tabToFront = function(channel)
	{

		var ftChannelOne = $scope.featuredChannels[2];
		var ftChannelTwo = $scope.featuredChannels[3];
		var index = $scope.concealedChannels.indexOf(channel);

		$scope.featuredChannels[2] = channel;
		$scope.featuredChannels[3] = ftChannelOne;

		if($scope.hasStorage)
		{
			$scope.concealedChannels.splice(index, 1);  
			$scope.concealedChannels.push(ftChannelTwo);
		}

		$( "#channelTab li" ).removeClass("active")

		$scope.$storage.psc = channel;
		$scope.$storage.ssc = ftChannelOne;

		channel.selected = true;
		$scope.selectChannel(channel);
	};

	$scope.selectChannel = function(channel)
	{
		$scope.currentChannel = channel.name;
		$scope.currentChannelID = channel.id;
		$scope.noMorePostsFetching = false;

		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$scope.channelContent = channelPreloader();
			},

			data: {
				channel : channel.id,
			},

			url: "/api/manager/getChannel",

			success_callback: function(response_data)
			{
				response_content = response_data.split("||||");
				$scope.channelContent = response_content[0];
				$scope.channelPostPointer = response_content[1];
			},

			failed_callback: function(response_data)
			{
				$scope.channelContent = "<div class='no-available-posts'>Content not available. Please refresh your page</div>";
				$scope.noMorePostsFetching = true;
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadMorePosts = function()
	{
		if(!$scope.noMorePostsFetching){

			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html(channelPreloader());
				},

				data: {
					channel : $scope.currentChannelID,
					pointer : $scope.channelPostPointer
				},

				url: "/api/manager/getMorePosts",

				validation: function()
				{

				},

				success_callback: function(response_data)
				{
					response_content = response_data.split("||||");
					if(response_content[1] != 0)
					{
						$scope.channelContent = $scope.channelContent + response_content[0];
						$scope.channelPostPointer = response_content[1];
						$scope.noMorePostsFetching = false;
						$(".posts-loader").html('');
					}else{
						$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No more posts available</div>";
						$scope.noMorePostsFetching = true;
						$(".posts-loader").html('');
					}
				},

				failed_callback: function(response_data)
				{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No more posts available</div>";
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html('');
				},
			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.createPost = function()
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".status-post .field").attr("disabled", true);
				$(".status-post .trigger").html(circularPreloader());
			},

			data: {
				content : $scope.postContent,
				channel_id : $scope.currentChannelID
			},

			url: "/api/manager/createPost",

			validation: function()
			{
				if (($scope.postContent === undefined || $scope.postContent === "")) {
					throw "You didn't write any message to post";
				}
			},

			success_callback: function(response_data)
			{
				$scope.channelContent = response_data + $scope.channelContent;
				$(".status-post .field").removeAttr("disabled");
				$(".status-post .trigger").html("Post");
				$( ".channel-post" ).removeClass( "new-item-transition" )
				$scope.postContent = "";
				$scope.hideNoPostShow = true;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Posting Failed', response_data.message, 'warning');
				$(".status-post .field").removeAttr("disabled");
				$(".status-post .trigger").html("Post");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.toggleReplyComment = function(postID)
	{
		$scope['post' + postID] = !$scope['post' + postID];

		if($scope['post' + postID])
		{
			$scope.fetchReplies(postID);
		}

	};

	$scope.fetchReplies = function(postID)
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$scope['p' + postID + 'r'] = channelPreloader();
			},

			data: {
				parent_id : postID
			},

			url: "/api/manager/fetchReplies",

			success_callback: function(response_data)
			{
				$scope['p' + postID + 'r'] = response_data;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Status Failed', response_data.message, 'warning');
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Login");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.replyPost = function(postParent, replyContent)
	{
		$scope.requests = {

			no_status : true,

			pre_request: function()
			{
				$(".post-reply-" + postParent + " .field").attr("disabled", true);
				$(".post-reply-" + postParent + " .trigger").html(circularPreloader());
			},

			data: {
				content : replyContent,
				parent_post : postParent,
				channel_id : $scope.currentChannelID
			},

			url: "/api/manager/replyPost",

			validation: function()
			{
				if ((replyContent === undefined || replyContent === ""))
				{
					throw "You didn't write any message reply";
				}
			},

			success_callback: function(response_data)
			{
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Reply");

				var current_replies = ($scope['p' + postParent + 'r'] === undefined) ? '' : $scope['p' + postParent + 'r'];

				$scope['p' + postParent + 'r'] = current_replies + response_data;
				$scope['replyContentPost' + postParent] = "";
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Reply Failed', 'Failed to reply post, Please try again', 'warning');
				$(".post-reply-" + postParent + " .field").removeAttr("disabled");
				$(".post-reply-" + postParent + " .trigger").html("Reply");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.upVote = function(postID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".up-vote-" + postID).html(givePointPreloader());
			},

			data: {
				post_id : postID
			},

			url: "/api/manager/upVotePost",

			success_callback: function(response_data)
			{
				$scope['post' + postID + 'point'] = $scope['post' + postID + 'point'] + 1;
				$(".up-vote-" + postID).html('<span class="glyphicon glyphicon-upload" aria-hidden="true">');
				$scope['hideUpVote' + postID] = true;
			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('UpVote Failed', response_data.message, 'warning');
				$(".up-vote-" + postID).html('<span class="glyphicon glyphicon-upload" aria-hidden="true">');
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.fetchNotifications = function()
	{
		if(!$('.nav-not').hasClass('open')) 
		{
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noticeNotificationsContent = channelPreloader();
				},

				url: "/api/manager/fetchNotifications",

				success_callback: function(response_data)
				{
					$scope.noticeNotificationsContent = response_data;
					$scope.notificationsCountShow = false;
				},

				failed_callback: function(response_data)
				{
					SweetAlert.swal('Fetching Failed', response_data.message, 'warning');
				},

			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.fetchLatestMessages = function()
	{
		if(!$('.nav-msg').hasClass('open')) 
		{
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noticeMessagesContent = channelPreloader();
				},

				url: "/api/manager/fetchLatestMessages",

				success_callback: function(response_data)
				{
					$scope.noticeMessagesContent = response_data;
				},

				failed_callback: function(response_data)
				{
					SweetAlert.swal('Fetching Failed', response_data.message, 'warning');
				},

			}
			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.fetchRequests = function()
	{
		if(!$('.nav-req').hasClass('open')) 
		{
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$scope.noticeRequestsContent = channelPreloader();
				},

				url: "/api/manager/fetchRequests",

				success_callback: function(response_data)
				{
					$scope.noticeRequestsContent = response_data;
				},

				failed_callback: function(response_data)
				{
					SweetAlert.swal('Fetching Failed', response_data.message, 'warning');
				},

			}

			sendHttpRequest.generic($scope.requests);
		}
	};

	$scope.approveRequest = function(managerID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".acceptRequest" + managerID).attr("disabled", true);
				$(".acceptRequest" + managerID).html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/approveRequest",

			success_callback: function(response_data)
			{
				$scope['hideRequest' + managerID] = true;
				$(".requestControl" + managerID).html('<p class="success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Accepted</p>');
				$scope.requestsCount--;
				$scope.accountFriends++;
			},

			failed_callback: function(response_data)
			{
				$(".acceptRequest" + managerID).removeAttr("disabled");
				$(".acceptRequest" + managerID).html("Accept");
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.declineRequest = function(managerID)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".declineRequest" + managerID).attr("disabled", true);
				$(".declineRequest" + managerID).html(circularPreloader());
			},

			data: {
				manager_id : managerID
			},

			url: "/api/manager/declineRequest",

			success_callback: function(response_data)
			{
				$scope['hideRequest' + managerID] = true;
				$(".requestControl" + managerID).html('<p class="success"><span class="glyphicon glyphicon-exclamation-signye" aria-hidden="true"></span> Declined</p>');
				$scope.requestsCount--;
			},

			failed_callback: function(response_data)
			{
				$(".declineRequest" + managerID).removeAttr("disabled");
				$(".declineRequest" + managerID).html("Decline");
				SweetAlert.swal("Request Failed", response_data.message, "warning");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.notifier = function()
	{
		$scope.requests = {

			no_status : true,

			url: "/api/manager/notifier",

			success_callback: function(response_data)
			{
				$scope.onlineFriends = response_data.onlineFriends;

				if(response_data.requests > 0){
					$scope.requestsCountShow = true;
					$scope.requestsCount = response_data.requests;
				}

				if(response_data.newMessages > 0){
					$scope.newMessagesCountShow = true;
					$scope.newMessagesCount = response_data.newMessages;
				}else{
					$scope.newMessagesCountShow = false;
				}

				if(response_data.notifications > 0){
					$scope.notificationsCountShow = true;
					$scope.notificationsCount = response_data.notifications;
				}
				
				polling.recursive($scope.notifier, window.notifier_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.flagOnline = function()
	{
		$scope.requests = {

			no_status : true,

			url: "/api/manager/flagOnline",

			success_callback: function(response_data)
			{
				polling.recursive($scope.flagOnline, window.flag_online_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.conversationCycle = function()
	{
		$scope.requests = {

			no_status : true,
			method : 'GET',

			url: "/api/manager/fetchConversations/" + $scope.conversationManagerSlug,

			success_callback: function(response_data)
			{
				response_content = response_data.split("/*super#0291873645#master*/");
				if(response_content[1] != 0)
				{
					if($scope.lastConversationCursor < response_content[2])
					{
						$("#conversationsContent").html(response_content[1]);
						$('.nano').nanoScroller();
						$(".nano").nanoScroller({ scroll: 'bottom' });
						$scope.lastConversationCursor = response_content[2];
					}

				}else{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No conversation available</div>";
					$(".posts-loader").html('');
				}

				polling.recursive($scope.conversationCycle,  window.conversation_polling_interval);
				
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadConversations = function(manager_slug)
	{
		$scope.conversationManagerSlug = manager_slug;
		$scope.requests = {

			no_status : true,
			method : 'GET',

			pre_request: function()
			{
				$("#conversationsContent").html(channelPreloader());
			},

			url: "/api/manager/fetchConversations/" + manager_slug,

			success_callback: function(response_data)
			{
				response_content = response_data.split("/*super#0291873645#master*/");
				$scope.lastConversationCursor = response_content[2];

				if(response_content[1] != 0)
				{
					$scope.conversationPointer = response_content[0]; // user for ponting the last msg
					$scope.conversationMarker = response_content[0]; // user for scroll cursor

					$("#conversationsContent").html(response_content[1]);
					$('.nano').nanoScroller();
					$(".nano").nanoScroller({ scroll: 'bottom' });

				}else{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No conversation available</div>";
					$(".posts-loader").html('');
				}

				setTimeout($scope.conversationCycle,  window.conversation_polling_interval);
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.paginate = function(container, paginationURL, next, loading)
	{
		var next = (typeof next === 'undefined') ? false : next;
		var loading = (typeof loading === 'undefined') ? true : loading;

		$scope.requests = {

			no_status : true,
			method : 'GET',

			pre_request: function()
			{
				if(next==false && loading==true) $("#" + container).html(channelPreloader());
				if(next==true && loading==true) $("#" + container).css('opacity',0.5);
			},

			data: {
				manager : 'me',
			},

			url: paginationURL,

			success_callback: function(response_data)
			{
				$scope[container] = response_data;
				$("#" + container).css('opacity', 1);
			},

			failed_callback: function(response_data)
			{
				$scope[container] = 'Failed to fetch request';
				$("#" + container).css('opacity', 1);
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.sendMessage = function(managerID, managerSlug)
	{
		$scope.requests = {

			pre_request: function()
			{
				$(".composer .submit").attr("disabled", true);
				$(".composer .submit").html(circularPreloader());
				$scope.disableMessage = true;
			},

			data: {
				recipient : managerID,
				conversation_id : $("#conversationIDval").val(),
				message : $scope.message
			},

			url: "/api/manager/sendMessage",

			validation: function()
			{
				if (($scope.message === undefined || $scope.message === ""))
				{
					throw "You didn't write any message";
				}
			},

			success_callback: function(response_data)
			{
				$(".composer .submit").removeAttr("disabled");
				$(".composer .submit").html("Send");
				$scope.disableMessage = false;
				$scope.reveal('conversationsContent', '/api/manager/fetchConversations/' + managerSlug, false, false);
				$scope.message = "";
				$scope.markMessageRead(managerID);

			},

			failed_callback: function(response_data)
			{
				SweetAlert.swal('Sending Failed', response_data.message, 'warning');
				$(".composer .submit").removeAttr("disabled");
				$(".composer .submit").html("Send");
				$scope.disableMessage = false;
				$scope.markMessageRead(managerID);
			},


		}
		sendHttpRequest.generic($scope.requests);
	}

	$scope.reveal = function(container, paginationURL, next, loading)
	{
		var next = (typeof next === 'undefined') ? false : next;
		var loading = (typeof loading === 'undefined') ? true : loading;

		$scope.requests = {

			no_status : true,
			method : 'GET',

			pre_request: function()
			{
				if(next==false && loading==true) $("#" + container).html(channelPreloader());
				if(next==true && loading==true) $("#" + container).css('opacity',0.5);
			},

			data: {
				manager : 'me',
			},

			url: paginationURL,

			success_callback: function(response_data)
			{

				response_content = response_data.split("/*super#0291873645#master*/");
				if(response_content[1] != 0)
				{
					$scope.conversationPointer = response_content[0];
					$scope.conversationMarker = response_content[0];

					$("#" + container).html(response_content[1]);
					$('.nano').nanoScroller();
					$("#" + container).css('opacity', 1);
					$(".nano").nanoScroller({ scroll: 'bottom' });

				}else{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No conversation available</div>";
					$scope.noMorePostsFetching = true;
					$(".posts-loader").html('');
				}
			},

			failed_callback: function(response_data)
			{
				$scope[container] = 'Failed to fetch request';
				$("#" + container).css('opacity', 1);
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.markMessageRead = function(manager_id)
	{
		$scope.requests = {

			no_status : true,

			data: {
				manager_id : manager_id,
			},

			url: "/api/manager/markMessageRead",

			success_callback: function(response_data)
			{
				console.log('Message has been read');
			},

			failed_callback: function(response_data)
			{
				console.log('Message read error');
			},
		}
		sendHttpRequest.generic($scope.requests);
	};

	$scope.loadMoreConversations = function()
	{
		if(!$scope.noMoreConversationsFetching){
			$scope.haltConversationCycle = true;
			$scope.requests = {

				no_status : true,

				pre_request: function()
				{
					$(".conversations-loader").html(channelPreloader());
				},

				data: {
					recipient : $scope.messageRecipient,
					pointer : $scope.conversationPointer,
				},

				url: "/api/manager/getMoreConversations",

				validation: function()
				{

				},

				success_callback: function(response_data)
				{
					var container = "conversationsContent";

					response_content = response_data.split("/*super#0291873645#master*/");
					if(response_content[0] != 0)
					{
						$scope.conversationPointer = response_content[0];

						$("#" + container).prepend(response_content[1]);
						$('.nano').nanoScroller();
						$("#" + container).css('opacity', 1);
						$(".nano").nanoScroller({ scrollTo: $('#msg-' + $scope.conversationMarker) });
						$scope.conversationMarker = response_content[0];

					}else{
						$("#" + container).prepend("<div class='no-available-conversations'>No more messages</div>");
						$scope.noMoreConversationsFetching = true;
						$(".posts-loader").html('');
					}
					$(".conversations-loader").html('');
				},

				failed_callback: function(response_data)
				{
					$scope.channelContent = $scope.channelContent + "<div class='no-available-posts'>No more posts available</div>";
					$(".conversations-loader").html('');
				},
			}
			sendHttpRequest.generic($scope.requests);
		}
	}

	$scope.onLoad = function()
	{
		$scope.notifier();
		$scope.flagOnline();
	};

	$scope.onLoad();

	$scope.$watch('requestsCount', function(requestsCount) {
		if(requestsCount < 1){
			$scope.requestsCountShow = false;
		}
	});

});

loadMorePosts = function()
{
	angular.element(document.getElementById('PBAHotpicks')).scope().loadMorePosts();
}
loadFriends = function()
{
	angular.element(document.getElementById('PBAHotpicks')).scope().paginate('friendsContent', '/api/manager/fetchFriends');
}
loadMessages = function()
{
	angular.element(document.getElementById('PBAHotpicks')).scope().paginate('messagesContent', '/api/manager/fetchMessages');
}
loadConversations = function(manager_slug)
{
	angular.element(document.getElementById('PBAHotpicks')).scope().loadConversations(manager_slug);
}
markMessageRead = function(manager_slug)
{
	angular.element(document.getElementById('PBAHotpicks')).scope().markMessageRead(manager_slug);
}
loadMoreConversations = function(manager_slug)
{
	angular.element(document.getElementById('PBAHotpicks')).scope().loadMoreConversations();
}
conversationCycle = function(manager_slug)
{
	angular.element(document.getElementById('PBAHotpicks')).scope().conversationCycle(manager_slug);
}