$(function() {

    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2015-11-10',
            iphone: 2666
        }, {
            period: '2015-11-11',
            iphone: 2778
        }, {
            period: '2015-11-12',
            iphone: 4912
        }, {
            period: '2015-11-13',
            iphone: 3767
        }, {
            period: '2015-11-15',
            iphone: 6810
        }],
        xkey: 'period',
        ykeys: ['iphone'],
        labels: ['iPhone'],
        pointSize: 1,
        hideHover: 'auto',
        resize: true
    });

});
