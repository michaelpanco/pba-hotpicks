pbahotpicks.controller('PTPAdmistrator', function($scope, sendHttpRequest, SweetAlert, validation, polling){

	$scope.login = [];
	
	$scope.loginAdmin = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$scope.loginAlert = false;
				$(".login-admin .field").attr("disabled", true);
				$(".login-admin .trigger").html(circularPreloader());
			},

			data: {
				email : params.email,
				password : params.password
			},

			url: "/api/admin/login",

			validation: function(){
				if ((params.email === undefined || params.email === "") || (params.password === undefined || params.password === "")) {
					throw 'Please enter your email and password.';
				}
			},

			success_callback: function(response_data){

				window.location = window.location.origin + '/' + admin_slug + '/ptp-admin';
			},

			failed_callback: function(response_data){
				$scope.loginAlert = true;
				$scope.errMessage = response_data.message;
				$scope.login.password = "";
				$(".login-admin .field").removeAttr("disabled");
				$(".login-admin .trigger").html("Login");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateTeamProfile = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-team-profile-admin .field").attr("disabled", true);
				$(".update-team-profile-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.teamID,
				name : $scope.teamName,
				slug : $scope.teamSlug,
				manager : $scope.teamManager,
				coach : $scope.teamCoach,
				assistant_coaches : $scope.teamAsstCoach,
				joined : $scope.teamJoined,
				titles : $scope.teamTitles,
				win : $scope.teamWin,
				loss : $scope.teamLoss,
				logo : $scope.teamLogo,
			},

			url: "/api/admin/updateTeamProfile",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-team-profile-admin .field").removeAttr("disabled");
				$(".update-team-profile-admin .trigger").html("Update Team");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/teams/' + $scope.teamID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Team Update Failed', response_data.message, 'warning');
				$(".update-team-profile-admin .field").removeAttr("disabled");
				$(".update-team-profile-admin .trigger").html("Update Team");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.createPlayer = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".create-player-admin .field").attr("disabled", true);
				$(".create-player-admin .trigger").html(circularPreloader());
			},

			data: {
				firstname : $scope.playerFirstname,
				lastname : $scope.playerLastname,
				slug : $scope.playerSlug,
				team_id : $scope.selectedTeamID,
				position : $scope.playerPosition,
				jersey : $scope.playerJersey,
				birthday : $scope.playerBirthday,
				height : $scope.playerHeight,
				school : $scope.playerSchool,
				avatar : $scope.playerAvatar,
				salary : $scope.playerSalary
			},

			url: "/api/admin/createPlayer",

			validation: function(){
				if ($scope.playerFirstname === undefined || $scope.playerFirstname === "") {
					throw "Please enter the player's firstname";
				}
				if ($scope.playerLastname === undefined || $scope.playerLastname === "") {
					throw "Please enter the player's lastname";
				}
				if ($scope.playerSlug === undefined || $scope.playerSlug === "") {
					throw "Please enter the player's slug";
				}
				if ($scope.selectedTeamID === undefined || $scope.selectedTeamID === "" || $scope.selectedTeamID == false) {
					throw "Please select a team";
				}
				if ($scope.playerPosition === undefined || $scope.playerPosition === "") {
					throw "Please enter the player's position";
				}
				if ($scope.playerJersey === undefined || $scope.playerJersey === "") {
					throw "Please enter the player's jersey";
				}
				if ($scope.playerBirthday === undefined || $scope.playerBirthday === "") {
					throw "Please enter the player's birthday";
				}
				if ($scope.playerHeight === undefined || $scope.playerHeight === "") {
					throw "Please enter the player's height";
				}
				if ($scope.playerSchool === undefined || $scope.playerSchool === "") {
					throw "Please enter the player's school";
				}
				if ($scope.playerAvatar === undefined || $scope.playerAvatar === "") {
					throw "Please enter the player's avatar";
				}
				if ($scope.playerSalary === undefined || $scope.playerSalary === "") {
					throw "Please enter the player's Salary";
				}
			},

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".create-player-admin .field").removeAttr("disabled");
				$(".create-player-admin .trigger").html("Create Game");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/players/' + response_data.player_id;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Create Player Failed', response_data.message, 'warning');
				$(".create-player-admin .field").removeAttr("disabled");
				$(".create-player-admin .trigger").html("Create Game");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};


	$scope.createExpressBet = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".create-express-bet-admin .field").attr("disabled", true);
				$(".create-express-bet-admin .trigger").html(circularPreloader());
			},

			data: {
				description : $scope.expressBetDescription,
				option1 : $scope.expressBetOption1,
				option2 : $scope.expressBetOption2,
				betlimit : $scope.expressBetLimit,
				gamedate : $scope.expressBetGameDate,
				expiry : $scope.expressBetGameExpiry
			},
			
			url: "/api/admin/createExpressBet",

			validation: function(){
				if ($scope.expressBetOption1 === undefined || $scope.expressBetOption1 === "") {
					throw 'Please complete all the given fields.';
				}
				if ($scope.expressBetOption2 === undefined || $scope.expressBetOption2 === "") {
					throw 'Please complete all the given fields.';
				}
			},

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".create-express-bet-admin .field").removeAttr("disabled");
				$(".create-express-bet-admin .trigger").html("Create Express Bet");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/expressbets';
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Create Express Bet Failed', response_data.message, 'warning');
				$(".create-express-bet-admin .field").removeAttr("disabled");
				$(".create-express-bet-admin .trigger").html("Create Express Bet");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updatePlayerProfile = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-player-profile-admin .field").attr("disabled", true);
				$(".update-player-profile-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.playerID,
				firstname : $scope.playerFirstname,
				lastname : $scope.playerLastname,
				slug : $scope.playerSlug,
				position : $scope.playerPosition,
				jersey : $scope.playerJersey,
				team_id : $scope.selectedTeamID,
				birthday : $scope.playerBirthday,
				school : $scope.playerSchool,
				height : $scope.playerHeight,
				avatar : $scope.playerAvatar,
				status : $scope.selectedStatus,
				salary : $scope.playerSalary,
			},

			url: "/api/admin/updatePlayerProfile",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-team-profile-admin .field").removeAttr("disabled");
				$(".update-team-profile-admin .trigger").html("Update Player");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/players/' + $scope.playerID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Team Update Failed', response_data.message, 'warning');
				$(".update-team-profile-admin .field").removeAttr("disabled");
				$(".update-team-profile-admin .trigger").html("Update Player");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.createGame = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".create-game-admin .field").attr("disabled", true);
				$(".create-game-admin .trigger").html(circularPreloader());
			},

			data: {
				team_x_id : $scope.selectedXTeamID,
				team_y_id : $scope.selectedYTeamID,
				venue : $scope.gameVenue,
				season : $scope.gameSeason,
				conference : $scope.gameConference,
				game_no : $scope.gameNo,
				cluster_no : $scope.gameCluster,
				game_type : $scope.gameType,
				sequence : $scope.gameSequence,
				datetime : $scope.gameDateTime
			},

			url: "/api/admin/createGame",

			validation: function(){
				if ($scope.selectedXTeamID === undefined || $scope.selectedXTeamID === "" || $scope.selectedXTeamID == false) {
					throw 'Please complete all the given fields.';
				}
				if ($scope.selectedYTeamID === undefined || $scope.selectedYTeamID === "" || $scope.selectedYTeamID == false) {
					throw 'Please complete all the given fields.';
				}
				if ($scope.gameNo === undefined || $scope.gameNo === "") {
					throw 'Please complete all the given fields.';
				}
				if ($scope.gameCluster === undefined || $scope.gameCluster === "") {
					throw 'Please complete all the given fields.';
				}
				if ($scope.gameDateTime === undefined || $scope.gameDateTime === "") {
					throw 'Please complete all the given fields.';
				}
			},

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".create-game-admin .field").removeAttr("disabled");
				$(".create-game-admin .trigger").html("Create Game");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/games/';
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Create Game Failed', response_data.message, 'warning');
				$(".create-game-admin .field").removeAttr("disabled");
				$(".create-game-admin .trigger").html("Create Game");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateGameDetails = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-game-details-admin .field").attr("disabled", true);
				$(".update-game-details-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.gameID,
				team_x_id : $scope.selectedTeamXID,
				team_y_id : $scope.selectedTeamYID,
				winner : $scope.selectedTeamWinnerID,
				score : $scope.gameScore,
				venue : $scope.gameVenue,
				season : $scope.gameSeason,
				conference : $scope.gameConference,
				game_no : $scope.gameNo,
				cluster_no : $scope.gameClusterNo,
				game_type : $scope.gameType,
				sequence : $scope.gameSequence,
				datetime : $scope.gameDateTime,
			},

			url: "/api/admin/updateGameDetails",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-game-details-admin .field").removeAttr("disabled");
				$(".update-game-details-admin .trigger").html("Update Player");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/games/' + $scope.gameID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Game Update Failed', response_data.message, 'warning');
				$(".update-game-details-admin .field").removeAttr("disabled");
				$(".update-game-details-admin .trigger").html("Update Player");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.performance = [];

	$scope.createPerformance = function(performance)
	{
		$scope.requests = {

			pre_request: function(){
				$(".create-performance-admin .field").attr("disabled", true);
				$(".create-performance-admin .trigger").html(circularPreloader());
			},

			data: {
				game_id : performance.gameID,
				player_id : performance.playerID,
				team_id : performance.teamID,
				opponent : performance.opponent,
				points : performance.playerPoints,
				assists : performance.playerAssists,
				rebounds : performance.playerRebounds,
				steals : performance.playerSteals,
				blocks : performance.playerBlocks,
				turnovers : performance.playerTurnovers,
				win_game : performance.winGame,
			},

			url: "/api/admin/createGamePerformance",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".create-performance-admin .field").removeAttr("disabled");
				$(".create-performance-admin .trigger").html("Add Performance");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/games/result/' + performance.gameID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Game Update Failed', response_data.message, 'warning');
				$(".create-performance-admin .field").removeAttr("disabled");
				$(".create-performance-admin .trigger").html("Add Performance");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.publishGameResult = function(gameID)
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-game-details-admin .publish").attr("disabled", true);
			},

			data: {
				id : gameID,
			},

			url: "/api/admin/publishGameResult",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-game-details-admin .publish").removeAttr("disabled");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/games/result/' + gameID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Publish Game Failed', response_data.message, 'warning');
				$(".update-game-details-admin .publish").removeAttr("disabled");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateManagerProfile = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-manager-profile-admin .field").attr("disabled", true);
				$(".update-manager-profile-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.managerID,
				firstname : $scope.managerFirstname,
				lastname : $scope.managerLastname,
				slug : $scope.managerSlug,
				gender : $scope.managerGender,
				status : $scope.managerStatus,
				credits : $scope.managerCredits,
				restrain_until : $scope.managerRestrainUntil,
				birthday : $scope.managerBirthday,
				privilege : $scope.managerPrivilege,
				favteam : $scope.managerFavTeam,
				fav2ndteam : $scope.managerFav2ndTeam,
				favplayer : $scope.managerFavPlayer,
				favmatchup : $scope.managerFavMatchup,
			},

			url: "/api/admin/updateManagerProfile",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-manager-profile-admin .field").removeAttr("disabled");
				$(".update-manager-profile-admin .trigger").html("Update Manager");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/managers/' + $scope.managerID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Team Update Failed', response_data.message, 'warning');
				$(".update-manager-profile-admin .field").removeAttr("disabled");
				$(".update-manager-profile-admin .trigger").html("Update Manager");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateLeagueProfile = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-league-profile-admin .field").attr("disabled", true);
				$(".update-league-profile-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.leagueID,
				name : $scope.leagueName,
				desc : $scope.leagueDesc,
				host : $scope.leagueHost,
				sequence : $scope.leagueSequence,
				status : $scope.leagueStatus,
				date_created : $scope.leagueDateCreated,
			},

			url: "/api/admin/updateLeagueProfile",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-league-profile-admin .field").removeAttr("disabled");
				$(".update-league-profile-admin .trigger").html("Update Manager");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/leagues/' + $scope.leagueID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('League Update Failed', response_data.message, 'warning');
				$(".update-league-profile-admin .field").removeAttr("disabled");
				$(".update-league-profile-admin .trigger").html("Update League");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateExpressBet = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-express-bet-admin .field").attr("disabled", true);
				$(".update-express-bet-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.betID,
				description : $scope.betDescription,
				option : $scope.betArrayOption,
				betlimit : $scope.expressBetLimit,
				won : $scope.betWinner,
				gamedate : $scope.betGameDate,
				expiry : $scope.betGameExpiry
			},

			url: "/api/admin/updateExpressBet",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-express-bet-admin .field").removeAttr("disabled");
				$(".update-express-bet-admin .trigger").html("Update Express Bet");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/expressbets/' + $scope.betID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Express Bet Update Failed', response_data.message, 'warning');
				$(".update-express-bet-admin .field").removeAttr("disabled");
				$(".update-express-bet-admin .trigger").html("Update Express Bet");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.createProduct = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".create-product-admin .field").attr("disabled", true);
				$(".create-product-admin .trigger").html(circularPreloader());
			},

			data: {
				name : $scope.productName,
				type : $scope.selectedProductType,
				product_id : $scope.productID,
				image : $scope.productImage,
				price : $scope.productPrice,
			},

			url: "/api/admin/createProduct",

			validation: function(){
				if ($scope.productName === undefined || $scope.productName === "") {
					throw 'Please enter product name.';
				}
				if ($scope.selectedProductType === undefined || $scope.selectedProductType === "") {
					throw 'Please select product type.';
				}
				if ($scope.productID === undefined || $scope.productID === "") {
					throw 'Please enter product ID.';
				}
				if ($scope.productImage === undefined || $scope.productImage === "") {
					throw 'Please enter product Image.';
				}
				if ($scope.productPrice === undefined || $scope.productPrice === "") {
					throw 'Please enter product Price.';
				}
			},


			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".create-product-admin .field").removeAttr("disabled");
				$(".create-product-admin .trigger").html("Create Product");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/products';
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Create Game Failed', response_data.message, 'warning');
				$(".create-product-admin .field").removeAttr("disabled");
				$(".create-product-admin .trigger").html("Create Product");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateProduct = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-product-admin .field").attr("disabled", true);
				$(".update-product-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.productID,
				name : $scope.productName,
				image : $scope.productImage,
				type : $scope.productType,
				product_id : $scope.productProductID,
				price : $scope.productPrice
			},

			url: "/api/admin/updateProduct",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-product-admin .field").removeAttr("disabled");
				$(".update-product-admin .trigger").html("Update Product");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/products/' + $scope.productID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Product Update Failed', response_data.message, 'warning');
				$(".update-product-admin .field").removeAttr("disabled");
				$(".update-product-admin .trigger").html("Update Product");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.createAnnouncement = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".create-announcement-admin .field").attr("disabled", true);
				$(".create-announcement-admin .trigger").html(circularPreloader());
			},

			data: {
				text : $scope.announcementText,
				order : $scope.announcementOrder
			},

			url: "/api/admin/createAnnouncement",

			validation: function(){
				if (($scope.announcementText === undefined || $scope.announcementText === "") || ($scope.announcementOrder === undefined || $scope.announcementOrder=== "")) {
					throw 'Please complete all the given fields.';
				}
			},

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".create-announcement-admin .field").removeAttr("disabled");
				$(".create-announcement-admin .trigger").html("Create Announcement");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/announcements';
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Create Announcement', response_data.message, 'warning');
				$(".create-announcement-admin .field").removeAttr("disabled");
				$(".create-announcement-admin .trigger").html("Create Announcement");
			},
		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateAnnouncement = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-announcement-admin .field").attr("disabled", true);
				$(".update-announcement-admin .trigger").html(circularPreloader());
			},

			data: {
				id : $scope.announcementID,
				text : $scope.announcementText,
				order : $scope.announcementOrder
			},

			url: "/api/admin/updateAnnouncement",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-announcement-admin .field").removeAttr("disabled");
				$(".update-announcement-admin .trigger").html("Update Product");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/announcements/' + $scope.announcementID;
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Announcement Update Failed', response_data.message, 'warning');
				$(".update-announcement-admin .field").removeAttr("disabled");
				$(".update-announcement-admin .trigger").html("Update Product");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.deleteAnnouncement = function(announcement_id)
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-announcement-admin .field").attr("disabled", true);
				$(".update-announcement-admin .delete-trigger").html(circularPreloader());
			},

			data: {
				id : announcement_id
			},

			url: "/api/admin/deleteAnnouncement",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-announcement-admin .field").removeAttr("disabled");


			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/announcements';
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Announcement Delete Failed', response_data.message, 'warning');
				$(".update-announcement-admin .field").removeAttr("disabled");
			},

		}

		SweetAlert.swal({
			title: "Hey!",
			text: "Are you sure you want to delete this announcement?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#45B549",
			confirmButtonText: "Yes",
			closeOnConfirm: true
		},
		function(isConfirm){
			if (isConfirm) {
				sendHttpRequest.generic($scope.requests);
			}
		});
	};

	$scope.setting = [];

	$scope.updateSettings = function(params)
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-setting-admin .field").attr("disabled", true);
				$(".update-setting-admin .trigger").html(circularPreloader());
			},

			data: {
				ninterval : params.noti,
				finterval : params.flag,
				cinterval : params.conv,
				sequence : params.sequ,
				initsalary : params.init,
				minparticipants : params.mini,
				maxparticipants : params.maxi,
				season : params.seas,
				allow_trade : params.allo,
				distribute_reward : params.dist,
				begin_league : params.begi,
			},

			url: "/api/admin/updateSettings",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-setting-admin .field").removeAttr("disabled");
				$(".update-setting-admin .trigger").html("Update Settings");

			   	setTimeout(function () {
			       window.location = window.location.origin + '/' + admin_slug + '/settings';
			    }, 1000);
			},

			failed_callback: function(response_data){
				SweetAlert.swal('Setting Update Failed', response_data.message, 'warning');
				$(".update-setting-admin .field").removeAttr("disabled");
				$(".update-setting-admin .trigger").html("Update Settings");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};

	$scope.updateStatistics = function()
	{
		$scope.requests = {

			pre_request: function(){
				$(".update-statistic").attr("disabled", true);
			},

			url: "/api/admin/updateStatistics",

			success_callback: function(response_data){
				SweetAlert.swal("Successful", response_data.message, "success");
				$(".update-statistic").removeAttr("disabled");

			},

			failed_callback: function(response_data){
				SweetAlert.swal('Statistic update was failed', response_data.message, 'warning');
				$(".update-statistic").removeAttr("disabled");
			},

		}

		sendHttpRequest.generic($scope.requests);
	};


});